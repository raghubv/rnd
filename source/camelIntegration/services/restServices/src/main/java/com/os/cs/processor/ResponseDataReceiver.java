package com.os.cs.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.os.cs.util.Constants;

@Component
public class ResponseDataReceiver implements Processor
{

	private static Logger log = Logger.getLogger(ResponseDataReceiver.class);
	
	public void process(Exchange exchange) throws Exception 
	{
		log.info(" Receiving response data done successfully. ");
		
		exchange.getOut().setBody(exchange.getIn().getBody());
		exchange.getOut().setHeader(Constants.F_TRANSACTION_DETAILS_ID, exchange.getIn().getHeader(Constants.F_TRANSACTION_DETAILS_ID,Long.class));
	}
}

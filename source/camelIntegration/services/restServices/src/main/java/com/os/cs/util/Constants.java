package com.os.cs.util;

public class Constants {

	//REQUEST CODES
	public static final String _OPERATION="operation";
	public static final String _READ="read";	
	public static final String _INSERT="insert";
	public static final String _UPDATE="update";	
	public static final String _DELETE="delete";
	public static final String _AGGREGATE="aggregate";
	public static final String _COLLECTION="collection";
	public static final String _DB="db";
	public static final String _QUERY="query";
	public static final String _DATA="data";
	public static final String _PROCEDURE_NAME="proc_nm";
	public static final String _PROCEDURE_DATA="proc_data";
	
	//RESPONSE CODES
	public static final String _RESPONSE_CODE="res_code";
	public static final String _RESPONSE_MESSAGE="res_msg";	
	public static final String _RESULT="result";
	public static final String _NUMBER_OF_OPERATIONS="no_of_ops";	
	public static final String _MESSAGE="message";
	public static final String _SUCCESS="Success";
	public static final String _FAILURE="Failure";
	
	//REST API
	public static final String REST_API_AUTH_USERNAME="rest.api.auth.username";
	public static final String REST_API_AUTH_PASSWORD="rest.api.auth.password";
	
	//STORED PROCEDURES
	public static final String JAVA_SQL_TYPES_CLASSNAME="java.sql.Types";
	
	public static final String MONGO_DB_COLLECTION_NAME="COLLECTION_NAME";
	public static final String MONGO_DB_SEQ_NAME="SEQ_NAME";
	public static final String MONGO_DB_SEQ_COLUMN_NAME="SEQ_COLUMN_NAME";
	public static final String MONGO_DB_DATA = "DB_OBJECT";
	public static final String MONGO_DB_QUERY = "QUERY_OBJECT";
	public static final String _PROCEDURES_PARAMS_INFO="params_info";
	
	//collection
	public static final String C_COUNTERS="counters";
	public static final String C_USER="user";
	
	//schema
	public static final String S_INTERFACE = "interface";	
	
	public static final String INCEREMENT_OPERATOR = "$inc";
	public static final int INCEREMENT_VALUE_BY_ONE = 1;
	
	//sequences
	public static final String SEQ_TRANSACTION_SUMMARY = "trans_summary_seq";
	public static final String SEQ_TRANSACTION_SUMMARY_DETAILS = "trans_summary_dtls_seq";
	
	
	//collections
	public static final String C_TRANSACTION_SUMMARY = "transaction_summary";
	public static final String C_TRANSACTION_SUMMARY_DETAILS = "transaction_summary_details";
	
	
	//fields
	public static final String F_TRANSACTION_ID = "transaction_id";
	public static final String F_TRANSACTION_DETAILS_ID = "transaction_details_id";
	public static final String F_PRE_REQ = "pre_req";
	public static final String F_POST_REQ = "post_req";
	public static final String F_PRE_RES = "pre_res";
	public static final String F_POST_RES = "post_res";
	public static final String F_TRANSACTION_DETAILS = "summary_details";
	public static final String F_STATUS = "status";
	public static final String F_STATUS_NAME = "status_name";
	public static final String F_DESCRIPTION="description";
	public static final String F_ID = "_id";
	public static final String F_SEQ = "seq";
	
	public static final String PAYLOAD = "payload";
	public static final String INTERFACE = "interface";
	public static final String HEADER = "header";
	public static final String ENVELOPE = "envelope";
	
}

package com.os.cs.util;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;

public class MongoTemplate {

	private static Logger log = Logger.getLogger(MongoTemplate.class);

	private static MongoClientOptions clientOptions;
	private static MongoCredential credential;
	private static MongoClient adminMongoClient;
	private  static DB db;

	public MongoTemplate(String userName, String password, String schemaName, String maxConnection,
			String connectionTimeOut, String replicas) {
		try {
			credential = MongoCredential.createMongoCRCredential(userName, schemaName, password.toCharArray());
			clientOptions = MongoClientOptions.builder().connectionsPerHost(Integer.parseInt(maxConnection.toString()))
					.connectTimeout(Integer.parseInt(connectionTimeOut.toString())).build();

			List<ServerAddress> serverAddressLs = new ArrayList<ServerAddress>();
			String[] replicaSet = replicas.split(",");
			for (String replica : replicaSet) {
				serverAddressLs.add(new ServerAddress(replica.split(":")[0], Integer.parseInt(replica.split(":")[1])));
			}

			adminMongoClient = new MongoClient(serverAddressLs, Arrays.asList(credential), clientOptions);
			adminMongoClient.setReadPreference(ReadPreference.secondaryPreferred());
			db=adminMongoClient.getDB(schemaName);
			
			log.info("TokuMx-Java Adapter Initiated Successfully For Replica-Set user....!");
		} catch (UnknownHostException unknownHostException) {
			log.error("static:: The Process of MongoClient Creation Failed, Due to: "
					+ unknownHostException.getMessage());
		} catch (NullPointerException nullPointerException) {
			log.error("static:: Null Pointer Exception Message is: >> " + nullPointerException.getMessage());
		} catch (Exception exception) {
			log.error("static:: Exception Message is: >> " + exception.getMessage());
		}
	}

	public DBCollection getCollection(String collectionName) {
		return db.getCollection(collectionName);
	}

	/**
	 * 
	 * @param dbname
	 * @param collectionName
	 * @return the collection under the dbname.
	 */
	public DBCollection getCollection(String dbname,String collectionName) {
		return adminMongoClient.getDB(dbname).getCollection(collectionName);
	}

	public Set<String> getCollectionNames() {
		return db.getCollectionNames();
	}

	public CommandResult executeCommand(BasicDBObject cmd) {
		return db.command(cmd);
	}

}

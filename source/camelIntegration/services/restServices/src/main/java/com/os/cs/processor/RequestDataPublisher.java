package com.os.cs.processor;

import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.os.cs.util.Constants;
import com.os.cs.util.PropertiesFileLoader;
import com.os.cs.util.StoredProcedureBean;

@Component
public class RequestDataPublisher
{

	private static Logger log = Logger.getLogger(RequestDataPublisher.class);
	
	@Autowired
	private DriverManagerDataSource jdbcDataSource;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	public void invokeRESTService(Exchange exchange) throws Exception 
	{
		log.info(" [invokeRESTService] Request Data publishing done successfully.");
		
		exchange.getOut().setBody(exchange.getIn().getBody());
		
	}
	
	public void invokeSOAPService(Exchange exchange) throws Exception 
	{
		log.info(" [invokeSOAPService] Request Data publishing done successfully.");
		
		exchange.getOut().setBody(exchange.getIn().getBody());
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void invokeStoredProcedure(Exchange exchange) throws Exception 
	{
		log.info(" [invokeStoredProcedure] Request Data publishing done successfully.");
		
		exchange.getOut().setBody(exchange.getIn().getBody());
		exchange.getOut().setHeader(Constants.F_TRANSACTION_DETAILS_ID, exchange.getIn().getHeader(Constants.F_TRANSACTION_DETAILS_ID,Long.class));
		Map request = objectMapper.readValue(exchange.getIn().getBody(String.class), Map.class);
		
		Map<String, Object> procParamsConf = (Map<String, Object>) PropertiesFileLoader.
						storedProcedureConfJSON.get((String)request.get(Constants._PROCEDURE_NAME));
		List<Map<String, Object>> parameters = (List<Map<String, Object>>)
						procParamsConf.get(Constants._PROCEDURES_PARAMS_INFO);
				
		StoredProcedureBean storedProcedureBean=new StoredProcedureBean(
						this.jdbcDataSource, 
						Constants.JAVA_SQL_TYPES_CLASSNAME, 
						(String)request.get(Constants._PROCEDURE_NAME), 
						true,
						parameters);		

		Map<String, Object> result= storedProcedureBean.executeStoredProcedure(
						(Map<String, Object>)request.get(Constants._PROCEDURE_DATA),
						procParamsConf);
				
		exchange.getOut().setBody(objectMapper.writeValueAsString(result));
		
	}
}



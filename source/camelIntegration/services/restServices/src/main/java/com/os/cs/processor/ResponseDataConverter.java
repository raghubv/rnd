package com.os.cs.processor;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.os.cs.util.Constants;

@Component
public class ResponseDataConverter
{
	private static Logger log = Logger.getLogger(ResponseDataConverter.class);
	
	@Autowired
	private MongoProcessor mongoProcessor;
	
	public void prePaidActivationResponse(Exchange exchange) throws Exception 
	{
		log.info(" [INFO] Entry in ResponseDataConverter - prePaidActivationResponse() ");
		
		String preRes = exchange.getIn().getBody(String.class);
		
		JSONObject postRes = XML.toJSONObject(preRes);
		
		BasicDBObject obj = new BasicDBObject();
		obj.put(Constants.F_PRE_RES, preRes);
		obj.put(Constants.F_POST_RES, postRes.toString());
		BasicDBObject dataObject = new BasicDBObject()
		.append(Constants.MONGO_DB_COLLECTION_NAME, Constants.C_TRANSACTION_SUMMARY_DETAILS)
		.append(Constants.MONGO_DB_DATA, new BasicDBObject().append("$set",obj))
		.append(Constants.MONGO_DB_QUERY, 
				new BasicDBObject().append(Constants.F_TRANSACTION_DETAILS_ID, 
						exchange.getIn().getHeader(Constants.F_TRANSACTION_DETAILS_ID,Long.class)));
		
		DBObject updatedObject = mongoProcessor.update(dataObject);
		
		exchange.getOut().setBody(updatedObject);
		
	}
}

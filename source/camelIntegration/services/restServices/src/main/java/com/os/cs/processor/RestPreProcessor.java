package com.os.cs.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.restlet.data.Parameter;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("unchecked")
public class RestPreProcessor implements Processor 
{
	@SuppressWarnings("rawtypes")
	private static Map headers = new HashMap();

	static 
	{
		// set required headers hear
		headers.put("authorization", 1);
		headers.put("event_type", 1);
	}

	public void process(Exchange exchange) throws Exception 
	{
		List<Parameter> ls = (List<Parameter>) exchange.getIn().getHeaders().get("org.restlet.http.headers");
		exchange.getOut().setBody(exchange.getIn().getBody(String.class));
		for (Parameter parameter : ls) 
		{
			if (headers.containsKey(parameter.getName()))
				exchange.setProperty(parameter.getName(), parameter.getValue());
		}
	}
}

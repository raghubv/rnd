package com.os.cs.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

public class RESTAPIServiceProvider
{
	private static String  HTTP_HEADER_ACCEPT ="accept";
	private static String  HTTP_HEADER_APPLICATION_JSON ="application/json; charset=UTF-8";
	private static String  RESPONSE_MESSAGE = "res_msg";
	private static String  RESPONSE_CODE ="res_code";
	private static int  RESPONSE_CODE_VALUE = -402;
	private static Logger log = Logger.getLogger(RESTAPIServiceProvider.class);
	
	public static BasicDBObject httpPOSTRequest(String requestURL, String requestBody) throws Exception{
		BasicDBObject responseObject = new BasicDBObject();
		String responseContentAsString=new String();
		StringBuffer responseContent = new StringBuffer();
		String url = "";
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			url = requestURL;
			HttpPost postRequest = new HttpPost(url);
			postRequest.addHeader(RESTAPIServiceProvider.HTTP_HEADER_ACCEPT, RESTAPIServiceProvider.HTTP_HEADER_APPLICATION_JSON);
			
			String userName = PropertiesFileLoader.getValueAsString(Constants.REST_API_AUTH_USERNAME);
	        String password = PropertiesFileLoader.getValueAsString(Constants.REST_API_AUTH_PASSWORD);
	        String credential = userName + ":" + password; 
	        String basicAuth = "Basic "+ new String(Base64.encodeBase64(credential.getBytes(Charset.forName("US-ASCII"))));
	        postRequest.addHeader("Authorization", basicAuth);
			
			postRequest.setEntity(new StringEntity(requestBody.toString(), "UTF-8"));
			HttpResponse response = httpClient.execute(postRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				responseObject.put(RESTAPIServiceProvider.RESPONSE_MESSAGE, response.getStatusLine().getReasonPhrase());
				responseObject.put(RESTAPIServiceProvider.RESPONSE_CODE, response.getStatusLine().getStatusCode());
			}else{
				BufferedReader responseReader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
				while((responseContentAsString = responseReader.readLine()) != null){
					responseContent.append(responseContentAsString);
				}
				httpClient.getConnectionManager().shutdown();
				responseObject = (BasicDBObject) JSON.parse(responseContent.toString());
			}
		}catch(Exception exception){
			responseObject.put(RESTAPIServiceProvider.RESPONSE_MESSAGE, exception.getMessage());
			responseObject.put("RESPONSE_CODE",  RESTAPIServiceProvider.RESPONSE_CODE_VALUE);
		}
		
		String infoString = "\n#------------------------------------------------------------------------------------------------------------------------------#\n" + "URL	:	"+url+	"\nRequest	:	"+requestBody+	"\nResponse :"+responseContent+"\n";
	
		log.info(infoString);
		return responseObject;
	}
}
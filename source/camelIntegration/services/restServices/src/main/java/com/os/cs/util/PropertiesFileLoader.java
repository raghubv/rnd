package com.os.cs.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PropertiesFileLoader {
	
	private static Logger log = LoggerFactory.getLogger(PropertiesFileLoader.class);
	
	static Properties properties;
	
	@SuppressWarnings("rawtypes")
	public static Map storedProcedureConfJSON = null;
	
	static
	{
		PropertiesFileLoader.loadProperties();
	}
	
	public static void reset()
	{
		PropertiesFileLoader.loadProperties();
	}
	
	private static  void loadProperties(){
		PropertiesFileLoader.properties = new Properties();
		FileInputStream inputStream = null;
		try{
			File file = new File(System.getenv("CONFIG_PROPERTIES_FILE"));
			if(file.canRead()){
				inputStream = new FileInputStream(System.getenv("CONFIG_PROPERTIES_FILE"));
				PropertiesFileLoader.properties.load(new InputStreamReader(inputStream,"UTF-8"));
				log.info("static:: All configuration.properties Loaded Successfully..!");
			}else{
				log.error("static:: Loading common.properties into memory is Failed..!");
			}
			//Properties applicationProperties=null;
			try{
				/*applicationProperties=new Properties();
				applicationProperties.load(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream("/stored-procedures-params.properties"),"UTF-8"));
				PropertiesFileLoader.properties.putAll(applicationProperties);*/
				ObjectMapper objectMapper = new ObjectMapper();
				InputStream config = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("/stored-procedures-params.json");
				storedProcedureConfJSON = objectMapper.readValue(config, LinkedHashMap.class);
				config.close();
				log.info("static:: All stored-procedures-params.json Loaded Successfully..!");
			}catch(Exception e){
				log.error("static:: Loading stored-procedures-params.json into memory is Failed..!");
			}
		}catch (IOException ioException){
		    log.error("static:: Exception occured While loading Properties into Applciation Container, and Message is: >> "+ioException.getMessage());
		}catch (Exception exception){
		    log.error("static:: Exception occured While loading Properties into Applciation Container, and Message is: >> "+exception.getMessage());
		}
	}
	
	public static String getValueAsString(String key) throws Exception{
		String keyValue=null;
		try{
			if(key!=null){
				if(PropertiesFileLoader.properties.containsKey(key)){
					keyValue=PropertiesFileLoader.properties.get(key).toString();
					log.debug("getValueAsString:: returning key:value pair is: ["+ key+":"+keyValue+"]");
				}else{
					log.info("getValueAsString:: key["+ key+"] Not Found.");
				}
			}else{
				throw new Exception("Invalid key[null] is passed to look-up in properties file..!");
			}
		}catch(Exception exception){
			log.error("getValueAsString:: Exception Message is: >> "+exception.getMessage());
			throw exception;
		}
		return keyValue;
	}
	
	public static Object get(String key) throws Exception{
		Object keyValue=null;
		try{
			if(key!=null){
				if(PropertiesFileLoader.properties.containsKey(key)){
					keyValue=PropertiesFileLoader.properties.get(key);
					log.debug("get:: returning key:value pair is: ["+ key+":"+keyValue+"]");
				}else{
					log.info("get:: key["+ key+"] Not Found.");
				}
			}else{
				throw new Exception("Invalid key[null] is passed to look-up in properties file..!");
			}
		}catch(Exception exception){
			log.error("get:: Exception Message is: >> "+exception.getMessage());
			throw exception;
		}
		return keyValue;
	}
	
}

package com.os.cs.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;
import com.os.cs.util.Constants;

@Component
public class RequestDataReader implements Processor
{

	private static Logger log = Logger.getLogger(RequestDataReader.class);
	
	@Autowired
	private MongoProcessor mongoProcessor;
	
	public void process(Exchange exchange) throws Exception 
	{
		log.info(" [INFO] Entry in RequestDataReader - process() ");
		
		String reqString = exchange.getIn().getBody(String.class);
		BasicDBObject reqObj = (BasicDBObject)JSON.parse(reqString);
		
		//transaction summary
		BasicDBObject dataObject = new BasicDBObject()
		.append(Constants.MONGO_DB_COLLECTION_NAME, Constants.C_TRANSACTION_SUMMARY)
		.append(Constants.MONGO_DB_SEQ_NAME, Constants.SEQ_TRANSACTION_SUMMARY)
		.append(Constants.MONGO_DB_SEQ_COLUMN_NAME, Constants.F_TRANSACTION_ID)
		.append(Constants.MONGO_DB_DATA, reqObj.get(Constants.HEADER));
		
		dataObject = mongoProcessor.insert(dataObject);
		
		BasicDBObject trans_dtls_obj = ((BasicDBObject)reqObj.get(Constants.PAYLOAD));
		exchange.getOut().setBody(trans_dtls_obj);
		exchange.getOut().setHeader(Constants.F_TRANSACTION_ID, dataObject.getLong(Constants.F_TRANSACTION_ID));
		
		log.info(" [INFO] Exit in RequestDataReader - process() ");
	}
}

package com.os.cs.processor;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.os.cs.util.Constants;

@Component
public class RequestDataConverter
{
	private static Logger log = Logger.getLogger(RequestDataConverter.class);
	
	@Autowired
	private MongoProcessor mongoProcessor;
	
	public void prePaidActivationRequest(Exchange exchange) throws Exception 
	{
		log.info(" [INFO] Entry in RequestDataConverter - prePaidActivationRequest() ");
		
		String preReq = exchange.getIn().getBody(String.class);
		
		//convert input request into third party format
		JSONObject json = new JSONObject(preReq);
		String postReq = XML.toString(json);
		
		//transaction summary details
		BasicDBObject dataObject = new BasicDBObject()
		.append(Constants.MONGO_DB_COLLECTION_NAME, Constants.C_TRANSACTION_SUMMARY_DETAILS)
		.append(Constants.MONGO_DB_SEQ_NAME, Constants.SEQ_TRANSACTION_SUMMARY_DETAILS)
		.append(Constants.MONGO_DB_SEQ_COLUMN_NAME, Constants.F_TRANSACTION_DETAILS_ID)
		.append(Constants.MONGO_DB_DATA, 
				new BasicDBObject().append(Constants.F_PRE_REQ, preReq)
								   .append(Constants.F_POST_REQ, postReq)
								   .append(Constants.F_TRANSACTION_ID, exchange.getIn().getHeader(Constants.F_TRANSACTION_ID,Long.class)));
		
		dataObject = mongoProcessor.insert(dataObject);
		
		exchange.getOut().setBody(postReq);
		exchange.getOut().setHeader(Constants.F_TRANSACTION_DETAILS_ID, dataObject.getLong(Constants.F_TRANSACTION_DETAILS_ID));
		
		log.info(" [INFO] Exit in RequestDataConverter - prePaidActivationRequest() ");
	}
}

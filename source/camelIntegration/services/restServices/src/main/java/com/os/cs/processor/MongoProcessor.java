package com.os.cs.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.os.cs.util.Constants;
import com.os.cs.util.MongoTemplate;

@Component
public class MongoProcessor implements Processor 
{
	private static Logger log = Logger.getLogger(MongoProcessor.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;

	@SuppressWarnings("rawtypes")
	public void process(Exchange exchange) throws Exception 
	{
		Object responseObj = new String();
		try 
		{
			Map request = objectMapper.readValue(exchange.getIn().getBody(String.class), Map.class);
			String operation = (String) request.get(Constants._OPERATION);
			if (operation.equalsIgnoreCase(Constants._READ))
				responseObj = read(new BasicDBObject(request));
			else if (operation.equalsIgnoreCase(Constants._INSERT))
				responseObj = insert(new BasicDBObject(request));
			else if (operation.equalsIgnoreCase(Constants._UPDATE))
				responseObj = update(new BasicDBObject(request));
			else if (operation.equalsIgnoreCase(Constants._DELETE))
				responseObj = delete(new BasicDBObject(request));
			else if (operation.equalsIgnoreCase(Constants._AGGREGATE))
				responseObj = aggregate(new BasicDBObject(request));

		} 
		catch (JsonGenerationException e) 
		{
			responseObj = makeRespObj(e.toString(), 1);
		} 
		catch (JsonMappingException e) 
		{
			responseObj = makeRespObj(e.toString(), 1);
		} 
		catch (IOException e) 
		{
			responseObj = makeRespObj(e.toString(), 1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			responseObj = makeRespObj(e.toString(), 1);
		}

		exchange.getOut().setBody(objectMapper.writeValueAsString(responseObj));

	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object read(BasicDBObject requestBody) throws JsonGenerationException, JsonMappingException, IOException 
	{
		DBCursor dbCursor = mongoTemplate.getCollection((String) requestBody.get("collection"))
				.find(new BasicDBObject((Map) requestBody.get("query")));
		List ls = new ArrayList();
		for (DBObject dbObject : dbCursor) 
		{
			ls.add(dbObject);
		}
		return makeRespObj(ls, 0);
	}
	
	public BasicDBObject insert(BasicDBObject requestBody) throws Exception 
	{
		BasicDBObject dbObject = (BasicDBObject)requestBody.get(Constants.MONGO_DB_DATA);
		
		if(requestBody.containsField(Constants.MONGO_DB_SEQ_NAME))
		{
			long id = nextSequence(requestBody.get(Constants.MONGO_DB_SEQ_NAME));
			dbObject.put(Constants.F_ID, id);
			
			if(requestBody.containsField(Constants.MONGO_DB_SEQ_COLUMN_NAME))
			{
				dbObject.put(requestBody.get(Constants.MONGO_DB_SEQ_COLUMN_NAME).toString(), id);
			}
		}
		mongoTemplate.getCollection(Constants.S_INTERFACE,(String) requestBody.get(Constants.MONGO_DB_COLLECTION_NAME)).insert(dbObject);

		return dbObject;
	}
	
	public DBObject update(BasicDBObject requestBody) 
	{
		DBObject updatedObj = mongoTemplate.getCollection(Constants.S_INTERFACE,(String) requestBody.get(Constants.MONGO_DB_COLLECTION_NAME))
				.findAndModify((DBObject)requestBody.get(Constants.MONGO_DB_QUERY), null, null, false, (DBObject)requestBody.get(Constants.MONGO_DB_DATA), true, false);

		return updatedObj;
	}
	
	@SuppressWarnings("rawtypes")
	private Object delete(BasicDBObject requestBody) 
	{
		WriteResult wrs = mongoTemplate.getCollection("collection")
				.remove(new BasicDBObject((Map) requestBody.get("query")));

		return makeRespObj(wrs, 0);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object aggregate(BasicDBObject requestBody)
	{
		List aggList = new ArrayList();
		List<Map> resultList = new ArrayList<Map>();
		
		List data = (List) requestBody.get("data");
		for (Object dataObj : data) 
		{
			aggList.add(new BasicDBObject((Map) dataObj));
		}
		
		AggregationOutput aggregattionOutput = mongoTemplate.getCollection((String) requestBody.get("collection"))
				.aggregate(aggList);
		Iterable<DBObject> resultListItr = aggregattionOutput.results();
		for (DBObject dbObject : resultListItr) 
		{
			resultList.add(dbObject.toMap());
		}

		return makeRespObj(resultList, 0);
	}

	@SuppressWarnings("rawtypes")
	public BasicDBObject makeRespObj(Object result, int respCode) 
	{
		BasicDBObject respObj = new BasicDBObject();
		BasicDBObject messageObj = new BasicDBObject();
		respObj.put(Constants._RESPONSE_CODE, respCode);
		respObj.put(Constants._RESPONSE_MESSAGE, respCode == 0 ? Constants._SUCCESS : Constants._FAILURE);

		if (result instanceof WriteResult) 
		{
			messageObj.put(Constants._NUMBER_OF_OPERATIONS, ((WriteResult) result).getN());
		} 
		else if (result instanceof List) 
		{
			messageObj.put(Constants._NUMBER_OF_OPERATIONS, ((List) result).size());
			messageObj.put(Constants._RESULT, ((List) result));
		}
		respObj.put(Constants._MESSAGE, messageObj);
		return respObj;

	}
	
	public long nextSequence(Object sequenceName) throws Exception 
	{
		long sequenceId = 0; 
		try
		{
			//DB schema = mongoTemplate.getDb();
			DBCollection collection = mongoTemplate.getCollection(Constants.S_INTERFACE,Constants.C_COUNTERS);
			DBObject dbObject = collection.findAndModify(new BasicDBObject(Constants.F_ID, sequenceName), 
					null, null, false, new BasicDBObject(Constants.INCEREMENT_OPERATOR, 
					new BasicDBObject(Constants.F_SEQ, Constants.INCEREMENT_VALUE_BY_ONE)), true, false);
			
			sequenceId = Long.parseLong(String.valueOf(dbObject.get(Constants.F_SEQ)));
		}
		catch(NullPointerException nullPointerException)
		{
			log.error("Null Pointer Exception Message is: >> "+nullPointerException.getMessage());
			throw nullPointerException;
		}
		catch(NumberFormatException numberFormatException)
		{
			log.error("Exception Message is: >> "+numberFormatException.getMessage());
			throw numberFormatException;
		}
		catch(Exception exception)
		{
			log.error("Exception Message is: >> "+exception.getMessage());
			throw exception;
		}
		return sequenceId;
	}
}

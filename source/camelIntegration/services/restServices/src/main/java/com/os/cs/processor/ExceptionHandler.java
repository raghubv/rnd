package com.os.cs.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ExceptionHandler implements Processor 
{
	private static Logger log = Logger.getLogger(ExceptionHandler.class);
	
	public void process(Exchange exchange) throws Exception
	{
		Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class); 
        String endPoint = exchange.getProperty(Exchange.FAILURE_ENDPOINT, String.class);
        
        log.error(" Exception occured while executing camel flow :: Message   :: "+ exception);
        log.error(" Exception occured while executing camel flow :: Exception :: "+ exception.getClass().getName());
        log.error(" Exception occured while executing camel flow :: End Point :: "+ endPoint);
		exchange.getOut().setBody("InProgress");
	}

}

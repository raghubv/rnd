package org.apache.camel.component.restlet;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultHeaderFilterStrategy;
import org.restlet.engine.http.header.HeaderConstants;

public class RestletHeaderFilterStrategy extends DefaultHeaderFilterStrategy {

    public RestletHeaderFilterStrategy() {
        // No IN filters and copy all headers from Restlet to Camel
        
        // OUT filters (from Camel headers to Restlet headers)
        // filter headers used internally by this component
        getOutFilter().add(RestletConstants.RESTLET_LOGIN);
        getOutFilter().add(RestletConstants.RESTLET_PASSWORD);
        
        // The "CamelAcceptContentType" header is not added to the outgoing HTTP 
        // headers but it will be going out as "Accept.
        getOutFilter().add(Exchange.ACCEPT_CONTENT_TYPE);
        getOutFilter().add(Exchange.HTTP_BASE_URI);
        getOutFilter().add(Exchange.HTTP_CHARACTER_ENCODING);
        getOutFilter().add(HeaderConstants.ATTRIBUTE_HEADERS);
        
        

    }
}

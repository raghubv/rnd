##############################################################
#                      Configuration                         #
##############################################################


1.Configure all require properties to UploadConfig.properties which is used by spring application context xml
(Here spring-application-context.xml)
	
Example: Below properties used for mongodb connection & processing thread pool configuration
Need To Add in Property file 
####### LOG Path ######
log4j.logpath = /home/appuser/snoc/oplog_jobs/conf/log4j.properties
####### MongoDB Source ######
mongodb.source.host=192.168.2.253:22020,192.168.2.253:22022,192.168.2.253:22024
mongodb.source.port=27017
mongodb.source.db=local
mongodb.source.userName=admin
mongodb.source.password=admin
mongodb.source.connectionperhost=5
mongodb.source.connectionmultiplier=4
mongodb.source.connecttimeout=30000
mongodb.source.maxwaittime=25000
mongodb.source.sockettimeout=30000
####### Mongodb Destination ######
mongodb.destination.host=192.168.2.253:22020,192.168.2.253:22022,192.168.2.253:22024
mongodb.destination.port=27017
mongodb.destination.db=snoc_report
mongodb.destination.username=admin
mongodb.destination.password=admin
mongodb.destination.connectionperhost=5
mongodb.destination.connectionmultiplier=4
mongodb.destination.connecttimeout=30000
mongodb.destination.maxwaittime=25000
mongodb.destination.sockettimeout=30000
########## Crud Oplog Executor  #########
crudOplog.executor.corePoolSize=10
crudOplog.executor.maxPoolSize=15
crudOplog.executor.queueCapacity=2500000


2. Run: windows_run.bat  for windows
   Run: linux_run.sh     for linux





package com.os.jdbc.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class AggrLookUpRowMapper implements RowMapper<DBObject> {

	private static final Logger log = Logger
			.getLogger(AggrLookUpRowMapper.class);

	public DBObject mapRow(ResultSet rs, int rowNum) throws SQLException {
		BasicDBObject dbObject = null;
		try {
			dbObject = new BasicDBObject();
			dbObject.put(Constants.LOOK_UP_ID_N, rs.getLong(Constants.LOOK_UP_ID_N));
			dbObject.put(Constants.LOOK_UP_TYPE_N, rs.getLong(Constants.LOOK_UP_TYPE_N));
			dbObject.put(Constants.LOOK_UP_NAME_V, rs.getString(Constants.LOOK_UP_NAME_V));
			dbObject.put(Constants.LOOK_UP_TYPE_NAME_V, rs.getString(Constants.LOOK_UP_TYPE_NAME_V));
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		return dbObject;
	}

}

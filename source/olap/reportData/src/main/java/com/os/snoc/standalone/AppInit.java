package com.os.snoc.standalone;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.os.jdbc.impl.KPIAggregatorDaoImpl;
import com.os.jdbc.impl.KPIDailyDaoImpl;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;
import com.mongodb.MongoException;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * AppInit: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class AppInit {

	private static final Logger log = Logger.getLogger(AppInit.class);

	public static void main(String[] args) {
		ApplicationContext context = null;
		try {
			context = new ClassPathXmlApplicationContext(Constants._Application_Context);
			
		//	context = new ClassPathXmlApplicationContext("application-context.xml");
			/*For Debug, Enable the following code*/
			/*And Disable the "While Debug Disable code" in spring-applciation-context.xml file*/ 
			KPIAggregatorDaoImpl daoImpl = (KPIAggregatorDaoImpl) context.getBean("kpiAggregatorDaoImpl");
			daoImpl.processReportData();
		} catch (MongoException e) {
			((ClassPathXmlApplicationContext) context).close();
			log.error("[error]" + LoggerTrace.getStackTrace(e));

		}

	}

}

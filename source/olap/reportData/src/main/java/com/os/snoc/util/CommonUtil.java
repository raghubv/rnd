package com.os.snoc.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
*
*
<b>Purpose:</b><br>
* CommonUtil:<br>
*<br>
*
*
<b>DesignReference:</b><br>
*<br>
*<br>
*
*
<b>CopyRights:</b><br>
* os 2015<br>
*<br>
*
*
<b>RevisionHistory:</b>
*
*
<pre>
*<b>
* Sl No Modified Date Author</b>
* ==============================================
* 1 Feb 5, 2015 @author Kannan.N -- Base Release
*
*
</pre>
*
*<br>
 */
public class CommonUtil 
{
	static Logger log = Logger.getLogger(CommonUtil.class);
	private static Properties prioperties = null;
	static {
		try {
			prioperties = new Properties();
			prioperties.load(new FileInputStream(System.getProperty("CONFIG_PATH")));
			
		} catch (IOException ioe) {
			log.error("[error]" + ioe.getStackTrace());
		}
	}

	public static String getProperty(String key) {
		return prioperties.getProperty(key);
	}

}

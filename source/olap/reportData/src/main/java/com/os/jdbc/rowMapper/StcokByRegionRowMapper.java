package com.os.jdbc.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.os.jdbc.dao.PgJTemplete;
import com.os.snoc.util.Constants;
import com.os.snoc.util.DateTimeUtility;
import com.os.snoc.util.LoggerTrace;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class StcokByRegionRowMapper implements RowMapper<DBObject> {

	private static final Logger log = Logger
			.getLogger(StcokByRegionRowMapper.class);

	public PgJTemplete pgJTemplete;
	private static ResourceBundle sql = ResourceBundle.getBundle("sql");
	
	public StcokByRegionRowMapper(PgJTemplete pgJTemplete) {
		super();
		this.pgJTemplete = pgJTemplete;
	}


	public DBObject mapRow(ResultSet rs, int rowNum) throws SQLException {
		BasicDBObject dbObject = null;
		try {
			dbObject = new BasicDBObject();
			dbObject.put(Constants.AGGR_ID_N, rs.getLong(Constants.AGGR_ID_N));
			dbObject.put(Constants.ORG_ID_N, rs.getLong(Constants.ORG_ID_N));
			dbObject.put(Constants.ORG_NAME_V, rs.getString(Constants.ORG_NAME_V));
			dbObject.put(Constants.NODE_ID_N, rs.getLong(Constants.NODE_ID_N));
			dbObject.put(Constants.LEGAL_NAME_V, rs.getString(Constants.LEGAL_NAME_V));
			dbObject.put(Constants.ORG_TYPE_N, rs.getLong(Constants.ORG_TYPE_N));
			dbObject.put(Constants.ORG_TYPE_NAME_V, rs.getString(Constants.ORG_TYPE_NAME_V));
			dbObject.put(Constants.REGION_ID, rs.getLong(Constants.REGION_ID));
			dbObject.put(Constants.REGION_NAME, rs.getString(Constants.REGION_NAME));
			dbObject.put(Constants.DAY_ID_N, rs.getLong(Constants.DAY_ID_N));
			dbObject.put(Constants.DAY_ID_D, DateTimeUtility.getDate(rs.getLong(Constants.DAY_ID_N)));
			long inStock = rs.getLong(Constants.TOTAL_IN_STOCK);
			long outStock = rs.getLong(Constants.TOTAL_OUT_STOCK);
			float ratio = 0.0F;
			dbObject.put(Constants.TOTAL_IN_STOCK, inStock);
			dbObject.put(Constants.TOTAL_OUT_STOCK, outStock);
			if(outStock!=0){
				ratio=(float) inStock/outStock;
			}else{
				ratio= Float.POSITIVE_INFINITY;
			}
			dbObject.put(Constants.STOCK_RATIO, ratio);
			
			BasicDBList attributes = new BasicDBList();
			String query = sql.getString("aggr_lookup_sub_query");
			List<DBObject> listOfdbObj = pgJTemplete.getDataObjectsUsingPreparedStatement(query,
							new Object[] {rs.getLong(Constants.AGGR_ID_N)},
							new AggrLookUpRowMapper());
			if(listOfdbObj!=null){
				for(DBObject o:listOfdbObj){
					attributes.add(o);
				}
			}	
			dbObject.put(Constants.ATTRIBUTES, attributes);
			
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		return dbObject;

	}


}

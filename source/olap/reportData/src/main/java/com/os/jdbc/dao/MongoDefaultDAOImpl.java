package com.os.jdbc.dao;

import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoDefaultDAOImpl extends MongoDAO{

	public MongoTemplate defaultTemplate;

	public void setDefaultTemplate(MongoTemplate defaultTemplate) {
		this.defaultTemplate = defaultTemplate;
	}

	public MongoTemplate getDefaultTemplate() {
		return defaultTemplate;
	}
	
}

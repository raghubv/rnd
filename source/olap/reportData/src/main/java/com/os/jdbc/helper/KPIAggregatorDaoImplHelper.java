package com.os.jdbc.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;




import com.os.jdbc.dao.MongoDAO;
import com.os.jdbc.dao.MongoDefaultDAOImpl;
import com.os.jdbc.dao.MongoInventoryDAOImpl;
import com.os.jdbc.dao.MongoProductDAOImpl;
import com.os.jdbc.dao.MongoSnocDAOImpl;
import com.os.jdbc.dao.PgJTemplete;
import com.os.jdbc.impl.KPIAggregatorDaoImpl;
import com.os.jdbc.rowMapper.KsVsNonKstRowMapper;
import com.os.jdbc.rowMapper.PrimarySalesUserCountRowMapper;
import com.os.jdbc.rowMapper.ProdCategoryMonthlyRowMapper;
import com.os.jdbc.rowMapper.SecondarySalesUserCountRowMapper;
import com.os.jdbc.rowMapper.StockValueRowMapper;
import com.os.snoc.util.CommonUtil;
import com.os.snoc.util.Constants;
import com.os.snoc.util.DateTimeUtility;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Constants: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	Oct 19, 2015	@author Kannan.N   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */

public class KPIAggregatorDaoImplHelper 
{

	private static Logger log = Logger.getLogger(KPIAggregatorDaoImpl.class);

	private MongoDefaultDAOImpl mongoDefaultDAO;
	private MongoInventoryDAOImpl mongoInventoryDAO;
	private MongoSnocDAOImpl mongoSnocDAO;
	private MongoProductDAOImpl mongoProductDAO;	
	private PgJTemplete pgJTemplete;

	private static ResourceBundle sql = ResourceBundle.getBundle("sql");
	
	private SimpleDateFormat timeZoneFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	

	public int processOrgTypewiseProductCategorySales(long date)
	{
		int count = 0;
		log.debug("Processing Organization Typewise Product Category Sales..");
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection monthlyProdCategorySalesCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_PRODUCT_CATEGORY_SALES_AGRR);
		
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("prod_category_sales_agg_query");
		log.debug("[query] " + query);
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();  
		namedParameters.put("extOrgLookupType", Integer.valueOf(CommonUtil.getProperty("org_ext_lookup_type")));
		namedParameters.put("rootNode", Integer.valueOf(CommonUtil.getProperty("root_node")));
		namedParameters.put("extRegionLookuptype", Integer.valueOf(CommonUtil.getProperty("region_ext_lookup_type")));
		namedParameters.put("metricsType", Integer.valueOf(CommonUtil.getProperty("prod_category_metrics_type")));
		namedParameters.put("month", date);
		namedParameters.put("events", CommonUtil.getProperty("prod_category_sales_events"));
		
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, namedParameters,
				new ProdCategoryMonthlyRowMapper(pgJTemplete));
		
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null)
		{
			count = listOfdbObj.size();
			if (count > 0)
			{
				
				DBObject dbObject = null;
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(int i=0; i<listOfdbObj.size(); i++)
				{
					dbObject = listOfdbObj.get(i);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					DBCursor cursor = monthlyProdCategorySalesCol.find(findObject);
					if(cursor.size()==0)
					{
						monthlyProdCategorySalesCol.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("value", Long.valueOf(dbObject.get("value").toString())).
								append("qty", Long.valueOf(dbObject.get("qty").toString())).
								append("order_count", Long.valueOf(dbObject.get("order_count").toString())));
						monthlyProdCategorySalesCol.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
		}
		return count;
	}
	
	public int processPrimarySalesUserCount(long date)
	{
		int count = 0;
		log.debug("Processing Primary Sales Vs User count..");
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection monthlyProdCategorySalesCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_PRIMARY_SALES_USER_COUNT_AGRR);
		
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("primary_sales_user_count");
		log.debug("[query] " + query);
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();  
		namedParameters.put("extOrgLookupType", Integer.valueOf(CommonUtil.getProperty("org_ext_lookup_type")));
		namedParameters.put("orgLookupId", Integer.valueOf(CommonUtil.getProperty("primary_org_lookup_id")));
		namedParameters.put("extRegionLookupType", Integer.valueOf(CommonUtil.getProperty("region_ext_lookup_type")));
		namedParameters.put("metricsType", Integer.valueOf(CommonUtil.getProperty("prod_category_metrics_type")));
		namedParameters.put("month", date);
		namedParameters.put("eventType", Integer.valueOf(CommonUtil.getProperty("primary_sales_users_event")));
		

		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, namedParameters,
				new PrimarySalesUserCountRowMapper(pgJTemplete));
		
		
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null)
		{
			count = listOfdbObj.size();
			if (count > 0)
			{
				
				DBObject dbObject = null;
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(int i=0; i<listOfdbObj.size(); i++)
				{
					dbObject = listOfdbObj.get(i);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					DBCursor cursor = monthlyProdCategorySalesCol.find(findObject);
					if(cursor.size()==0)
					{
						monthlyProdCategorySalesCol.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("value", Long.valueOf(dbObject.get("value").toString())).
								append("user_count", Long.valueOf(dbObject.get("user_count").toString())).
								append("order_count", Long.valueOf(dbObject.get("order_count").toString())));
						monthlyProdCategorySalesCol.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
		}
		return count;
	}
	
	public int processSecondarySalesUserCount(long date)
	{
		int count = 0;
		log.debug("Processing Secondary Sales Vs User count..");
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection collection = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_SECONDARY_SALES_USER_COUNT_AGRR);
		
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("secondary_sales_user_count");
		log.debug("[query] " + query);
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();  
		namedParameters.put("extOrgLookupType", Integer.valueOf(CommonUtil.getProperty("org_ext_lookup_type")));
		namedParameters.put("channelSalesOrgType",CommonUtil.getProperty("channel_sales_org_type"));
		namedParameters.put("extRegionLookupType", Integer.valueOf(CommonUtil.getProperty("region_ext_lookup_type")));
		namedParameters.put("metricsType", Integer.valueOf(CommonUtil.getProperty("prod_category_metrics_type")));
		namedParameters.put("month", date);
		namedParameters.put("eventType", Integer.valueOf(CommonUtil.getProperty("secondary_sales_users_event")));
		namedParameters.put("rootNode", Integer.valueOf(CommonUtil.getProperty("root_node")));
		
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, namedParameters,
				new SecondarySalesUserCountRowMapper(pgJTemplete));
		
		
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null)
		{
			count = listOfdbObj.size();
			if (count > 0)
			{
				
				DBObject dbObject = null;
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(int i=0; i<listOfdbObj.size(); i++)
				{
					dbObject = listOfdbObj.get(i);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					DBCursor cursor = collection.find(findObject);
					if(cursor.size()==0)
					{
						collection.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("value", Long.valueOf(dbObject.get("value").toString())).
								append("user_count", Long.valueOf(dbObject.get("user_count").toString())).
								append("order_count", Long.valueOf(dbObject.get("order_count").toString())));
						collection.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
		}
		return count;
	}
	
	public int processStockValue(long date)
	{
		int count = 0;
		log.debug("Processing Stock Value..");
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection collection = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_STOCK_VALUE_AGRR);
		
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("stock_value");
		log.debug("[query] " + query);
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();  
		namedParameters.put("extOrgLookupType", Integer.valueOf(CommonUtil.getProperty("org_ext_lookup_type")));
		namedParameters.put("extRegionLookupType", Integer.valueOf(CommonUtil.getProperty("region_ext_lookup_type")));
		namedParameters.put("extBrandLookupType", Integer.valueOf(CommonUtil.getProperty("brand_ext_lookup_type")));
		namedParameters.put("metricsType", Integer.valueOf(CommonUtil.getProperty("prod_category_metrics_type")));
		namedParameters.put("month", date);
		namedParameters.put("rootNode", Integer.valueOf(CommonUtil.getProperty("root_node")));
		
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, namedParameters,
				new StockValueRowMapper(pgJTemplete));
		
		
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null)
		{
			count = listOfdbObj.size();
			if (count > 0)
			{
				
				DBObject dbObject = null;
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(int i=0; i<listOfdbObj.size(); i++)
				{
					dbObject = listOfdbObj.get(i);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					DBCursor cursor = collection.find(findObject);
					if(cursor.size()==0)
					{
						collection.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("opening_stock", Long.valueOf(dbObject.get("opening_stock").toString())).
								append("stock_in", Long.valueOf(dbObject.get("stock_in").toString())).
								append("stock_out", Long.valueOf(dbObject.get("stock_out").toString())).
								append("closing_stock", Long.valueOf(dbObject.get("closing_stock").toString())));
						collection.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
		}
		return count;
	}
	
	public int processKsVsNonKs(long date)
	{
		int count = 0;
		log.debug("Processing Ks Vs Non Ks");
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection collection = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_KS_VS_NONKS_AGRR);
		
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("ksVsNonKs_count_SS_TS");
		log.debug("[query] " + query);
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();  
		namedParameters.put("extRegionLookupType", Integer.valueOf(CommonUtil.getProperty("region_ext_lookup_type")));
		namedParameters.put("metricsType", Integer.valueOf(CommonUtil.getProperty("prod_category_metrics_type")));
		namedParameters.put("month", date);
		namedParameters.put("ksVsnonksEvents", CommonUtil.getProperty("ksVsnonks_count_events"));
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, namedParameters,
				new KsVsNonKstRowMapper(pgJTemplete));
		
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null)
		{
			count = listOfdbObj.size();
			if (count > 0)
			{
				
				DBObject dbObject = null;
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(int i=0; i<listOfdbObj.size(); i++)
				{
					dbObject = listOfdbObj.get(i);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					DBCursor cursor = collection.find(findObject);
					if(cursor.size()==0)
					{
						collection.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("order_count", Long.valueOf(dbObject.get("order_count").toString())).
								append("qty", Long.valueOf(dbObject.get("qty").toString())).
								append("ks_order_count", Long.valueOf(dbObject.get("ks_order_count").toString())).
								append("ks_qty", Long.valueOf(dbObject.get("ks_qty").toString())).
								append("nonks_order_count", Long.valueOf(dbObject.get("nonks_order_count").toString())).
								append("nonks_qty", Long.valueOf(dbObject.get("nonks_qty").toString())));
						collection.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
		}
		return count;
	}
	
	public int processSimSoldVsActivated(long date) throws ParseException, JSONException
	{
		log.debug("Processing Sim Sold Vs Activated From Mongo DB");
		
		int count = 0;
		
		List<String> simProdsIdList = new ArrayList<String>();
		Map<String,String> regionMap = new HashMap<String, String>();
		String query=null;
		DBObject queryObject = null;
		DBObject projectObject = null;
		
		/* Creating Collection to Insert into tokuMX */
		DBCollection prdCollection = mongoProductDAO.getProductTemplate().getCollection(Constants.C_PRODUCT_VALUES);
		DBCollection transValCollection = mongoSnocDAO.getSnocTemplate().getCollection(Constants.C_TRANS_VAL);
		
		/*Take the list of sim products from Mongo DB*/
		log.debug("Taking the sim products");
		
		query = sql.getString("prod_prodValues");
		
		query = query.replaceAll("simProdCatId", "\""+CommonUtil.getProperty("sim_prod_cat_id")+"\"");
		
		queryObject = (DBObject) JSON.parse(query);
		
		simProdsIdList=getProductFromMongo(prdCollection,queryObject);
		
		log.debug("No.of SIM Products :"+simProdsIdList.size());
		
		/*Take the Region Id*/
		log.debug("Taking the Regions");
		
		query = sql.getString("region_transVal");
		
		query = query.replaceAll("regionExtLookupType", "\""+CommonUtil.getProperty("region_ext_lookup_type")+"\"");
		
		queryObject = (DBObject) JSON.parse(query.split("~")[0]);
		
		projectObject = (DBObject) JSON.parse(query.split("~")[1]);
		
		regionMap = getAllRegions(transValCollection, queryObject,projectObject);
		
		log.debug("Regions : "+regionMap.size());
		
		/*Calculating start month date and end month date for querying serial_ledger collection on prd_movement created_dt field*/
		Date stDate = dateFormat.parse(date+"01");
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(stDate);
		
		cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		Date enDate = dateFormat.parse(date+""+cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		log.debug("Start and End date for query : "+timeZoneFormat.format(stDate)+" to "+timeZoneFormat.format(enDate));

		count = processSimSold(simProdsIdList, regionMap, stDate, enDate,date);
		
		count = count + processSimActivation(simProdsIdList, regionMap, stDate, enDate, date);
	
		return count;
	}
	
	private int processSimSold(List<String> prodList,Map<String,String> regionMap, Date stDate,Date enDate,Long date)
	{
		
		log.debug("Processig SIM Sold Count");
		DBObject dbObject = new BasicDBObject();
		DBCursor cursor = null;
		String query=null;
		DBObject queryObject=null;
		DBObject projectObject=null;
		int noOfDoc=0;
		int count=0;
		
		try
		{
			DBCollection invCollection = mongoInventoryDAO.getInventoryTemplate().getCollection(Constants.C_SERIAL_LEDGER);
			DBCollection orgCollection = mongoSnocDAO.getSnocTemplate().getCollection(Constants.C_ORG);
			DBCollection collection = mongoDefaultDAO.getDefaultTemplate().getCollection(Constants.C_SIM_SOLD_VS_ACT_AGRR);
			
			//Query on serial ledger collection
			query = sql.getString("simSoldCount_serlLedger");
			
			query = query.replaceAll("soldStatus", CommonUtil.getProperty("sim_sold_status"));
			query = query.replaceAll("prdId", prodList.toString().substring(1, prodList.toString().length()-1));
			query = query.replaceAll("terTiaryOrdType", CommonUtil.getProperty("tertiary_order_type"));

			//Setting the start and end date for pro_movement created_dt field
			BasicDBList basicDBList = (BasicDBList) JSON.parse(query);
	        
			//Drill down to prdt_movmnt.create_dt field
			BasicDBObject ob1 = (BasicDBObject) basicDBList.get(2);
			BasicDBObject ob2 = (BasicDBObject) ob1.get("$match");
			BasicDBList ob3 = (BasicDBList) ob2.get("$and");
			BasicDBObject ob4 = (BasicDBObject) ob3.get(0);
			BasicDBObject ob5 = (BasicDBObject) ob4.get("prdt_movmnt.create_dt");
			
			ob5.put("$gte", timeZoneFormat.parse(timeZoneFormat.format(stDate)));
			ob5.put("$lte", timeZoneFormat.parse(timeZoneFormat.format(enDate)));
			
			log.debug("Mongo query from serila ledger collection : "+basicDBList.toString());
			
			//Converting the basicDBList to list<DBObject>
			List<DBObject> dbObjList = new ArrayList<DBObject>();
			for(int i=0; i<basicDBList.size(); i++)
			{
				dbObjList.add((DBObject)basicDBList.get(i));
				
			}
			//Execute the query
			AggregationOutput aggregationOutput = MongoDAO.aggregateDocument(invCollection, dbObjList);
			
			Iterable<DBObject> iterable = aggregationOutput.results();
			Iterator<DBObject> iterator = iterable.iterator();
			
			Map<String,String> map = new HashMap<String, String>(); //Map for storing nodeid with region combination
			Set<String> orgId = new HashSet<String>(); //unique Node id list
			BasicDBObject temp=null;
			BasicDBObject temp1=null;
			
			//Strore org id in map for mapping with its region
			while(iterator.hasNext())
			{
				temp = (BasicDBObject) iterator.next();
				temp1 = (BasicDBObject) temp.get("prdt_movmnt");
				map.put(temp1.getString("org_id"),null);
				orgId.add(temp1.getString("org_id"));
				noOfDoc++;
			}
			
			log.debug("No.of documents return from serial_ledger collection query : "+noOfDoc);
			log.debug("Org Ids : "+orgId.size());
			
			log.debug("Mapping org with region...");
			for(String key : map.keySet())
			{
				query = sql.getString("orgRegion_org");
				query=query.replaceAll("orgId", key);
				queryObject = (DBObject) JSON.parse(query.split("~")[0]);
				projectObject = (DBObject) JSON.parse(query.split("~")[1]);
				
				//Get the region by org id
				cursor = MongoDAO.queryDocuments(orgCollection, queryObject, projectObject);
				if(cursor.hasNext())
				{
					temp = (BasicDBObject) cursor.next();
					temp1 =(BasicDBObject) ((BasicDBList) temp.get("addresses")).get(0);
	 
					if(temp1!=null && !temp1.isEmpty())
					{
						// Get the region name from region map by passing region id
						map.put(key, temp1.getString("state")+"~"+regionMap.get(temp1.getString("state"))); 
					}
					else
					{
						log.debug("Org Id -->"+key+" don't have region");
					}
				}
			}
			log.debug("After Mapping org with region :"+map.toString()); 
			
			// map storing key as regionid+month combination, value as a detailed data
			Map<String,BasicDBObject> doc = new HashMap<String, BasicDBObject>();  
			
			Iterator<DBObject> iterator1 = iterable.iterator();
			String rgDtls;
			String _id;
			while(iterator1.hasNext())
			{
				temp = (BasicDBObject) iterator1.next();
				temp1 = (BasicDBObject) temp.get("prdt_movmnt");
				rgDtls = map.get(temp1.getString("org_id"));
				_id=CommonUtil.getProperty("kyvi.tenant.name")+"~"+rgDtls.split("~")[0]+"~"+date;
				if(!doc.containsKey(_id))
				{
					BasicDBObject ob = new BasicDBObject();
					ob.put("_id", _id); //regionId plus date combination
					ob.put("month", date);
					ob.put("date", dateFormat.parse(date+"01"));
					ob.put("sold_count", 1);
					ob.put("region_id", rgDtls.split("~")[0]);
					ob.put("region_name", rgDtls.split("~")[1]);
					ob.put("tenant_details",CommonUtil.getProperty("kyvi.tenant.name"));
					ob.put("tenant_ref", new DBRef("tenant",CommonUtil.getProperty("kyvi.tenant.name").split("~")[1]));
					doc.put(_id, ob);
				}
				else
				{
					BasicDBObject ob = new BasicDBObject();
					ob= doc.get(_id);
					ob.put("sold_count", ob.getInt("sold_count")+1);
				}
			}
			
			// Inserting Fetched Data into TokuMX 
			count = doc.size();
			if (count > 0)
			{
				
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(String key:doc.keySet())
				{
					dbObject = doc.get(key);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					cursor = collection.find(findObject);
					if(cursor.size()==0)
					{
						collection.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						updateObject.put("$set", new BasicDBObject("sold_count", Long.valueOf(dbObject.get("sold_count").toString())));
						collection.update(updateFindObject, updateObject);
					}
				}
			}
				log.debug("[count] " + count);
			
		}
		catch(Exception exception)
		{
			log.error("Error occured in processSimSold()",exception);
		}
		return count;
	}
	
	private int processSimActivation(List<String> prodList,Map<String,String> regionMap, Date stDate,Date enDate,Long date)
	{
		
		log.debug("Processig SIM Activation Count");
		
		DBObject dbObject = new BasicDBObject();
		DBCursor cursor = null;
		DBCursor cursor1 = null;
		String query=null;
		DBObject queryObject=null;
		DBObject projectObject=null;
		Iterator<DBObject> iterator = null;
		int noOfDoc=0;
		int count=0;
		
		try
		{
			DBCollection invCollection = mongoInventoryDAO.getInventoryTemplate().getCollection(Constants.C_SERIAL_LEDGER);
			DBCollection orgCollection = mongoSnocDAO.getSnocTemplate().getCollection(Constants.C_ORG);
			DBCollection collection = mongoDefaultDAO.getDefaultTemplate().getCollection(Constants.C_SIM_SOLD_VS_ACT_AGRR);
			
			//Query on serial ledger collection
			query = sql.getString("simActCount_serlLedger");
			
			query = query.replaceAll("prodActStatus", CommonUtil.getProperty("sim_act_status"));
			query = query.replaceAll("product", prodList.toString().substring(1, prodList.toString().length()-1));

			//Setting the start and end date for pro_movement created_dt field
			 queryObject = (DBObject) JSON.parse(query.split("~")[0]);
			 projectObject = (DBObject) JSON.parse(query.split("~")[1]);
	        
			//Drill down to prdt_movmnt.create_dt field
			BasicDBList ob1 = (BasicDBList) queryObject.get("$and");
			BasicDBObject ob2 = (BasicDBObject) ob1.get(1);
			BasicDBObject ob3 = (BasicDBObject) ob2.get("activated_dt");
			
			ob3.put("$gte", timeZoneFormat.parse(timeZoneFormat.format(stDate)));
			ob3.put("$lte", timeZoneFormat.parse(timeZoneFormat.format(enDate)));
			
			log.debug("Mongo query from serial ledger collection : "+queryObject.toString()+","+projectObject);

			//Exceute query
			cursor = MongoDAO.queryDocuments(invCollection, queryObject, projectObject);		
			
			Map<String,String> map = new HashMap<String, String>(); //Map for storing nodeid with region combination
			Set<String> orgId = new HashSet<String>(); //unique Node id list
			BasicDBObject temp=null;
			BasicDBList tempDblist=null;
			BasicDBObject temp1=null;
			
			//Strore org id in map for mapping with its region
			iterator = cursor.iterator();
			while(iterator.hasNext())
			{
				temp = (BasicDBObject) iterator.next();
				if(temp.getString("prd_sl_status").equalsIgnoreCase("773"))
				{
					tempDblist =  (BasicDBList) temp.get("prdt_movmnt");
					temp1 = (BasicDBObject) tempDblist.get(tempDblist.size()-1);
					map.put(temp1.getString("org_id"),null);
					orgId.add(temp1.getString("org_id"));
				}	
				noOfDoc++;
			}
			
			log.debug("No.of documents return from serial_ledger collection query : "+noOfDoc);
			log.debug("Org Ids : "+orgId.size());
			
			log.debug("Mapping org with region...");
			for(String key : map.keySet())
			{
				query = sql.getString("orgRegion_org");
				query=query.replaceAll("orgId", key);
				queryObject = (DBObject) JSON.parse(query.split("~")[0]);
				projectObject = (DBObject) JSON.parse(query.split("~")[1]);
				
				//Get the region by org id
				cursor1 = MongoDAO.queryDocuments(orgCollection, queryObject, projectObject);
				if(cursor1.hasNext())
				{
					temp = (BasicDBObject) cursor1.next();
					temp1 =(BasicDBObject) ((BasicDBList) temp.get("addresses")).get(0);
	 
					if(temp1!=null && !temp1.isEmpty())
					{
						// Get the region name from region map by passing region id
						map.put(key, temp1.getString("state")+"~"+regionMap.get(temp1.getString("state"))); 
					}
					else
					{
						log.debug("Org Id -->"+key+" don't have region");
					}
				}
			}
			log.debug("After Mapping org with region :"+map.toString()); 
			
			// map storing key as regionid+month combination, value as a detailed data
			Map<String,BasicDBObject> doc = new HashMap<String, BasicDBObject>();  
			iterator = cursor.iterator();
			String rgDtls;
			Integer days=0;;
			String mapKey = null;
			
			while(iterator.hasNext())
			{
				rgDtls=null;
				temp = (BasicDBObject) iterator.next();
				if(temp.getString("prd_sl_status").equalsIgnoreCase("773"))
				{
					tempDblist =  (BasicDBList) temp.get("prdt_movmnt");
					temp1 = (BasicDBObject) tempDblist.get(tempDblist.size()-1);
					if(!temp1.getString("ord_type").equalsIgnoreCase(CommonUtil.getProperty("tertiary_order_type")))
					{
						throw new Exception("Activation status 773 is there, but order type 303(Tertiary) is not", null);
					}
					rgDtls=map.get(temp1.getString("org_id"));
					days=DateTimeUtility.getDays(timeZoneFormat.parse(timeZoneFormat.format(temp1.getDate("create_dt"))), timeZoneFormat.parse(timeZoneFormat.format(temp.getDate("activated_dt"))));
					mapKey =CommonUtil.getProperty("kyvi.tenant.name")+"~"+rgDtls.split("~")[0]+"~"+date;
					
				}
				else
				{
					mapKey =CommonUtil.getProperty("kyvi.tenant.name")+"~"+date.toString();
				}
				
				if(!doc.containsKey(mapKey))
				{
					BasicDBObject ob = new BasicDBObject();
					ob.put("_id", mapKey); //regionId plus date combination
					ob.put("month", date);
					ob.put("date", dateFormat.parse(date+"01"));
					ob.put("tenant_details",CommonUtil.getProperty("kyvi.tenant.name"));
					ob.put("tenant_ref", new DBRef("tenant",CommonUtil.getProperty("kyvi.tenant.name").split("~")[1]));
					ob.put("act_count", 1);
					if(rgDtls!=null)
					{
						ob.put("region_id", rgDtls.split("~")[0]);
						ob.put("region_name", rgDtls.split("~")[1]);
						ob.put("0-10_days", 0);
						ob.put("11-20_days", 0);
						ob.put("21-30_days", 0);
						ob.put("31-40_days", 0);
						ob.put("41-50_days", 0);
						ob.put("above_50_days", 0);
						if(days>=0 && days<=10)
							ob.put("0-10_days", 1);
						else if(days>=11 && days<=20)
							ob.put("11-20_days", 1);
						else if(days>=21 && days<=30)
							ob.put("21-30_days", 1);
						else if(days>=31 && days<=40)
							ob.put("31-40_days", 1);
						else if(days>=41 && days<=50)
							ob.put("41-50_days", 1);
						else
							ob.put("above_50_days",1);
						
					}
					doc.put(mapKey, ob);
				}
				else
				{
					BasicDBObject ob = new BasicDBObject();
					ob= doc.get(mapKey);
					ob.put("act_count", ob.getInt("act_count")+1);
					if(rgDtls!=null)
					{
						if(days>=0 && days<=10)
							ob.put("0-10_days", ob.getInt("0-10_days")+1);
						else if(days>=11 && days<=20)
							ob.put("11-20_days", ob.getInt("11-20_days")+1);
						else if(days>=21 && days<=30)
							ob.put("21-30_days", ob.getInt("21-30_days")+1);
						else if(days>=31 && days<=40)
							ob.put("31-40_days", ob.getInt("31-40_days")+1);
						else if(days>=41 && days<=50)
							ob.put("41-50_days", ob.getInt("41-50_days")+1);
						else
							ob.put("above_50_days",ob.getInt("above_50_days")+1);
						
					}
					
				}
			}
			// Inserting Fetched Data into TokuMX 
			count=doc.size();
			if (count > 0)
			{
				DBObject findObject = null;
				DBObject updateFindObject = null;
				DBObject updateObject = null;
				for(String key : doc.keySet())
				{
					dbObject = doc.get(key);
					findObject = new BasicDBObject();
					findObject.put("_id", dbObject.get("_id").toString());
					cursor = collection.find(findObject);
					if(cursor.size()==0)
					{
						collection.insert(dbObject);
					}
					else
					{
						updateFindObject = new BasicDBObject();
						updateFindObject.put("_id", dbObject.get("_id").toString());
						updateObject = new BasicDBObject();
						if(dbObject.containsField("region_id"))
						{
							updateObject.put("$set", new BasicDBObject("act_count", Long.valueOf(dbObject.get("act_count").toString()))
							.append("0-10_days", Long.valueOf(dbObject.get("0-10_days").toString()))
							.append("11-20_days", Long.valueOf(dbObject.get("11-20_days").toString()))
							.append("21-30_days", Long.valueOf(dbObject.get("21-30_days").toString()))
							.append("31-40_days", Long.valueOf(dbObject.get("31-40_days").toString()))
							.append("41-50_days", Long.valueOf(dbObject.get("41-50_days").toString()))
							.append("above_50_days", Long.valueOf(dbObject.get("above_50_days").toString())));
						}
						else
						{
							updateObject.put("$set", new BasicDBObject("act_count", Long.valueOf(dbObject.get("act_count").toString())));
						}
						collection.update(updateFindObject, updateObject);
					}
				}
			}
			log.debug("[count] " + count);
			
		}
		catch(Exception exception)
		{
			log.error("Error occured in processSimActivation()",exception);
		}
		return count;
	}
	private List<String> getProductFromMongo(DBCollection collection, DBObject queryObject)
	{
		DBCursor cursor = null;
		DBObject dbObject = null;
		List<String> productList = new ArrayList<String>();
		
		try
		{
			log.debug("Product collection query :  "+queryObject.toString());
			cursor = MongoDAO.queryDocuments(collection, queryObject); 
			
			/*Adding product id to a list*/
			while(cursor.hasNext())
			{
				dbObject = cursor.next();
				productList.add(dbObject.get("_id").toString());
			}
		}
		catch(Exception exception)
		{
			log.error("Error occured in getProductFromMongo ", exception);
		}
		return productList;
	}
	
	private Map<String,String> getAllRegions(DBCollection collection, DBObject queryObject,DBObject projectObject)
	{
		DBCursor cursor = null;
		DBObject dbObject = null;
		Map<String,String> map = new HashMap<String,String>();
		
		try
		{
			log.debug("Region collection query "+queryObject.toString()+","+projectObject.toString()); 
			cursor = MongoDAO.queryDocuments(collection, queryObject, projectObject);
		
			/*Adding region details to map*/
			while(cursor.hasNext())
			{
				dbObject = cursor.next();
				map.put(dbObject.get("_id").toString(), dbObject.get("description").toString());
			}
		}
		catch(Exception exception)
		{
			log.error("Error occured in getAllRegions() ",exception);
		}
		return map;
	}
	public void setMongoDefaultDAO(MongoDefaultDAOImpl mongoDefaultDAO) {
		this.mongoDefaultDAO = mongoDefaultDAO;
	}

	public void setPgJTemplete(PgJTemplete pgJTemplete) {
		this.pgJTemplete = pgJTemplete;
	}

	public MongoInventoryDAOImpl getMongoInventoryDAO() {
		return mongoInventoryDAO;
	}

	public void setMongoInventoryDAO(MongoInventoryDAOImpl mongoInventoryDAO) {
		this.mongoInventoryDAO = mongoInventoryDAO;
	}

	public MongoSnocDAOImpl getMongoSnocDAO() {
		return mongoSnocDAO;
	}

	public void setMongoSnocDAO(MongoSnocDAOImpl mongoSnocDAO) {
		this.mongoSnocDAO = mongoSnocDAO;
	}

	public MongoProductDAOImpl getMongoProductDAO() {
		return mongoProductDAO;
	}

	public void setMongoProductDAO(MongoProductDAOImpl mongoProductDAO) {
		this.mongoProductDAO = mongoProductDAO;
	}

}

package com.os.jdbc.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.os.jdbc.dao.PgJTemplete;
import com.os.snoc.util.CommonUtil;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class KsVsNonKstRowMapper implements RowMapper<DBObject> {

	private static final Logger log = Logger
			.getLogger(KsVsNonKstRowMapper.class);
	
	public PgJTemplete pgJTemplete;
	public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private static ResourceBundle sql = ResourceBundle.getBundle("sql");
	
	public KsVsNonKstRowMapper(PgJTemplete pgJTemplete) {
		super();
		this.pgJTemplete = pgJTemplete;
	}



	public DBObject mapRow(ResultSet rs, int rowNum) throws SQLException {
		BasicDBObject dbObject = null;
		try {
			dbObject = new BasicDBObject();
			dbObject.put("_id", CommonUtil.getProperty("kyvi.tenant.name")+"~"+rs.getString("sales_level_id")+"~"+rs.getString("region_id")+
						"~"+rs.getString("month"));
			dbObject.put("sales_level_id", Long.valueOf(rs.getString("sales_level_id")));
			dbObject.put("sales_level", rs.getString("sales_level"));
			dbObject.put("month", rs.getLong("month"));
			dbObject.put("order_count", rs.getLong("order_count"));
			dbObject.put("qty", rs.getLong("qty"));
			dbObject.put("ks_order_count", rs.getLong("ks_order_count"));
			dbObject.put("ks_qty", rs.getLong("ks_qty"));
			dbObject.put("nonks_order_count", rs.getLong("nonks_order_count"));
			dbObject.put("nonks_qty", rs.getLong("nonks_qty"));
			dbObject.put("region_id", rs.getLong("region_id"));
			dbObject.put("region_name", rs.getString("region_name"));
			dbObject.put("tenant_details",CommonUtil.getProperty("kyvi.tenant.name"));
			dbObject.put("tenant_ref", new DBRef("tenant",CommonUtil.getProperty("kyvi.tenant.name").split("~")[1]));
			dbObject.put("date", dateFormat.parse(rs.getString("month")+"01"));

			
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		return dbObject;

	}


}

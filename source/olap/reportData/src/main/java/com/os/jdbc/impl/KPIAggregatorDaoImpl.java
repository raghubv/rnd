package com.os.jdbc.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.os.jdbc.JdbcDao;
import com.os.jdbc.helper.KPIAggregatorDaoImplHelper;
import com.os.snoc.util.DateTimeUtility;
import com.os.snoc.util.LoggerTrace;

/**
 * 
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	Oct 19, 2015	@author Kannan.N		-- Base Release
 * 
 * 
 * </pre>
 * 
 * <br>
 */
public class KPIAggregatorDaoImpl implements JdbcDao {

	private static Logger log = Logger.getLogger(KPIAggregatorDaoImpl.class);
	private KPIAggregatorDaoImplHelper kpiAggregatorDaoImplHelper;
	private int initialize;

	public int getInitialize() {
		return initialize;
	}



	public void setInitialize(int initialize) {
		this.initialize = initialize;
	}



	/**
	 * 
	 * @author Kannan.N <b>Algorithm:</b>
	 * 
	 *         <pre>
	 *         migrateDataFromPostgresToMongoDB
	 * 
	 *         </pre>
	 * 
	 * @return void
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.jdbc.JdbcDao#migrateDataFromPostgresToMongoDB()
	 */
	

	public void setKPIAggregatorDaoImplHelper(KPIAggregatorDaoImplHelper kpiAggregatorDaoImplHelper) {
		this.kpiAggregatorDaoImplHelper = kpiAggregatorDaoImplHelper;
	}



	public KPIAggregatorDaoImplHelper getKpiAggregatorDaoImplHelper() {
		return kpiAggregatorDaoImplHelper;
	}



	public void setKpiAggregatorDaoImplHelper(
			KPIAggregatorDaoImplHelper kpiAggregatorDaoImplHelper) {
		this.kpiAggregatorDaoImplHelper = kpiAggregatorDaoImplHelper;
	}



	public void processReportData() {
		log.info("[begin...]");
		
		try {
			log.debug("Process Started..!");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
			Date now =  DateTimeUtility.addDays(new Date(), initialize);
			String monthAsString = dateFormat.format(now);
			log.debug("Month : "+monthAsString);

			kpiAggregatorDaoImplHelper.processOrgTypewiseProductCategorySales(Long.parseLong(monthAsString));
			kpiAggregatorDaoImplHelper.processPrimarySalesUserCount(Long.parseLong(monthAsString));
			kpiAggregatorDaoImplHelper.processSecondarySalesUserCount(Long.parseLong(monthAsString));
			kpiAggregatorDaoImplHelper.processStockValue(Long.parseLong(monthAsString));
			kpiAggregatorDaoImplHelper.processKsVsNonKs(Long.parseLong(monthAsString));
			kpiAggregatorDaoImplHelper.processSimSoldVsActivated(Long.parseLong(monthAsString));
			
			log.debug("Process Finished..!");
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		log.info("[end...]");
	}
}

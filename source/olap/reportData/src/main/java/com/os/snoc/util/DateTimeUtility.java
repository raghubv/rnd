package com.os.snoc.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DateTimeUtility {
	
	private static Logger log = Logger.getLogger(DateTimeUtility.class);
	
	public static Date addDays(Date date, int days) throws Exception{
		Date returnDate = date;
		try{
			DateTime dateTime = new DateTime(date);
			returnDate= dateTime.plusDays(days).toDate();
		}catch(Exception exception){
			log.error("addDays:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return returnDate;
	}
	public static Date addMonths(Date date, int months) throws Exception{
		Date returnDate = date;
		try{
			DateTime dateTime = new DateTime(date);
			returnDate= dateTime.plusMonths(months).toDate();
		}catch(Exception exception){
			log.error("addMonths:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return returnDate;
	}
	
	public static Date addYears(Date date, int years) throws Exception{
		Date returnDate = date;
		try{
			DateTime dateTime = new DateTime(date);
			returnDate= dateTime.plusYears(years).toDate();
		}catch(Exception exception){
			log.error("addDays:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return returnDate;
	}
	
	public static boolean isSameDate(Date date1, Date date2) throws Exception{
		boolean isSame = false;
		try{
			DateTime dateTime1 = new DateTime(date1);
			DateTime dateTime2 = new DateTime(date2);
			isSame = (dateTime1.getMillis()==dateTime2.getMillis())?true:false;
		}catch(Exception exception){
			log.error("isSameDate:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return isSame;
	}
	
	public static boolean isSameDay(Date date1, Date date2) throws Exception{
		boolean isSame = false;
		try{
			DateTime dateTime1 = new DateTime(date1);
			DateTime dateTime2 = new DateTime(date2);
			isSame = (dateTime1.getYear()==dateTime2.getYear() && dateTime1.getDayOfYear()==dateTime2.getDayOfYear())?true:false;
		}catch(Exception exception){
			log.error("isSameDay:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return isSame;
	}
	
	/**
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * 		return TRUE if: date1 > date2 otherwise return FALSE
	 * @throws Exception
	 */
	public static boolean isNewer(Date date1, Date date2) throws Exception{
		boolean isSame = false;
		try{
			DateTime dateTime1 = new DateTime(date1);
			DateTime dateTime2 = new DateTime(date2);
			isSame = (dateTime1.getMillis()>dateTime2.getMillis())?true:false;
		}catch(Exception exception){
			log.error("isNewer:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return isSame;
	}
	

	/**
	 *  
	 *  Expecting Formats:
	 * 1. Java.Util.Date Object
	 * 2. UTC Format: "2014-09-14T07:10:32.897Z"-------["yyyy-MM-dd'T'kk:mm:ss.SSS'Z'"] 
	 * 3. Java.Util.Date Object In string Format: "Mon Oct 06 16:34:04 IST 2014"--------["E MMM dd kk:mm:ss Z yyyy"]
	 * 
	 * @return Java.Util.Date or null
	 */
	public static Date getDate(Object  object) throws Exception{
//		log.debug("getDate:: Parsing Object : "+object);
		Date date = null;
		try{
			if(object != null && !object.toString().equals("")){
				try{
					date  = new DateTime(object, DateTimeZone.UTC).toDate();
				}catch(IllegalArgumentException argumentException){
					date = new SimpleDateFormat("E MMM dd kk:mm:ss Z yyyy").parse(object.toString());
				}
			}
		}catch(Exception exception){
			log.error("getDate:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
//		log.debug("getDate:: Returning Date Object : "+date);
		return date;
	}
	
	public static Date getDate(long  dt) throws Exception{
		Date date = null;
		try{
			date = new SimpleDateFormat("yyyyMMdd").parse(String.valueOf(dt));
		}catch(Exception exception){
			log.error("getDate:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
		return date;
	}
	
	public static Date getEndOfTheDay() throws Exception {
		Date date = null;
		try{
			DateTime dateTime  = new DateTime();
			dateTime=dateTime.withHourOfDay(23);
			dateTime=dateTime.withMinuteOfHour(59);
			dateTime=dateTime.withSecondOfMinute(59);
			dateTime=dateTime.withMillisOfSecond(999);
			date = new DateTime(dateTime, DateTimeZone.UTC).toDate();
		}catch(Exception exception){
			log.error("getEndOfTheDay:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
//		log.debug("getEndOfTheDay:: Returning Date Object : "+date);
		return date;
	}
	
	public static Date getStartOfTheDay() throws Exception {
		Date date = null;
		try{
			DateTime dateTime  = new DateTime();
			dateTime=dateTime.withHourOfDay(0);
			dateTime=dateTime.withMinuteOfHour(0);
			dateTime=dateTime.withSecondOfMinute(0);
			dateTime=dateTime.withMillisOfSecond(0);
			date = new DateTime(dateTime, DateTimeZone.UTC).toDate();
		}catch(Exception exception){
			log.error("getStartOfTheDay:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
//		log.debug("getStartOfTheDay:: Returning Object : "+date);
		return date;
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 * 
	 * Expecting Formats:
	 * 1. Java.Util.Date Object
	 * 2. UTC Format: "2014-09-14T07:10:32.897Z"-------["yyyy-MM-dd'T'kk:mm:ss.SSS'Z'"] 
	 * 
	 */
	public static Date getEndOfTheDay(Object  object) throws Exception {
//		log.debug("getEndOfTheDay:: Parsing Object : "+object);
		Date date = null;
		try{
			if(object!=null && !object.toString().equals("")){
				DateTime dateTime  = new DateTime(object);
				dateTime=dateTime.withHourOfDay(23);
				dateTime=dateTime.withMinuteOfHour(59);
				dateTime=dateTime.withSecondOfMinute(59);
				dateTime=dateTime.withMillisOfSecond(999);
				date = new DateTime(dateTime, DateTimeZone.UTC).toDate();
			}
		}catch(Exception exception){
			log.error("getEndOfTheDay:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
//		log.debug("getEndOfTheDay:: Returning Object : "+date);
		return date;
	}
	
	/**
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 * 
	 * Expecting Formats:
	 * 1. Java.Util.Date Object
	 * 2. UTC Format: "2014-09-14T07:10:32.897Z"-------["yyyy-MM-dd'T'kk:mm:ss.SSS'Z'"] 
	 * 
	 */
	public static Date getStartOfTheDay(Object  object) throws Exception {
//		log.debug("getStartOfTheDay:: Parsing Object : "+object);
		Date date = null;
		try{
			if(object!=null && !object.toString().equals("")){
				DateTime dateTime  = new DateTime(object);
				dateTime=dateTime.withHourOfDay(0);
				dateTime=dateTime.withMinuteOfHour(0);
				dateTime=dateTime.withSecondOfMinute(0);
				dateTime=dateTime.withMillisOfSecond(0);
				date = new DateTime(dateTime, DateTimeZone.UTC).toDate();
			}
		}catch(Exception exception){
			log.error("getStartOfTheDay:: Exception is: >>"+exception.getMessage());
			throw exception;
		}
//		log.debug("getStartOfTheDay:: Returning Object : "+date);
		return date;
	}
	
	public static int getDays(Date d1, Date d2)
	{
		 return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}		
	
	
	
	/**
	 *  
	 * Use this Method for Create Current DateTime Instance in GivenDiteTimeZone
	 * e.g. if India[+05:30] Zone-Id Passed, The Following will be returned
	 * 	"2014-05-20T12:19:01.571+05:30"
	 * 
	 *//*
	public static DateTime getCurrentDTZDateTime(String DateTimeZoneId){
		return new DateTime(DateTimeZone.forID(DateTimeZoneId));
	}
	
	*//**
	 *  
	 * Use this Method for Create Current Indian DateTime Instance
	 * e.g.
	 * 	"2014-05-20T12:19:01.571+05:30"
	 * 
	 *//*
	public static DateTime getCurrentIndiaDateTime(){
		return new DateTime(DateTimeZone.forID("Asia/Kolkata"));
	}
	
	*//**
	 *  
	 * Use this Method for Create DateTime Instance in UTC
	 * @ 1st Param, Supported String Format:
	 * 		"19-Apr-2014"
	 * e.g.
	 * 	"2014-04-19T00:00:00.000Z"
	 * 
	 *//*
	public static DateTime getUTCDateTime(String dateTimeAsString){
		//TODO:
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'KK:mm:ss.SSS'Z'");
//		String date = dateFormat.format(new Date(dateTimeAsString));
		return new DateTime(dateTimeAsString, DateTimeZone.forID("UTC"));
	}
	
	*//**
	 *  
	 * Use this Method for Create DateTime Instance in UTC From the given Date Object 
	 * e.g.
	 * 	"2014-04-19T07:33:04.332Z"
	 * 
	 *//*
	public static DateTime getUTCDateTime(Date dateTimeAsDate){
		return new DateTime(dateTimeAsDate, DateTimeZone.forID("UTC"));
	}
	
	*//**
	 *  
	 * Use this Method for Convert Given date String into DateTime Instance with Given DataTimeZone
	 * @ 1st Param, Supported String Format:
	 * 		"19-Apr-2014"
	 * e.g.	if India[+05:30] Zone-Id Passed, The Following will be returned
	 * "2014-04-19T00:00:00.000+05:30"
	 *//*
	public static DateTime getDTZDateTime(String dateTimeAsString, String DateTimeZoneId){
		//TODO:
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'KK:mm:ss.SSS'Z'");
//		String date = dateFormat.format(new Date(dateTimeAsString));
		return new DateTime(dateTimeAsString, DateTimeZone.forID(DateTimeZoneId));
	}
	
	*//**
	 *  
	 * Use this Method for Convert Given Date Object into DateTime Instance with Given DataTimeZone
	 * e.g.	if India[+05:30] Zone-Id Passed, The Following will be returned
	 * 	"2014-05-20T12:19:01.571+05:30"
	 * 
	 *//*
	public static DateTime getDTZDateTime(Date dateTimeAsDate, String DateTimeZoneId){
		return new DateTime(dateTimeAsDate, DateTimeZone.forID(DateTimeZoneId));
	}
	
	*//**
	 *  
	 * Use this Method for Convert DateTime Zone from UTC to India Zone[+05:30]
	 * @ 1st Param, Supported String Format:
	 * 		"19-Apr-2014"
	 * e.g.
	 * "2014-04-19T00:00:00.000+05:30"
	 *//*
	public static DateTime getIndiaDateTime(String dateTimeAsString){
		//TODO:
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'KK:mm:ss.SSS'Z'");
//		String date = dateFormat.format(new Date(dateTimeAsString));
		return new DateTime(dateTimeAsString, DateTimeZone.forID("Asia/Kolkata"));
	}
	
	*//**
	 *  
	 * Use this Method for Create DateTime Instance in India Zone[+05:30] From the given Date Object 
	 * e.g.
	 * 	"2014-05-20T12:19:01.571+05:30"
	 * 
	 *//*
	public static DateTime getIndiaDateTime(Date dateTimeAsDate){
		return new DateTime(dateTimeAsDate, DateTimeZone.forID("Asia/Kolkata"));
	}*/
	
}



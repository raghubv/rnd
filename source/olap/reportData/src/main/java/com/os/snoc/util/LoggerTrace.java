package com.os.snoc.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * <b>Purpose:</b><br>
 * LoggerTrace <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class LoggerTrace {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getStackTrace
	 * 
	 * </pre>
	 * 
	 * @param e
	 * @return
	 */
	public static String getStackTrace(Exception e) {
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		return stack.toString();
	}

}

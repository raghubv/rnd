package com.os.jdbc.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.os.jdbc.dao.PgJTemplete;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class KPIMonthlyRowMapper implements RowMapper<DBObject> {

	private static final Logger log = Logger
			.getLogger(KPIMonthlyRowMapper.class);
	
	public PgJTemplete pgJTemplete;
	private static ResourceBundle sql = ResourceBundle.getBundle("sql");
	
	public KPIMonthlyRowMapper(PgJTemplete pgJTemplete) {
		super();
		this.pgJTemplete = pgJTemplete;
	}



	public DBObject mapRow(ResultSet rs, int rowNum) throws SQLException {
		BasicDBObject dbObject = null;
		try {
			dbObject = new BasicDBObject();
			dbObject.put(Constants.AGGR_ID_N, rs.getLong(Constants.AGGR_ID_N));
			dbObject.put(Constants.EVENT_TYPE_N,
					rs.getInt(Constants.EVENT_TYPE_N));
			dbObject.put(Constants.ORG_ID_N, rs.getLong(Constants.ORG_ID_N));
			dbObject.put(Constants.ORG_NAME_V,
					rs.getString(Constants.ORG_NAME_V));
			dbObject.put(Constants.ORG_TYPE_N, rs.getInt(Constants.ORG_TYPE_N));
			dbObject.put(Constants.TRANS_ORG_ID_N,
					rs.getLong(Constants.TRANS_ORG_ID_N));
			dbObject.put(Constants.NODE_ID_N, rs.getLong(Constants.NODE_ID_N));
			dbObject.put(Constants.NODE_NAME_V,
					rs.getString(Constants.NODE_NAME_V));
			dbObject.put(Constants.LEGAL_NAME_V,
					rs.getString(Constants.LEGAL_NAME_V));
			dbObject.put(Constants.POSITION_ID_N,
					rs.getInt(Constants.POSITION_ID_N));
			dbObject.put(Constants.ROOT_NODE_N,
					rs.getInt(Constants.ROOT_NODE_N));
			dbObject.put(Constants.MTR_ID_N, rs.getLong(Constants.MTR_ID_N));
			dbObject.put(Constants.MTR_NAME_V,
					rs.getString(Constants.MTR_NAME_V));
			dbObject.put(Constants.MTR_TYPE_N, rs.getInt(Constants.MTR_TYPE_N));
			dbObject.put(Constants.MTR_TYPE_NAME_V,
					rs.getString(Constants.MTR_TYPE_NAME_V));
			dbObject.put(Constants.RLTD_MTR_ID_N,
					rs.getLong(Constants.RLTD_MTR_ID_N));
			dbObject.put(Constants.RLTD_MTR_ID_NAME_V,
					rs.getString(Constants.RLTD_MTR_ID_NAME_V));
			dbObject.put(Constants.RLTD_MTR_TYPE_ID_N,
					rs.getLong(Constants.RLTD_MTR_TYPE_ID_N));
			dbObject.put(Constants.RLTD_MTR_TYPE_ID_NAME_V,
					rs.getString(Constants.RLTD_MTR_TYPE_ID_NAME_V));
			dbObject.put(Constants.QTY_N, rs.getLong(Constants.QTY_N));
			dbObject.put(Constants.VALUE_N, rs.getLong(Constants.VALUE_N));
			dbObject.put(Constants.UOM_N, rs.getInt(Constants.UOM_N));
			dbObject.put(Constants.CURRENCY_V,
					rs.getString(Constants.CURRENCY_V));
			dbObject.put(Constants.REF_CODE_V,
					rs.getString(Constants.REF_CODE_V));
			dbObject.put(Constants.NO_OF_EVENTS_N,
					rs.getInt(Constants.NO_OF_EVENTS_N));
			dbObject.put(Constants.MONTH_ID_N, rs.getLong(Constants.MONTH_ID_N));
			dbObject.put(Constants.QTR_N, rs.getInt(Constants.QTR_N));
			dbObject.put(Constants.RGN_ID_N, rs.getLong(Constants.RGN_ID_N));
			dbObject.put(Constants.RGN_NAME_V, rs.getString(Constants.RGN_NAME_V));
			
			BasicDBList attributes = new BasicDBList();
			String query = sql.getString("aggr_lookup_sub_query");
			List<DBObject> listOfdbObj = pgJTemplete.getDataObjectsUsingPreparedStatement(query,
							new Object[] {rs.getLong(Constants.AGGR_ID_N)},
							new AggrLookUpRowMapper());
			if(listOfdbObj!=null){
				for(DBObject o:listOfdbObj){
					attributes.add(o);
				}
			}	
			dbObject.put(Constants.ATTRIBUTES, attributes);
			
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		return dbObject;

	}


}

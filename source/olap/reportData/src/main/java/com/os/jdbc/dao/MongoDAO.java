package com.os.jdbc.dao;

import java.util.List;

import com.mongodb.AggregationOutput;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

public class MongoDAO {

	public static WriteResult insertDocument(DBCollection collection,DBObject insertObjct){
		return collection.insert(insertObjct);
	}
	
	public static WriteResult updateDocument(DBCollection collection,DBObject queryObject, DBObject updateObject){
		return collection.update(queryObject, updateObject);
	}
	
	public static DBObject queryDocument(DBCollection collection,DBObject queryObject){
		return collection.findOne(queryObject);
	}
	
	public static DBCursor queryDocuments(DBCollection collection,DBObject queryObject){
		return collection.find(queryObject);
	}
	
	public static DBCursor queryDocuments(DBCollection collection,DBObject queryObject,DBObject projectObject){
		return collection.find(queryObject,projectObject);
	}
	
	public static WriteResult removeDocument(DBCollection collection,DBObject queryObjct){
		return collection.remove(queryObjct);
	}
	
	public static AggregationOutput aggregateDocument(DBCollection collection,List<DBObject> dbList){
		return collection.aggregate(dbList);
	}
	
	
}

package com.os.jdbc.dao;

import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoProductDAOImpl {

	public MongoTemplate productTemplate;

	public void setProductTemplate(MongoTemplate productTemplate) {
		this.productTemplate = productTemplate;
	}

	public MongoTemplate getProductTemplate() {
		return productTemplate;
	}
	
}

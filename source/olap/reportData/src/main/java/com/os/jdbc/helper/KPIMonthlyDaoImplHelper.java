package com.os.jdbc.helper;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.os.jdbc.dao.MongoDefaultDAOImpl;
import com.os.jdbc.dao.PgJTemplete;
import com.os.jdbc.impl.KPIDailyDaoImpl;
import com.os.jdbc.rowMapper.KPIMonthlyRowMapper;
import com.os.snoc.util.Constants;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Constants: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	July 23, 2015	@author G Md Rafi   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */

public class KPIMonthlyDaoImplHelper {

	private static Logger log = Logger.getLogger(KPIDailyDaoImpl.class);

	private MongoDefaultDAOImpl mongoDefaultDAO;
	private PgJTemplete pgJTemplete;

	private static ResourceBundle sql = ResourceBundle.getBundle("sql");

	public int processOrgMonthlyMetricesData(long date) {
		int count = 0;
		log.debug("Processing Organization Monthly Metrices Data...");
		/* Creating Collection to Insert into tokuMX */
		DBCollection monthlyReportCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_ORG_MONTHLY_METRICS_AGRR);
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("org_monthly_metrics_agg_query");
		log.debug("[query] " + query);
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] { date },
				new KPIMonthlyRowMapper(pgJTemplete));
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null) {
			count = listOfdbObj.size();
			if (count > 0) {
				monthlyReportCol.insert(listOfdbObj);
			}
			log.debug("[count] " + count);
		}
		return count;
	}

	public void setMongoDefaultDAO(MongoDefaultDAOImpl mongoDefaultDAO) {
		this.mongoDefaultDAO = mongoDefaultDAO;
	}

	public void setPgJTemplete(PgJTemplete pgJTemplete) {
		this.pgJTemplete = pgJTemplete;
	}

}

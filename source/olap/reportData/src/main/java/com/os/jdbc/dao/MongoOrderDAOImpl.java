package com.os.jdbc.dao;

import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoOrderDAOImpl {

	public MongoTemplate orderTemplate;

	public void setOrderTemplate(MongoTemplate orderTemplate) {
		this.orderTemplate = orderTemplate;
	}

	public MongoTemplate getOrderTemplate() {
		return orderTemplate;
	}
	
}

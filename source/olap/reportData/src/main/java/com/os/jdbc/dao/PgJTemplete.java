package com.os.jdbc.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.mongodb.DBObject;

public class PgJTemplete {
	public DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void persistDataIntoPg(String string, Object[] objects) {
		JdbcTemplate template = new JdbcTemplate(dataSource);
		int result = template.update(string, objects);
		if(result ==0){
            System.out.println("Operation Failed..!");
        }
	}

	public void updateExistingData(String string, Object[] objects) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        int result = jdbcTemplate.update(string, objects);
        if(result ==0){
            System.out.println("Operation Failed..!");
        }
	}

	public void removeDataById(String string, Object[] object) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        int result = jdbcTemplate.update(string, object);
        if(result ==0){
            System.out.println("Operation Failed..!");
        }
	}
	
	public DBObject getDataById(String query, Object[] object, RowMapper<DBObject> rowMapper) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        DBObject dbObject = jdbcTemplate.queryForObject(query, object, rowMapper);
        return dbObject;
	}

	public List<DBObject> getDataObjects(String query, Object[] objects, RowMapper<DBObject> rowMapper) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<DBObject> dbObjects= jdbcTemplate.query(query, objects, rowMapper);
		return dbObjects;
	}
	
	public List<DBObject> getDataObjectsUsingPreparedStatement(String query, final Object[] objects, RowMapper<DBObject> rowMapper) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<DBObject> dbObjects= jdbcTemplate.query(query,
				new PreparedStatementSetter() {
		            public void setValues(PreparedStatement preparedStatement) throws SQLException {
		            	int index=1;
		            	for(Object object: objects){
		            		preparedStatement.setObject(index++, object);
		            	}
		            }
		          }
				, rowMapper);
		return dbObjects;
	}
	
	public List<DBObject> getDataObjects(String query, Map<String,Object> map, RowMapper<DBObject> rowMapper) {
		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		List<DBObject> dbObjects= jdbcTemplate.query(query, map, rowMapper);
		return dbObjects;
	}
	

}

package com.os.jdbc.dao;

import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoSnocDAOImpl extends MongoDAO {
	
	public MongoTemplate snocTemplate;

	public void setSnocTemplate(MongoTemplate snocTemplate) {
		this.snocTemplate = snocTemplate;
	}
	
	public MongoTemplate getSnocTemplate() {
		return snocTemplate;
	}
	

}

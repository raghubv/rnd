package com.os.snoc.util;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Constants: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1		May 19, 2015		@author Raghu		-- Base Release
 * 2		July 23, 2015		@author G Md Rafi	-- 
 * 
 * </pre>
 * 
 * <br>
 */
public final class Constants {

	public static final String _collectionName = "collectionName";
	public static final String _fields = "fields";
	public static final String _field = "field";
	public static final String _db = "db";
	public static final String _mappingType = "mappingType";

	public static final String _config_Old_Op_log_DateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String _Application_Context = "spring-application-context.xml";

	public static final String _OpLogTemplateBean = "opLogTemplate";
	public static final String _OplogCrudBean = "oplogCrud";
	public static final String _oplog_rs = "oplog.rs";
	public static final String _$natural = "$natural";
	public static final String _$gt = "$gt";
	public static final String _$in = "$in";
	public static final String _GTE = "$gte";
	public static final String _LTE = "$lte";
	public static final String _ts = "ts";
	public static final String _opsns = "ops.ns";
	public static final String _ops = "ops";
	public static final String _op = "op";
	public static final String _ns = "ns";
	public static final String _o = "o";
	public static final String _o2 = "o2";
	
	
	public static final String C_ORG_DAILY_METRICS_AGRR = "org_daily_metrics";
	public static final String C_ORG_MONTHLY_METRICS_AGRR = "org_monthly_metrics";
	public static final String C_STOCK_BY_REGION_AGRR = "stock_by_region";
	public static final String C_STOCK_BY_ET_AGRR = "stock_by_entity_type";
	/*public static final String C_STOCK_BY_ET_NODE_AGRR = "stock_by_node";
	public static final String C_STOCK_BY_ET_USER_AGRR = "stock_by_user";*/
	public static final String C_USER_BY_SALES_AGRR = "users_by_sales";
	
	
	public static final String C_PRODUCT_CATEGORY_SALES_AGRR = "product_category_sales";
	public static final String C_PRIMARY_SALES_USER_COUNT_AGRR = "primary_sales_user_count";
	public static final String C_SECONDARY_SALES_USER_COUNT_AGRR = "secondary_sales_user_count";
	public static final String C_STOCK_VALUE_AGRR = "stock_value";
	public static final String C_KS_VS_NONKS_AGRR = "ks_vs_nonks_order_count";
	public static final String C_SIM_SOLD_VS_ACT_AGRR = "sim_sold_vs_activated";
	
	public static final String C_PRODUCT_VALUES = "product_values";
	public static final String C_SERIAL_LEDGER = "serial_ledger";
	public static final String C_ORG= "organization";
	public static final String C_TRANS_VAL= "transitionValueCollection";
	
	
	
	public static final String C_SALES_ORDERS = "sales_orders";
	public static final String C_USER_TRANS = "user_trans";
	
	
	public static final String AGGR_ID_N = "aggregation_id_n";
	public static final String DAY_ID_N = "day_id_n";
	public static final String DAY_ID_D = "day_id_d";
	public static final String EVENT_TYPE_N = "event_type_n";
	public static final String MTR_ID_N = "metrics_id_n";
	public static final String QTY_N = "qty_n";
	public static final String VALUE_N = "value_n";
	public static final String NO_OF_EVENTS_N = "no_of_events_n";
	public static final String ORG_ID_N = "org_id_n";
	public static final String TRANS_ORG_ID_N = "trans_org_id_n";
	public static final String BEAT_INS_ID_N = "beat_instance_id_n";
	public static final String NODE_ID_N = "node_id_n";
	public static final String NODE_NAME_V = "node_name_v";
	public static final String ORG_NAME_V = "org_name_v";
	public static final String ORG_TYPE_N = "org_type_n";
	public static final String LEGAL_NAME_V = "legal_name_v";
	public static final String POSITION_ID_N = "position_id_n";
	public static final String ROOT_NODE_N = "root_node_n";
	public static final String REF_CODE_V = "ref_code_v";
	public static final String MTR_NAME_V = "metrics_name_v";
	public static final String MTR_TYPE_N = "metrics_type_n";
	public static final String MTR_TYPE_NAME_V = "metrics_type_name_v";
	public static final String UOM_N = "unit_of_measure_n";
	public static final String CURRENCY_V = "currency_v";
	public static final String RLTD_MTR_TYPE_ID_N = "related_metrics_type_id_n";
	public static final String RLTD_MTR_TYPE_ID_NAME_V = "related_metrics_type_id_name_v";
	public static final String RLTD_MTR_ID_N = "related_metrics_id_n";
	public static final String RLTD_MTR_ID_NAME_V = "related_metrics_name_v";
	public static final String RGN_ID_N = "region_id_n";
	public static final String RGN_NAME_V = "region_name_v";
	public static final String ATTRIBUTES = "attributes";
	
	public static final String MONTH_ID_N = "month_id_n";
	public static final String QTR_N = "qtr_n";
	
	public static final String REGION_ID = "region_id_n";
	public static final String REGION_NAME = "region_name_v";
	public static final String ORG_TYPE_NAME_V = "org_type_name_v";
	public static final String TOTAL_IN_STOCK = "total_in_stock";
	public static final String TOTAL_OUT_STOCK = "total_out_stock";
	public static final String STOCK_RATIO = "stock_ratio";
	
	public static final String LOOK_UP_ID_N = "lookup_id_n";
	public static final String LOOK_UP_NAME_V = "lookup_name_v";
	public static final String LOOK_UP_TYPE_N = "lookup_type_n";
	public static final String LOOK_UP_TYPE_NAME_V = "lookup_type_name_v";
	
	public static final String ORDER_CRTD_DT = "ord_creatd_dt";
	
}

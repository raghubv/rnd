package com.os.jdbc.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.os.jdbc.JdbcDao;
import com.os.jdbc.helper.KPIDailyDaoImplHelper;
import com.os.jdbc.helper.KPIMonthlyDaoImplHelper;
import com.os.snoc.util.DateTimeUtility;
import com.os.snoc.util.LoggerTrace;

/**
 * 
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class KPIMonthlyDaoImpl implements JdbcDao {

	private static Logger log = Logger.getLogger(KPIMonthlyDaoImpl.class);
	private KPIMonthlyDaoImplHelper kPIMonthlyDaoImplHelper;
	private int initialize;

	/**
	 * 
	 * @author Rafi <b>Algorithm:</b>
	 * 
	 *         <pre>
	 *         migrateDataFromPostgresToMongoDB
	 * 
	 *         </pre>
	 * 
	 * @return void
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.jdbc.JdbcDao#migrateDataFromPostgresToMongoDB()
	 */

	

	public void setInitialize(int initialize) {
		this.initialize = initialize;
	}

	public void setkPIMonthlyDaoImplHelper(KPIMonthlyDaoImplHelper kPIMonthlyDaoImplHelper) {
		this.kPIMonthlyDaoImplHelper = kPIMonthlyDaoImplHelper;
	}

	public void processReportData() {
		log.info("[begin...]");
		try {
			log.debug("Process Started..!");

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date now = DateTimeUtility.addDays(new Date(), initialize);
			String dateString = dateFormat.format(now);
			/* Monthly Tasks [1st day of every month] */
			dateFormat = new SimpleDateFormat("dd");
			log.info("Processing Monthly Aggregation Data..!");
			dateFormat = new SimpleDateFormat("yyyyMM");
			Date month = DateTimeUtility.addMonths(now, -1);
			String monthAsString = dateFormat.format(month);
			kPIMonthlyDaoImplHelper.processOrgMonthlyMetricesData(Long.parseLong(monthAsString));
			log.debug("Process Finished..!");
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		log.info("[end...]");
	}
}

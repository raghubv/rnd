package com.os.snoc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Configuration {
	private static final Logger log = Logger.getLogger(Configuration.class);
	private static Map configurationData = null;
	private static List<String> nameSpaces = null;

	static {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// Index Keys
			System.out.println("1>>"+System.getProperty("CONFIG_PATH"));

			InputStream configurationDataConfig = new FileInputStream(new File(
					System.getProperty("CONFIG_PATH")
							+ "configuration.json"));

			configurationData = objectMapper.readValue(configurationDataConfig,
					LinkedHashMap.class);
			configurationDataConfig.close();

			nameSpaces = new ArrayList();
			Set<Map.Entry<String, Object>> set = configurationData.entrySet();
			for (Entry<String, Object> entry : set) {
				nameSpaces.add(entry.getKey());
			}

		} catch (JsonParseException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
	}

	public static List<String> getNameSpaces() {
		return nameSpaces;
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getConfigurationData
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	public static Map getConfigurationData() {
		return configurationData;
	}

}

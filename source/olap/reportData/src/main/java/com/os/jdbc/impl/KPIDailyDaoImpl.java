package com.os.jdbc.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.os.jdbc.JdbcDao;
import com.os.jdbc.helper.KPIDailyDaoImplHelper;
import com.os.snoc.util.DateTimeUtility;
import com.os.snoc.util.LoggerTrace;

/**
 * 
 * 
 * 
 * <b>Purpose:</b><br>
 * KPIDailyDaoImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	May 19, 2015	@author Raghu		-- Base Release
 * 2	July 23, 2015	@author G Md Rafi	--
 * 
 * </pre>
 * 
 * <br>
 */
public class KPIDailyDaoImpl implements JdbcDao {

	private static Logger log = Logger.getLogger(KPIDailyDaoImpl.class);
	private KPIDailyDaoImplHelper kpiDailyDaoImplHelper;
	private int initialize;

	/**
	 * 
	 * @author Rafi <b>Algorithm:</b>
	 * 
	 *         <pre>
	 *         migrateDataFromPostgresToMongoDB
	 * 
	 *         </pre>
	 * 
	 * @return void
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.jdbc.JdbcDao#migrateDataFromPostgresToMongoDB()
	 */

	public void setKpiDailyDaoImplHelper(KPIDailyDaoImplHelper kpiDailyDaoImplHelper) {
		this.kpiDailyDaoImplHelper = kpiDailyDaoImplHelper;
	}

	public void setInitialize(int initialize) {
		this.initialize = initialize;
	}

	public void processReportData() {
		log.info("[begin...]");
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date now = DateTimeUtility.addDays(new Date(), initialize);
			String dateString = dateFormat.format(now);
			long date = Long.parseLong(dateString);

			/* Daily Tasks [Every day] */
			log.debug("[date]"+date);
			
			kpiDailyDaoImplHelper.processOrgDailyMetricesData(date);
			kpiDailyDaoImplHelper.processStcokAggData(date);
			kpiDailyDaoImplHelper.getUsersAssociatedWithSales(dateString);

			/* Monthly Tasks [1st day of every month] */
				dateFormat = new SimpleDateFormat("dd");
				log.info("Processing Monthly Aggregation Data..!");
				dateFormat = new SimpleDateFormat("yyyyMM");
				Date month = DateTimeUtility.addMonths(now, -1);
				String monthAsString = dateFormat.format(month);
				kpiDailyDaoImplHelper.processOrgMonthlyMetricesData(Long.parseLong(monthAsString));
		
			log.debug("Process Finished..!");
		} catch (Exception e) {
			log.error("[Error] " + LoggerTrace.getStackTrace(e));
		}
		log.info("[end...]");
	}
}

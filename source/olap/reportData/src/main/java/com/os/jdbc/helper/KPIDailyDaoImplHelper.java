package com.os.jdbc.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.os.jdbc.dao.MongoDefaultDAOImpl;
import com.os.jdbc.dao.MongoOrderDAOImpl;
import com.os.jdbc.dao.MongoSnocDAOImpl;
import com.os.jdbc.dao.PgJTemplete;
import com.os.jdbc.impl.KPIDailyDaoImpl;
import com.os.jdbc.rowMapper.KPIDaillyRowMapper;
import com.os.jdbc.rowMapper.KPIMonthlyRowMapper;
import com.os.jdbc.rowMapper.StcokByETRowMapper;
import com.os.jdbc.rowMapper.StcokByRegionRowMapper;
import com.os.snoc.util.Constants;
import com.os.snoc.util.DateTimeUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Constants: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1	July 23, 2015	@author G Md Rafi   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */

public class KPIDailyDaoImplHelper {

	private static Logger log = Logger.getLogger(KPIDailyDaoImpl.class);

	private MongoDefaultDAOImpl mongoDefaultDAO;
	private MongoSnocDAOImpl mongoSnocDAO;
	private MongoOrderDAOImpl mongoOrderDAO;
	private PgJTemplete pgJTemplete;

	private static ResourceBundle sql = ResourceBundle.getBundle("sql");

	public int processOrgDailyMetricesData(long date) {
		int count = 0;
		log.debug("Processing Organization Daily Metrices Data...");
		/* Creating Collection to Insert into tokuMX */
		DBCollection dailyReportCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_ORG_DAILY_METRICS_AGRR);
		/* Fetching Daily Aggregation Data From Postgres */
		String query = sql.getString("org_daily_metrics_agg_query");
		log.debug("[query] " + query);
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] { date },
				new KPIDaillyRowMapper(pgJTemplete));
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null) {
			count = listOfdbObj.size();
			if (count > 0) {
				dailyReportCol.insert(listOfdbObj);
			}
			listOfdbObj = null;
			log.debug("[count] " + count);
		}

		return count;
	}

	public int processOrgMonthlyMetricesData(long date) {
		int count = 0;
		log.debug("Processing Organization Monthly Metrices Data...");
		/* Creating Collection to Insert into tokuMX */
		DBCollection monthlyReportCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_ORG_MONTHLY_METRICS_AGRR);
		/* Fetching Monthly Aggregation Data From Postgres */
		String query = sql.getString("org_monthly_metrics_agg_query");
		log.debug("[query] " + query);
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] { date },
				new KPIMonthlyRowMapper(pgJTemplete));
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null) {
			count = listOfdbObj.size();
			if (count > 0) {
				monthlyReportCol.insert(listOfdbObj);
			}
			log.debug("[count] " + count);
		}
		return count;
	}

	public void processStcokAggData(long date) {
		log.debug("Processing Stock Aggregation Data...");
		/* Creating Collection to Insert into tokuMX */
		DBCollection stockByRegionCol = mongoDefaultDAO.getDefaultTemplate()
				.getCollection(Constants.C_STOCK_BY_REGION_AGRR);
		DBCollection stockByOrgCol = mongoDefaultDAO.getDefaultTemplate().getCollection(Constants.C_STOCK_BY_ET_AGRR);
		/*
		 * DBCollection stockByNodeCol = mongoDefaultDAO.getDefaultTemplate()
		 * .getCollection(Constants.C_STOCK_BY_ET_NODE_AGRR); DBCollection
		 * stockByUserCol = mongoDefaultDAO.getDefaultTemplate()
		 * .getCollection(Constants.C_STOCK_BY_ET_USER_AGRR);
		 */

		/* Fetching Stock by Region Data From Postgres */
		int count = 0;
		String query = sql.getString("stock_region_agg_query");
		log.debug("[query] " + query);
		List<DBObject> listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] { date },
				new StcokByRegionRowMapper(pgJTemplete));
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null) {
			count = listOfdbObj.size();
			if (count > 0) {
				stockByRegionCol.insert(listOfdbObj);
			}
			log.debug("[count] " + count);
		}

		/* Fetching Stock by Organization Data From Postgres */
		query = sql.getString("stock_entity_type_agg_query");
		log.debug("[query] " + query);
		listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] { date }, new StcokByETRowMapper());
		/* Inserting Fetched Data into TokuMX */
		if (listOfdbObj != null) {
			count = listOfdbObj.size();
			if (count > 0) {
				stockByOrgCol.insert(listOfdbObj);
			}
			log.debug("[count]" + count);
		}

		/*
		 * Fetching Stock by Node Data From Postgres query =
		 * sql.getString("stock_node_agg_query"); log.debug("[query] " + query);
		 * listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] {}, new
		 * StcokByNodeRowMapper()); Inserting Fetched Data into TokuMX
		 * if(listOfdbObj!=null){ stockByNodeCol.insert(listOfdbObj);
		 * log.debug("[count]" + listOfdbObj.size()); }
		 * 
		 * Fetching Stock by User Data From Postgres query =
		 * sql.getString("stock_user_agg_query"); log.debug("[query] " + query);
		 * listOfdbObj = pgJTemplete.getDataObjects(query, new Object[] {}, new
		 * StcokByUserRowMapper()); Inserting Fetched Data into TokuMX
		 * if(listOfdbObj!=null){ stockByUserCol.insert(listOfdbObj);
		 * log.debug("[count]" + listOfdbObj.size()); }
		 */
	}

	public void getUsersAssociatedWithSales(String dateString) throws Exception {
		log.debug("Processing Users Associate to Sales...");
		DBCollection salesOrders = mongoOrderDAO.getOrderTemplate().getCollection(Constants.C_SALES_ORDERS);
		// BasicDBObject query = new BasicDBObject(/*"_id", 19105*//*Here, Query
		// need all the data*/);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(dateString);
		Date startDate = DateTimeUtility.getStartOfTheDay(date);
		Date endDate = DateTimeUtility.getEndOfTheDay(date);
		log.debug("[startDate]"+startDate);
		log.debug("[endDate]"+endDate);
		BasicDBObject query = new BasicDBObject(Constants.ORDER_CRTD_DT,
				new BasicDBObject(Constants._GTE, startDate).append(Constants._LTE, endDate));
		DBCursor salesOrderCursor = salesOrders.find(query);
		int count = 0;
		List<Long> users = null;
		for (DBObject dbObject : salesOrderCursor) {
			Object data = dbObject.get("data");
			long ordId = Long.parseLong(String.valueOf(dbObject.get("ord_id")));
			if (data != null) {
				BasicDBObject dataObject = (BasicDBObject) data;
				users = new ArrayList<Long>();
				String hierarchyString = dataObject.getString("hierarchy_rollup");
				if (hierarchyString != null) {
					count++;
					DBCollection usersBySales = mongoDefaultDAO.getDefaultTemplate()
							.getCollection(Constants.C_USER_BY_SALES_AGRR);
					BasicDBObject salesUsers = getUsersAssociatedWithSales(dataObject);
					String[] hierarchyList = hierarchyString.split("#");
					for (String hierarchy : hierarchyList) {
						String[] entities = hierarchy.split("~");
						if (entities.length > 1 && entities[1] != null) {
							DBCollection userTrans = mongoSnocDAO.getSnocTemplate()
									.getCollection(Constants.C_USER_TRANS);
							try {
								long nodeId = Long.parseLong(entities[1]);
								DBCursor userTransCursor = userTrans.find(new BasicDBObject("node_id", nodeId));
								for (DBObject userTransObj : userTransCursor) {
									users.add(((BasicDBObject) userTransObj).getLong("user_id"));
								}
							} catch (NumberFormatException e) {
							}
						}
					}
					salesUsers.put("usrers", users);
					salesUsers.put("ord_id", ordId);
					usersBySales.insert(salesUsers);
				}
			}
		}
		log.info("[count] " + count);
	}

	private BasicDBObject getUsersAssociatedWithSales(BasicDBObject basicDBObject) throws Exception {
		BasicDBObject result = new BasicDBObject();
		try {
			result.put("ord_type", basicDBObject.getLong("ord_type"));
			result.put("ord_name", basicDBObject.getString("ord_name"));
			result.put("ord_creatd_by", basicDBObject.getString("ord_creatd_by"));
			result.put("created_node_id", basicDBObject.getString("created_node_id"));
			result.put("ord_creatd_dt", basicDBObject.getDate("ord_creatd_dt"));
			result.put("src_channel", basicDBObject.getString("src_channel"));
			result.put("fm_node", basicDBObject.getLong("fm_node"));
			result.put("fm_acc_id", basicDBObject.getLong("fm_acc_id"));
			result.put("to_node", basicDBObject.getLong("to_node"));
			result.put("to_acc_id", basicDBObject.getLong("to_acc_id"));
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	/**
	 * Accepted format: yyyyMMdd Default Time Zone: local Time Zone is
	 * Considered as Default
	 * 
	 * @param date
	 * @return {@link Date}
	 * @throws Exception
	 */
	public static Date getDate(String dateAsString) throws Exception {
		Date date = null;
		try {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
			DateTime dateTime = formatter.parseDateTime(dateAsString);
			date = dateTime.toDate();
		} catch (Exception e) {
			throw e;
		}
		return date;
	}

	public void setMongoDefaultDAO(MongoDefaultDAOImpl mongoDefaultDAO) {
		this.mongoDefaultDAO = mongoDefaultDAO;
	}

	public void setMongoSnocDAO(MongoSnocDAOImpl mongoSnocDAO) {
		this.mongoSnocDAO = mongoSnocDAO;
	}

	public void setMongoOrderDAO(MongoOrderDAOImpl mongoOrderDAO) {
		this.mongoOrderDAO = mongoOrderDAO;
	}

	public void setPgJTemplete(PgJTemplete pgJTemplete) {
		this.pgJTemplete = pgJTemplete;
	}

}

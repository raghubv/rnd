package com.os.jdbc.dao;

import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoInventoryDAOImpl {

	public MongoTemplate inventoryTemplate;

	public void setInventoryTemplate(MongoTemplate inventoryTemplate) {
		this.inventoryTemplate = inventoryTemplate;
	}

	public MongoTemplate getInventoryTemplate() {
		return inventoryTemplate;
	}
	
}

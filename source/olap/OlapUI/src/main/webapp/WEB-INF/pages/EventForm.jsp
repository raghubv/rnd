<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>

<body>
	<h2>ETL>>Aggregation & Computation>>Event Mapping</h2>

	<form:form method="POST" commandName="eventDefinition"
		action="/OlapUi/olap/event">
		<%-- 		<form:errors path="*" cssClass="errorblock" element="div" /> --%>
		<table>
			<thead>

				Event Mapping
				<br /> Map ETL & Aggregation to event
				<br />
			</thead>
			<tr>
				<td><form:textarea path="eventMappingDfn" rows="20" cols="100" /></td>
				<td><form:errors path="eventMappingDfn" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3" align="center"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>
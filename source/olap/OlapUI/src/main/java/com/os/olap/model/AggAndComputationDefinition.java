package com.os.olap.model;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * AggAndComputationDefinition: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Mar 23, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class AggAndComputationDefinition {



	@NotEmpty
	String aggAndComputeDfn;

	public String getAggAndComputeDfn() {
		return aggAndComputeDfn;
	}

	public void setAggAndComputeDfn(String aggAndComputeDfn) {
		this.aggAndComputeDfn = aggAndComputeDfn;
	}
	
	


}
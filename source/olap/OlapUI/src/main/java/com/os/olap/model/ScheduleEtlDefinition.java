package com.os.olap.model;

import org.hibernate.validator.constraints.NotEmpty;

public class ScheduleEtlDefinition {

	
	@NotEmpty
	String etlScheduleDfn;
	
	
	

	public String getEtlScheduleDfn() {
		return etlScheduleDfn;
	}


	public void setEtlScheduleDfn(String etlScheduleDfn) {
		this.etlScheduleDfn = etlScheduleDfn;
	}


		
	

}
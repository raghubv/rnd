package com.os.olap.model;

import org.hibernate.validator.constraints.NotEmpty;

public class EventDefinition {


	@NotEmpty
	String eventMappingDfn;

	public String getEventMappingDfn() {
		return eventMappingDfn;
	}

	public void setEventMappingDfn(String eventMappingDfn) {
		this.eventMappingDfn = eventMappingDfn;
	}
	

}
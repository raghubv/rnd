package com.os.olap.model;

import org.hibernate.validator.constraints.NotEmpty;

public class EtlDefinition {


	public String getEtlDfn() {
		return etlDfn;
	}

	public void setEtlDfn(String etlDfn) {
		this.etlDfn = etlDfn;
	}

	@NotEmpty
	String etlDfn;

}
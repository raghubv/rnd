Changes configuration.properties specific to environment

Windows Set Up
	SET  LOG4J_CONFIG_LOCATION=Winows_path\logger\
	SET  LOG4J_REFRESH_INTERVAL=5000

Linux Set Up
	export LOG4J_CONFIG_LOCATION=Linux_path/logger/
	export LOG4J_REFRESH_INTERVAL=5000



<!--default-log4j.xml  file copy to LOG4J_CONFIG_LOCATION Start-->
	<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">
 <log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">
	<appender name="consoleAppender" class="org.apache.log4j.ConsoleAppender">
		<layout class="org.apache.log4j.PatternLayout">
		<param name="ConversionPattern" value="[%d{dd MMMMM yyyy HH:mm:ss}] %-5p [%l] %x - %m%n"/>
		</layout>
	</appender>
	<appender name="fileAppender" class="org.apache.log4j.DailyRollingFileAppender">
		<param name="file" value="${catalina.home}/logs/olap-app-1.log"/>
		<param name="append" value="true"/>
		<layout class="org.apache.log4j.PatternLayout">
			<param name="ConversionPattern" value="[%d{dd MMMMM yyyy HH:mm:ss}] %-5p [%l] %x - %m%n"/>
		</layout>
	</appender>
	<!-- Quartz  -->
   <category name="org.quartz" additivity="false">
       <priority value="ERROR" />
       <appender-ref ref="fileAppender"/>
   </category>
   <category name="org.apache" additivity="false">
       <priority value="ERROR" />
       <appender-ref ref="fileAppender"/>
   </category>
   <category name="org.springframework" additivity="false">
       <priority value="ERROR" />
       <appender-ref ref="fileAppender"/>
   </category>
   <category name="com.os" additivity="false">
       <priority value="DEBUG" />
       <appender-ref ref="fileAppender"/>
   </category>
    <root>
		<level value="WARN" /> 
		<appender-ref ref="consoleAppender"/>
		<appender-ref ref="fileAppender"/>
   </root>
</log4j:configuration>
<!---End-->

Once war file is deployed, war file specific log4j.xml created in   CONFIG_PROPERTIES_FILE location


Sample tomcat.sh


#loading Configuration
echo "loading Configuration"
export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8 -server  -Xms4096m -Xmx4096m -XX:PermSize=32m -XX:MaxPermSize=2048m -Xss4m -XX:ReservedCodeCacheSize=512m -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000 -XX:+UseConcMarkSweepGC"
export CONFIG_PROPERTIES_FILE="/home/analytics/script/configuration.properties"
export LOG4J_CONFIG_LOCATION=/home/analytics/script/
export LOG4J_REFRESH_INTERVAL=5000
echo "loading Configuration completed..."
/home/analytics/apache-tomcat-7.0.57/bin/startup.sh &
echo "Server started..."






Sample Cron Expression meaning

* "0 0 * * * *" = the top of every hour of every day.
* "*/10 * * * * *" = every ten seconds.
* "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
* "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
* "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
* "0 0 0 25 12 ?" = every Christmas Day at midnight



Configured configuration.properties


########## MONGODB #########
mongodb.host=192.168.2.164
mongodb.port=27017
mongodb.dbname=Analytics
mongodb.connectionperhost=40
mongodb.blockconnectionmultiplier=4
mongodb.connecttimeout=1000
mongodb.maxwaittime=360000
mongodb.autoconnectretry=true
mongodb.maxautoconnectretry=1500
mongodb.socketkeepalive=false
mongodb.sockettimeout=360000
mongodb.slaveok=true
########## ETL #########
etl.cron=0/30 * * * * ?
etl.corePoolSize=2
etl.maxPoolSize=3
etl.queueCapacity=100
########## Aggregate Initiator #########
aggregate.initiator.cron=0/40 0/2 * * * ?
aggregate.initiator.corePoolSize=1
aggregate.initiator.maxPoolSize=2
aggregate.initiator.queueCapacity=3
########## Aggregate Executor  #########
aggregate.executor.cron=0/59 * * * * ?
aggregate.executor.corePoolSize=6
aggregate.executor.maxPoolSize=7
aggregate.executor.queueCapacity=2500
########## Aggregate Instance #########
aggregate.instance.minnoofinstancesforcreateinstance=50000
########## Aggregate Instance #########
aggregate.errorHandler.cron=0/40 0/45 * * * ?


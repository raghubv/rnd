package com.os.aggregate.daoImpl;

import static com.os.util.Constants.YYYYMMdd_DATE_FORMAT;
import static com.os.util.Constants.YYYY_MM_DD_HH_MM_SS_DATE_FORMAT;
import static com.os.util.Constants._$in;
import static com.os.util.Constants._$set;
import static com.os.util.Constants._Agg_Status_Active;
import static com.os.util.Constants._Agg_Status_ERROR;
import static com.os.util.Constants._Agg_Status_Inprogress;
import static com.os.util.Constants._AggregateAndComputeDfn;
import static com.os.util.Constants._AggregateAndComputeInstances;
import static com.os.util.Constants._AggregateSchedule;
import static com.os.util.Constants._AggregatedAndComputedDate;
import static com.os.util.Constants._CollectionName;
import static com.os.util.Constants._ETLJobInstances;
import static com.os.util.Constants._EventMappingDfn;
import static com.os.util.Constants._aggAndCompInstStatus;
import static com.os.util.Constants._aggAndCompInstStatus_Active;
import static com.os.util.Constants._aggAndCompInstStatus_ERROR;
import static com.os.util.Constants._aggAndCompInstStatus_Inprogress;
import static com.os.util.Constants._aggregatedAndComputedDateDay;
import static com.os.util.Constants._aggregatedAndComputedDateMap;
import static com.os.util.Constants._aggregatedAndComputedDateMonth;
import static com.os.util.Constants._aggregatedAndComputedDateYear;
import static com.os.util.Constants._aggregation;
import static com.os.util.Constants._barChartAPIOutputType;
import static com.os.util.Constants._barChartAPITextX;
import static com.os.util.Constants._barChartAPIaggregate;
import static com.os.util.Constants._barChartAPIls;
import static com.os.util.Constants._collections;
import static com.os.util.Constants._compute;
import static com.os.util.Constants._data;
import static com.os.util.Constants._dataRows_datePath;
import static com.os.util.Constants._dateParams;
import static com.os.util.Constants._dbParams;
import static com.os.util.Constants._dfn_Map_Data;
import static com.os.util.Constants._endTimeStamp;
import static com.os.util.Constants._eventName;
import static com.os.util.Constants._expressions;
import static com.os.util.Constants._fieldName;
import static com.os.util.Constants._fieldValue;
import static com.os.util.Constants._formulaDfn;
import static com.os.util.Constants._formulaId;
import static com.os.util.Constants._funcationArgs;
import static com.os.util.Constants._funcationName;
import static com.os.util.Constants._groupBy;
import static com.os.util.Constants._id;
import static com.os.util.Constants._inCollection;
import static com.os.util.Constants._jsFunction;
import static com.os.util.Constants._jsFunctions;
import static com.os.util.Constants._mappingDBRef;
import static com.os.util.Constants._outCollection;
import static com.os.util.Constants._pipeline;
import static com.os.util.Constants._requestMessage;
import static com.os.util.Constants._result;
import static com.os.util.Constants._scheduleDetails;
import static com.os.util.Constants._scheduled;
import static com.os.util.Constants._scheduledTimeStamp;
import static com.os.util.Constants._startTimeStamp;
import static com.os.util.Constants._status;
import static com.os.util.Constants._status_Active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.os.aggregate.dao.AggregateAndComputeDao;
import com.os.util.BarChartMapper;
import com.os.util.Constants;
import com.os.util.Constants.responseCode;
import com.os.util.DBUtils;
import com.os.util.DateUtils;
import com.os.util.EvaluateExpression;
import com.os.util.LoggerTrace;
import com.os.util.MessageUtility;
import com.os.util.QueryUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.WriteConcern;
import com.mongodb.util.JSON;


public class AggregateAndComputeDaoImpl implements AggregateAndComputeDao {

	@Autowired
	MongoTemplate mongoTemplate;

	static final Logger log = Logger.getLogger(AggregateAndComputeDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.aggregateAndCompute.dao.AggregateAndComputeDao#
	 * aggregateAndCompute(java.util.Map)
	 */
	public String aggregateAndCompute(Map<Object, Object> requestPayload) {
		try {
			boolean isDebugEnabled = log.isDebugEnabled();

			DBObject result = new BasicDBObject();
			Map aggregateParams = new LinkedHashMap();
			ObjectMapper objectMapper = new ObjectMapper();
			Map todayDateMap = new HashMap();
			Date toDayDate = new Date();
			String todayStr = YYYYMMdd_DATE_FORMAT.format(toDayDate);
			todayDateMap.put(_aggregatedAndComputedDateDay, (todayStr.substring(6)));
			todayDateMap.put(_aggregatedAndComputedDateMonth, (todayStr.substring(4, 6)));
			todayDateMap.put(_aggregatedAndComputedDateYear, (todayStr.substring(0, 4)));
			Integer today = Integer.valueOf(todayStr);

			String compute = (String) requestPayload.get(_compute);
			aggregateParams.putAll((Map) requestPayload.get(_groupBy));
			if (isDebugEnabled)
				log.debug("[compute]" + compute);
			if (isDebugEnabled)
				log.debug("[inputParams]" + aggregateParams);

			DBObject formulaQuery = new BasicDBObject();
			formulaQuery.put(_formulaId, compute);
			DBObject fromulaDfn = mongoTemplate.getCollection(_AggregateAndComputeDfn).findOne(formulaQuery);
			Map formulaDfn = (Map) JSON.parse((String) fromulaDfn.get(_formulaDfn));
			if (isDebugEnabled)
				log.debug("[formulaDfn]" + formulaDfn);

			// To get global params from db
			if (formulaDfn.containsKey(_dbParams)) {
				DBObject dbParams = (DBObject) formulaDfn.get(_dbParams);
				aggregateParams.putAll(DBUtils.getDBParams(mongoTemplate, dbParams.toMap()));
			}
			// To get already computed data from collection
			if (formulaDfn.containsKey(_collections)) {
				List<String> collections = (List) formulaDfn.get(_collections);
				for (String collection : collections) {
					DBObject dbQry = new BasicDBObject(_groupBy, requestPayload.get(_groupBy));
					dbQry.put(_dataRows_datePath, today);
					if (isDebugEnabled)
						log.debug("[collection]" + collection);
					if (isDebugEnabled)
						log.debug("[dbQry]" + dbQry);
					DBObject dbdoc = mongoTemplate.getCollection(collection).findOne(dbQry);
					if (isDebugEnabled)
						log.debug("[dbdoc]" + dbdoc);
					if (dbdoc != null) {
						Set set = ((Map) dbdoc.get(_data)).entrySet();
						for (Object object : set) {
							aggregateParams.put(collection + "." + ((Map.Entry) object).getKey(),
									((Map.Entry) object).getValue());
						}
					}
				}
			}
			// To get configuration date params
			if (formulaDfn.containsKey(_dateParams)) {
				aggregateParams.putAll(DateUtils.evaluateDate((Map) formulaDfn.get(_dateParams)));
			}
			String inCollection = (String) formulaDfn.get(_inCollection);
			String outCollection = (String) formulaDfn.get(_outCollection);

			// Mongodb Aggregate
			if (formulaDfn.containsKey(_aggregation)) {

				List aggregatePipeline = (List) QueryUtils
						.prepareQuery(((Map) formulaDfn.get(_aggregation)), aggregateParams).get(_pipeline);
				if (isDebugEnabled)
					log.debug("[ls]" + aggregatePipeline);
				AggregationOutput aggregateOutput = mongoTemplate.getCollection(inCollection)
						.aggregate(aggregatePipeline);
				Map aggregateAndComputeMap = aggregateOutput.getCommandResult().toMap();
				if (isDebugEnabled)
					log.debug("[aggregateAndComputeMap]" + aggregateAndComputeMap);
				result.putAll((Map) ((List) aggregateAndComputeMap.get(_result)).get(0));

			}
			// Evaluate expression
			if (formulaDfn.containsKey(_expressions)) {

				List<DBObject> expressions = (List<DBObject>) formulaDfn.get(_expressions);
				for (DBObject dbObj : expressions) {
					result.put((String) dbObj.get(_fieldName),
							EvaluateExpression.evaluateExpression((String) dbObj.get(_fieldValue), aggregateParams));
				}
			}
			// Evaluate JS Function
			if (formulaDfn.containsKey(_jsFunctions)) {
				List<DBObject> jsFunctions = (List<DBObject>) formulaDfn.get(_jsFunctions);
				for (DBObject jsFunction : jsFunctions) {
					result.put((String) jsFunction.get(_fieldName),
							EvaluateExpression.evaluateJSFunction((String) jsFunction.get(_jsFunction),
									(String) jsFunction.get(_funcationName), (String) jsFunction.get(_funcationArgs),
									aggregateParams));
				}
			}
			result.removeField(_id);

			/** Query for Match--Start **/
			DBObject qryByGroupByAndComputedDate = new BasicDBObject();
			qryByGroupByAndComputedDate.put(_groupBy, requestPayload.get(_groupBy));
			qryByGroupByAndComputedDate.put(_AggregatedAndComputedDate, today);

			DBObject resultItem = new BasicDBObject();
			resultItem.put(_groupBy, requestPayload.get(_groupBy));
			resultItem.put(_AggregatedAndComputedDate, today);
			resultItem.put(_data, result);
			resultItem.put(_aggregatedAndComputedDateMap, todayDateMap);
			mongoTemplate.getCollection(outCollection).update(qryByGroupByAndComputedDate, resultItem, true, false);

			return MessageUtility.response(responseCode.Success, result.toMap());
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Aggregate_And_Compute_Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.aggregateAndCompute.dao.AggregateAndComputeDao#
	 * addAggregateAndComputeFormula(java.util.Map)
	 */
	public String addAggregateAndComputeFormula(Map<Object, Object> requestPayload) {
		Map requestMessage = (Map) requestPayload.get(_requestMessage);
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Set set = requestMessage.entrySet();
			for (Object object : set) {
				Map.Entry mapEntry = (Map.Entry) object;
				DBObject dbObject = new BasicDBObject();
				dbObject.put(_formulaId, (String) mapEntry.getKey());
				dbObject.put(_formulaDfn, objectMapper.writeValueAsString(mapEntry.getValue()));
				mongoTemplate.getCollection(_AggregateAndComputeDfn).insert(dbObject);

			}
			return MessageUtility.response(responseCode.Success);
		} catch (JsonGenerationException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility.response(responseCode.Add_Aggregate_Formula_Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.aggregate.dao.AggregateAndComputeDao#scheduleAggregate
	 * (java.util.Map)
	 */
	public String scheduleAggregate(Map<Object, Object> requestPayload) {
		boolean isDebugEnabled = log.isDebugEnabled();
		try {

			Map request = (Map) requestPayload.get(_requestMessage);
			if (isDebugEnabled)
				log.debug("[request]" + request);
			DBObject eventDfnQry = new BasicDBObject();
			eventDfnQry.put(_eventName, (String) request.get(_eventName));
			if (isDebugEnabled)
				log.debug("[eventDfnQry]" + eventDfnQry);
			DBObject eventDfn = mongoTemplate.getCollection(_EventMappingDfn).findOne(eventDfnQry,
					new BasicDBObject(_id, 1));
			if (isDebugEnabled)
				log.debug("[eventDfn]" + eventDfn);
			ObjectId objectId = (ObjectId) eventDfn.get(_id);
			DBObject aggregateSchedule = new BasicDBObject();
			aggregateSchedule.put(_mappingDBRef, new DBRef(mongoTemplate.getDb(), _EventMappingDfn, objectId));
			Map scheduledDetails = (Map) request.get(_scheduleDetails);
			Map scheduledDtls = new HashMap();
			List<Map> timeStampLs = (List<Map>) scheduledDetails.get(_scheduledTimeStamp);
			List<Map> timeLs = new ArrayList<Map>();
			for (Map map : timeStampLs) {
				Map ins = new HashMap();

				ins.put(_startTimeStamp, YYYY_MM_DD_HH_MM_SS_DATE_FORMAT.parse((String) map.get(_startTimeStamp)));
				ins.put(_endTimeStamp, YYYY_MM_DD_HH_MM_SS_DATE_FORMAT.parse((String) map.get(_endTimeStamp)));
				timeLs.add(ins);

			}
			scheduledDtls.put(_scheduledTimeStamp, timeLs);
			scheduledDtls.put(_scheduled, (List<Map>) scheduledDetails.get(_scheduled));

			aggregateSchedule.put(_scheduleDetails, scheduledDtls);
			aggregateSchedule.put(_status, _status_Active);
			if (isDebugEnabled)
				log.debug("[etlSchedule]" + aggregateSchedule);
			mongoTemplate.getCollection(_AggregateSchedule).insert(aggregateSchedule);
			return MessageUtility.response(responseCode.Success);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Schedule_Aggregation_Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.aggregate.dao.AggregateAndComputeDao#reInitiateAggregate()
	 */
	public String reInitiateAggregate() {
		try {
			DBObject dbQuery = new BasicDBObject();

			List<String> statusList = new ArrayList<String>();
			statusList.add(_Agg_Status_Inprogress);
			statusList.add(_Agg_Status_ERROR);
			dbQuery.put(_status, new BasicDBObject(_$in, statusList));
			DBObject dbUpdate = new BasicDBObject();
			dbUpdate.put(_$set, new BasicDBObject(_status, _Agg_Status_Active));
			log.debug("[dbQuery]" + dbQuery);
			log.debug("[dbUpdate]" + dbUpdate);
			log.error("[dbQuery]" + dbQuery);
			log.error("[dbUpdate]" + dbUpdate);
			mongoTemplate.getCollection(_AggregateAndComputeInstances).update(dbQuery, dbUpdate, false, true,
					WriteConcern.JOURNAL_SAFE);
			return MessageUtility.response(responseCode.Success);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.aggregate.dao.AggregateAndComputeDao#
	 * reCreateInstancesOfAggregation()
	 */
	public String reCreateInstancesOfAggregation() {
		try {
			DBObject dbQuery = new BasicDBObject();

			List<String> statusList = new ArrayList<String>();
			statusList.add(_aggAndCompInstStatus_Inprogress);
			statusList.add(_aggAndCompInstStatus_ERROR);
			dbQuery.put(_aggAndCompInstStatus, new BasicDBObject(_$in, statusList));
			DBObject dbUpdate = new BasicDBObject();
			dbUpdate.put(_$set, new BasicDBObject(_aggAndCompInstStatus, _aggAndCompInstStatus_Active));
			log.debug("[dbQuery]" + dbQuery);
			log.debug("[dbUpdate]" + dbUpdate);
			log.error("[dbQuery]" + dbQuery);
			log.error("[dbUpdate]" + dbUpdate);
			mongoTemplate.getCollection(_ETLJobInstances).update(dbQuery, dbUpdate, false, true,
					WriteConcern.JOURNAL_SAFE);
			return MessageUtility.response(responseCode.Success);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.aggregate.dao.AggregateAndComputeDao#getAggregatedData
	 * (java.util.Map)
	 */
	public String getAggregatedData(Map<Object, Object> requestPayload) {
		try {

			Map request = (Map) requestPayload.get(_requestMessage);
			log.debug("[requestPayload]" + request);
			log.debug("[pipeLine]" + (List) request.get(_barChartAPIaggregate));
			AggregationOutput aggregattionOutput = mongoTemplate.getCollection((String) request.get(_CollectionName))
					.aggregate((List) request.get(_barChartAPIaggregate));
			Iterable<DBObject> it = aggregattionOutput.results();
			List<Map> ls = new ArrayList<Map>();
			for (DBObject dbObject : it) {
				ls.add(dbObject.toMap());
				log.debug("[dbObject]" + dbObject);
			}
			if ((Integer) request.get(_barChartAPIOutputType) == 1)
				return MessageUtility.response(responseCode.Success, (DBObject) (new BasicDBObject(_barChartAPIls,
						BarChartMapper.getGoogleMap(ls, (String) request.get(_barChartAPITextX)))));
			else if ((Integer) request.get(_barChartAPIOutputType) == 0)
				return MessageUtility.response(responseCode.Success,
						(DBObject) (new BasicDBObject(_barChartAPIls, ls)));
			else if ((Integer) request.get(_barChartAPIOutputType) == 2)
				return MessageUtility.response(responseCode.Success, (DBObject) (new BasicDBObject(_barChartAPIls,
						BarChartMapper.getNVD3Map(ls, (String) request.get(_barChartAPITextX)))));

		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.aggregate.dao.AggregateAndComputeDao#getMapData(java.util.
	 * Map)
	 */
	public String getMapData(String jsName) {
		try {

			log.debug("[jsName]" + jsName.replace(".js", ""));
			DBObject response = mongoTemplate.getCollection(_dfn_Map_Data)
					.findOne(new BasicDBObject(_id, jsName.replace(".js", "")), new BasicDBObject(_id, 0));
			log.debug("[response]" + response);
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValueAsString(response);

			return "AmCharts.maps." + jsName.replace(".js", "") + "=" + objectMapper.writeValueAsString(response);

		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.aggregate.dao.AggregateAndComputeDao#findOne(java.util.
	 * Map)
	 */
	public String findOne(Map<Object, Object> requestPayload) {
		try {

			Map request = (Map) requestPayload.get(_requestMessage);
			log.debug("[requestPayload]" + request);
			log.debug("[query]" + (Map) request.get(Constants._db_query));
			DBObject dbObjectRes = mongoTemplate.getCollection((String) request.get(_CollectionName))
					.findOne(new BasicDBObject((Map) request.get(Constants._db_query)));
			log.debug("[dbObjectRes]" + dbObjectRes);

			return MessageUtility.response(responseCode.Success, dbObjectRes);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

	public String find(Map<Object, Object> requestPayload) {
		try {

			Map request = (Map) requestPayload.get(_requestMessage);
			log.debug("[requestPayload]" + request);
			log.debug("[query]" + (Map) request.get(Constants._db_query));
			DBCursor dbObjectRes = mongoTemplate.getCollection((String) request.get(_CollectionName))
					.find(new BasicDBObject((Map) request.get(Constants._db_query)),new BasicDBObject((Map) request.get(Constants._db_fields)));
			List<DBObject> ls = new ArrayList<DBObject>();
			for (DBObject dbObject : dbObjectRes) {
				ls.add(dbObject);
			}
			log.debug("[dbObjectRes]" + ls);
		
			DBObject dbObject= new BasicDBObject("ls", ls); 
			log.debug("[dbObject]" + dbObject);

			return MessageUtility.response(responseCode.Success, dbObject);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Failed);
	}

}

package com.os.util;

import java.io.File;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * ColorUtils: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Aug 2, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ColorUtils {

	synchronized static String getRandomColor(){
		   String[] letters = "0123456789ABCDEF".split("");
		    String color = "#";
		    for (int i = 0; i < 6; i++ ) {
		    	double val=Math.random() * 15;
		        color += letters[(int)Math.round(val)];
		    }
		    return color;

		    
	}
	
	public static void main(String[] args ){
	
		System.out.println(">>"+getRandomColor());
	}

	
	
}

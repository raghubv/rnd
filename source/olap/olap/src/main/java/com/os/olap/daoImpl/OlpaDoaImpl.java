package com.os.olap.daoImpl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.os.olap.dao.OlapDao;
import com.os.util.Configuration;
import static com.os.util.Constants.*;
import com.mongodb.BasicDBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * OlpaDoaImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Jan 19, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class OlpaDoaImpl implements OlapDao {
	@Autowired
	MongoTemplate mongoTemplate;
	static final Logger log = Logger.getLogger(OlpaDoaImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.olap.dao.OlapDao#createSystemIndex()
	 */
	public void createSystemIndex() {
		ObjectMapper objectMapper = new ObjectMapper();
		log.debug("[indexKyes]" + Configuration.getIndexKeys());

		Map indexKeysMap = Configuration.getIndexKeys();
		log.debug("[collectionMap]" + indexKeysMap);
		Set set = indexKeysMap.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String collectionNmae = (String) entry.getKey();
			List<Map> values = (List<Map>) entry.getValue();
			for (Map map : values) {
				log.debug("[collectionNmae]" + collectionNmae);
				mongoTemplate.getCollection(collectionNmae).createIndex(
						new BasicDBObject((Map) map.get(_keys)));

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.olap.dao.OlapDao#insertSystemConfigurationData()
	 */
	public void insertSystemConfigurationData() {
		ObjectMapper objectMapper = new ObjectMapper();
		log.debug("[Configuration]" + Configuration.getConfigurationData());

		Map configurationDataMap = Configuration.getConfigurationData();
		log.debug("[configurationDataMap]" + configurationDataMap);
		Set set = configurationDataMap.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String collectionNmae = (String) entry.getKey();
			List<Map> values = (List<Map>) entry.getValue();
			for (Map map : values) {
				log.debug("[collectionNmae]" + collectionNmae);
				if (mongoTemplate.getCollection(collectionNmae).findOne(
						new BasicDBObject(_key, map.get(_key))) == null)
					mongoTemplate.getCollection(collectionNmae).insert(
							new BasicDBObject(map));

			}

		}

	}

}

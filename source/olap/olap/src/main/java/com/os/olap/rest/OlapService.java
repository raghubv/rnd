package com.os.olap.rest;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.os.aggregate.dao.AggregateAndComputeDao;
import com.os.etl.dao.ETLDao;
import com.os.eventMapping.dao.EventMappingDao;
import com.os.olap.dao.OlapDao;
import com.os.util.Constants.responseCode;
import com.os.util.LoggerTrace;
import com.os.util.MessageUtility;
import com.mongodb.util.JSON;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * SnocOlapRS: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 2, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Component
@Path("/json")
public class OlapService {
	static final Logger log = Logger.getLogger(OlapService.class);

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	OlapDao olapDao;

	@Autowired
	EventMappingDao eventMapping;

	@Autowired
	ETLDao etlConfiguration;

	@Autowired
	AggregateAndComputeDao aggregateAndCompute;

	@PostConstruct
	public void createCollectionIndex() {
		olapDao.createSystemIndex();
	}

	@PostConstruct
	public void insertConfigurationData() {
		olapDao.insertSystemConfigurationData();

	}

	@PostConstruct
	public void reInitiateAggregate() {
		aggregateAndCompute.reInitiateAggregate();
	}

	@PostConstruct
	public void reCreateInstanceOfAggregation() {
		aggregateAndCompute.reCreateInstancesOfAggregation();
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeEventService
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	@Path("/event/{method}")
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeEventService(@PathParam("method") String method, @RequestBody String requestBody) {
		log.info("[requestBody]" + requestBody);
		String outPut = null;
		try {
			outPut = (String) eventMapping.getClass().getDeclaredMethod(method, Map.class).invoke(eventMapping,
					(Map) JSON.parse(requestBody));
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(responseCode.Failed);

		}
		return outPut;
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeETLService
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	@Path("/etl/{method}")
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeETLService(@PathParam("method") String method, @RequestBody String requestBody) {
		log.info("[requestBody]" + requestBody);
		String outPut = null;
		try {
			outPut = (String) etlConfiguration.getClass().getDeclaredMethod(method, Map.class).invoke(etlConfiguration,
					(Map) JSON.parse(requestBody));
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(responseCode.Failed);

		}
		return outPut;
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeAggregateService
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	@Path("/aggregate/{method}")
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeAggregateService(@PathParam("method") String method, @RequestBody String requestBody) {
		log.info("[requestBody]" + requestBody);
		String outPut = null;
		try {
			outPut = (String) aggregateAndCompute.getClass().getDeclaredMethod(method, Map.class)
					.invoke(aggregateAndCompute, (Map) JSON.parse(requestBody));
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(responseCode.Failed);

		}
		return outPut;
	}
	/**
	 * 
	 * @param jsName
	 * @return
	 */
	@Path("/js/{jsname}")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeAggregateService(@PathParam("jsname") String jsName) {
		log.info("[jsName]" + jsName);
		String outPut = null;
		try {
			outPut = aggregateAndCompute.getMapData(jsName.replace(".js", ""));
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(responseCode.Failed);

		}
		return outPut;
	}
	/**
	 * 
	 * @param method
	 * @param requestBody
	 * @return
	 */
	@Path("/dbquery/{method}")
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String dbquery(@PathParam("method") String method, @RequestBody String requestBody) {
		log.info("[requestBody]" + requestBody);
		String outPut = null;
		try {
			outPut = (String) aggregateAndCompute.getClass().getDeclaredMethod(method, Map.class)
					.invoke(aggregateAndCompute, (Map) JSON.parse(requestBody));
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(responseCode.Failed);

		}
		return outPut;
	}
	
	
}
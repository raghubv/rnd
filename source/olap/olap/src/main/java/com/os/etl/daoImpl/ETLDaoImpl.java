package com.os.etl.daoImpl;

import static com.os.util.Constants.YYYY_MM_DD_HH_MM_SS_DATE_FORMAT;
import static com.os.util.Constants._ETLProcessDefinition;
import static com.os.util.Constants._ETLSchedule;
import static com.os.util.Constants._endTimeStamp;
import static com.os.util.Constants._etlDBRef;
import static com.os.util.Constants._etlDfnId;
import static com.os.util.Constants._etlLs;
import static com.os.util.Constants._eventName;
import static com.os.util.Constants._id;
import static com.os.util.Constants._idPath;
import static com.os.util.Constants._requestMessage;
import static com.os.util.Constants._scheduleDetails;
import static com.os.util.Constants._scheduled;
import static com.os.util.Constants._scheduledTimeStamp;
import static com.os.util.Constants._startTimeStamp;
import static com.os.util.Constants._status;
import static com.os.util.Constants._status_Active;
import static com.os.util.Constants._type;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.os.etl.dao.ETLDao;
import com.os.util.Constants.responseCode;
import com.os.util.LoggerTrace;
import com.os.util.MessageUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.WriteResult;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * ETLConfigurationImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 2, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ETLDaoImpl implements ETLDao {

	@Autowired
	MongoTemplate mongoTemplate;

	static final Logger log = Logger.getLogger(ETLDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.etl.dao.ETLDao#addETLDefinition(java.util.Map)
	 */
	public String addETLDefinition(Map<Object, Object> requestPayload) {
		boolean isDebugEnabled = log.isDebugEnabled();
		log.info("[Begin]");
		try {
			Map request = (Map) requestPayload.get(_requestMessage);
			request.put(_status, _status_Active);
			if (isDebugEnabled)
				log.debug("[request]" + request);
			WriteResult result = mongoTemplate.getCollection(
					_ETLProcessDefinition).insert(
					new BasicDBObject(request));

			return MessageUtility.response(responseCode.Success);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(responseCode.Add_ETL_Dfn_Failed);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.etl.dao.ETLDao#updateETLDefintiion(java.util.Map)
	 */
	public String updateETLDefintiion(Map<Object, Object> requestPayload) {
		return MessageUtility.response(responseCode.YetToImplement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.etl.dao.ETLDao#deleteETLDefintiion(java.util.Map)
	 */
	public String deleteETLDefintiion(Map<Object, Object> requestPayload) {

		return MessageUtility.response(responseCode.YetToImplement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.etl.dao.ETLDao#scheduleETLProcess(java.util.Map)
	 */
	public String scheduleETLProcess(Map<Object, Object> requestPayload) {
		boolean isDebugEnabled = log.isDebugEnabled();
		try {

			Map request = (Map) requestPayload.get(_requestMessage);
			if (isDebugEnabled)
				log.debug("[request]" + request);
			DBObject etlDfnQry = new BasicDBObject();
			etlDfnQry.put(_eventName,(String) request.get(_eventName));
			if (isDebugEnabled)
				log.debug("[etlDfnQry]" + etlDfnQry);
			DBObject etlDfn = mongoTemplate.getCollection(
					_ETLProcessDefinition).findOne(etlDfnQry,
					new BasicDBObject(_id, 1));
			if (isDebugEnabled)
				log.debug("[etlDfn]" + etlDfn);
			ObjectId objectId = (ObjectId) etlDfn.get(_id);
			DBObject etlSchedule = new BasicDBObject();
			etlSchedule.put(_etlDBRef,
					new DBRef(mongoTemplate.getDb(),
							_ETLProcessDefinition, objectId));
			Map scheduledDetails = (Map) request
					.get(_scheduleDetails);
			Map scheduledDtls = new HashMap();
			List<Map> timeStampLs = (List<Map>) scheduledDetails
					.get(_scheduledTimeStamp);
			List<Map> timeLs = new ArrayList<Map>();
			for (Map map : timeStampLs) {
				Map ins = new HashMap();

				ins.put(_startTimeStamp,
						YYYY_MM_DD_HH_MM_SS_DATE_FORMAT
								.parse((String) map
										.get(_startTimeStamp)));
				ins.put(_endTimeStamp,
						YYYY_MM_DD_HH_MM_SS_DATE_FORMAT
								.parse((String) map
										.get(_endTimeStamp)));
				timeLs.add(ins);

			}
			scheduledDtls.put(_scheduledTimeStamp, timeLs);

			scheduledDtls.put(_scheduled,
					(List<Map>) scheduledDetails.get(_scheduled));

			etlSchedule.put(_scheduleDetails, scheduledDtls);
			etlSchedule.put(_status,_status_Active);
			if (isDebugEnabled)
				log.debug("[etlSchedule]" + etlSchedule);
			mongoTemplate.getCollection(_ETLSchedule).insert(
					etlSchedule);
			return MessageUtility.response(responseCode.Success);
		} catch (ParseException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(responseCode.Scheduled_ETL_Process_Failed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.os.etl.dao.ETLDao#getETLDfnIds(java.util.Map)
	 */
	public String getETLDfnIds(Map<Object, Object> requestPayload) {
		boolean isDebugEnabled = log.isDebugEnabled();
		try {
			String response;
			ObjectMapper objectMapper = new ObjectMapper();

			DBObject keys = new BasicDBObject();
			keys.put(_eventName, 1);
			keys.put(_type, 1);
			keys.put(_id, 1);
			DBObject qry = new BasicDBObject(_status,
					_status_Active);
			DBCursor dbCursor = mongoTemplate.getCollection(
					_ETLProcessDefinition).find(qry, keys);
			List ls = new ArrayList();
			for (DBObject dbObject : dbCursor) {

				Map dbObj = objectMapper.readValue(dbObject.toString(),
						LinkedHashMap.class);

				String id = (String) PropertyUtils.getProperty(dbObj,
						_idPath);
				dbObj.remove(_id);
				dbObj.put(_etlDfnId, id);
				ls.add(dbObj);
			}

			DBObject res = new BasicDBObject(_etlLs, ls);
			return MessageUtility.response(responseCode.Success, res);
		} catch (JsonGenerationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(responseCode.Get_ETL_Dfn_Ids_Failed);
	}

}

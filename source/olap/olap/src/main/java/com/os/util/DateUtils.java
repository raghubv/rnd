package com.os.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * DateUtils: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 9, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class DateUtils {
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. evaluateDate
		* 
		* </pre> 
		*
		* @return Map
	 */
	@Deprecated
	public static Map evaluateDate(Map inputParams, Map dateMap) {

		Set set = dateMap.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String key = (String) entry.getKey();
			Map inputMap = (Map) entry.getValue();
			inputParams.put(key, DateUtils.getDate(inputMap));
		}
		return inputParams;
	}
	
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. evaluateDate
		* 
		* </pre> 
		*
		* @return Map
	 */
	public static Map evaluateDate( Map dateConfigParam) {
		Map dateParams=new HashMap();
		Set set = dateConfigParam.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String key = (String) entry.getKey();
			Map inputMap = (Map) entry.getValue();
			dateParams.put(key, DateUtils.getDate(inputMap));
		}
		return dateParams;
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. evaluateDate
		* 
		* </pre> 
		*
		* @return Map
	 */
	public static Map evaluateDate(String prefix, Map dateConfigParam) {
		Map dateParams=new HashMap();
		Set set = dateConfigParam.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String key = (String) entry.getKey();
			Map inputMap = (Map) entry.getValue();
			dateParams.put(prefix+"."+key, DateUtils.getDate(inputMap));
		}
		return dateParams;
	}
	
	
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getCalendar
		* 
		* </pre> 
		*
		* @return Calendar
	 */
	public static Calendar getCalendar(){
		Calendar calendar = Calendar.getInstance();
		return calendar;
		
	}
	
	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getDate
	 * 
	 * </pre>
	 *
	 * @return Date
	 */
	public static Date getDate(Map inputMap) {

		Calendar calendar =getCalendar();
		if (((String) inputMap.get("dayOfMonth")).equalsIgnoreCase("min"))
			calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance()
					.getActualMinimum(Calendar.DAY_OF_MONTH));
		if (((String) inputMap.get("dayOfMonth")).equalsIgnoreCase("max"))
			calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance()
					.getActualMaximum(Calendar.DAY_OF_MONTH));

		calendar.add(Calendar.MONTH,
				Integer.valueOf((String) inputMap.get("month")));
		calendar.add(Calendar.DATE,
				Integer.valueOf((String) inputMap.get("day")));
		calendar.set(Calendar.HOUR_OF_DAY,
				Integer.valueOf((String) inputMap.get("hour")));
		calendar.set(Calendar.MINUTE,
				Integer.valueOf((String) inputMap.get("minute")));
		calendar.set(Calendar.SECOND,
				Integer.valueOf((String) inputMap.get("second")));

		System.out.println("[>>]" + calendar.getTime());

		return calendar.getTime();

	}

	static public void main(String[] args) {

		Map input = new HashMap();
		input.put("month", "-1");
		input.put("day", "0");
		input.put("hour", "0");
		input.put("minute", "0");
		input.put("second", "0");
		input.put("second", "0");
		input.put("dayOfMonth", "min");

		System.out.println(getDate(input));

		input.put("month", "0");
		input.put("day", "-1");
		input.put("hour", "0");
		input.put("minute", "0");
		input.put("second", "0");
		input.put("dayOfMonth", "none");

		System.out.println(getDate(input));

		System.out.println(">>"+getCalendar().get(Calendar.YEAR));
		System.out.println(">>"+getCalendar().get(Calendar.MONTH)+1);
		System.out.println(">>"+getCalendar().get(Calendar.HOUR_OF_DAY));
		System.out.println(">>"+getCalendar().getTime());
		System.out.println(">>"+getCalendar().get(Calendar.DAY_OF_MONTH));
		
	}

}

package com.os.aggregate.dao;

import java.util.Map;

public interface AggregateAndComputeDao {

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. aggregateAndCompute
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	public String aggregateAndCompute(Map<Object, Object> requestPayload);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. addAggregateAndComputeFormula
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	public String addAggregateAndComputeFormula(
			Map<Object, Object> requestPayload);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. scheduleAggregate
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	public String scheduleAggregate(Map<Object, Object> requestPayload);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. reInitiateAggregate
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	public String reInitiateAggregate();

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. reCreateInstanceOfAggregation
	 * 
	 * </pre>
	 *
	 * @return String
	 */
	public String reCreateInstancesOfAggregation();

	
/**
 * 
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. getAggregatedData
	* 
	* </pre> 
	*
	* @return String
 */
	public String getAggregatedData(Map<Object, Object> requestPayload);
	/**
	 * 
	 * @param jsName
	 * @return
	 */
	public String getMapData(String jsName);
	
	/**
	 * 
	 * @param requestPayload
	 * @return
	 */
	public String findOne(Map<Object, Object> requestPayload);
}

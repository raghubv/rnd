package com.os.aggregate.scheduler;

import static com.os.util.Constants.YYYYMMdd_DATE_FORMAT;
import static com.os.util.Constants._$gte;
import static com.os.util.Constants._$lte;
import static com.os.util.Constants._$or;
import static com.os.util.Constants._Agg_Status_Active;
import static com.os.util.Constants._Agg_Status_ERROR;
import static com.os.util.Constants._Agg_Status_Inprogress;
import static com.os.util.Constants._AggregateAndComputeDfn;
import static com.os.util.Constants._AggregateAndComputeInstances;
import static com.os.util.Constants._AggregateSchedule;
import static com.os.util.Constants._AggregatedAndComputedDate;
import static com.os.util.Constants._ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path;
import static com.os.util.Constants._ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Days_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Hours_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Months_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Years_Path;
import static com.os.util.Constants._addToParams;
import static com.os.util.Constants._addToResult;
import static com.os.util.Constants._aggregateAndCompute;
import static com.os.util.Constants._aggregatedAndComputedDateDay;
import static com.os.util.Constants._aggregatedAndComputedDateMap;
import static com.os.util.Constants._aggregatedAndComputedDateMonth;
import static com.os.util.Constants._aggregatedAndComputedDateYear;
import static com.os.util.Constants._aggregationName;
import static com.os.util.Constants._aggregations;
import static com.os.util.Constants._collections;
import static com.os.util.Constants._compute;
import static com.os.util.Constants._data;
import static com.os.util.Constants._dataRows_datePath;
import static com.os.util.Constants._dateParam;
import static com.os.util.Constants._dateParams;
import static com.os.util.Constants._dbParam;
import static com.os.util.Constants._dbParams;
import static com.os.util.Constants._execute;
import static com.os.util.Constants._executeType;
import static com.os.util.Constants._expressionName;
import static com.os.util.Constants._expressions;
import static com.os.util.Constants._fieldName;
import static com.os.util.Constants._fieldValue;
import static com.os.util.Constants._formulaDfn;
import static com.os.util.Constants._formulaId;
import static com.os.util.Constants._funcationArgs;
import static com.os.util.Constants._funcationName;
import static com.os.util.Constants._groupBy;
import static com.os.util.Constants._id;
import static com.os.util.Constants._inCollection;
import static com.os.util.Constants._jsFunction;
import static com.os.util.Constants._jsFunctions;
import static com.os.util.Constants._mappingDBRef;
import static com.os.util.Constants._outCollections;
import static com.os.util.Constants._pipeline;
import static com.os.util.Constants._queryParams;
import static com.os.util.Constants._result;
import static com.os.util.Constants._status;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.os.util.Constants.responseCode;
import com.os.util.DBUtils;
import com.os.util.DateUtils;
import com.os.util.EvaluateExpression;
import com.os.util.LoggerTrace;
import com.os.util.MessageUtility;
import com.os.util.QueryUtils;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;


public class AggregateAndComputeExecutor  {

	private static final Logger log = Logger
			.getLogger(AggregateAndComputeExecutor.class);

	static private enum executeType {
		dateParams, dbParams, collectionParams, jsExpressions, jsFunctions, aggregations
	};

	static int counter = 0;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor aggregateTaskExecutor;

	private Integer  limit;
	

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public void execute() {
		boolean isDebugEnabled = log.isDebugEnabled();

		try {
			int count = limit - counter;
			if (counter >= 0 && count>0 && counter<= limit) {
				Calendar calendar = DateUtils.getCalendar();
				DBObject aggregateSchduleQry = new BasicDBObject();

				List<DBObject> orConditList = new ArrayList<DBObject>();
				DBObject scheduledTimeStamp = new BasicDBObject();

				scheduledTimeStamp
						.put(_ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path,
								new BasicDBObject(_$lte, calendar.getTime()));
				scheduledTimeStamp.put(
						_ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path,
						new BasicDBObject(_$gte, calendar.getTime()));
				scheduledTimeStamp.put(_status, _Agg_Status_Active);

				orConditList.add(scheduledTimeStamp);

				DBObject scheduled = new BasicDBObject();
				scheduled.put(_ScheduleDetails_Scheduled_Years_Path,
						calendar.get(Calendar.YEAR));
				scheduled.put(_ScheduleDetails_Scheduled_Months_Path,
						calendar.get(Calendar.MONTH) + 1);
				scheduled.put(_ScheduleDetails_Scheduled_Days_Path,
						calendar.get(Calendar.DAY_OF_MONTH));
				scheduled.put(_ScheduleDetails_Scheduled_Hours_Path,
						calendar.get(Calendar.HOUR_OF_DAY));
				scheduled.put(_status, _Agg_Status_Active);
				orConditList.add(scheduled);
				aggregateSchduleQry.put(_$or, orConditList);

				log.debug("[aggregateSchduleQry]" + aggregateSchduleQry);
				DBCursor dbCursor = mongoTemplate.getCollection(
						_AggregateSchedule).find(aggregateSchduleQry,
						new BasicDBObject(_mappingDBRef, 1));
				for (DBObject dbObject : dbCursor) {
					log.debug("[dbObject]" + dbObject);
					// Query aggregate and computation instances
					DBObject aggAndComputeInstancesQry = new BasicDBObject();
					aggAndComputeInstancesQry.put(_status, _Agg_Status_Active);
					aggAndComputeInstancesQry.put(_mappingDBRef,
							dbObject.get(_mappingDBRef));
					if (isDebugEnabled)
						log.debug("[aggAndComputeInstancesQry]"
								+ aggAndComputeInstancesQry);

					DBCursor aggAndComputeInstancesCursor = mongoTemplate
							.getCollection(_AggregateAndComputeInstances)
							.find(aggAndComputeInstancesQry).limit(count--);

					for (DBObject aggregateAndComputeInstance : aggAndComputeInstancesCursor) {
						if (isDebugEnabled)
							log.debug("[aggregateAndComputeInstance]"
									+ aggregateAndComputeInstance);

						aggregateAndComputeInstance.put(_status,
								_Agg_Status_Inprogress);
						mongoTemplate.getCollection(
								_AggregateAndComputeInstances).update(
								new BasicDBObject(_id,
										(ObjectId) aggregateAndComputeInstance
												.get(_id)),
								aggregateAndComputeInstance);

						aggregateTaskExecutor
								.execute(new AggregateAndComputeProcessor(
										aggregateAndComputeInstance,
										mongoTemplate));
						counter++;

					}
				}
			}else
				counter=0;
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
	}
	/**
	 * 
	 * 
	 * <b>Purpose:</b><br>
	 * AggregateAndComputeProcessor: <br>
	 * <br>
	 * 
	 * <b>DesignReference:</b><br>
	 * <br>
	 * <br>
	 * 
	 * <b>CopyRights:</b><br>
	 * os 2013<br>
	 * <br>
	 * 
	 * <b>RevisionHistory:</b>
	 * 
	 * <pre>
	 * <b>
	 * Sl No   Modified Date        Author</b>
	 * ==============================================
	 * 1       Jun 19, 2015		   @author Raghu   -- Base Release
	 * 
	 * </pre>
	 * 
	 * <br>
	 */
	private class AggregateAndComputeProcessor implements Runnable {
		private Logger log = Logger
				.getLogger(AggregateAndComputeProcessor.class);
		boolean isDebugEnabled = log.isDebugEnabled();
		private DBObject aggregateAndComputeInstance = null;
		private MongoTemplate mongoTemplate = null;

		public void run() {
			if (isDebugEnabled)
				log.debug("[aggregateAndComputeInstance]"
						+ aggregateAndComputeInstance);

			/** Update initiated status **/
			DBRef eventMappingDbRef = (DBRef) aggregateAndComputeInstance
					.get(_mappingDBRef);
			DBObject eventMappingDfnQry = new BasicDBObject(_id,
					eventMappingDbRef.getId());
			if (isDebugEnabled)
				log.debug("[eventMappingDfnQry]" + eventMappingDfnQry);
			DBObject eventMappingDfn = mongoTemplate.getCollection(
					eventMappingDbRef.getRef()).findOne(eventMappingDfnQry);
			if (isDebugEnabled)
				log.debug("[eventMappingDfn]" + eventMappingDfn);
			List<DBObject> aggregateAndComputeLs = (List<DBObject>) eventMappingDfn
					.get(_aggregateAndCompute);
			Map aggAndComputeReq = new HashMap();
			Map groupByQueryParams = (Map) aggregateAndComputeInstance
					.get(_queryParams);
			aggAndComputeReq.put(_groupBy, groupByQueryParams);
			for (DBObject aggregateAndCompute : aggregateAndComputeLs) {
				String formulaId = (String) aggregateAndCompute.get(_formulaId);
				aggAndComputeReq.put(_compute, formulaId);
				if (isDebugEnabled)
					log.debug("[aggAndComputeReq]" + aggAndComputeReq);
				DBObject aggAndComputeRes = (DBObject) JSON
						.parse(aggregateAndCompute(aggAndComputeReq));

				if (isDebugEnabled)
					log.debug("[aggAndComputeRes]" + aggAndComputeRes);

			}
			mongoTemplate.getCollection(_AggregateAndComputeInstances).remove(
					new BasicDBObject(_id,
							(ObjectId) aggregateAndComputeInstance.get(_id)));

			counter--;

		}

		/**
		 * 
		 * @param aggregateAndComputeInstance
		 * @param mongoTemplate
		 * @param aggregateAndComputeDao
		 */
		public AggregateAndComputeProcessor(
				DBObject aggregateAndComputeInstance,
				MongoTemplate mongoTemplate) {
			super();
			this.aggregateAndComputeInstance = aggregateAndComputeInstance;
			this.mongoTemplate = mongoTemplate;

		}

		/**
		 * 
		 * 
		 * <b>Algorithm:</b>
		 * 
		 * <pre>
		 * 1. aggregateAndCompute
		 * 
		 * </pre>
		 *
		 * @return String
		 */
		public String aggregateAndCompute(Map<Object, Object> requestPayload) {
			try {
				boolean isDebugEnabled = log.isDebugEnabled();
				if (isDebugEnabled)
					log.debug("[requestPayload]" + requestPayload);
				DBObject result = new BasicDBObject();
				Map aggregateParams = new LinkedHashMap();
				ObjectMapper objectMapper = new ObjectMapper();
				Map todayDateMap = new HashMap();
				Date toDayDate = DateUtils.getCalendar().getTime();
				String todayStr = YYYYMMdd_DATE_FORMAT.format(toDayDate);
				todayDateMap.put(_aggregatedAndComputedDateDay,
						(todayStr.substring(6)));
				todayDateMap.put(_aggregatedAndComputedDateMonth,
						(todayStr.substring(4, 6)));
				todayDateMap.put(_aggregatedAndComputedDateYear,
						(todayStr.substring(0, 4)));
				Integer today = Integer.valueOf(todayStr);

				String compute = (String) requestPayload.get(_compute);
				aggregateParams.putAll((Map) requestPayload.get(_groupBy));
				if (isDebugEnabled)
					log.debug("[compute]" + compute);
				if (isDebugEnabled)
					log.debug("[inputParams]" + aggregateParams);

				DBObject formulaQuery = new BasicDBObject();
				formulaQuery.put(_formulaId, compute);
				DBObject fromulaDfn = mongoTemplate.getCollection(
						_AggregateAndComputeDfn).findOne(formulaQuery);
				Map formulaDfn = (Map) JSON.parse((String) fromulaDfn
						.get(_formulaDfn));
				if (isDebugEnabled)
					log.debug("[formulaDfn]" + formulaDfn);
				List<DBObject> executeLs = (List<DBObject>) formulaDfn
						.get(_execute);
				for (DBObject execute : executeLs) {
					Map outputMap = new HashMap();

					boolean isAddToResult = false;
					boolean isAddToParams = false;

					isAddToResult = Boolean.valueOf((String) execute
							.get(_addToResult));
					isAddToParams = Boolean.valueOf((String) execute
							.get(_addToParams));

					switch (executeType.valueOf((String) execute
							.get(_executeType))) {

					case dateParams:
						outputMap.putAll(DateUtils.evaluateDate(_dateParam,
								(Map) execute.get(_dateParams)));
						break;
					case dbParams:
						DBObject dbParams = (DBObject) execute.get(_dbParams);
						outputMap.putAll(DBUtils.getDBParams(_dbParam,
								mongoTemplate, dbParams.toMap()));
						break;
					case collectionParams:
						List<String> collections = (List) execute
								.get(_collections);
						for (String collection : collections) {
							DBObject dbQry = new BasicDBObject(_groupBy,
									requestPayload.get(_groupBy));
							dbQry.put(_dataRows_datePath, today);
							if (isDebugEnabled)
								log.debug("[collection]" + collection);
							if (isDebugEnabled)
								log.debug("[dbQry]" + dbQry);
							DBObject dbdoc = mongoTemplate.getCollection(
									collection).findOne(dbQry);
							if (isDebugEnabled)
								log.debug("[dbdoc]" + dbdoc);
							if (dbdoc != null) {
								Set set = ((Map) dbdoc.get(_data)).entrySet();
								for (Object object : set) {
									outputMap.put(collection + "."
											+ ((Map.Entry) object).getKey(),
											((Map.Entry) object).getValue());
								}
							}
						}

						break;
					case jsExpressions: {
						List<DBObject> expressions = (List<DBObject>) execute
								.get(_expressions);

						for (DBObject expression : expressions) {
							outputMap.put(
									(String) expression.get(_expressionName)
											+ "."
											+ (String) expression
													.get(_fieldName),
									EvaluateExpression.evaluateExpression(
											(String) expression
													.get(_fieldValue),
											aggregateParams));
						}

					}

						break;

					case jsFunctions: {
						List<DBObject> jsFunctions = (List<DBObject>) execute
								.get(_jsFunctions);
						for (DBObject jsFunction : jsFunctions) {
							log.debug("Script[fieldName,scritptReturnValue]"
									+ (String) jsFunction.get(_funcationName)
									+ "."
									+ (String) jsFunction.get(_fieldName)
									+ ","
									+ EvaluateExpression.evaluateJSFunction(
											(String) jsFunction
													.get(_jsFunction),
											(String) jsFunction
													.get(_funcationName),
											(String) jsFunction
													.get(_funcationArgs),
											aggregateParams).toString());

							outputMap.put(
									(String) jsFunction.get(_funcationName)
											+ "."
											+ (String) jsFunction
													.get(_fieldName),
									EvaluateExpression.evaluateJSFunction(
											(String) jsFunction
													.get(_jsFunction),
											(String) jsFunction
													.get(_funcationName),
											(String) jsFunction
													.get(_funcationArgs),
											aggregateParams));

						}
					}
						break;

					case aggregations: {

						List<DBObject> aggregations = (List<DBObject>) execute
								.get(_aggregations);
						for (DBObject aggregation : aggregations) {

							String inCollection = (String) aggregation
									.get(_inCollection);
							List aggregatePipeline = (List) QueryUtils
									.prepareQuery(aggregation.toMap(),
											aggregateParams).get(_pipeline);
							if (isDebugEnabled)
								log.debug("[aggregatePipeline]"
										+ aggregatePipeline);
							log.debug("[inCollection]" + inCollection);
							log.debug("[command]"
									+ mongoTemplate.getCollection(inCollection)
											.aggregate(aggregatePipeline)
											.getCommand());
							AggregationOutput aggregateOutput = mongoTemplate
									.getCollection(inCollection).aggregate(
											aggregatePipeline);
							Map aggregateAndComputeMap = aggregateOutput
									.getCommandResult().toMap();
							if (isDebugEnabled)
								log.debug("[aggregateAndComputeMap]"
										+ aggregateAndComputeMap);
							if (!((List) aggregateAndComputeMap.get(_result))
									.isEmpty())
								outputMap.putAll(DBUtils.addPrefix(
										(String) aggregation
												.get(_aggregationName),
										(Map) ((List) aggregateAndComputeMap
												.get(_result)).get(0)));

						}
					}

						break;
					default:
						break;
					}

					log.debug("[executeType,addToParams,addToResult]"
							+ executeType.valueOf((String) execute
									.get(_executeType)) + "," + isAddToParams
							+ "," + isAddToResult);
					log.debug("[execute]" + execute);
					if (isAddToParams)
						aggregateParams.putAll(outputMap);
					if (isAddToResult)
						result.putAll(outputMap);

					log.debug("[outputMap]" + outputMap);
					log.debug("[aggregateParams]" + aggregateParams);
					log.debug("[result]" + result);

				}
				log.debug("[aggregateParams]" + aggregateParams);
				log.debug("[result]" + result);
				Set set = DBUtils.outPutMapping(result.toMap(),
						(List<Map>) formulaDfn.get(_outCollections)).entrySet();
				for (Object object : set) {
					Map.Entry mapEntry = (Map.Entry) object;
					log.debug("[key]" + mapEntry.getKey());
					log.debug("[value]" + mapEntry.getValue());
					result.removeField(_id);
					DBObject resultItem = new BasicDBObject();
					DBObject dbQuery = new BasicDBObject();
					dbQuery.putAll((Map) requestPayload.get(_groupBy));
					dbQuery.put(_AggregatedAndComputedDate, today);
					resultItem.put(_id, dbQuery);

					resultItem.put(_data, (Map) mapEntry.getValue());
					resultItem.put(_aggregatedAndComputedDateMap, todayDateMap);
					mongoTemplate.getCollection((String) mapEntry.getKey())
							.update(new BasicDBObject(_id, dbQuery),
									resultItem, true, false);
				}

				return MessageUtility.response(responseCode.Success,
						result.toMap());
			} catch (Exception e) {
				/** Update initiated status **/
				aggregateAndComputeInstance.put(_status, _Agg_Status_ERROR);
				mongoTemplate
						.getCollection(_AggregateAndComputeInstances)
						.update(new BasicDBObject(_id,
								(ObjectId) aggregateAndComputeInstance.get(_id)),
								aggregateAndComputeInstance);

				log.error("[Error]" + LoggerTrace.getStackTrace(e));
				counter--;
			}
			return MessageUtility.response(responseCode.Failed);
		}

	}
}

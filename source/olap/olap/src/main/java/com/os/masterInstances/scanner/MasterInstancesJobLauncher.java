package com.os.masterInstances.scanner;

import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * MasterTemplateJobLauncher: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Jun 4, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class MasterInstancesJobLauncher extends QuartzJobBean {

	private static final Logger log = Logger
			.getLogger(MasterInstancesJobLauncher.class);

	

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. execute
	 * 
	 * </pre>
	 *
	 * @return void
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org
	 * .quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws org.quartz.JobExecutionException {
		Map<String, Object> jobDataMap = context.getMergedJobDataMap();
//		((InstanceInitiatorHandler) jobDataMap
//				.get(_instanceInitiatorHandlerHandlerRef)).execute();
		log.debug(">>>>>>>>>>>here raghu");

	}

}

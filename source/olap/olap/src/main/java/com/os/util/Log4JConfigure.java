package com.os.util;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Log4JConfigure: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Aug 11, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Log4JConfigure {
	
	
	public Log4JConfigure(String filePath) 
	{
		org.apache.log4j.PropertyConfigurator.configureAndWatch(filePath, 6000);
	}

}

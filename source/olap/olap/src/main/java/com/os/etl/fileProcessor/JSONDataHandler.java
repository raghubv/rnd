package com.os.etl.fileProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import static com.os.util.Constants.*;
import com.os.util.ETLUtils;
import com.os.util.FileUtils;
import com.os.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

public class JSONDataHandler {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor etlTaskExecutor;

	private static List<String> processingFileLs = new ArrayList<String>();

	static synchronized boolean addToProcessingThread(String fileName) {
		if (processingFileLs.contains(fileName))
			return false;
		processingFileLs.add(fileName);
		return true;
	}

	static synchronized void removeFromProcessingThread(String fileName) {
		processingFileLs.remove(fileName);
	}

	static final Logger log = Logger.getLogger(JSONDataHandler.class);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. process
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void process(Map<Object, Object> inputMap) {
		boolean isDebugEnabled = log.isDebugEnabled();

		try {
			if (isDebugEnabled)
				log.debug("[map]" + inputMap);
			List<Map> dirs;
			dirs = (List<Map>) PropertyUtils.getProperty(inputMap,
					scanDirectory_path);

			DBObject mappingQuery = new BasicDBObject();
			mappingQuery.put(_eventName,
					inputMap.get(_eventName));
			if (isDebugEnabled)
				log.debug("[mappingQuery]" + mappingQuery);
			DBObject eventMapping = mongoTemplate.getCollection(
					_EventMappingDfn).findOne(mappingQuery);
			if (isDebugEnabled)
				log.debug("[dirs]" + dirs);
			for (Map path : dirs) {
				File[] files = FileUtils.listf((String) path
						.get(_from));
				for (File file : files) {
					if (isDebugEnabled)
						log.debug("[Active thread count]"
								+ etlTaskExecutor.getActiveCount());
					
					if(addToProcessingThread(file.getName()))
					etlTaskExecutor.execute(new JSONProcessor(inputMap,
							eventMapping, path, file));
				}

			}
		} catch (IllegalAccessException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

	}

	private class JSONProcessor implements Runnable {
		private Logger log = Logger.getLogger(JSONProcessor.class);
		private boolean isDebugEnabled = log.isDebugEnabled();
		private Map<Object, Object> inputMap;
		private DBObject eventMapping;
		private Map path;
		private File file;

		public JSONProcessor(Map<Object, Object> inputMap,
				DBObject eventMapping, Map path, File file) {
			super();
			this.inputMap = inputMap;
			this.eventMapping = eventMapping;
			this.path = path;
			this.file = file;
		}

		public void run() {

			try {

				// ETL Job Instance from ETL Definiton
				DBObject etlInstance = new BasicDBObject();
				etlInstance.put(_etlStatus,_etlStatus_Inprogress);
				etlInstance.put(_etlDBRef,
						new DBRef(mongoTemplate.getDb(),_ETLProcessDefinition, inputMap
										.get(_id)));
				etlInstance.put(_from, file.getAbsolutePath());
				if (isDebugEnabled)
					log.debug("[etlInstance]" + etlInstance);
				etlInstance.put(_mappingDBRef, new DBRef(
						mongoTemplate.getDb(), _EventMappingDfn,
						eventMapping.get(_id)));
				mongoTemplate.getCollection(_ETLJobInstances).insert(
						etlInstance);
				DBRef etlJobInsDBRef = new DBRef(mongoTemplate.getDb(),_ETLJobInstances,
						etlInstance.get(_id));
				if (isDebugEnabled)
					log.debug("[file]" + file.getAbsolutePath());
				String collectionName = (String) PropertyUtils.getProperty(
						inputMap, _savetocollection_path);
				List<Map> ls = new ArrayList<Map>();

				/** JSON File Processor-Start **/
				FileInputStream fis = new FileInputStream(file);
				ObjectMapper objmapper = new ObjectMapper();
				Map map = objmapper.readValue(fis, Map.class);
				List<Map> list = (List<Map>) map.get(_rows);
				for (Map object : list) {
					Map dbMap = ETLUtils.convertToMappingType(
							(Map) PropertyUtils.getProperty(inputMap,
									_typeMapping_path),

							object);
					ls.add(dbMap);
				}

				/** JSON File Processor-End **/
				for (Map mapObj : ls) {
					// ETL JOB Instance Ref
					mapObj.put(_etlJobInsDBRef, etlJobInsDBRef);
					DBObject dbCollectionObj = new BasicDBObject(mapObj);
					mongoTemplate.getCollection(collectionName).insert(
							dbCollectionObj);

				}

				org.apache.commons.io.FileUtils.copyFile(
						file.getAbsoluteFile(),
						new File((String) path.get(_to)
								+ file.getName()));
				org.apache.commons.io.FileUtils.deleteQuietly(file
						.getAbsoluteFile());

				/* Status Update Back */
				etlInstance.put(_to, (String) path.get(_to)
						+ file.getName());
				etlInstance.put(_etlStatus,_etlStatus_Completed);
				etlInstance.put(_aggAndCompInstStatus,_aggAndCompInstStatus_Active);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id,
								etlInstance.get(_id)), etlInstance);
				/* Status Update Back */
				removeFromProcessingThread(file.getName());

			} catch (IllegalAccessException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (InvocationTargetException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (NoSuchMethodException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (FileNotFoundException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (IOException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			}

		}

	}

}

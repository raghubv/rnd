package com.os.etl.fileProcessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import static com.os.util.Constants.*;

import com.os.util.DBUtils;
import com.os.util.ETLUtils;
import com.os.util.FileUtils;
import com.os.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * CSVDataHandler: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 3, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class CSVDataHandler {
	private static final Logger log = Logger.getLogger(CSVDataHandler.class);
	
    
	private static List<String> processingFileLs=new ArrayList<String>();
	
	static synchronized boolean addToProcessingThread(String fileName){
		if(processingFileLs.contains(fileName))
			return false;
		processingFileLs.add(fileName);
		return true;
	}
	
	
	static synchronized void removeFromProcessingThread(String fileName){
		processingFileLs.remove(fileName);
	}
	
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor etlTaskExecutor;

	public void process(Map<Object, Object> inputMap) {
		boolean isDebugEnabled = log.isDebugEnabled();

		try {
			if (isDebugEnabled)
				log.debug("[map]" + inputMap);
			List<Map> dirs;
			dirs = (List<Map>) PropertyUtils.getProperty(inputMap,
					scanDirectory_path);
			DBObject mappingQuery = new BasicDBObject();
			mappingQuery.put(_eventName,
					inputMap.get(_eventName));
			if (isDebugEnabled)
				log.debug("[mappingQuery]" + mappingQuery);
			DBObject eventMapping = mongoTemplate.getCollection(
					_EventMappingDfn).findOne(mappingQuery);
			if (isDebugEnabled)
				log.debug("[dirs]" + dirs);
			for (Map path : dirs) {
				File[] files = FileUtils.listf((String) path
						.get(_from));
				for (File file : files) {
					if (isDebugEnabled)
						log.debug("[Active thread count]"
								+ etlTaskExecutor.getActiveCount());
					if(addToProcessingThread(file.getName()))
					etlTaskExecutor.execute(new CSVProcessor(inputMap,
							eventMapping, path, file));
				}

			}
		} catch (IllegalAccessException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

	}

	private class CSVProcessor implements Runnable {

		private Logger log = Logger.getLogger(CSVProcessor.class);
		private boolean isDebugEnabled = log.isDebugEnabled();
		private Map<Object, Object> inputMap;
		private DBObject eventMapping;
		private Map path;
		private File file;

		public CSVProcessor(Map<Object, Object> inputMap,
				DBObject eventMapping, Map path, File file) {
			super();
			this.inputMap = inputMap;
			this.eventMapping = eventMapping;
			this.path = path;
			this.file = file;
		}

		public void run() {

			try {
				// ETL Job Instance from ETL Definiton
				DBObject etlInstance = new BasicDBObject();
				etlInstance.put(_etlStatus,
						_etlStatus_Inprogress);
				etlInstance.put(_etlDBRef,
						new DBRef(mongoTemplate.getDb(),_ETLProcessDefinition, inputMap
										.get(_id)));
				etlInstance.put(_from, file.getAbsolutePath());
				int refNo=DBUtils.getCounter(mongoTemplate, 101);
				etlInstance.put(_etlRefNo,refNo);
				
				if (isDebugEnabled)
					log.debug("[etlInstance]" + etlInstance);
				etlInstance.put(_mappingDBRef, new DBRef(
						mongoTemplate.getDb(), _EventMappingDfn,
						eventMapping.get(_id)));
				mongoTemplate.getCollection(_ETLJobInstances).insert(
						etlInstance);
				DBRef etlJobInsDBRef = new DBRef(mongoTemplate.getDb(),
						_ETLJobInstances,
						etlInstance.get(_id));
				// ETL Job Instance
				if (isDebugEnabled)
					log.debug("[file]" + file.getAbsolutePath());
				String collectionName = (String) PropertyUtils.getProperty(
						inputMap, _savetocollection_path);
				List<Map> ls = new ArrayList<Map>();

				/** CSV File Processor-Start **/
				FileReader fr = new FileReader(file.getAbsolutePath());
				BufferedReader br = new BufferedReader(fr);
				String line = null;
				String[] headers = br.readLine().split(",");

				while ((line = br.readLine()) != null) {
					// use comma as separator
					String[] fieldValues = line.split(",");
					Map row = new HashMap();
					int i = 0;
					for (String fieldName : headers) {
						row.put(fieldName, fieldValues[i]);
						i++;
					}
					Map rowData = ETLUtils.convertToMappingTypeAndField(
							(Map) PropertyUtils.getProperty(inputMap,
									_typeMapping_path),

							(Map) PropertyUtils.getProperty(inputMap,_fieldMapping_path),

							row);
					ls.add(rowData);

				}
				fr.close();

				br.close();
				/** CSV File Processor-End **/
				for (Map map : ls) {
					// ETL JOB Instance Ref
					//map.put(_etlJobInsDBRef, etlJobInsDBRef);
					map.put(_etlRefNo,refNo);
					DBObject dbCollectionObj = new BasicDBObject(map);
					mongoTemplate.getCollection(collectionName).insert(
							dbCollectionObj);

				}

				org.apache.commons.io.FileUtils.copyFile(
						file.getAbsoluteFile(),
						new File((String) path.get(_to)
								+ file.getName()));
				org.apache.commons.io.FileUtils.deleteQuietly(file
						.getAbsoluteFile());

				/* Status Update Back */
				etlInstance.put(_to, (String) path.get(_to)
						+ file.getName());
				etlInstance.put(_etlStatus,_etlStatus_Completed);
				etlInstance.put(_aggAndCompInstStatus,_aggAndCompInstStatus_Active);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id,
								etlInstance.get(_id)), etlInstance);
				/* Status Update Back */

				removeFromProcessingThread(file.getName());
			} catch (IllegalAccessException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (InvocationTargetException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (NoSuchMethodException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (FileNotFoundException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (IOException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			}

		}

	}

}

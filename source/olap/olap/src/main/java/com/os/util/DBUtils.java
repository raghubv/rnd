package com.os.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * DBUtils: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 22, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class DBUtils {

	static final Logger log = Logger.getLogger(DBUtils.class);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getDBRefObject
	 * 
	 * </pre>
	 *
	 * @return DBObject
	 */
	public static DBObject getDBRefObject(MongoTemplate mongoTemplate,
			DBRef dbRef) {

		return mongoTemplate.getCollection(dbRef.getRef()).findOne(
				new BasicDBObject(Constants._id, dbRef.getId()));
	}

	public static int getCounter(MongoTemplate mongoTemplate, int key) {
		DBObject updateDBObject = new BasicDBObject();
		updateDBObject.put(Constants._$inc, new BasicDBObject(Constants._value,
				1));
		DBObject dbObject = mongoTemplate
				.getCollection(Constants._Conf_Counter).findAndModify(
						new BasicDBObject(Constants._key, key), updateDBObject);
		return (Integer) dbObject.get(Constants._value);

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getDBParams
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	public synchronized static Map getDBParams(MongoTemplate mongoTemplate,
			Map globalParms) {
		boolean isDebugEnabled = log.isDebugEnabled();
		if (isDebugEnabled)
			log.debug("[globalParms]" + globalParms);
		Map dbParams = new HashMap();
		Set set = globalParms.entrySet();
		for (Object object : set) {
			Map.Entry mapEntry = (Map.Entry) object;
			dbParams.put(mapEntry.getKey(),
					getDBParam(mongoTemplate, (Map) mapEntry.getValue()));
		}
		log.debug("[outPut]" + dbParams);
		return dbParams;

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getDBParams
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	public synchronized static Map getDBParams(String prefix,
			MongoTemplate mongoTemplate, Map globalParms) {
		boolean isDebugEnabled = log.isDebugEnabled();
		if (isDebugEnabled)
			log.debug("[globalParms]" + globalParms);
		Map dbParams = new HashMap();
		Set set = globalParms.entrySet();
		for (Object object : set) {
			Map.Entry mapEntry = (Map.Entry) object;
			dbParams.put(prefix + "." + mapEntry.getKey(),
					getDBParam(mongoTemplate, (Map) mapEntry.getValue()));
		}
		log.debug("[outPut]" + dbParams);
		return dbParams;

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. addPrefix
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	public synchronized static Map addPrefix(String prefix, Map inpuMap) {
		boolean isDebugEnabled = log.isDebugEnabled();
		log.debug("[prefix]"+prefix);
		log.debug("[inpuMap]"+inpuMap);
		Map outPut = new HashMap();
		Set set = inpuMap.entrySet();
		for (Object element : set) {
			Map.Entry mapEntry = (Map.Entry) element;
			outPut.put(prefix + "." + mapEntry.getKey(), mapEntry.getValue());
		}
		log.debug("[outPut]"+outPut);
		return outPut;

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. outPutMapping
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	public synchronized static Map outPutMapping(Map resultMap,
			List<Map> outCollections) {
		boolean isDebugEnabled = log.isDebugEnabled();
		log.debug("[resultMap]"+resultMap);
		log.debug("[outCollections]"+outCollections);
		Map outPut = new HashMap();
		for (Map outCollection : outCollections) {

			String collectionName = (String) outCollection
					.get("collectionName");
			Map outPutFields = (Map) outCollection.get("outPutFields");
			Set set = outPutFields.entrySet();
			Map collectionResponseMap = new HashMap();
			for (Object object : set) {
				Map.Entry mapEntry = (Map.Entry) object;
				collectionResponseMap.put(mapEntry.getKey(),
						resultMap.get(mapEntry.getValue()));

			}
			outPut.put(collectionName, collectionResponseMap);
			log.debug("[outPut]"+outPut);
		}

		return outPut;

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getDBParam
	 * 
	 * </pre>
	 *
	 * @return Object
	 */
	private static Object getDBParam(MongoTemplate mongoTemplate,
			Map globalParmConfig) {
		boolean isDebugEnabled = log.isDebugEnabled();

		String collectionName = (String) globalParmConfig
				.get(Constants._dbcollectionName);
		DBObject dbQry = new BasicDBObject(
				(Map) globalParmConfig.get(Constants._dbQuery));
		DBObject dbProjectKeys = new BasicDBObject(
				(Map) globalParmConfig.get(Constants._dbProjectKeys));
		if (isDebugEnabled)
			log.debug("[collectionName]" + collectionName);
		if (isDebugEnabled)
			log.debug("[dbQry]" + dbQry);
		if (isDebugEnabled)
			log.debug("[dbProjectKeys]" + dbProjectKeys);
		DBObject responseDBObject = mongoTemplate.getCollection(collectionName)
				.findOne(dbQry, dbProjectKeys);
		log.debug("[responseDBObject]" + responseDBObject);
		return responseDBObject.get((String) globalParmConfig
				.get(Constants._dbMappingField));

	}
}

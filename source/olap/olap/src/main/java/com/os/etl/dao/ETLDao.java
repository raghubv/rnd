package com.os.etl.dao;

import java.util.Map;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * ETLConfiguration: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 2, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface ETLDao {
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. addETLDefinition
		* 
		* </pre> 
		*
		* @return String
	 */
	public String addETLDefinition(Map<Object, Object> requestPayload);

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. updateETLDefintiion
		* 
		* </pre> 
		*
		* @return String
	 */
	public String updateETLDefintiion(Map<Object, Object> requestPayload);

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. deleteETLDefintiion
		* 
		* </pre> 
		*
		* @return String
	 */
	public String deleteETLDefintiion(Map<Object, Object> requestPayload);

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. scheduleETLProcess
		* 
		* </pre> 
		*
		* @return String
	 */
	public String scheduleETLProcess(Map<Object, Object> requestPayload);

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getETLDfnIds
		* 
		* </pre> 
		*
		* @return String
	 */
	public String getETLDfnIds(Map<Object, Object> requestPayload);

}

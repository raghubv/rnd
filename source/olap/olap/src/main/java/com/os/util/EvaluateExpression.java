package com.os.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * EvaluateExpression: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 17, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class EvaluateExpression {

	static final Logger log = Logger.getLogger(EvaluateExpression.class);
	static private ScriptEngineManager mgr = new ScriptEngineManager();
	static private ScriptEngine engine = mgr
			.getEngineByName(Constants._javaScript);

/**
 * 
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. evaluateExpression
	* 
	* </pre> 
	*
	* @return Object
 */
	public static Object evaluateExpression(String expressionTemplate,
			Map<String, Object> inputMap) throws ScriptException   {

			Set set = inputMap.entrySet();
			for (Object object : set) {

				expressionTemplate = expressionTemplate.replaceAll(
						((Map.Entry) object).getKey().toString(),
						((Map.Entry) object).getValue().toString());
			}
			log.debug("[expressionTemplate]" + expressionTemplate);
			return engine.eval(expressionTemplate);
		

	}
/**
 * 
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. evaluateJSFunction
	* 
	* </pre> 
	*
	* @return Object
 */
	public static synchronized Object evaluateJSFunction(String jsFunction,
			String jsfnNmae, String expressionTemplate,
			Map<String, Object> inputMap) throws ScriptException, NoSuchMethodException {


			Set set = inputMap.entrySet();
			for (Object object : set) {

				expressionTemplate = expressionTemplate.replaceAll(
						((Map.Entry) object).getKey().toString(),
						((Map.Entry) object).getValue().toString());
			}
			return evaluateJSFunction(jsFunction, jsfnNmae,
					expressionTemplate.split(","));
		
	}

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. evaluateJSFunction
		* 
		* </pre> 
		*
		* @return Object
	 */
	public static synchronized Object evaluateJSFunction(String jsFunction,
			String jsfnNmae, Object... object) throws ScriptException, NoSuchMethodException {
		boolean isDebugEnabled=log.isDebugEnabled();
		if(isDebugEnabled)
		log.debug("[jsFunction]" + jsFunction);
		if(isDebugEnabled)
		log.debug("[jsfnNmae]" + jsfnNmae);
		if(log.isDebugEnabled())
		for (Object object2 : object) {
			if(isDebugEnabled)
			log.debug("[object]" + object2.toString());	
		}
			engine.eval(jsFunction);
			Invocable jsInvoke = (Invocable) engine;
			Object result = jsInvoke.invokeFunction(jsfnNmae, object);
			if(isDebugEnabled)
				log.debug("[result1]"+result);
			
			return result;
		

	}

	public static void main(String[] args) throws Exception {
		boolean isDebugEnabled=log.isDebugEnabled();
		Map inputMap = new HashMap();
		inputMap.put("A", 10.99999);
		inputMap.put("B", 20);
		inputMap.put("C", 10);
		inputMap.put("D", 5);
		inputMap.put("E", 10);
		inputMap.put("F", 5);
		// System.out.println("Eval"
		// + EvaluateExpression.evaluateExpression("A*B-(C%D)+(E/F)",
		// inputMap));

		
		System.out.println("Eval"
				+ EvaluateExpression.evaluateExpression("B^C", inputMap));

		// System.out.println("evaluateJS"+evaluateJSFunction("function add (a, b) {c = a + b; return c; }",
		// new Object[]{13,4} ));
		//String str = ".5,100,100";
		String str = "";
		Object[] objarry = str.split(",");
		//Object[] objarry = new Object[5];
//		
//		System.out
//				.println("evaluateJS"
//						+ evaluateJSFunction(
//								"function ftd(norm,alt,cb) {if((norm*alt)>cb) return 0; else return 1; }",
//								"ftd", objarry));
//	
		System.out
		.println("evaluateJS >>"
				+ evaluateJSFunction(
						"function noOfDaysOfMonth() {  var today= new Date(); var leap=0; var year= today.getFullYear();  if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) leap=1;  var month= today.getMonth(); return 31 - (((month+1) == 2) ?(3 - leap) : (((month+1) - 1) % 7 % 2)); }",
						"noOfDaysOfMonth", objarry));
	
	}

}

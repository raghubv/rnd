package com.os.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * ETLUtils: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 4, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ETLUtils {

	static final Logger log = Logger.getLogger(ETLUtils.class);

	public static enum mappedType {
		numberType(0), stringType(100), dateTimeStampTypeYYYYMMDDHHMMSS(1), dateTypeYYYYMMDD(
				2), dateTypeMMDDYYYY(3), dateTypeDDMMYYYY(4);

		private int value;

		private mappedType(int value) {
			this.value = value;
		}

		public String getMappedType() {
			return String.valueOf(this.value);
		}
	};

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. convertToMappingTypeAndFiled
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	static public Map convertToMappingTypeAndField(Map typeMap, Map fieldMap,
			Map inputMap) {

		return convertToMappedStructure(fieldMap,
				convertToMappingType(typeMap, inputMap));
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. convertToMappingType
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	static public Map convertToMappingType(Map typeMap, Map inputMap) {

		Map outMap = new HashMap();
		try {
			Set set = inputMap.entrySet();
			for (Object object : set) {
				Map.Entry entry = (Map.Entry) object;
				String key = (String) entry.getKey();

				if (!(entry.getValue() instanceof Map)) {

					if (entry.getValue() instanceof String) {

						String value = (String) entry.getValue();
						switch (mappedType.valueOf((String) typeMap.get(key))) {
						case numberType:
							outMap.put(key, Integer.valueOf(value));
							break;

						case dateTimeStampTypeYYYYMMDDHHMMSS:
							outMap.put(key,
									Constants.YYYY_MM_DD_HH_MM_SS_DATE_FORMAT
											.parse(value));
							break;
						case dateTypeYYYYMMDD:
							outMap.put(key, Constants.YYYY_MM_yy_DATE_FORMAT
									.parse(value));
							break;
						case dateTypeMMDDYYYY:
							outMap.put(key, Constants.MM_DD_YYYY_DATE_FORMAT
									.parse(value));
							break;
						case dateTypeDDMMYYYY:
							outMap.put(key, Constants.DD_MM_YYYY_DATE_FORMAT
									.parse(value));
							break;
						case stringType:
							outMap.put(key, value);
							break;

						default:
							outMap.put(key, value);
							break;
						}

					} else {
						outMap.put(key, entry.getValue());
					}
				} else
					outMap.put(
							key,
							convertToMappingType(typeMap,
									(Map) entry.getValue()));

			}

		} catch (ParseException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

		log.debug("[outMap]" + outMap);
		return outMap;

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. convertToMappedStructure
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	static public Map convertToMappedStructure(Map fieldMap, Map inputMap) {
		boolean isDebugEnabled = log.isDebugEnabled();
		if (isDebugEnabled)
			log.debug("[fieldMap]" + fieldMap);
		if (isDebugEnabled)
			log.debug("[inputMap]" + inputMap);
		Map outMap = new HashMap();
		try {
			Set set = fieldMap.entrySet();
			for (Object object : set) {
				Map.Entry entry = (Map.Entry) object;
				String key = (String) entry.getKey();
				Object obj = entry.getValue();
				if (obj instanceof Map) {
					outMap.put(key,
							convertToMappedStructure((Map) obj, inputMap));
				} else if (obj instanceof List) {
					List ls = (List) obj;
					List outLs = new ArrayList();
					for (Object obj1 : ls) {
						if (obj1 instanceof Map)
							outLs.add(convertToMappedStructure((Map) obj1,
									inputMap));

						else
							outLs.add(inputMap.get((String) obj));

					}
					outMap.put(key, outLs);
				} else
					outMap.put(key, inputMap.get((String) obj));

			}

		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

		log.debug("[outMap]" + outMap);
		return outMap;

	}

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. convert
		* 
		* </pre> 
		*
		* @return Object
	 */
	public static synchronized Object convert(Map input) {
		try {
			Set set = input.entrySet();
			for (Object object : set) {
				Map.Entry entry = (Map.Entry) object;
				String key = (String) entry.getKey();
				Object value = (Object) entry.getValue();

				if (value instanceof Map) {
					input.put(key, convert((Map) value));
				} else if (value instanceof List) {
					input.put(key, convert((List) value));
				} else if (key.equalsIgnoreCase("$date")) {
					Date date = Constants.MONGODB_DATE_FORMAT.parse(value
							.toString());
					return (Constants.MONGODB_DATE_FORMAT.parse(value
							.toString()));
				} else {
					input.put(key, value);
				}
			}

		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return input;
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. convert
		* 
		* </pre> 
		*
		* @return Object
	 */
	private static synchronized Object convert(List input) {
		List objLs = new ArrayList(input);
		try {
			for (Object object : input) {
				if (object instanceof Map) {
					objLs.remove(object);
					objLs.add(convert((Map) object));
				}
			}

		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));

		}
		return objLs;
	}

}

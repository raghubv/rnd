package com.os.aggregate.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.os.aggregate.dao.AggregateAndComputeDao;


public class AggregateAndComputeErrorHandler {

	private static final Logger log = Logger
			.getLogger(AggregateAndComputeErrorHandler.class);
	@Autowired
	AggregateAndComputeDao aggregateAndCompute;

	public void execute() {
		log.info("Entering");
		aggregateAndCompute.reCreateInstancesOfAggregation();
		aggregateAndCompute.reInitiateAggregate();
		log.info("Exiting");
	}

}

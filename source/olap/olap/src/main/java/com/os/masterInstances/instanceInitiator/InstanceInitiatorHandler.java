package com.os.masterInstances.instanceInitiator;

import static com.os.util.Constants.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.os.util.DateUtils;
import com.os.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * QueueDataHandler: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Feb 24, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Component
public class InstanceInitiatorHandler {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor aggregateMasterInstancesTaskExecutor;

	static final Logger log = Logger.getLogger(InstanceInitiatorHandler.class);
	private boolean isDebugEnabled = log.isDebugEnabled();


	public void run(){

		Calendar calendar = DateUtils.getCalendar();
		DBObject masterInstanceSchduleQry = new BasicDBObject();
		List<DBObject> orConditList = new ArrayList<DBObject>();
		DBObject scheduledTimeStamp = new BasicDBObject();
		scheduledTimeStamp.put(
				_ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path,
				new BasicDBObject(_$lte, calendar.getTime()));
		scheduledTimeStamp.put(
				_ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path,
				new BasicDBObject(_$gte, calendar.getTime()));
		scheduledTimeStamp.put(_status, _status_Active);
		orConditList.add(scheduledTimeStamp);
		DBObject scheduled = new BasicDBObject();
		scheduled.put(_ScheduleDetails_Scheduled_Years_Path,
				calendar.get(Calendar.YEAR));
		scheduled.put(_ScheduleDetails_Scheduled_Months_Path,
				calendar.get(Calendar.MONTH) + 1);
		scheduled.put(_ScheduleDetails_Scheduled_Days_Path,
				calendar.get(Calendar.DAY_OF_MONTH));
		scheduled.put(_ScheduleDetails_Scheduled_Hours_Path,
				calendar.get(Calendar.HOUR_OF_DAY));
		scheduled.put(_status, _Agg_Status_Active);
		orConditList.add(scheduled);
		masterInstanceSchduleQry.put(_$or, orConditList);

		if (isDebugEnabled)
			log.debug("[etlSchduleQry]" + masterInstanceSchduleQry);
		DBCursor masterTemplate = mongoTemplate.getCollection(
				_Master_Instances_Schedule).find(masterInstanceSchduleQry,
				new BasicDBObject(_eventMappingDBRef, 1));
		for (DBObject schedulerInstance : masterTemplate) {
			if (isDebugEnabled)
				log.debug("[schedulerInstance]" + schedulerInstance);
			process((DBRef) schedulerInstance.get(_eventMappingDBRef));
		}

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. process
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void process(DBRef eventMapDBRef) {
		boolean isDebugEnabled = log.isDebugEnabled();

		try {
			if (isDebugEnabled)
				log.debug("[eventMapDBRef]" + eventMapDBRef);
			DBObject eventMapping = eventMapDBRef.fetch();
			List<DBObject> masterCollections = (List<DBObject>) eventMapping
					.get(_masterCollections);
			for (DBObject masterCollection : masterCollections) {
				log.debug("[masterCollection]" + masterCollection);
				aggregateMasterInstancesTaskExecutor
						.execute(new MasterAggregateAndComputeInstancesProcessor(
								eventMapping, masterCollection));
			}

		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

	}

	/**
	 * 
	 * 
	 * <b>Purpose:</b><br>
	 * MasterTemplateProcessor: <br>
	 * <br>
	 * 
	 * <b>DesignReference:</b><br>
	 * <br>
	 * <br>
	 * 
	 * <b>CopyRights:</b><br>
	 * os 2013<br>
	 * <br>
	 * 
	 * <b>RevisionHistory:</b>
	 * 
	 * <pre>
	 * <b>
	 * Sl No   Modified Date        Author</b>
	 * ==============================================
	 * 1       Jun 5, 2015		   @author Raghu   -- Base Release
	 * 
	 * </pre>
	 * 
	 * <br>
	 */
	private class MasterAggregateAndComputeInstancesProcessor implements
			Runnable {
		private Logger log = Logger
				.getLogger(MasterAggregateAndComputeInstancesProcessor.class);
		private boolean isDebugEnabled = log.isDebugEnabled();

		private DBObject eventMapping;
		private DBObject masterCollection;

		public MasterAggregateAndComputeInstancesProcessor(
				DBObject eventMapping, DBObject masterTemplate) {
			super();
			this.eventMapping = eventMapping;
			this.masterCollection = masterTemplate;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {

			try {
				DBCursor dbCursor = mongoTemplate.getCollection(
						(String) masterCollection.get(_collectionName))
						.find((new BasicDBObject(
								(Map) masterCollection.get(_query))),
								(new BasicDBObject((Map) masterCollection
										.get(_project))));

				Iterator<DBObject> ls = dbCursor.iterator();
				for (DBObject dbObject : dbCursor) {

					log.debug("[queryParam]" + dbObject);
					Map aggregateAndComputeInstance = new HashMap();
					aggregateAndComputeInstance.put(_queryParams, dbObject);
					aggregateAndComputeInstance
							.put(_status, _Agg_Status_Active);
					aggregateAndComputeInstance.put(_createdDate, new Date());
					aggregateAndComputeInstance.put(_mappingDBRef, new DBRef(
							mongoTemplate.getDb(), _EventMappingDfn,
							eventMapping.get(_id)));
					mongoTemplate.getCollection(_AggregateAndComputeInstances)
							.insert(new BasicDBObject(
									aggregateAndComputeInstance));
				}

			} catch (Exception e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			}
		}
	}

}

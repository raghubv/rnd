package com.os.etl.queueProcessor;
import static com.os.util.Constants._ETLJobInstances;
import static com.os.util.Constants._ETLProcessDefinition;
import static com.os.util.Constants._EventMappingDfn;
import static com.os.util.Constants._aggAndCompInstStatus;
import static com.os.util.Constants._aggAndCompInstStatus_Active;
import static com.os.util.Constants._consumerBrokerURL_path;
import static com.os.util.Constants._consumerQueueName_path;
import static com.os.util.Constants._etlDBRef;
import static com.os.util.Constants._etlRefNo;
import static com.os.util.Constants._etlStatus;
import static com.os.util.Constants._etlStatus_Completed;
import static com.os.util.Constants._etlStatus_Inprogress;
import static com.os.util.Constants._eventName;
import static com.os.util.Constants._id;
import static com.os.util.Constants._mappingDBRef;
import static com.os.util.Constants._savetocollection_path;
import static com.os.util.Constants.scanQueue_path;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.os.util.DBUtils;
import com.os.util.ETLUtils;
import com.os.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * QueueDataHandler: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Feb 24, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */

public class QueueDataHandler {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor etlTaskExecutor;

	static final Logger log = Logger.getLogger(QueueDataHandler.class);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. process
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void process(Map<Object, Object> inputMap) {
		boolean isDebugEnabled = log.isDebugEnabled();

		try {
			if (isDebugEnabled)
				log.debug("[map]" + inputMap);
			List<Map> queues;
			queues = (List<Map>) PropertyUtils.getProperty(inputMap,
					scanQueue_path);

			DBObject mappingQuery = new BasicDBObject();
			mappingQuery.put(_eventName, inputMap.get(_eventName));
			if (isDebugEnabled)
				log.debug("[mappingQuery]" + mappingQuery);
			DBObject eventMapping = mongoTemplate.getCollection(
					_EventMappingDfn).findOne(mappingQuery);
			if (isDebugEnabled)
				log.debug("[dirs]" + queues);
			for (Map queue : queues) {

				if (isDebugEnabled)
					log.debug("[Active thread count]"
							+ etlTaskExecutor.getActiveCount());
				etlTaskExecutor.execute(new JSONProcessor(inputMap,
						eventMapping, queue));

			}
		} catch (IllegalAccessException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

	}

	private class JSONProcessor implements Runnable {
		private Logger log = Logger.getLogger(JSONProcessor.class);
		private boolean isDebugEnabled = log.isDebugEnabled();
		private Map<Object, Object> inputMap;
		private DBObject eventMapping;
		private Map queue;

		public JSONProcessor(Map<Object, Object> inputMap,
				DBObject eventMapping, Map queue) {
			super();
			this.inputMap = inputMap;
			this.eventMapping = eventMapping;
			this.queue = queue;
		}

		public void run() {

			try {
				String brokerUrl = (String) PropertyUtils.getProperty(queue,
						_consumerBrokerURL_path);
				String target = (String) PropertyUtils.getProperty(queue,
						_consumerQueueName_path);
				log.debug("[brokerUrl]"+brokerUrl);
				log.debug("[target]"+target);
				ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
						brokerUrl);
				Connection connection = connectionFactory.createConnection();
				connection.start();
				Session session = connection.createSession(false,
						Session.AUTO_ACKNOWLEDGE);
				Destination destination = session.createQueue(target);
				MessageConsumer consumer = session.createConsumer(destination);
				Message message = consumer.receive();
				int refNo = DBUtils.getCounter(mongoTemplate, 101);
				int count = 1;
				// ETL Job Instance from ETL Definiton
				DBObject etlInstance = new BasicDBObject();
				etlInstance.put(_etlStatus, _etlStatus_Inprogress);
				etlInstance.put(_etlDBRef, new DBRef(mongoTemplate.getDb(),
						_ETLProcessDefinition, inputMap.get(_id)));
				etlInstance.put(_etlRefNo, refNo);
				if (isDebugEnabled)
					log.debug("[etlInstance]" + etlInstance);
				etlInstance.put(_mappingDBRef, new DBRef(mongoTemplate.getDb(),
						_EventMappingDfn, eventMapping.get(_id)));
				mongoTemplate.getCollection(_ETLJobInstances).insert(
						etlInstance);
				TextMessage textMessage = (TextMessage) message;
				while (message != null && message instanceof TextMessage
						&& count <= 50000) {
					count++;
					String collectionName = (String) PropertyUtils.getProperty(
							inputMap, _savetocollection_path);
					ObjectMapper objmapper = new ObjectMapper();
					Map map = objmapper.readValue(textMessage.getText(),
							Map.class);
					map.put(_etlRefNo, refNo);
					mongoTemplate.getCollection(collectionName).insert(
							new BasicDBObject((Map) ETLUtils.convert(map)));
					message = consumer.receiveNoWait();
					textMessage = (TextMessage) message;
				}
				/* Status Update Back */
				etlInstance.put(_etlStatus, _etlStatus_Completed);
				etlInstance.put(_aggAndCompInstStatus,
						_aggAndCompInstStatus_Active);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id, etlInstance.get(_id)),
						etlInstance);
				session.close();
				connection.close();
				// Queue Configuration--End

			} catch (IllegalAccessException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (InvocationTargetException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (NoSuchMethodException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (FileNotFoundException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (IOException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			} catch (JMSException e) {
				log.error("[Error]" + LoggerTrace.getStackTrace(e));
			}

		}
	}

}

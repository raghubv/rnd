package com.os.util;

import java.util.Date;

/**
 * 
 * <b>Purpose:</b><br>
 * Utility <br>
 * <br>
 *
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 *
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 *
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Utility {

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. checkNullString
	 * 
	 * </pre>
	 * 
	 * @param inputString
	 * @return
	 */
	public static String checkNullString(String inputString) {
		if (inputString != null)
			return inputString;
		else
			return "";
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. checkNullInteger
	 * 
	 * </pre>
	 * 
	 * @param inputString
	 * @return
	 */
	public static Integer checkNullInteger(String inputString) {
		if (inputString != null && !inputString.isEmpty())
			return Integer.valueOf(inputString);
		else
			return 0;
	}

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getDate
		* 
		* </pre> 
		*
		* @return Date
	 */
	public static synchronized Date getDate() {
		return new Date();
	}
	
	
	

	
	
	
}


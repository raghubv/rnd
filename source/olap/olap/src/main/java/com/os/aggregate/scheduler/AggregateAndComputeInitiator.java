package com.os.aggregate.scheduler;

import static com.os.util.Constants._$group;
import static com.os.util.Constants._$match;
import static com.os.util.Constants._$unwind;
import static com.os.util.Constants._Agg_Status_Active;
import static com.os.util.Constants._AggregateAndComputeInstances;
import static com.os.util.Constants._ETLJobInstances;
import static com.os.util.Constants._EventMappingDfn;
import static com.os.util.Constants._aggAndCompInstStatus;
import static com.os.util.Constants._aggAndCompInstStatus_Active;
import static com.os.util.Constants._aggAndCompInstStatus_Completed;
import static com.os.util.Constants._aggAndCompInstStatus_ERROR;
import static com.os.util.Constants._aggAndCompInstStatus_Inprogress;
import static com.os.util.Constants._createdDate;
import static com.os.util.Constants._etlDBRef;
import static com.os.util.Constants._etlJobInsDBRef;
import static com.os.util.Constants._etlRefNo;
import static com.os.util.Constants._groupBy;
import static com.os.util.Constants._id;
import static com.os.util.Constants._mappingDBRef;
import static com.os.util.Constants._queryParams;
import static com.os.util.Constants._result;
import static com.os.util.Constants._savetocollection_path;
import static com.os.util.Constants._status;
import static com.os.util.Constants._unwindLs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.os.util.DBUtils;
import com.os.util.LoggerTrace;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * <b>Purpose:</b><br>
 * ClaimTimerJobLauncherDetails <br>
 * <br>
 *
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 *
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 *
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 May 13, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class AggregateAndComputeInitiator {

	private static final Logger log = Logger
			.getLogger(AggregateAndComputeInitiator.class);
	static int counter = 0;
	boolean isDebugEnabled = log.isDebugEnabled();

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ThreadPoolTaskExecutor aggregateInitiatorTaskExecutor;

	private Integer limit;
	private Integer minOfAggInstance;

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public void setMinOfAggInstance(Integer minOfAggInstance) {
		this.minOfAggInstance = minOfAggInstance;
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createInstanceForAggregateAndCompute
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void createInstanceForAggregateAndCompute() {

		try {

			int count = limit - counter;
			if (counter >= 0 && count > 0 && counter <= limit) {
				if (mongoTemplate.getCollection(_AggregateAndComputeInstances)
						.count() < minOfAggInstance) {
					DBObject etlJobInstanceQuery = new BasicDBObject();
					etlJobInstanceQuery.put(_aggAndCompInstStatus,
							_aggAndCompInstStatus_Active);
					if (isDebugEnabled)
						log.debug("[etlJobInstanceQuery]" + etlJobInstanceQuery);

					DBCursor eltJobInstances = mongoTemplate
							.getCollection(_ETLJobInstances)
							.find(etlJobInstanceQuery).limit(count--);
					for (DBObject etlJobInstance : eltJobInstances) {
						if (isDebugEnabled)
							log.debug("[etlInstance]" + etlJobInstance);
						aggregateInitiatorTaskExecutor
								.execute(new AggregateInitiator(mongoTemplate,
										etlJobInstance));
						counter++;
					}
				}

			} else
				counter = 0;
		} catch (RejectedExecutionException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
	}

	/**
	 * 
	 * 
	 * <b>Purpose:</b><br>
	 * AggregateInitiator: <br>
	 * <br>
	 * 
	 * <b>DesignReference:</b><br>
	 * <br>
	 * <br>
	 * 
	 * <b>CopyRights:</b><br>
	 * os 2013<br>
	 * <br>
	 * 
	 * <b>RevisionHistory:</b>
	 * 
	 * <pre>
	 * <b>
	 * Sl No   Modified Date        Author</b>
	 * ==============================================
	 * 1       Mar 17, 2015		   @author Raghu   -- Base Release
	 * 
	 * </pre>
	 * 
	 * <br>
	 */
	public class AggregateInitiator implements Runnable {
		private MongoTemplate mongoTemplate = null;
		private DBObject etlJobInstance = null;

		/**
		 * 
		 * @param mongoTemplate
		 * @param eltJobInstance
		 */
		public AggregateInitiator(MongoTemplate mongoTemplate,
				DBObject eltJobInstance) {
			this.mongoTemplate = mongoTemplate;
			this.etlJobInstance = eltJobInstance;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			try {
				etlJobInstance.put(_aggAndCompInstStatus,
						_aggAndCompInstStatus_Inprogress);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id, etlJobInstance.get(_id)),
						etlJobInstance);

				DBObject eventMappingDfn = DBUtils.getDBRefObject(
						mongoTemplate,
						(DBRef) etlJobInstance.get(_mappingDBRef));
				log.debug("[eventMapping]" + eventMappingDfn);
				DBObject etlJobDfn = DBUtils.getDBRefObject(mongoTemplate,
						(DBRef) etlJobInstance.get(_etlDBRef));
				log.debug("[etlDfn]" + etlJobDfn);
				String collectionName = (String) PropertyUtils.getProperty(
						etlJobDfn, _savetocollection_path);
				log.debug("[collectionName]" + collectionName);

				// Match By Input

				DBObject dbqry = new BasicDBObject();
				dbqry.put(_etlRefNo, etlJobInstance.get(_etlRefNo));

				List<DBObject> aggregate = new ArrayList<DBObject>();
				DBObject matchBy = new BasicDBObject();
				matchBy.put(_$match, dbqry);
				aggregate.add(matchBy);

				// Unwind before String
				if (eventMappingDfn.containsField(_unwindLs)) {
					List<String> unwindLs = (List<String>) eventMappingDfn
							.get(_unwindLs);
					for (String unwind : unwindLs) {
						DBObject unwindDb = new BasicDBObject();
						unwindDb.put(_$unwind, unwind);
						aggregate.add(unwindDb);
					}
				}
				// Group By Input
				DBObject groupBy = new BasicDBObject();
				groupBy.put(_$group, eventMappingDfn.get(_groupBy));
				aggregate.add(groupBy);

				log.debug("[aggregate]" + aggregate);
				// Aggregate
				Map aggregateResponse = mongoTemplate
						.getCollection(collectionName).aggregate(aggregate)
						.getCommandResult().toMap();
				log.debug("[aggregate]" + aggregateResponse);
				// Create aggregate and Compute instances
				List<Map> aggregateResultLs = new ArrayList<Map>();
				aggregateResultLs.addAll((List<Map>) aggregateResponse
						.get(_result));
				log.debug("[aggregateResultLs]" + aggregateResultLs);
				for (Map aggregateResult : aggregateResultLs) {
					Map aggregateAndComputeInstance = new HashMap();
					aggregateAndComputeInstance.put(_queryParams,
							aggregateResult.get(_id));
					aggregateAndComputeInstance
							.put(_status, _Agg_Status_Active);
					aggregateAndComputeInstance.put(_createdDate, new Date());
					aggregateAndComputeInstance.put(_etlJobInsDBRef, new DBRef(
							mongoTemplate.getDb(), _ETLJobInstances,
							etlJobInstance.get(_id)));
					aggregateAndComputeInstance.put(_mappingDBRef, new DBRef(
							mongoTemplate.getDb(), _EventMappingDfn,
							eventMappingDfn.get(_id)));
					mongoTemplate.getCollection(_AggregateAndComputeInstances)
							.insert(new BasicDBObject(
									aggregateAndComputeInstance));
				}

				// Set Status ETL Status
				etlJobInstance.put(_aggAndCompInstStatus,
						_aggAndCompInstStatus_Completed);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id, etlJobInstance.get(_id)),
						etlJobInstance);

			} catch (Exception e) {

				etlJobInstance.put(_aggAndCompInstStatus,
						_aggAndCompInstStatus_ERROR);
				mongoTemplate.getCollection(_ETLJobInstances).update(
						new BasicDBObject(_id, etlJobInstance.get(_id)),
						etlJobInstance);

				log.error("[Error]" + LoggerTrace.getStackTrace(e));

			}
			counter--;
		}

	}


}

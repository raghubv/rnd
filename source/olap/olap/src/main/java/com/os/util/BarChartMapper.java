package com.os.util;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class BarChartMapper {
	static final Logger log = Logger.getLogger(BarChartMapper.class);

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		String str = "[{\"_id\": {\"group\": 201507}," + "\"values\": [{\"Y\": 56220," + "\"X\": \"Product Categories\""
				+ "},{" + "\"Y\": 56220,\"X\": \"Products\"}]}," + "{\"_id\": {\"group\": 201506},"
				+ "\"values\": [{\"Y\": 5874696," + "\"X\": \"Product Categories\"},{"
				+ "\"Y\": 5874696,\"X\": \"Products\"}]}" + "]";

		ObjectMapper objMapper = new ObjectMapper();
		List<Map> ls = objMapper.readValue(str, List.class);

		System.out.println(">>>" + getGoogleMap(ls, "YearMonth"));
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getGoogleMap
	 * 
	 * </pre>
	 *
	 * @return List
	 */
	synchronized public static List getGoogleMap(List<Map> ls, String textX) {
		List res = new LinkedList();
		try {
			List XList = new LinkedList();
			XList.add(textX);
			List YList = new LinkedList();
			Map matrixMap = new HashMap();

			for (Map object : ls) {
				Object idGroup = PropertyUtils.getProperty(object, Constants._barChartAPIIdGrouop);
				log.debug("[_id.group]" + idGroup);
				List<Map> values = (List<Map>) object.get(Constants._barChartAPIValues);
				for (Map value : values) {
					Object objectX = value.get(Constants._barChartAPIX);
					Object objectY = value.get(Constants._barChartAPIY);

					log.debug("[value]" + value);
					if (!XList.contains(objectX))
						XList.add(objectX);
					matrixMap.put(objectX + ":" + idGroup, objectY);
				}
				if (!YList.contains(idGroup))
					YList.add(idGroup.toString());
			}
			res.add(XList);
			log.debug("[XList]" + XList);
			log.debug("[YList]" + YList);
			log.debug("[res]" + res);
			for (Object objy : YList) {
				boolean isTrue = true;
				List item = new ArrayList();
				for (Object objx : XList) {
					if (!isTrue) {
						log.debug("[value]" + matrixMap.get(objx + ":" + objy));
						item.add(matrixMap.get(objx + ":" + objy));

					} else {
						item.add(objy);
						isTrue = false;
					}
				}
				res.add(item);
			}
		} catch (IllegalAccessException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return res;
	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 *  
	* 1. getNVD3Map
	 * 
	 * </pre>
	 *
	 * @return List
	 */
	synchronized public static List getNVD3Map(List<Map> ls, String textX) {
		List res = new LinkedList();
		try {
			for (Map obj : ls) {

				obj.put("color", ColorUtils.getRandomColor());
				Object idGroup = PropertyUtils.getProperty(obj, Constants._barChartAPIIdGrouop);
				if (idGroup instanceof Map) {
					obj.put("key", ((Map) idGroup).get("key1") + ":" + ((Map) idGroup).get("key2"));
				} else
					obj.put("key", idGroup);

				obj.remove("_id");
				res.add(obj);
			}

		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return res;
	}

}

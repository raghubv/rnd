package com.os.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * ServletContextListener to initialize Logging for the application
 */

public class Log4jConfigListener implements ServletContextListener {
	private static long DEFAULT_INTERVAL = 3000;
	static final Logger log = Logger.getLogger(Log4jConfigListener.class);

	public void contextDestroyed(ServletContextEvent event) {
		// Turn off logging when the Servlet Context is destroyed
		LogManager.shutdown();
	}
	/**
	 * 
	 * @param logConfPath
	 * @param appContext
	 */
	public static  boolean configureLogger(final String logConfPath,
			final String appContext) {

		ArrayList<String> lines = new ArrayList<String>();
		String line = null;
		try {
			
			File fileInput = new File(logConfPath + "/default-log4j.xml");
			File fileOutPut = new File(logConfPath + appContext
					+ "-log4j.xml");
			fileOutPut.createNewFile();
			FileReader fr = new FileReader(fileInput);
			BufferedReader br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				if (line.contains("webContext"))
					line = line.replace("webContext", appContext);
				lines.add(line);
			}
			fr.close();
			br.close();

			FileWriter fw = new FileWriter(fileOutPut);
			BufferedWriter out = new BufferedWriter(fw);
			for (String s : lines) {
				out.write(s);
				out.write("\n");
			}

			out.flush();
			out.close();
			fw.close();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		
		}
		return false;

	}

	public void contextInitialized(ServletContextEvent event) {
		// Read configuration properties from web.xml
		
		String logConfPath=System.getenv("LOG4J_CONFIG_LOCATION");
		System.err.println("[logConfPath]"+logConfPath);
		String appContext=event.getServletContext().getContextPath();
		String interval = System.getenv("LOG4J_REFRESH_INTERVAL");
		String file = logConfPath+appContext+"-log4j.xml"; 
		long delay = DEFAULT_INTERVAL; // Default delay is 5 minutes
		// Check for interval parameter
		if (interval != null && !"".equals(interval)) {
			try {
				delay = Long.parseLong(interval);
			} catch (NumberFormatException e) {
				// Can't really log the error since we haven't initialized Log4J
				// Will use the default value
				delay = DEFAULT_INTERVAL;
			}
		}
		
		// Check for file parameter
		if (file != null && !"".equals(file)) {
			if (!((new File(file)).exists())) {
				
				configureLogger(logConfPath, appContext);
//				DOMConfigurator.configureAndWatch(file, delay);
				
//				throw new IllegalArgumentException(
//						"Invalid 'LOG4J_CONFIG_LOCATION' parameter value '"
//								+ file + "'.");
//			
				
				
			}
			DOMConfigurator.configureAndWatch(file, delay); 
		}
	}
}
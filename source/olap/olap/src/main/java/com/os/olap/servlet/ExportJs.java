package com.os.olap.servlet;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.os.aggregate.dao.AggregateAndComputeDao;
import com.os.util.LoggerTrace;

/**
 * 
 * <b>Purpose:</b><br>
 * ExportImage <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 3, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ExportJs extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7400783693917472193L;
	private static final Logger log = Logger.getLogger(ExportJs.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			// TODO Auto-generated method stub
			WebApplicationContext context = WebApplicationContextUtils
					.getWebApplicationContext(getServletContext());
			AggregateAndComputeDao aggregateAndCompute = (AggregateAndComputeDao) context.getBean("aggregateAndCompute");
			// JPEGTranscoder t = new JPEGTranscoder();
			String jsName = req.getPathInfo().replaceAll("/", "");
	
			OutputStream ostream = resp.getOutputStream();
			
			ostream.write(aggregateAndCompute.getMapData(jsName).getBytes());
			
			ostream.flush();
			ostream.close();
			resp.setContentType("js");
			super.doGet(req, resp);
			
					} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
	}

}

package com.os.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Configuration {
	private static final Logger log = Logger.getLogger(Configuration.class);
	private static Map indexKeys = null;
	private static Map configurationData = null;
	
	static {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			//Index Keys
			InputStream indexKeysConfig = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(Constants._System_Index_keys);
			indexKeys=objectMapper.readValue(indexKeysConfig,
					LinkedHashMap.class);
			indexKeysConfig.close();	
			
			InputStream configurationDataConfig = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(Constants._System_Configuration_Data);
			configurationData=objectMapper.readValue(configurationDataConfig,
					LinkedHashMap.class);
			configurationDataConfig.close();	
			
					
		} catch (JsonParseException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getIndexKeys
		* 
		* </pre> 
		*
		* @return Map
	 */
	public static Map getIndexKeys() {
		return indexKeys;
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getConfigurationData
		* 
		* </pre> 
		*
		* @return Map
	 */
	public static Map getConfigurationData() {
		return configurationData;
	}
	
	
	
	

}

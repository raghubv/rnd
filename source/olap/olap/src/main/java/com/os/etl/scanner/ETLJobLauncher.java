package com.os.etl.scanner;

import static com.os.util.Constants._$gte;
import static com.os.util.Constants._$lte;
import static com.os.util.Constants._$or;
import static com.os.util.Constants._Agg_Status_Active;
import static com.os.util.Constants._ETLSchedule;
import static com.os.util.Constants._ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path;
import static com.os.util.Constants._ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Days_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Hours_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Months_Path;
import static com.os.util.Constants._ScheduleDetails_Scheduled_Years_Path;
import static com.os.util.Constants._etlDBRef;
import static com.os.util.Constants._id;
import static com.os.util.Constants._status;
import static com.os.util.Constants._status_Active;
import static com.os.util.Constants._type;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.os.etl.fileProcessor.CSVDataHandler;
import com.os.etl.fileProcessor.JSONDataHandler;
import com.os.etl.fileProcessor.XMLDataHandler;
import com.os.etl.queueProcessor.QueueDataHandler;
import com.os.util.DateUtils;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

/**
 * 
 * <b>Purpose:</b><br>
 * ClaimTimerJobLauncherDetails <br>
 * <br>
 *
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 *
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 *
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 May 13, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ETLJobLauncher {

	private static final Logger log = Logger.getLogger(ETLJobLauncher.class);

	static private enum parseType {
		CSV, XML, JSON, Queue
	};

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CSVDataHandler csvDataHandler;
	@Autowired
	XMLDataHandler xmlDataHandler;
	@Autowired
	JSONDataHandler jsonDataHandler;
	@Autowired
	QueueDataHandler queueDataHandler;

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. performETL
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	private void performETL(DBObject dbObject) {

		switch (parseType.valueOf((String) dbObject.get(_type))) {
		case CSV:
			csvDataHandler.process(dbObject.toMap());
			break;
		case XML:
			xmlDataHandler.process(dbObject.toMap());
			break;
		case JSON:
			jsonDataHandler.process(dbObject.toMap());
			break;
		case Queue:
			queueDataHandler.process(dbObject.toMap());
			break;
		default:
			break;

		}

	}

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. execute
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void execute() {

		boolean isDebugEnabled = log.isDebugEnabled();

		Calendar calendar = DateUtils.getCalendar();
		DBObject etlSchduleQry = new BasicDBObject();

		List<DBObject> orConditList = new ArrayList<DBObject>();
		DBObject scheduledTimeStamp = new BasicDBObject();

		scheduledTimeStamp.put(
				_ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path,
				new BasicDBObject(_$lte, calendar.getTime()));
		scheduledTimeStamp.put(
				_ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path,
				new BasicDBObject(_$gte, calendar.getTime()));
		scheduledTimeStamp.put(_status, _status_Active);

		orConditList.add(scheduledTimeStamp);

		DBObject scheduled = new BasicDBObject();
		scheduled.put(_ScheduleDetails_Scheduled_Years_Path,
				calendar.get(Calendar.YEAR));
		scheduled.put(_ScheduleDetails_Scheduled_Months_Path,
				calendar.get(Calendar.MONTH) + 1);
		scheduled.put(_ScheduleDetails_Scheduled_Days_Path,
				calendar.get(Calendar.DAY_OF_MONTH));
		scheduled.put(_ScheduleDetails_Scheduled_Hours_Path,
				calendar.get(Calendar.HOUR_OF_DAY));
		scheduled.put(_status, _Agg_Status_Active);
		orConditList.add(scheduled);
		etlSchduleQry.put(_$or, orConditList);

		if (isDebugEnabled)
			log.debug("[etlSchduleQry]" + etlSchduleQry);
		DBCursor schedulerCursor = mongoTemplate.getCollection(_ETLSchedule)
				.find(etlSchduleQry, new BasicDBObject(_etlDBRef, 1));
		for (DBObject schedulerInstance : schedulerCursor) {
			if (isDebugEnabled)
				log.debug("[schedulerInstance]" + schedulerInstance);
			DBRef dbRef = (DBRef) schedulerInstance.get(_etlDBRef);
			DBObject etlDfn = mongoTemplate.getCollection(dbRef.getRef())
					.findOne(new BasicDBObject(_id, dbRef.getId()));
			if (isDebugEnabled)
				log.debug("[etlDfn]" + etlDfn);
			performETL(etlDfn);
		}

	}


}

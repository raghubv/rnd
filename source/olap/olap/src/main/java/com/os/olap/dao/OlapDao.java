package com.os.olap.dao;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * OlapDao: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Jan 15, 2015		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface OlapDao {

	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. createSystemIndex
		* 
		* </pre> 
		*
		* @return String
	 */
	void  createSystemIndex();
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. insertSystemData
		* 
		* </pre> 
		*
		* @return void
	 */
	void  insertSystemConfigurationData();
	
}

package com.os.util;

import java.text.SimpleDateFormat;

/**
 * 
 * <b>Purpose:</b><br>
 * Constants <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 1, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface Constants {

	public static enum responseCode {
		Success(0), Failed(100), Add_ETL_Dfn_Failed(101), Scheduled_ETL_Process_Failed(
				102), Get_ETL_Dfn_Ids_Failed(103), Aggregate_And_Compute_Failed(
				104), Add_Aggregate_Formula_Failed(105), Schedule_Aggregation_Failed(
				106), Add_Event_Mapping_Failed(107), YetToImplement(404);

		private int value;

		private responseCode(int value) {
			this.value = value;
		}

		public String getResponseCode() {
			return String.valueOf(this.value);
		}
	};

	/** Simple Date Format **/
	public static final SimpleDateFormat DD_MM_YYYY_DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yyyy");

	public static final SimpleDateFormat YYYYMMdd_DATE_FORMAT = new SimpleDateFormat(
			"yyyyMMdd");

	public static final SimpleDateFormat MM_DD_YYYY_DATE_FORMAT = new SimpleDateFormat(
			"MM/dd/yyyy");

	public static final SimpleDateFormat YYYY_MM_yy_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");
	public static final SimpleDateFormat YYYY_MM_DD_HH_MM_SS_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static final SimpleDateFormat MONGODB_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	/** Start--Collection Names **/
	public static final String _ETLProcessDefinition = "Dfn_ETL_Process";
	public static final String _ETLSchedule = "Instances_ETL_Schedule";
	public static final String _ETLJobInstances = "Instances_ETL_Job";
	public static final String _EventMappingDfn = "Dfn_Event_Mapping";
	public static final String _AggregateAndComputeInstances = "Instances_Aggregate_And_Compute";
	public static final String _AggregateAndComputeDfn = "Dfn_Aggregate_And_Compute";
	public static final String _AggregateSchedule = "Instances_Aggregate_Schedule";
	public static final String _DuplicateSchedule = "Instances_Duplicate_Schedule";
	public static final String _Conf_Counter = "Conf_Counter";

	/** End--Collection Names **/

	// public static final String _outGoingData = "outGoingData";
	public static final String _aggregateAndCompute = "aggregateAndCompute";

	/** Request **/

	public static final String _type = "type";
	public static final String _status = "status";
	public static final String _queryParams = "queryParams";
	public static final String _createdDate = "createdDate";
	public static final String _CollectionName = "collectionName";

	public static final String _status_Active = "A";
	public static final String _status_Completed = "C";
	public static final String _status_Inprogress = "I";

	/** Aggregation and computation status **/
	public static final String _Agg_Status_Active = "A";
	public static final String _Agg_Status_Completed = "C";
	public static final String _Agg_Status_Inprogress = "I";
	public static final String _Agg_Status_ERROR = "E";

	public static final String scanDirectory_path = "configuration.scanDirectory";

	public static final String scanQueue_path = "configuration.scanQueue";

	public static final String backUpDirectory_path = "configuration.backUpDirectory";
	public static final String _eventName = "eventName";

	public static final String _savetocollection_path = "configuration.savetocollection";
	public static final String _typeMapping_path = "configuration.typeMapping";
	public static final String _fieldMapping_path = "configuration.fieldMapping";
	public static final String _consumerBrokerURL_path = "consumer.brokerURL";
	public static final String _consumerQueueName_path = "consumer.queueName";

	/** Mongo DB Operator-Start **/
	public static final String _$exists = "$exists";
	public static final String _$gte = "$gte";
	public static final String _$gt = "$gt";
	public static final String _$lte = "$lte";
	public static final String _$lt = "$lt";
	public static final String _$in = "$in";
	public static final String _$inc = "$inc";
	public static final String _$or = "$or";
	public static final String _$and = "$and";
	public static final String _$set = "$set";
	public static final String _$push = "$push";
	public static final String _$elemMatch = "$elemMatch";
	/** Mongo DB Operator-End **/
	/** Mongod DB Aggregation command **/
	public static final String _$match = "$match";
	public static final String _$group = "$group";
	public static final String _$unwind = "$unwind";
	public static final String _$sum = "$sum";
	public static final String _$limit = "$limit";
	public static final String _$skip = "$skip";
	public static final String _$sort = "$sort";
	public static final String _$addToSet = "$addToSet";
	public static final String _$_id = "$_id";
	public static final String _$ = "$";
	public static final String _id = "_id";
	public static final String _id_List = "_idList";
	public static final String _counts = "counts";

	public static final String _aggregateAndCompute_Formulae = "/formulae.json";
	/** Parse Path **/
	public static final String _collectionPath = ".collection";
	public static final String _dateParamsPath = ".dateParams";
	public static final String _aggregationPath = ".aggregation";
	public static final String _aggregationPipelinePath = ".aggregation.pipeline";
	public static final String _idPath = "_id.$oid";
	public static final String _pipeline = "pipeline";
	public static final String _formulaId = "formulaId";
	public static final String _formulaDfn = "formulaDfn";
	public static final String _etlDfnId = "etlDfnId";
	public static final String _eventDfnId = "eventDfnId";
	public static final String _emptyResponse = "{}";
	public static final String _compute = "compute";
	public static final String _inputParams = "inputParams";
	public static final String _inCollection = "inCollection";
	public static final String _computationType = "computationType";

	public static final String _execute = "execute";
	public static final String _executeType = "executeType";
	public static final String _collections = "collections";
	public static final String _expressions = "expressions";
	public static final String _expressionName = "expressionName";
	public static final String _jsFunctions = "jsFunctions";
	public static final String _primaryJSFunction = "primaryJSFunction";
	public static final String _jsFunction = "jsFunction";
	public static final String _funcationName = "funcationName";
	public static final String _funcationArgs = "args";
	public static final String _outCollection = "outCollection";
	public static final String _outCollections = "outCollections";
	public static final String _dateParams = "dateParams";
	public static final String _dateParam = "dateParam";
	public static final String _aggregation = "aggregation";
	public static final String _aggregations = "aggregations";
	public static final String _aggregationName = "aggregationName";
	public static final String _addToResult = "addToResult";
	public static final String _addToParams = "addToParams";
	public static final String _expression = "expression";
	public static final String _collectionDBRef = "collectionDBRef";
	public static final String _etlDBRef = "etlDBRef";
	public static final String _eventMappingDBRef = "eventMappingDBRef";
	public static final String _etlDfnDBRef = "etlDfnDBRef";
	public static final String _etlJobInsDBRef = "etlJobInsDBRef";
	public static final String _mappingDBRef = "mappingDBRef";
	public static final String _fieldName = "fieldName";
	public static final String _fieldValue = "fieldValue";
	public static final String _javaScript = "JavaScript";
	public static final String _scheduleDetails = "scheduleDetails";
	public static final String _scheduledTimeStamp = "scheduledTimeStamp";
	public static final String _startTimeStamp = "startTimeStamp";
	public static final String _endTimeStamp = "endTimeStamp";
	public static final String _scheduledDaysOfMonth = "scheduledDaysOfMonth";
	public static final String _scheduledhoursOfDay = "scheduledHoursOfday";

	public static final String _scheduled = "scheduled";
	public static final String _rows = "rows";
	public static final String _from = "from";
	public static final String _etlRefNo = "etlRefNo";
	public static final String _to = "to";
	public static final String _result = "result";
	public static final String _scheduledStartTimeStamp_Path = "scheduleDetails.scheduledTimeStamp.startTimeStamp";
	public static final String _scheduledEndTimeStamp_Path = "scheduleDetails.scheduledTimeStamp.endTimeStamp";
	public static final String _scheduledDaysOfMonthFrom_Path = "scheduleDetails.daysOfMonth.from";
	public static final String _scheduledDaysOfMonthTo_Path = "scheduleDetails.daysOfMonth.to";

	public static final String _scheduledStartHourOfDay_path = "scheduleDetails.scheduledHoursOfday.startHour";
	public static final String _scheduledEndHourOfDay_path = "scheduleDetails.scheduledHoursOfday.endHour";

	/** Scheduled Time Stamp **/
	public static final String _ScheduleDetails_ScheduledTimeStamp_StartTimeStamp_Path = "scheduleDetails.scheduledTimeStamp.startTimeStamp";
	public static final String _ScheduleDetails_ScheduledTimeStamp_EndTimeStamp_Path = "scheduleDetails.scheduledTimeStamp.endTimeStamp";

	/** Scheduled **/
	public static final String _ScheduleDetails_Scheduled_Years_Path = "scheduleDetails.scheduled.years";
	public static final String _ScheduleDetails_Scheduled_Months_Path = "scheduleDetails.scheduled.months";
	public static final String _ScheduleDetails_Scheduled_Days_Path = "scheduleDetails.scheduled.days";
	public static final String _ScheduleDetails_Scheduled_Hours_Path = "scheduleDetails.scheduled.hours";

	/** Configuration for getting db param as input **/
	public static final String _dbcollectionName = "dbcollectionName";
	public static final String _dbParams = "dbParams";
	public static final String _dbParam = "dbParam";
	public static final String _dbQuery = "dbQuery";
	public static final String _dbProjectKeys = "dbProjectKeys";
	public static final String _dbMappingField = "dbMappingField";
	public static final String _groupBy = "groupBy";
	public static final String _unwindLs = "unwindLs";
	public static final String _dataRows = "rows";
	public static final String _data = "data";
	public static final String _rowsDot$ = "rows.$";
	public static final String _dataRows_datePath = "aggregatedAndComputedDate";
	public static final String _AggregatedAndComputedDate = "aggregatedAndComputedDate";

	public static final String _etlStatus = "etlStatus";
	public static final String _etlStatus_Inprogress = "I";
	public static final String _etlStatus_Active = "A";
	public static final String _etlStatus_Completed = "C";

	public static final String _aggAndCompInstStatus = "aggAndCompInstStatus";
	public static final String _aggAndCompInstStatus_Active = "A";
	public static final String _aggAndCompInstStatus_Inprogress = "I";
	public static final String _aggAndCompInstStatus_Completed = "C";
	public static final String _aggAndCompInstStatus_ERROR = "E";

	/** Configuration for getting db param as input **/

	// Response
	public static final String _etlLs = "eltLs";

	// Request
	public static final String _requestMessage = "requestMessage";

	public static final int _schedulerQueryLimit = 500;
	public static final int _aggregateInitiatorQueryLimit = 5000;

	public static final String _aggregateAndComputeRef = "aggregateAndCompute";
	public static final String _etlConfigurationRef = "etlConfiguration";
	public static final String _csvDataHandlerRef = "csvDataHandler";
	public static final String _jsonDataHandlerRef = "jsonDataHandler";
	public static final String _queueDataHandlerRef = "queueDataHandler";
	public static final String _xmlDataHandlerRef = "xmlDataHandler";
	public static final String _instanceInitiatorHandlerHandlerRef = "instanceInitiatorHandler";

	public static final String _aggregatedAndComputedDateMap = "aggregateAndComputedate";
	public static final String _hashCode = "hashCode";
	public static final String _aggregatedAndComputedDateMonth = "month";
	public static final String _aggregatedAndComputedDateDay = "day";
	public static final String _aggregatedAndComputedDateYear = "year";

	/** Spring bean ref **/
	public static final String _mongoTemplate_Bean_Ref = "mongoTemplate";
	public static final String _aggregateAndCompute_Bean_Ref = "aggregateAndCompute";
	public static final String _aggregateTaskExecutor_Bean_Ref = "aggregateTaskExecutor";

	public static final String _aggregateInitiatorTaskExecutor_Bean_Ref = "aggregateInitiatorTaskExecutor";

	public static final int _Agg_Scheduler_Limit = 7000;
	public static final String _Agg_Processor_Queue_capacity = "queueCapacity";
	public static final String _Agg_Initiator_Queue_capacity = "queueCapacity";
	public static final String _Min_No_Of_Agg_Instances = "minNoOfAggInstance";;

	// Index
	public static final String _System_Index_keys = "/indexKeys.json";
	public static final String _System_Configuration_Data = "/configuration.json";

	public static final String _keys = "keys";
	public static final String _key = "key";
	public static final String _value = "value";
	public static final String _masterCollections = "masterCollections";
	public static final String _query = "query";
	public static final String _project = "project";
	public static final String _collectionName = "collectionName";
	public static final String _Master_Instances_Schedule = "Master_Instances_Schedule";


//BAR Chart API Constants
	
	public static final String _barChartAPIaggregate="aggregate";
	public static final String _barChartAPIls="ls";
	public static final String _barChartAPIOutputType="outputType";
	public static final String _barChartAPIIdGrouop="_id.group";
	public static final String _barChartAPIValues="values";
	public static final String _barChartAPIX="X";
	public static final String _barChartAPIY="Y";
	public static final String _barChartAPITextX="textX";
	public static final String _dfn_Map_Data="Dfn_Map_Data";
	public static final String _dfn_Map_Mapping="Dfn_Map_Mapping";
	public static final String _db_query="query";
	public static final String _db_fields="fields";
	
	

}

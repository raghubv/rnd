package com.os.eventMapping.dao;

import java.util.Map;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * EventMapping: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 16, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface EventMappingDao {
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. addEventMapping
		* 
		* </pre> 
		*
		* @return String
	 */
	public String addEventMapping(Map<Object, Object> requestPayload);
	
	
	
}

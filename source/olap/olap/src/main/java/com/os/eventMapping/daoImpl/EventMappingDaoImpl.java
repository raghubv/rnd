package com.os.eventMapping.daoImpl;

import static com.os.util.Constants._EventMappingDfn;
import static com.os.util.Constants._eventName;
import static com.os.util.Constants._requestMessage;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.os.eventMapping.dao.EventMappingDao;
import com.os.util.Constants.responseCode;
import com.os.util.LoggerTrace;
import com.os.util.MessageUtility;
import com.mongodb.BasicDBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * EventMappingImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 16, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class EventMappingDaoImpl implements EventMappingDao {

	@Autowired
	MongoTemplate mongoTemplate;

	static final Logger log = Logger.getLogger(EventMappingDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.os.eventMapping.dao.EventMappingDao#addEventMapping(java.
	 * util.Map)
	 */
	public String addEventMapping(Map<Object, Object> requestPayload) {
		try {
			Map requestMessage = (Map) requestPayload.get(_requestMessage);
			mongoTemplate.getCollection(_EventMappingDfn).update(
					new BasicDBObject(_eventName,
							requestMessage.get(_eventName)),
					new BasicDBObject(requestMessage), true, false);
			return MessageUtility.response(responseCode.Success);
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(responseCode.Add_Event_Mapping_Failed);
	}
}

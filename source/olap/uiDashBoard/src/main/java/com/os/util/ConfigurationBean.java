package com.os.util;

import java.util.Map;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 13, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ConfigurationBean {
	
	private Map configurationMap;

	
	public ConfigurationBean(Map configurationMap) {
		super();
		this.configurationMap = configurationMap;
	}

	public Map getConfigurationMap() {
		return configurationMap;
	}

	public void setConfigurationMap(Map configurationMap) {
		this.configurationMap = configurationMap;
	}
	
	
}

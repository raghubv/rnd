package com.os.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.os.restService.RestClient;
import com.os.util.Configuration;
import com.os.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/workflow")
public class WorkFlowController extends BaseController {

	static final Logger log = Logger.getLogger(WorkFlowController.class);

	static private enum viewVolumeType {
		Year, Month, Week, Day
	};

	static private enum viewByType {
		Value, Quantity, Volume
	};

	public WorkFlowController() {
		super();
		setSuccessView("workflow.view");
		setRedirectView("redirect:/wvieworkflow/workflow.do");
	}

	@RequestMapping(value = "/view.do", method = RequestMethod.GET)
	public String add(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = "/getProductSells.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getProductSells(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			switch (viewByType.valueOf((String) requestJson.get("searchByYValue"))) {

			case Quantity:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_week_statsByQty")));
				case Month:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_month_statsByQty")));
				case Year:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_year_statsByQty")));
				case Day:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_day_statsByQty")));
				}
			case Value:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_week_statsByVal")));
				case Month:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_month_statsByVal")));
				case Year:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_year_statsByVal")));
				case Day:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_day_statsByVal")));

				}

			}
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getMasterInfo.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getMasterInfo(@RequestBody String request) {
		try {
			return getInfoMaster(request);
		} catch (Exception e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/flowConfigure.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String flowConfigure(@RequestBody String request) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);

			Map serviceRequest = new HashMap();
			serviceRequest.put("operationType", "create");
			serviceRequest.put("action", "create");
			serviceRequest.put("event", requestJson.get("eventName"));
			serviceRequest.put("genericType", "yes");

			List<String> qlsPos = (List<String>) requestJson.get("qterms");
			List<Integer> qlsPosIds = new ArrayList<Integer>();

			for (String string : qlsPos) {
				qlsPosIds.add(Integer.valueOf(string));
			}
			Map mapQterms = new HashMap();
			mapQterms.put("position_id", qlsPosIds);
			serviceRequest.put("qterms", mapQterms);

			List<String> clsPos = (List<String>) requestJson.get("createdBy");
			List<Integer> clsPosIds = new ArrayList<Integer>();

			for (String string : clsPos) {
				clsPosIds.add(Integer.valueOf(string));
			}
			serviceRequest.put("workflow", true);
			serviceRequest.put("workflowname", requestJson.get("workflowName"));

			//

			List<Map> ls = (List<Map>) requestJson.get("ls");

			Map approvals = new HashMap();
			Map approvalDtls = new HashMap();
			Map notificationlDtls = new HashMap();
			
			Map defaultNotifier = new HashMap();
			
			 
			List<String> defaultNotifierLS=(List<String>)requestJson.get("notification_position_0");
			List sendToLs=new ArrayList();
			for (String string : defaultNotifierLS) {
				
				Map sendTo= new HashMap();
				sendTo.put("position",Integer.valueOf(string));
				sendTo.put("ref_org","source");
				Map sendToLinkedOrg= new HashMap();
				
				List orgTypeLs= new ArrayList();
				orgTypeLs.add(Integer.valueOf((String)requestJson.get("notification_link_org_type_0")));
				sendToLinkedOrg.put("org_type", orgTypeLs);
				sendTo.put("linked_org",sendToLinkedOrg);
				
				sendTo.put("is_global",Boolean.valueOf((String)requestJson.get("notification_is_global_0")));
				sendTo.put("future_task",Boolean.valueOf((String)requestJson.get("notification_future_task_0")));
				Map sendToNotification = new HashMap();
				sendToNotification.put("email", (String)requestJson.get("notification_email_0"));
				sendToNotification.put("sms", (String)requestJson.get("notification_sms_0"));
				sendToNotification.put("template_name", (String)requestJson.get("notification_template_0"));
				sendTo.put("notification",sendToNotification);
				sendToLs.add(sendTo);
				
			}
			defaultNotifier.put("send_to", sendToLs);
			
			notificationlDtls.put("notifier_0", defaultNotifier);
			
			int i = 1;
			for (Map map : ls) {

				// Approvals
				Map header = (Map) map.get("header");
				Map approve = new HashMap();
				Map approve_notification = new HashMap();
				approve_notification.put("send", Boolean.valueOf((String) header.get("header_notification_" + i)));
				approve_notification.put("send_to", "notifier_" + i);
				approve.put("notification", approve_notification);
				List approveByLs = new ArrayList();

				List<String> headerApprovedLs = (List<String>) header.get("header_approval_position_" + i);
				for (String string : headerApprovedLs) {
					Map approvedBy = new HashMap();
					approvedBy.put("position", Integer.valueOf(string));

					Map linked_orgMap = new HashMap();
					List headerOrgTypeList = new ArrayList();
					headerOrgTypeList.add(Integer.valueOf((String) header.get("header_organization_type_" + i)));
					linked_orgMap.put("org_type", headerOrgTypeList);

					approvedBy.put("linked_org", linked_orgMap);
					approvedBy.put("ref_org", "source");
					approvedBy.put("is_global", Boolean.valueOf((String) header.get("header_global_" + i)));
					approveByLs.add(approvedBy);
				}

				approve.put("approved_by", approveByLs);
				approvals.put("approval_" + i, approve);

				// Approval Details
				{
					Map approveDtls = (Map) map.get("approve");
					approvalDtls.put("wf_config_id", 1);
					Map workFlowInitMap = new HashMap();
					workFlowInitMap.put("class", "com.os.service.task.Check");
					workFlowInitMap.put("taskType", approveDtls.get("approvals_activity_" + i));
					workFlowInitMap.put("nextFlow", "approval_1");
					approvalDtls.put("init", workFlowInitMap);

					Map approval = new HashMap();
					approval.put("activity", approveDtls.get("approvals_activity_" + i));
					approval.put("taskName", approveDtls.get("approvals_task_name_" + i));
					approval.put("taskCreateClass", "com.os.workflow.listeners.impl.UserInProgressListener");
					approval.put("isGroupYTask", "");
					Map subApprove = new HashMap();
					subApprove.put("nextFlow",
							((String) approveDtls.get("approvals_approve_next_flow_" + i)).equalsIgnoreCase("end")
									? "end" : "approval_" + i);
					subApprove.put("approveClass", approveDtls.get("approvals_approveClass_" + i));
					approval.put("approve", subApprove);
					Map subReject = new HashMap();
					subReject.put("nextFlow",
							((String) approveDtls.get("approvals_reject_next_flow_" + i)).equalsIgnoreCase("end")
									? "end" : "approval_" + i);
					subReject.put("rejectClass", approveDtls.get("approvals_rejectClass_" + i));
					approval.put("reject", subReject);

					approvalDtls.put("approval_" + i, approval);

				}

				// Notification
				{
					Map notificationDtls = (Map) map.get("notification");
				
					Map notifier= new HashMap();
					List nLs= new ArrayList();
					nLs.add("initiater");
					notifier.put("notify_to", nLs);
					notifier.put("future_task", Boolean.valueOf((String)notificationDtls.get("notifications_future_task_"+i)));
					Map notificationMap= new HashMap();
					notificationMap.put("email", Boolean.valueOf((String)notificationDtls.get("notifications_email_"+i)));
					notificationMap.put("sms", Boolean.valueOf((String)notificationDtls.get("notifications_sms_"+i)));
					Map notificationApproveMap= new HashMap();
					notificationApproveMap.put("template_name", (String)notificationDtls.get("notifications_approve_template_"+i));
					notificationMap.put("approve", notificationApproveMap);
					Map notificationRejectMap= new HashMap();
					notificationRejectMap.put("template_name", (String)notificationDtls.get("notifications_reject_template_"+i));
					notificationMap.put("reject", notificationRejectMap);
					notifier.put("notification", notificationMap);
				
					
					notificationlDtls.put("notifier_"+i, notifier);	
					
				}
				i++;
			}
			serviceRequest.put("approvals", approvals);
			serviceRequest.put("wf_conf_dtls", approvalDtls);
			serviceRequest.put("wf_notification_dtls", notificationlDtls);

			System.out.println("serviceRequest>>" + serviceRequest);

			return callFlowConfigure(objectMapper.writeValueAsString(serviceRequest));
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}

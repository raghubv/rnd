package com.os.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.os.util.Configuration;
import com.os.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/stcok")
public class StockController extends BaseController {

	static final Logger log = Logger.getLogger(StockController.class);

	static private enum viewVolumeType {
		Year, Month, Week, Day
	};

	static private enum viewByType {
		Region, Entity
	};

	public StockController() {
		super();
		setSuccessView("stockregion.view");
		setRedirectView("redirect:/stcok/region.do");
	}

	@RequestMapping(value = "/region.do", method = RequestMethod.GET)
	public String add(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = "/getRegionsStockRatio.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getRegionsStockRatio(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			return getAggregatedData(
					objectMapper.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_ratio")));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getInStock.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getInStock(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			switch (viewByType.valueOf((String) requestJson.get("searchByYValue"))) {

			case Entity:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_in_week")));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_in_month")));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_in_year")));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_in_day")));
				}
			case Region:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_in_week")));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_in_month")));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_in_year")));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_in_day")));
				}

			}
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getOutStock.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getOutStock(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			switch (viewByType.valueOf((String) requestJson.get("searchByYValue"))) {

			case Entity:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_out_week")));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_out_month")));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_out_year")));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_out_day")));
				}
			case Region:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_out_week")));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_out_month")));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_out_year")));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(Configuration.getReuqestData().get("stock_by_region_out_day")));
				}

			}
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getEntityStock.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getEntityStock(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			return getAggregatedData(
					objectMapper.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity")));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getEntityStockIn.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getEntityStockIn(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			return getAggregatedData(
					objectMapper.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_in")));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getEntityStockOut.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getEntityStockOut(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			return getAggregatedData(
					objectMapper.writeValueAsString(Configuration.getReuqestData().get("stock_by_entity_out")));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

}

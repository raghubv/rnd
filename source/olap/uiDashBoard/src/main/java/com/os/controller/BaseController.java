
package com.os.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.os.restService.RestClient;
import com.os.util.Configuration;
import com.os.util.ConfigurationBean;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class BaseController {
	private String successView;
	private String failureView;
	private String redirectView;
	static final Logger log = Logger.getLogger(BaseController.class);

	@Autowired
	private ConfigurationBean configurationBean;

	public BaseController setSuccessView(String success) {
		this.successView = success;
		return this;
	}

	public BaseController setFailureView(String failure) {
		this.failureView = failure;
		return this;
	}

	public BaseController setRedirectView(String redirect) {
		this.redirectView = redirect;
		return this;
	}

	public final String getSuccessView() {
		return this.successView;
	}

	public final String getRedirectView() {
		return this.redirectView;
	}

	public final String getFailureView() {
		if (this.failureView == null || this.failureView.length() == 0) {
			return getSuccessView();
		}
		return this.failureView;
	}

	protected static synchronized String getAggregatedData(String servicerequest) {
		log.error("[servicerequest]" + servicerequest);
		return RestClient.post("dpc", "password", "http://localhost:8080/olap/rest/json/aggregate/getAggregatedData",
				servicerequest);
	}

	protected static synchronized String getmap(String jsName) {
		log.error("[jsName]" + jsName);
		return RestClient.get("dpc", "password", "http://localhost:8080/olap/rest/json/getmap/" + jsName);
	}

	protected static synchronized String findOne(String servicerequest) {
		log.error("[servicerequest]" + servicerequest);
		return RestClient.post("dpc", "password", "http://localhost:8080/olap/rest/json/dbquery/findOne",
				servicerequest);
	}

	protected static synchronized String post(String url, String servicerequest) {
		log.error("[servicerequest]" + servicerequest);
		return RestClient.post("dpc", "password", url, servicerequest);
	}

	protected  String getInfoMaster(String servicerequest) {
		log.error("[servicerequest]" + servicerequest);
		return RestClient.post("snoc", "password",
				"http://localhost:8080/workflowRS/rest/workflowRS/json/getInfoMaster", servicerequest);
	}

	protected String callFlowConfigure(String servicerequest) {
		log.error("[servicerequest]" + servicerequest);

		return RestClient.post((String)configurationBean.getConfigurationMap().get("callFlowConfigure_password"),
				(String)configurationBean.getConfigurationMap().get("callFlowConfigure_user"),
				(String)configurationBean.getConfigurationMap().get("callFlowConfigure"), servicerequest);
	}

}

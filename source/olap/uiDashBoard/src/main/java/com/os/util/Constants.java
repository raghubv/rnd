package com.os.util;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * os 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 17, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface Constants {
 
	
	/**Controller Repsonse Header**/
	public static final String _HEADER_JSON="Accept=application/json";
	public static final String _RES_JSON_UTF8="application/json; charset=utf-8";
	public static final String _API= "api";
	public static final String _salesdashboardControllerfindOneQuery="/findOne.do";
	public static final String _salesdashboardControllergetData="/getData.do";
	public static final String _salesdashboardControllerview="/view.do";
	public static final String _salesdashboardControllerPath="/salesdashboard";

}

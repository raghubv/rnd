/**
 * 
 */
var stock = function() {
	return {
		init : function() {
			$("#range_stat_day").click(function() {
				$('#searchByXValue').val("Day");
				resetDisplay();
			});
			$("#range_stat_week").click(function() {
				$('#searchByXValue').val("Week");
				resetDisplay();
			});
			$("#range_stat_month").click(function() {
				$('#searchByXValue').val("Month");
				resetDisplay();
			});
			$("#range_stat_year").click(function() {
				$('#searchByXValue').val("Year");
				resetDisplay();
			});
			$("#statsByRegion").click(function() {
				$('#searchByYValue').val("Value");
				resetDisplay();
			});
			$("#statsByEntity").click(function() {
				$('#searchByYValue').val("Entity");
				resetDisplay();
				
			});
			
			function resetDisplay(){
				$('#vmap_world').vectorMap('deselect', 'ua');
				$('#stockDisplay').css({
					'display' : 'none'
				});
			}
			function getStock() {
				$('#stockDisplay').css({
					'display' : 'block'
				});
				
				var searchByYValue = $('#searchByYValue').val();
				var searchByXValue = $('#searchByXValue').val();

				var request = {
					"searchByYValue" : searchByYValue,
					"searchByXValue" : searchByXValue
				};
				GlobalAjaxCall.callController('getInStock.do', request,
						function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							nv
									.addGraph(function() {
										var chart = nv.models.multiBarChart().showLegend(true).showControls(true);
										chart.xAxis.axisLabel($('#searchByXValue').val());
										chart.yAxis.axisLabel($('#searchByYValue').val()).tickFormat(d3.format(',.0d'));
									
										d3.select('#chart1 svg').datum(ls)
												.transition().duration(700)
												.call(chart);
										nv.utils.windowResize(chart.update);
										return chart;
									});
						});
				      
						
				
				
				
				var searchByYValue = $('#searchByYValue').val();
				var searchByXValue = $('#searchByXValue').val();
				var request = {
					"searchByYValue" : searchByYValue,
					"searchByXValue" : searchByXValue
				};
				GlobalAjaxCall.callController('getOutStock.do', request,
						function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));

							var ls = obj.responseMessage.ls;

							nv
									.addGraph(function() {
										var chart = nv.models.multiBarChart().showLegend(true).showControls(true);
										
										//var chart = nv.models.cumulativeLineChart().showLegend(true).showControls(true);
										d3.select(".nv-legendWrap").style(
												"float", "right");
										chart.xAxis.axisLabel($('#searchByXValue').val());
										chart.yAxis.axisLabel($('#searchByYValue').val()).tickFormat(d3.format(',.0d'));
										
										d3.select('#chart2 svg').datum(ls)
												.transition().duration(700)
												.call(chart);
										
										nv.utils.windowResize(chart.update);
										return chart;
									});
						});
				     
				
				
			}

			
		

			$(function() {
				var map;
				jQuery('#vmap_world').vectorMap(
						{
							map : 'world_en',
							regionsSelectable : true,
							backgroundColor : '#a5bfdd',
							borderColor : '#818181',
							borderOpacity : 0.25,
							borderWidth : 1,
							color : '#f4f3f0',
							enableZoom : true,
							hoverColor : '#c9dfaf',
							hoverOpacity : null,
							normalizeFunction : 'linear',
							scaleColors : [ '#b6d6ff', '#005ace' ],
							selectedColor : '#666666',
							selectedRegion : null,
							showTooltip : true,
							onRegionClick : function(element, code, region) {
								$('#vmap_world').vectorMap('deselect', code);
								$('#vmap_world').vectorMap('select', code);

								if (code.toUpperCase() == 'UA') {
								
									getStock();

								} else {
									resetDisplay();
								}
							},
							onRegionSelected : function() {
								if (window.localStorage) {
									window.localStorage.setItem(
											'jvectormap-selected-regions',
											JSON.stringify(map
													.getSelectedRegions()));
								}

							}
						});
				
				$('#vmap_world').vectorMap('select', 'ua');
				

			});
			getStock();
		}

	};
}();
var RevenueLoss = function () {

    return {

        // main function to initiate the module
        init: function () {
        	
        
			
			
        	$("#statsByLine").click(function() {
				$('#statsBy').val("Line");
				
			});
			$("#statsByBar").click(function() {
				$('#statsBy').val("Bar");
			
			});
			
            
            if (!jQuery().dataTable) {
                return;
            }

           
            $(function() {
				var map;

				jQuery('#vmap_world').vectorMap(
						{
							map : 'world_en',
							regionsSelectable : true,
							backgroundColor : '#a5bfdd',
							borderColor : '#818181',
							borderOpacity : 0.25,
							borderWidth : 1,
							color : '#f4f3f0',
							enableZoom : true,
							hoverColor : '#c9dfaf',
							hoverOpacity : null,
							normalizeFunction : 'linear',
							scaleColors : [ '#b6d6ff', '#005ace' ],
							selectedColor : '#666666',
							selectedRegion : null,
							showTooltip : true,
							onRegionClick : function(element, code, region) {
								$('#vmap_world').vectorMap('deselect', code);
								$('#vmap_world').vectorMap('select', code);
								
								
								
								if (code.toUpperCase() == 'UA') {
									var request={};
									
						
										
										var data={
												"responseMessage": {
													"ls": [{
														"values": [{
															"x": 6,
															"y": 1000
														},
														{
															"x": 7,
															"y": 1500
														},
														{
															"x": 8,
															"y": 1250
														}],
														"color": "#3D79EE",
														"key": "Total Sold"
													},
													{
														"values": [{
															"x": 6,
															"y": 100
														},
														{
															"x": 7,
															"y": 15
														},
														{
															"x": 8,
															"y": 50
														}],
														"color": "#0AC727",
														"key": "Not activated"
													},
													{
														"values": [{
															"x": 6,
															"y": 50
														},
														{
															"x": 7,
															"y": 25
														},
														{
															"x": 8,
															"y": 100
														}],
														"color": "#53EDE4",
														"key": "Zero Recharge"
													}]
												},
												"responseCode": "0",
												"responseDesc": "Your request processed successfully!"
											};
										
										
										$("#user_portlet_body_pie").css({
											'display' : 'block'
										});
										
										$( "#chart1 svg" ).empty();
										var obj = jQuery.parseJSON(JSON.stringify(data));
										var ls = obj.responseMessage.ls;
										nv
												.addGraph(function() {
													var chart ;
													
													if($('#statsBy').val()=='Bar')
													chart = nv.models.multiBarChart().showLegend(true).showControls(true);
													else
													chart =  nv.models.cumulativeLineChart().showLegend(true).showControls(true);
										            
													
													 chart.xAxis.axisLabel('Month');
													chart.yAxis.axisLabel('Count').tickFormat(d3.format(',.0d'));
													//chart.rotateLabels(-30);
													chart.yAxis.rotateLabels(-45)
													d3.select('#chart1 svg').datum(ls)
															.transition().duration(700)
															.call(chart);
													nv.utils.windowResize(chart.update);
													return chart;
												});
									
								

										
									
										
								} 

							},
							onRegionSelected : function() {
								if (window.localStorage) {
									window.localStorage.setItem(
											'jvectormap-selected-regions',
											JSON.stringify(map
													.getSelectedRegions()));
								}

							}
						});
				map.setSelectedRegions(JSON.parse(window.localStorage
						.getItem('jvectormap-selected-regions')
						|| '[]'));

			});
            
            
        }

    };
  

}();
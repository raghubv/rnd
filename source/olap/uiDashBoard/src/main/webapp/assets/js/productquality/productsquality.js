/**
 * 
 */
var ProductQuality = function() {
	return {
		init : function() {

			function resetInfo() {
				$('#productcatdashboard').css({
					'display' : 'none'
				});
				$('#productDashboard').css({
					'display' : 'none'
				});
			}
			$("#range_stat_day").click(function() {
				$('#searchByXValue').val("Day");
				resetInfo();
			});
			$("#range_stat_week").click(function() {
				$('#searchByXValue').val("Week");
				resetInfo();
			});
			$("#range_stat_month").click(function() {
				$('#searchByXValue').val("Month");
				resetInfo();
			});
			$("#range_stat_year").click(function() {
				$('#searchByXValue').val("Year");
				resetInfo();
			});

			$("#statsByValue").click(function() {
				$('#searchByYValue').val("Return");
				resetInfo();
			});
			function getProductsells() {
				$('#productcatdashboard').css({
					'display' : 'block'
				});
				$( "#productDashboard" ).focus();
				var searchByYValue = $('#searchByYValue').val();
				var searchByXValue = $('#searchByXValue').val();

				var request = {
					"searchByYValue" : searchByYValue,
					"searchByXValue" : searchByXValue
				};
				GlobalAjaxCall.callController('getProductSells.do', request,
						function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							nv
									.addGraph(function() {
										var chart = nv.models.multiBarChart().showLegend(false).showControls(false);
										chart.xAxis.axisLabel($('#searchByXValue').val());
										chart.yAxis.axisLabel($('#searchByYValue').val()).tickFormat(d3.format(',.0d'));
										//chart.rotateLabels(-30);
										chart.yAxis.rotateLabels(-45)
										d3.select('#chart1 svg').datum(ls)
												.transition().duration(700)
												.call(chart);
											
										d3.selectAll(".nv-bar").on("click",
												function(d) {
													getProductSellsSub(d);
												});
										
										nv.utils.windowResize(chart.update);
										return chart;
									});
						});
			}

			function getProductSellsSub(d) {
				$('#productDashboard').css({
					'display' : 'block'
				});
				$( "#productDashboard" ).focus();
				
				var searchByYValue = $('#searchByYValue').val();
				var searchByXValue = $('#searchByXValue').val();
				var request = {
					"searchByYValue" : searchByYValue,
					"searchByXValue" : searchByXValue,
					"key" : d.key
				};
				GlobalAjaxCall.callController('getProductSellsSub.do', request,
						function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));

							var ls = obj.responseMessage.ls;

							nv
									.addGraph(function() {
										var chart = nv.models.multiBarChart().showLegend(false).showControls(false);
										d3.select(".nv-legendWrap").style(
												"float", "right");
										//chart.rotateLabels(-30);
										chart.yAxis.rotateLabels(-45);
										
										chart.xAxis.axisLabel($('#searchByXValue').val());
										chart.yAxis.axisLabel($('#searchByYValue').val()).tickFormat(d3.format(',.0d'));
										
										d3.select('#chart2 svg').datum(ls)
												.transition().duration(700)
												.call(chart);
										
										nv.utils.windowResize(chart.update);
										return chart;
									});
						});
			}
			$("#statsByQty").click(function() {
				$('#searchByYValue').val("Damaged");
				resetInfo();
			});

			$(function() {
				var map;

				jQuery('#vmap_world').vectorMap(
						{
							map : 'world_en',
							regionsSelectable : true,
							backgroundColor : '#a5bfdd',
							borderColor : '#818181',
							borderOpacity : 0.25,
							borderWidth : 1,
							color : '#f4f3f0',
							enableZoom : true,
							hoverColor : '#c9dfaf',
							hoverOpacity : null,
							normalizeFunction : 'linear',
							scaleColors : [ '#b6d6ff', '#005ace' ],
							selectedColor : '#666666',
							selectedRegion : null,
							showTooltip : true,
							onRegionClick : function(element, code, region) {
								$('#vmap_world').vectorMap('deselect', code);
								$('#vmap_world').vectorMap('select', code);

								if (code.toUpperCase() == 'UA') {
									$('#productcatdashboard').css({
										'display' : 'block'
									});
									getProductsells();

								} else {
									$('#productcatdashboard').css({
										'display' : 'none'
									});
									$('#productDashboard').css({
										'display' : 'none'
									});
								}

								// var message = 'You clicked "' + region
								// + '" which has the code: '
								// + code.toUpperCase();

								// alert(message);
							},
							onRegionSelected : function() {
								if (window.localStorage) {
									window.localStorage.setItem(
											'jvectormap-selected-regions',
											JSON.stringify(map
													.getSelectedRegions()));
								}

							}
						});
				
				$('#vmap_world').vectorMap('select', 'ua');
				map.setSelectedRegions(JSON.parse(window.localStorage
						.getItem('jvectormap-selected-regions')
						|| '[]'));

			});
			getProductsells();
		}

	};
}();
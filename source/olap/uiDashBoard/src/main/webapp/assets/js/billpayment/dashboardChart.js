var DashBoardChart = function() {

	return {

		initDashBoard : function() {
			var form2 = $('#form_sample_2');

			form2.validate({
				errorElement : 'span', // default input error message container
				errorClass : 'help-inline', // default input error message class
				focusInvalid : false, // do not focus the last invalid input
				ignore : "",
				rules : {
					creditcard : {
						required : true,
						creditcard : true
					},
					name : {
						minlength : 2,
						required : true
					},
					cardAmount : {
						minlength : 2,
						required : true,
						number : true
					}
				},

				invalidHandler : function(event, validator) { // display error
					// alert on form
					// submit
					success1.hide();
					error1.show();
					App.scrollTo(error1, -200);
				},

				highlight : function(element) { // hightlight error inputs
					$(element).closest('.help-inline').removeClass('ok'); // display
					// OK
					// icon
					$(element).closest('.control-group').removeClass('success')
							.addClass('error'); // set error class to the
					// control group
				},

				unhighlight : function(element) { // revert the change dony by
					// hightlight
					$(element).closest('.control-group').removeClass('error'); // set
					// error
					// class
					// to
					// the
					// control
					// group
				},

				success : function(label) {
					label.addClass('valid').addClass('help-inline ok') // mark
					// the
					// current
					// input
					// as
					// valid
					// and
					// display
					// OK
					// icon
					.closest('.control-group').removeClass('error').addClass(
							'success'); // set success class to the control
					// group
				},

				submitHandler : function(form) {
					success1.show();
					error1.hide();
				}
			});

			var form1 = $('#form_sample_1');

			form1.validate({
				errorElement : 'span', // default input error message container
				errorClass : 'help-inline', // default input error message class
				focusInvalid : false, // do not focus the last invalid input
				ignore : "",
				rules : {
					payAmount : {
						minlength : 2,
						required : true,
						number : true
					}
				},

				invalidHandler : function(event, validator) { // display error
					// alert on form
					// submit
					success1.hide();
					error1.show();
					App.scrollTo(error1, -200);
				},

				highlight : function(element) { // hightlight error inputs
					$(element).closest('.help-inline').removeClass('ok'); // display
					// OK
					// icon
					$(element).closest('.control-group').removeClass('success')
							.addClass('error'); // set error class to the
					// control group
				},

				unhighlight : function(element) { // revert the change dony by
					// hightlight
					$(element).closest('.control-group').removeClass('error'); // set
					// error
					// class
					// to
					// the
					// control
					// group
				},

				success : function(label) {
					label.addClass('valid').addClass('help-inline ok') // mark
					// the
					// current
					// input
					// as
					// valid
					// and
					// display
					// OK
					// icon
					.closest('.control-group').removeClass('error').addClass(
							'success'); // set success class to the control
					// group
				},

				submitHandler : function(form) {
					success1.show();
					error1.hide();
				}
			});

			$("#billPaySumbit")
					.click(
							function() {

								if (!($("#BILLED_AMOUNT").val() >= $(
										"#payAmount").val() && $(
										"#MINIMUM_PAYABLE_AMOUNT").val() <= $(
										"#MINIMUM_PAYABLE_AMOUNT").val()))
									alert("Pay amount should be between billed amount & minimum payable amount");

								$("#DETAILS").css({
									'display' : 'none'
								});
								$("#BIIL_DETAILS").css({
									'display' : 'none'
								});
								$("#CREDIT_CARD_DETAILS").css({
									'display' : 'block'
								});
								$("#cardAmount").val($("#payAmount").val());

							});

			$("#creditCardPaySumbit").click(function() {
				$("#DETAILS").css({
					'display' : 'block'
				});
				$("#BIIL_DETAILS").css({
					'display' : 'none'
				});
				$("#CREDIT_CARD_DETAILS").css({
					'display' : 'none'
				});
			
				$("#paymentSuccess").css({
					'display' : 'block'
				});

			});

			$("#billSearchSumbit")
					.click(
							function() {
								
								$("#paymentSuccess").css({
									'display' : 'none'
								});
								var qry = "SELECT * FROM  MS_BILL_PAYMENT WHERE ACCOUNT_NUMBER ="
										+ $("#accountNumber").val()
										+ " AND MOBILE_NUMBER ="
										+ $("#mobileNumber").val();

								var req = {
									"sqlCmd" : qry
								}

								GlobalAjaxCall
										.callController(
												'postMethod.do',
												req,
												function(data) {
													$("#BIIL_DETAILS").css({
														'display' : 'block'
													});

													var obj = jQuery
															.parseJSON(JSON
																	.stringify(data));
													var ls = obj.response;
													if (ls.length > 0) {
														var res = ls[0]
														$("#BILLED_AMOUNT")
																.val(
																		res['BILLED_AMOUNT']);
														$(
																"#MINIMUM_PAYABLE_AMOUNT")
																.val(
																		res['MINIMUM_PAYABLE_AMOUNT']);
														$("#SIM_NUMBER")
																.val(
																		res['SIM_NUMBER']);
														$("#SIM_STATUS")
																.val(
																		res['SIM_STATUS']);
														$("#payAmount")
																.val(
																		res['MINIMUM_PAYABLE_AMOUNT']);

													}

												});

							});
		}

	};

}();
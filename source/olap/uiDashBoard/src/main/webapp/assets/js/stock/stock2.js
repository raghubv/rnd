var TableAdvanced = function () {



     var initStockRegion = function() {
        var oTable = $('#sample_2').dataTable( {           
            "aoColumnDefs": [
                { "aTargets": [ 0 ] }
            ],
            "aaSorting": [[1, 'asc']],
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
        });

        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    };

    var initStockEntity = function() {
        var oTable = $('#sample_3').dataTable( {           
            "aoColumnDefs": [
                { "aTargets": [ 0 ] }
            ],
            "aaSorting": [[1, 'asc']],
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
        });

        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    };
    return {

        //main function to initiate the module
        init: function () {
        	
        	$("#statsByRegion").click(function() {
				$('#searchByValue').val("Region");
			
				$("#region_stock_portlet-body").css({
					'display' : 'none'
				});
				$("#entity_stock_portlet-body").css({
					'display' : 'none'
				});
			});
			$("#statsByEntity").click(function() {
				$('#searchByValue').val("Entity");
				
				$("#region_stock_portlet-body").css({
					'display' : 'none'
				});
				$("#entity_stock_portlet-body").css({
					'display' : 'none'
				});
			});
			
			
            
            if (!jQuery().dataTable) {
                return;
            }

           
            $(function() {
				var map;

				jQuery('#vmap_world').vectorMap(
						{
							map : 'world_en',
							regionsSelectable : true,
							backgroundColor : '#a5bfdd',
							borderColor : '#818181',
							borderOpacity : 0.25,
							borderWidth : 1,
							color : '#f4f3f0',
							enableZoom : true,
							hoverColor : '#c9dfaf',
							hoverOpacity : null,
							normalizeFunction : 'linear',
							scaleColors : [ '#b6d6ff', '#005ace' ],
							selectedColor : '#666666',
							selectedRegion : null,
							showTooltip : true,
							onRegionClick : function(element, code, region) {
								$('#vmap_world').vectorMap('deselect', code);
								$('#vmap_world').vectorMap('select', code);

								if (code.toUpperCase() == 'UA') {
									var request={};
									
									if($('#searchByValue').val()== 'Region'){
									GlobalAjaxCall.callController('getRegionsStock.do', request,
											function(data) {
												var obj = jQuery.parseJSON(JSON.stringify(data));

											$("#region_stock_portlet-body").css({
												'display' : 'block'
											});
											$("#entity_stock_portlet-body").css({
												'display' : 'none'
											});
											$('#region_stock').empty();
												var ls = obj.responseMessage.ls;
												for(var i=0;i<ls.length;i++){
													var trow="<tr>"+
													"<td>"+ls[i]._id.country_name_v+"</td>"+
													"<td>"+ls[i]._id.region_name_v+"</td>"+
													"<td>"+ls[i].total_in_stock/ls[i].total_out_stock+"</td>"+
													"<td>"+ls[i].total_in_stock+"</td>"+
													"<td>"+ls[i].total_out_stock+"</td>"+
												   "</tr>";
													$('#region_stock').append(trow);
													
												}
												
												 initStockRegion();
											});
									}else if($('#searchByValue').val()== 'Entity'){
										GlobalAjaxCall.callController('getEntityStock.do', request,
												function(data) {
													var obj = jQuery.parseJSON(JSON.stringify(data));

												$("#entity_stock_portlet-body").css({
													'display' : 'block'
												});
												$("#region_stock_portlet-body").css({
													'display' : 'none'
												});	
												$('#entity_stock').empty();
													var ls = obj.responseMessage.ls;
													for(var i=0;i<ls.length;i++){
														var trow="<tr>"+
														"<td>"+ls[i].org_name_v+"</td>"+
														"<td>"+ls[i].org_type_name_v+"</td>"+
														"<td>"+ls[i].node_name_v+"</td>"+
														"<td>"+ls[i].stock_ratio+"</td>"+
														"<td>"+ls[i].total_in_stock+"</td>"+
														"<td>"+ls[i].total_out_stock+"</td>"+
													   "</tr>";
														$('#entity_stock').append(trow);
														console.log("*****"+trow);
													}
													
													initStockEntity();
												});
										}
										
								} 

								// var message = 'You clicked "' + region
								// + '" which has the code: '
								// + code.toUpperCase();

								// alert(message);
							},
							onRegionSelected : function() {
								if (window.localStorage) {
									window.localStorage.setItem(
											'jvectormap-selected-regions',
											JSON.stringify(map
													.getSelectedRegions()));
								}

							}
						});
				map.setSelectedRegions(JSON.parse(window.localStorage
						.getItem('jvectormap-selected-regions')
						|| '[]'));

			});
            
            
        }

    };
  

}();
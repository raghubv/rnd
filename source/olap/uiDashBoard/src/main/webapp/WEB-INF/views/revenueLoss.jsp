<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<link href="${context}/assets/css/nv.d3.css" rel="stylesheet"
	type="text/css">
<script src="${context}/assets/js/d3.min.js" charset="utf-8"></script>
<script src="${context}/assets/js/nv.d3.js"></script>
<script src="${context}/assets/js/stream_layers.js"></script>
<link rel="stylesheet" href="${context}/assets/ammap/ammap.css" type="text/css">
<script src="${context}/assets/ammap/ammap.js" type="text/javascript"></script>
<!-- map file should be included after ammap.js -->
<script src="${context}/assets/ammap/maps/js/worldLow.js"
	type="text/javascript"></script>
<script src="${context}/assets/ammap/maps/js/continentsLow.js"
	type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<script src="${context}/assets/js/revenueloss/revenueloss.js"
	type="text/javascript"></script>
<style>
text {
	font: 12px sans-serif;
}

svg {
	display: block;
}

html, body, #chart1, svg {
	margin: 0px;
	padding: 0px;
	height: 100%;
	width: 100%;
}
</style>
</head>
<input type="hidden" id="statsBy" name="statsBy" value="Bar" />
<input type="hidden" id="jvectormap-selected-regions"
	name="jvectormap-selected-regions" value="Year" />
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div id="portlet-config" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button"></button>
			<h3>Widget Settings</h3>
		</div>
		<div class="modal-body">Widget settings form goes here</div>
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="color-panel hidden-phone">
					<div class="color-mode-icons icon-color"></div>
					<div class="color-mode-icons icon-color-close"></div>
					<div class="color-mode">
						<p>THEME COLOR</p>
						<ul class="inline">
							<li class="color-black current color-default"
								data-style="default"></li>
							<li class="color-blue" data-style="blue"></li>
							<li class="color-brown" data-style="brown"></li>
							<li class="color-purple" data-style="purple"></li>
							<li class="color-grey" data-style="grey"></li>
							<li class="color-white color-light" data-style="light"></li>
						</ul>
						<label> <span>Layout</span> <select
							class="layout-option m-wrap small">
								<option value="fluid" selected>Fluid</option>
								<option value="boxed">Boxed</option>
						</select>
						</label> <label> <span>Header</span> <select
							class="header-option m-wrap small">
								<option value="fixed" selected>Fixed</option>
								<option value="default">Default</option>
						</select>
						</label> <label> <span>Sidebar</span> <select
							class="sidebar-option m-wrap small">
								<option value="fixed">Fixed</option>
								<option value="default" selected>Default</option>
						</select>
						</label> <label> <span>Footer</span> <select
							class="footer-option m-wrap small">
								<option value="fixed">Fixed</option>
								<option value="default" selected>Default</option>
						</select>
						</label>
					</div>
				</div>
				<!-- END BEGIN STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Dashboard <small>statistics and more</small>
				</h3>
				<ul class="breadcrumb">
					<li><i class="icon-home"></i> <a href="index.html">Home</a> <i
						class="icon-angle-right"></i></li>
					<li><a href="#">Dashboard</a></li>
					<li class="pull-right no-text-shadow">
						<div id="dashboard-report-range"
							class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"
							data-tablet="" data-desktop="tooltips" data-placement="top"
							data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i> <span></span> <i
								class="icon-angle-down"></i>
						</div>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>


		<!-- END PAGE HEADER-->
		<div id="dashboard">
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>
			<div class="row-fluid">
				<div class="span6">
					<!-- BEGIN REGIONAL STATS PORTLET-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-globe"></i>Stock
							</div>
							<div class="tools">
								<a href="" class="collapse"></a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="region_statistics_content">
								<div class="btn-toolbar">
									<div class="btn-group" data-toggle="buttons-radio">
										<a id="statsByBar" class="btn mini active">Bar</a> <a
											id="statsByLine" class="btn mini">Line</a>
									</div>
								</div>
								<div id="vmap_world" style="height: 400px;"></div>

							</div>
						</div>
					</div>
					<!-- END REGIONAL STATS PORTLET-->
				</div>


			<div class="span6"  id="user_portlet_body_pie"
				style="display: none">
				<div>
					<!-- BEGIN REGIONAL STATS PORTLET-->
					<div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">Revenue Loss</div>
							<div class="tools">
								<a href="" class="collapse"></a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="product_sells_statistics_content">

								<div id="chart1" style="height: 450px;">
									<svg> </svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- END PAGE CONTAINER-->
</div>
<script>
	$(document).ready(function() {
		RevenueLoss.init();
	});
</script>

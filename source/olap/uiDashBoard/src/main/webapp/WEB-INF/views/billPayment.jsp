<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<link href="${context}/assets/css/nv.d3.css" rel="stylesheet"
	type="text/css">
<script src="${context}/assets/js/d3.min.js" charset="utf-8"></script>
<script src="${context}/assets/js/nv.d3.js"></script>
<script src="${context}/assets/js/stream_layers.js"></script>
<link rel="stylesheet" href="${context}/assets/ammap/ammap.css"
	type="text/css">
<script src="${context}/assets/ammap/ammap.js" type="text/javascript"></script>
<!-- map file should be included after ammap.js -->
<%-- <script src="${context}/assets/ammap/maps/js/worldLow.js" --%>
<!-- 	type="text/javascript"></script> -->
<script src="/olap/js/worldLow.js" type="text/javascript"></script>
<script src="/olap/js/continentsLow.js" type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="${context}/assets/js/billpayment/dashboardChart.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/billpayment/dashboardMap.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/ajax/ajax.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="${context}/assets/plugins/gritter/css/jquery.gritter.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" media="screen" />
<link
	href="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
	href="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" />
<link rel="stylesheet" type="text/css"
	href="${context}/assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
<!-- END PAGE LEVEL STYLES -->
<style>
text {
	font: 12px sans-serif;
}

svg {
	display: block;
}

html, body, #chart1, svg {
	margin: 0px;
	padding: 0px;
	height: 100%;
	width: 100%;
}
</style>
</head>
<input type="hidden" id="statsBy" name="statsBy" value="Bar" />
<input type="hidden" id="jvectormap-selected-regions"
	name="jvectormap-selected-regions" value="Year" />
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div id="portlet-config" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button"></button>
			<h3>Widget Settings</h3>
		</div>
		<div class="modal-body">Widget settings form goes here</div>
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="row-fluid">
		<div class="span12">
			<div class="clearfix"></div>
		</div>
	</div>

	<!-- BEGIN PAGE CONTAINER-->


	<div class="container-fluid">
		<div id="dashboard">
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>

			<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i>Bill Payment
					</div>

				</div>
				<div class="portlet-body">
					<!-- BEGIN REGIONAL STATS PORTLET-->
					<div class="row-fluid form-horizontal"  id="DETAILS">
					<div class="alert alert-success" style="display:none" id="paymentSuccess" name="paymentSuccess">
									<button class="close" data-dismiss="alert"></button>
									<strong>Payment success!</strong> .
					</div>

						<div class="control-group">
							<label class="control-label">Account Number</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap" id="accountNumber" /> <span
									class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Mobile Number</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap" id="mobileNumber" /> <span
									class="help-inline"></span>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn blue" id="billSearchSumbit">Submit</button>

						</div>
					</div>
					<div style="display: none" id="BIIL_DETAILS"
						class="row-fluid form-horizontal">

						<div class="control-group">
							<label class="control-label">Billed Amount</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap" id="BILLED_AMOUNT"
									readonly="readonly" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Minimum Payable Amount</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap"
									id="MINIMUM_PAYABLE_AMOUNT" readonly="readonly" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">SIM Number</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap" id="SIM_NUMBER"
									readonly="readonly" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<input type="text" class="span6 m-wrap" id="SIM_STATUS"
									readonly="readonly" />
							</div>
						</div>
						<form action="#" id="form_sample_1" class="form-horizontal">
							<div class="control-group">
								<label class="control-label">Pay amount<span
									class="required">*</span></label>
								<div class="controls">
									<input type="text" class="span6 m-wrap" name="payAmount"
										id="payAmount" />
								</div>
							</div>
						</form>
						<div class="form-actions">
							<button type="submit" class="btn blue" id="billPaySumbit">Pay</button>

						</div>
					</div>

					<div style="display:none" id="CREDIT_CARD_DETAILS"
						class="row-fluid form-horizontal">
						<form action="#" id="form_sample_2" class="form-horizontal">
							<div class="control-group">
								<label class="control-label">Amount</label>
								<div class="controls">
									<input type="text" class="span6 m-wrap" id="cardAmount"
										name="cardAmount"  readonly="readonly"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Name</label>
								<div class="controls">
									<input type="text" class="span6 m-wrap" id="name" name="name"
										 />
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">Credit Card<span
									class="required">*</span></label>
								<div class="controls">
									<input name="creditcard" id="creditcard" type="text"
										class="span6 m-wrap" /> <span class="help-block">e.g:
										5500 0000 0000 0004</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Year</label>
								<div class="controls">
									<input type="text" class="span2 m-wrap" id="Year" name="Year"
										 />
								</div>
								<label class="control-label">Month</label>
								<div class="controls">
									<input type="text" class="span1 m-wrap" id="Month" name="Month"
										 />
								</div>
								<label class="control-label">IPIN</label>
								<div class="controls">
									<input type="password" class="span1 m-wrap" id="Ipin" name="Ipin"
										 />
								</div>
							</div>
							
						</form>
						<div class="form-actions">
							<button type="submit" class="btn blue" id="creditCardPaySumbit">Pay</button>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>

<script
	src="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.resize.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.pulsate.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/date.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/gritter/js/jquery.gritter.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${context}/assets/scripts/app.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/index.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/ui-jqueryui.js"></script>
<script type="text/javascript"
	src="${context}/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript"
	src="${context}/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="${context}/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript"
	src="${context}/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript"
	src="${context}/assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script>
	$(document).ready(function() {
		//App.init();

		DashBoardChart.initDashBoard();

		UIJQueryUI.init();

	});
</script>

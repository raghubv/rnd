package net.javabeat.springdata.executable;

import java.util.Date;

import net.javabeat.springdata.beans.RegistrationBean;
import net.javabeat.springdata.jpa.data.Address;
import net.javabeat.springdata.jpa.data.User;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Executable {
	public static RegistrationBean registrationBean;
	public static ClassPathXmlApplicationContext context;
	
	static {
		// Acquire Context
		context = new ClassPathXmlApplicationContext("SpringContext.xml");
	}
	
	public static void main(String [] args) throws Exception{
		// Create by MongoTemplate
		//createUserThroughMonogoTamplate();
		// Create By Spring Data
		//CreateUserThoughMongoRepository();
		insert();
		update();
	}
	
	
	public static void update(){
		Date date=new Date();
		
		for(int i=0;i<10000000;i++){
			
			String 
			
			req=
			
			"{"+
			    "\"_id\" : {"+
			        "\"fm_node\" : "+i+","+
			        "\"ord_type\" : 301,"+
			        "\"prd_ctgr\" : 5047,"+
			        "\"aggregatedAndComputedDate\" : 20150317"+
			    "},"+
			    "\"data\" : {"+
			        "\"totalQty\" : 106,"+
			        "\"avgVal\" : 20000,"+
			        "\"avgQty\" : 1,"+
			        "\"totalVal\" : 2120000"+
			    "},"+
			    "\"aggregateAndComputedate\" : {"+
			        "\"month\" : \"03\","+
			        "\"year\" : \"2015\","+
			        "\"day\" : \"17\""+
			    "}"+
			"}";
			
			
			String id="{"+
			    "\"_id\" : {"+
			        "\"fm_node\" : "+i+","+
			        "\"ord_type\" : 301,"+
			        "\"prd_ctgr\" : 5047,"+
			        "\"aggregatedAndComputedDate\" : 20150317"+
			    "}}";
	
		
		MongoTemplate mongoOps = (MongoTemplate)context.getBean("mongoTemplate");
		
		mongoOps.getCollection("test_out").update((DBObject)JSON.parse(id), (DBObject)JSON.parse(req), true, false);
		}
		Date endDate=new Date();
		long executionTime=(endDate.getTime()-date.getTime())/1000;
		System.out.println("Execution Time>>"+executionTime);
		
	}
	
	
	public static void insert(){
		Date date=new Date();
		
		for(int i=0;i<10000000;i++){
			
			String 
			
			req=
			
			"{"+
			    "\"_id\" : {"+
			        "\"fm_node\" : "+i+","+
			        "\"ord_type\" : 301,"+
			        "\"prd_ctgr\" : 5047,"+
			        "\"aggregatedAndComputedDate\" : 20150317"+
			    "},"+
			    "\"data\" : {"+
			        "\"totalQty\" : 106,"+
			        "\"avgVal\" : 20000,"+
			        "\"avgQty\" : 1,"+
			        "\"totalVal\" : 2120000"+
			    "},"+
			    "\"aggregateAndComputedate\" : {"+
			        "\"month\" : \"03\","+
			        "\"year\" : \"2015\","+
			        "\"day\" : \"17\""+
			    "}"+
			"}";
			
			
			String id="{"+
			    "\"_id\" : {"+
			        "\"fm_node\" : "+i+","+
			        "\"ord_type\" : 301,"+
			        "\"prd_ctgr\" : 5047,"+
			        "\"aggregatedAndComputedDate\" : 20150317"+
			    "}}";
	
		
		MongoTemplate mongoOps = (MongoTemplate)context.getBean("mongoTemplate");
		
		mongoOps.getCollection("test_out").insert((DBObject)JSON.parse(req));
		}
		Date endDate=new Date();
		long executionTime=(endDate.getTime()-date.getTime())/1000;
		System.out.println("Execution Time>>"+executionTime);
		
	}
	
	
	
	public static void createUserThroughMonogoTamplate(){
		MongoTemplate mongoOps = (MongoTemplate)context.getBean("mongoTemplate");
		User user = new User();
		user.setId("200");
		user.setFullName("Mongo Template");
		user.setStatus("A");
		user.setAge("29");
		Address address = new Address();
		address.setAddressId("200");
		address.setAddressValue("UK/London");
		user.setAddress(address);
		mongoOps.insert(user);
	}
	
	public static void CreateUserThoughMongoRepository(){
		User user = new User();
		user.setId("201");
		user.setFullName("Spring Data");
		user.setStatus("B");
		user.setAge("25");
		Address address = new Address();
		address.setAddressId("201");
		address.setAddressValue("UK/Manchester");
		user.setAddress(address);
		RegistrationBean bean = (RegistrationBean)context.getBean("registrationBean");
		bean.getRepository().save(user);
	}

}

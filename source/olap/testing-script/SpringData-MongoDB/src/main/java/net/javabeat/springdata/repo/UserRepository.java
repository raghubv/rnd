package net.javabeat.springdata.repo;

import net.javabeat.springdata.jpa.data.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String>{

}

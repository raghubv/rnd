package com.os.snoc.queue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

public class Producer {

	public static Logger logger = Logger.getLogger(Producer.class);

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		// FileWriter fileWriter;
		int NO_OF_PARTIES = 10;
		Map rows = new HashMap();
		List ls = new ArrayList();
		try {

			
				
				for (int day = 1; day <= 100; day++) {
					Integer d=(day%20);
					String strDay=d.toString().length()>1?d.toString():"0"+d.toString();
					
					for (int prodCat = 5077; prodCat <= 5077; prodCat++) {
						List<String> lsMsg = new ArrayList<String>();
						for (int node = 202001; node <= 302001; node++) {
						String str = "{" +
						// "\"_id\": "+String.valueOf(i)+","+
								"\"completed_dt\": \"\"," + "\"crtd_by\":"
								+ String.valueOf(node)
								+ ","
								+ "\"crtd_dt\": {"
								+ "\"$date\": \"2015-03-"+strDay+"T14:22:41.311Z\""
								+ "},"
								+ "\"data\": {"
								+ "\"ord_type\": 301,"
								+ "\"ord_name\": \"Secondary Sale\","
								+ "\"ord_comments\": null,"
								+ "\"ord_terms_condts\": \"\","
								+ "\"ord_creatd_dt\": "
								+ "{"
								+ "\"$date\":\"2015-03-"+strDay+"T12:22:41.296Z\""
								+ "},"
								+ "\"ord_delivery_dt\": \"\","
								+ "\"ord_value\": 20010,"
								+ "\"ord_tax_v\": 0,"
								+ "\"ord_discount_v\": 0,"
								+ "\"ord_total_v\": 8000,"
								+ "\"ord_ship_v\": 0,"
								+ "\"ord_trans_ref\": \"\","
								+ "\"ord_trans_ref_dt\": \"\","
								+ "\"ord_revision_no\": 0,"
								+ "\"ord_revision_dt\": \"\","
								+
								// "\"fm_node\": 10012,"+
								"\"fm_node\": "
								+ String.valueOf(node)
								+ ","
								+ "\"fm_acc_id\": 401252,"
								+ "\"to_node\": 10028,"
								+ "\"to_org_id\": 10016,"
								+ "\"to_acc_id\": 401272,"
								+ "\"to_facc_id\": 0,"
								+ "\"payment_dtls\": {"
								+ "\"bank\": {"
								+

								"},"
								+ "\"cheque\": {"
								+ "},"
								+ "\"cash\": {"
								+ "\"payment_Amt\": 800000,"
								+ "\"pay_date\": {"
								+ "\"$date\": \"2015-02-"+strDay+"T12:00:00.000Z\""
								+ "}"
								+ "}"
								+ "},"
								+ "\"ord_items\": ["
								+ "{"
								+ "\"ord_pred_no\": 0,"
								+ "\"ord_pred_dt\": \"\","
								+ "\"prd_ctgr\": "+String.valueOf(prodCat)+","
								+ "\"prd_ctgr_nm\": \"GSM\","
								+ "\"prd_id\": 5045,"
								+ "\"prd_nm\": \"RC10\","
								+ "\"uom\": \"194\","
								+ "\"prd_status\": 0,"
								+ "\"prd_addnl\": \"N\","
								+ "\"prd_reord\": 0,"
								+ "\"prd_qty\": 1,"
								+ "\"prd_appvd\": 0,"
								+ "\"prd_prc_allc\": 0,"
								+ "\"prd_prc_asgd\": 10,"
								+ "\"prd_total\": 10,"
								+ "\"prd_tax_v\": 0,"
								+ "\"prd_discount_v\": 0,"
								+ "\"prd_ship_v\": 0,"
								+ "\"prd_sent_dt\": \"\","
								+ "\"prd_rcv_dt\": \"\","
								+ "\"fm_node\": 10012,"
								+ "\"fm_acc_id\": 401252,"
								+ "\"supplr_ac_no\": 401252,"
								+ "\"to_node\": 10028,"
								+ "\"to_acc_id\": 401272,"
								+ "\"to_facc_id\": 0,"
								+ "\"prd_sl\": \"No\","
								+ "\"prd_serial_type\": 1"
								+ "},"
								+ "{"
								+ "\"ord_pred_no\": 0,"
								+ "\"ord_pred_dt\": \"\","
								+ "\"prd_ctgr\": "+String.valueOf(prodCat+1)+","
								+ "\"prd_ctgr_nm\": \"GSM\","
								+ "\"prd_id\": 5045,"
								+ "\"prd_nm\": \"RC10\","
								+ "\"uom\": \"194\","
								+ "\"prd_status\": 0,"
								+ "\"prd_addnl\": \"N\","
								+ "\"prd_reord\": 0,"
								+ "\"prd_qty\": 1,"
								+ "\"prd_appvd\": 0,"
								+ "\"prd_prc_allc\": 0,"
								+ "\"prd_prc_asgd\": 10,"
								+ "\"prd_total\": 10,"
								+ "\"prd_tax_v\": 0,"
								+ "\"prd_discount_v\": 0,"
								+ "\"prd_ship_v\": 0,"
								+ "\"prd_sent_dt\": \"\","
								+ "\"prd_rcv_dt\": \"\","
								+ "\"fm_node\": 10012,"
								+ "\"fm_acc_id\": 401252,"
								+ "\"supplr_ac_no\": 401252,"
								+ "\"to_node\": 10028,"
								+ "\"to_acc_id\": 401272,"
								+ "\"to_facc_id\": 0,"
								+ "\"prd_sl\": \"No\","
								+ "\"prd_serial_type\": 1"
								+ "},"
								+ "{"
								+ "\"ord_pred_no\": 0,"
								+ "\"ord_pred_dt\": \"\","
								+ "\"prd_ctgr\": 5047,"
								+ "\"prd_ctgr_nm\": \"Handset\","
								+ "\"prd_id\": 5055,"
								+ "\"prd_nm\": \"Mi3\","
								+ "\"uom\": \"194\","
								+ "\"prd_status\": 0,"
								+ "\"prd_addnl\": \"N\","
								+ "\"prd_reord\": 0,"
								+ "\"prd_qty\": 1,"
								+ "\"prd_appvd\": 0,"
								+ "\"prd_prc_allc\": 0,"
								+ "\"prd_prc_asgd\": 20000,"
								+ "\"prd_total\": 20000,"
								+ "\"prd_tax_v\": 0,"
								+ "\"prd_discount_v\": 0,"
								+ "\"prd_ship_v\": 0,"
								+ "\"prd_sent_dt\": \"\","
								+ "\"prd_rcv_dt\": \"\","
								+ "\"fm_node\": 10012,"
								+ "\"fm_acc_id\": 401252,"
								+ "\"supplr_ac_no\": 401252,"
								+ "\"to_node\": 10028,"
								+ "\"to_acc_id\": 401272,"
								+ "\"to_facc_id\": 0,"
								+ "\"prd_sl\": \"Yes\","
								+ "\"prd_serial_type\": 1,"
								+ "\"prd_slnos\": [{"
								+ "\"start\": 234567,"
								+ "\"end\": 234567,"
								+ "\"batch\": \"\","
								+ "\"crtd_dttm\": {"
								+ "\"$date\": \"2015-01-"+strDay+"T19:56:11.000Z\""
								+ "}"
								+ "}]"
								+ "}],"
								+ "\"to_node_addr\": {"
								+ "\"copy_adrs_type\": \"744\","
								+ "\"adrs_3\": \"\","
								+ "\"postal_code\": \"\","
								+ "\"adrs_4\": \"0\","
								+ "\"state\": \"501\","
								+ "\"adrs_2\": \"09\","
								+ "\"longitude\": \"33.39178300000003\","
								+ "\"latitude\": \"47.910483\","
								+ "\"zone\": \"601\","
								+ "\"city\": \"701\","
								+ "\"country\": \"401\""
								+ "},"
								+ "\"addnl_param\": {"
								+ "},"
								+ "\"ord_master_id\": 1709,"
								+ "\"ord_creatd_by\": 10614,"
								+ "\"org_id\": 2015,"
								+ "\"org_desc\": \"Sam Dist\""
								+ "},"
								+ "\"due_dt\": \"\","
								+ "\"err_code\": \"\","
								+ "\"err_msg\": \"\","
								+ "\"last_updt_dt\": \"\","
								+ "\"ord_comments\": null,"
								+ "\"ord_id\": 11282,"
								+ "\"ord_master_id\": 1709,"
								+ "\"ord_name\": \"Secondary Sale\","
								+ "\"ord_priority\": 1,"
								+ "\"ord_type\": 301,"
								+ "\"ref1\": \"\","
								+ "\"ref2\": \"\","
								+ "\"ref3\": \"\","
								+ "\"ref_id\": \"\","
								+ "\"retry_count\": \"\","
								+ "\"src1\": \"\","
								+ "\"src2\": \"\","
								+ "\"src3\": \"\","
								+ "\"status\": 173,"
								+ "\"tenant_id\": \"\","
								+ "\"updtd_by\": null,"
								+ "\"updtd_dt\": {\"$date\": \"2015-01-22T14:22:41.439Z\"}"
								+ ",\"wf_status\": 169,\"status_desc\": \"Completed\"}";


						System.out.println("-----END-----");
						lsMsg.add(str);
						
					}
						(new Producer()).sendQMessage("DailySalesSummary", lsMsg);
					
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//
	// public static void main(String[] args) {
	//
	// // TODO Auto-generated method stub
	// // FileWriter fileWriter;
	// int NO_OF_PARTIES = 10;
	// Map rows = new HashMap();
	// List ls = new ArrayList();
	// try {
	// System.out.println("-----Start-----");
	//
	// for (int i = 1; i <= 1; i++) {
	//
	//
	// for (int j = 1; j <= NO_OF_PARTIES; j++) {
	// for (int p = 1501; p <= 1510; p++) {
	//
	// Map inputMap = new HashMap();
	// inputMap.put("party_id_n", ((i * 10000) + j));
	// inputMap.put("role_type_n", 101);
	// inputMap.put("date_dt", "01/01/2015");
	// inputMap.put("category_id_n", "21");
	// inputMap.put("item_id_n", p);
	// inputMap.put("opening_stock_n", 1000);
	// inputMap.put("stock_in_n", 8000);
	// inputMap.put("stock_out_n", 7000);
	// inputMap.put("closing_stock_n", 2000);
	// inputMap.put("adjustments_in_n", 0);
	// inputMap.put("adjustments_out_n", 0);
	// inputMap.put("primary_sales_qty_n", 0);
	// inputMap.put("secondary_sales_qty_n", 8000);
	// inputMap.put("interim_sale_qty_n", 0);
	// inputMap.put("tertiary_sale_qty_n", 7000);
	// inputMap.put("transfers_in_n", 0);
	// inputMap.put("transfers_out_n", 0);
	// inputMap.put("returns_in_n", 0);
	// inputMap.put("returns_out_n", 0);
	// inputMap.put("primary_sales_amount_n", 0);
	// inputMap.put("secondary_sales_amount_n", 400000);
	// inputMap.put("interim_sales_amount_n", 0);
	// inputMap.put("tertiary_sales_amount_n", 350000);
	// inputMap.put("transfers_in_amount_n", 0);
	// inputMap.put("transfers_out_amount_n", 0);
	// inputMap.put("returns_in_amount_n", 0);
	// inputMap.put("returns_out_amount_n", 0);
	// inputMap.put("primary_sales_instances_n", 0);
	// inputMap.put("secondary_sales_instances_n", 0);
	// inputMap.put("interim_sales_instances_n", 3);
	// inputMap.put("tertiary_sales_instances_n", 50007);
	// inputMap.put("Location-1", 50008);
	// inputMap.put("Location-2", 50009);
	// inputMap.put("Location-3", 0);
	// inputMap.put("Status", 1);
	// inputMap.put("Beat_track_id", 0);
	// inputMap.put("Beat_plan_id", 0);
	// inputMap.put("level1", "3001~201");
	// inputMap.put("level2", "3001~201");
	// inputMap.put("level3", "2001~301");
	// inputMap.put("level4", "110017~404");
	// inputMap.put("level5", "110017~403");
	// inputMap.put("level6", "110017~402");
	// inputMap.put("level7", "110016~401");
	// inputMap.put("unit_price_n", 50);
	// inputMap.put("no_of_lapu_prc_instances", 0);
	// inputMap.put("tax_amount_n", 0);
	// inputMap.put("commission_amount_n", 0);
	//
	// ls.add(inputMap);
	//
	//
	// }
	// }
	//
	// }
	// rows.put("rows", ls);
	// ObjectMapper objectMapper = new ObjectMapper();
	// sendQMessage("test",
	// objectMapper.writeValueAsString(rows));
	// System.out.println("-----END-----");
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	public  void sendQMessage(String target,List<String> lsMsg) {
		try {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					"tcp://192.168.2.164:62643");
			
//			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
//					"tcp://localhost:61616");
			//
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			// Destination destination = session.createQueue("queue/"+target);
			Destination destination = session.createQueue(target);
			MessageProducer producer = session.createProducer(destination);
			for (String string : lsMsg) {
				TextMessage message = session.createTextMessage(string);
				producer.send(message);	

				logger.info("Sent message '" + message.getText() + "'" + " to "
						+ target + " destination:" + destination);
			}
			
			session.close();
			connection.close();

		} catch (JMSException e) {
			e.printStackTrace();
			logger.error("Exception occured in :["
					+ logger.getClass().getName() + "] function:sendQMessage "
					+ e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in :["
					+ logger.getClass().getName() + "] function:sendQMessage "
					+ e.getMessage());
		}
	}
}

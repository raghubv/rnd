package com.os.snoc.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

public class Consumer implements MessageListener{

	public static Logger logger = Logger.getLogger(Consumer.class);
	
	public static void main(String[] args){
	
		consumeQMessage("test", "testing by raghu");	
		}
	
	public static void consumeQMessage(String target,String reqMessage)
	{
		try 
		{
			ConnectionFactory connectionFactory =new ActiveMQConnectionFactory("tcp://localhost:61616");
	        Connection connection = connectionFactory.createConnection();
	        connection.start();
	        Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
	        Destination destination = session.createQueue(target);
	        MessageConsumer consumer = session.createConsumer(destination);
	        Message message=consumer.receiveNoWait();
	        System.out.println("Received message '"+ message);
	        if (message instanceof TextMessage) 
	        {
	            TextMessage textMessage = (TextMessage) message;
	            System.out.println("Received message '"+ textMessage.getText() + "'");
	        }
	        session.close();
	        connection.close();

		}
		catch (JMSException e) 
		{
			logger.error("Exception occured in :["+logger.getClass().getName()+"] function:consumeQMessage "+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {

			logger.error("Exception occured in :["+logger.getClass().getName()+"] function:consumeQMessage "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void onMessage(Message message) 
	{
		try 
		{
			String messageString = ((TextMessage) message).getText();
			System.out.println(messageString);
		} 
		catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

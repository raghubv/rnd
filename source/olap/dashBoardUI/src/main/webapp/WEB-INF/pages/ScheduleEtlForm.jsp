<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>

<body>
	<h2>ETL>>Aggregation & Computation>>Event Mapping>>Schedule</h2>

	<form:form method="POST" commandName="scheduleEtlDefinition"
		action="/OlapUi/olap/etlschedule">
		<%-- 		<form:errors path="*" cssClass="errorblock" element="div" /> --%>
		<table>
			<thead>
				Schedule ETL
				<br />
			</thead>
			<tr>
				<td><form:textarea path="etlScheduleDfn" rows="20" cols="100" /></td>
				<td><form:errors path="etlScheduleDfn" cssClass="error" /></td>
			</tr>

			<tr>
				<td colspan="3" align="center"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>
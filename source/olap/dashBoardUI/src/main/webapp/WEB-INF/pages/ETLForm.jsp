<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<script type="text/javascript">
	function loadETLConfigurationTemplate(type) {
		if (type == 'queue')
			document.getElementById("etlDfn").value = '${etlDefinitionQueue}';
		else if (type == 'json')
			document.getElementById("etlDfn").value = '${etlDefinitionJSON}';
		else if (type == 'xml')
			document.getElementById("etlDfn").value = '${etlDefinitionXML}';
		else if (type == 'csv')
			document.getElementById("etlDfn").value = '${etlDefinitionCSV}';

	}
</script>

<body>
	<h2>ETL</h2>

	<form:form method="POST" commandName="etlDefinition"
		action="/OlapUi/olap/etl">
		<%-- 		<form:errors path="*" cssClass="errorblock" element="div" /> --%>
		<table>
			<thead>

				Configure ETL Job
				<br /> 1.Extract: Specify data source for data
				extraction(Supporting file system, Queue)
				<br /> 2.Transform: Transaform the extracted data
				<br /> 3.Load: Load the data to data base
				<br />
			</thead>
			<br />
			<br />
			<tr>
				<td>Load ETL Template: <input type="radio" id="type"
					name="type" checked="checked"  onclick="loadETLConfigurationTemplate('queue');" ></<input>Queue<input
					type="radio" id="type" name="type"
					onclick="loadETLConfigurationTemplate('json');">JSON<input
					type="radio" id="type" name="type"
					onclick="loadETLConfigurationTemplate('xml');">XML<input
					type="radio" id="type" name="type"
					onclick="loadETLConfigurationTemplate('csv');">CSV
				</td>
			</tr>
			<tr>
				<td><form:textarea path="etlDfn" rows="20" cols="100" /></td>
				<td><form:errors path="etlDfn" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>
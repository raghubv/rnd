<!DOCTYPE HTML>
<html>
<head>
<script  src="./assets/js/canvasjs.min.js"></script>
<script  src="./assets/js/jquery-1.11.3.min.js"></script>
<script  src="./assets/js/combo-charts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#btn1").click(function(){
    	var request={
    			 "requestMessage": {
    				  "collectionName": "org_monthly_metrics",
    				  "outputType": 0,
    				  "textX":"Month", 	
    				  "aggregate": [
    				{ $match : {"event_type_n" : 1 } },
    				{
    				   "$group": {
    				    "_id": {
    				     "month_id_n": "$month_id_n",
    				     "metrics_type_n": "$metrics_type_n",
    				     "metrics_type_name_v": "$metrics_type_name_v"
    				    },
    				    "totalQty": {
    				     "$sum": "$qty_n"
    				    }
    				   }
    				  },
    				  {
    				   "$project": {
    				    "_id": 0,
    				    "month_id_n": "$_id.month_id_n",
    				    "metrics_type_n": "$_id.metrics_type_n",
    				    "metrics_type_name_v": "$_id.metrics_type_name_v",
    				    "totalQty": "$totalQty"
    				   }
    				  },
    				  {
    				   "$sort": {
    				    "month_id_n": 1,
    				    "metrics_type_n": 1
    				   }
    				  },
    				  {
    				   "$group": {
    				    "_id": {
    				     "group": "$month_id_n" 
    				     
    				    },
    				    "values":{ $push:  {"x": "$month_id_n", "y": "$totalQty" } }
    				   }
    				  }
    				 
    				  ]
    				 }
    				};
    var myString =  JSON.stringify(request); 
    var username='dpc';
    var password='password';
    $.ajax({
        url: 'http://localhost:8080/olap/rest/json/aggregate/getAggregatedData',
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data:myString,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic "
                + btoa(username + ":" + password));
        }        
        // snip repeat
    }).done (function(data){ 
    	comboCharts.drawCanvas(data,"Product Sells","chartContainer");
	});
    });
});
  
  
  
  </script>
  
</head>
<body>
<input type="button" id="btn1" name="btn1" value="display"/> 
  <div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
</body>
</html>
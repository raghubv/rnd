<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>

<body>
	<h2>ETL>>Aggregation & Computation</h2>

	<form:form method="POST" commandName="aggAndComputationDefinition"
		action="/OlapUi/olap/agg">
		<%-- 		<form:errors path="*" cssClass="errorblock" element="div" /> --%>
		<table>
			<thead>

				Configure Aggregate & computation formula
				<br /> 1.Configure DB Param
				<br /> 2.Configure Date Param
				<br /> 3.Configure Js function
				<br /> 4.Configure Js Expression
				<br /> 5.Configure Mongod aggregation
				<br /> 5.Configure to get aggregated data
				<br />
			</thead>
			<tr>
				<td><form:textarea path="aggAndComputeDfn" rows="20" cols="100" /></td>
				<td><form:errors path="aggAndComputeDfn" cssClass="error" /></td>
			</tr>

			<tr>
				<td colspan="3" align="center"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>
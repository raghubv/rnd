<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="./assets/css/nv.d3.css" rel="stylesheet" type="text/css">
    <script src="./assets/js/d3.min.js" charset="utf-8"></script>
    <script src="./assets/js/nv.d3.js"></script>
    <script src="./assets/js/stream_layers.js"></script>
    <script  src="./assets/js/jquery-1.11.3.min.js"></script>

    <style>
        text {
            font: 12px sans-serif;
        }
        svg {
            display: block;
        }
        html, body, #chart1, svg {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
        }
    </style>
</head>
<body class='with-3d-shadow with-transitions'>
<input type="button" id="btn1" name="btn1" value="display"/>
 
<div id="chart1" >
    <svg> </svg>
</div>

<script>


///
$(document).ready(function(){
    $("#btn1").click(function(){
    	var request={
    			 "requestMessage": {
    				  "collectionName": "org_monthly_metrics",
    				  "outputType": 2,
    				  "textX":"Month", 	
    				  "aggregate": [
    				{ $match : {"event_type_n" : 1 } },
    				{
    				   "$group": {
    				    "_id": {
    				     "month_id_n": "$month_id_n",
    				     "metrics_type_n": "$metrics_type_n",
    				     "metrics_type_name_v": "$metrics_type_name_v"
    				    },
    				    "totalQty": {
    				     "$sum": "$qty_n"
    				    }
    				   }
    				  },
    				  {
    				   "$project": {
    				    "_id": 0,
    				    "month_id_n": "$_id.month_id_n",
    				    "metrics_type_n": "$_id.metrics_type_n",
    				    "metrics_type_name_v": "$_id.metrics_type_name_v",
    				    "totalQty": "$totalQty"
    				   }
    				  },
    				  {
    				   "$sort": {
    				    "month_id_n": 1,
    				    "metrics_type_n": 1

    				   }
    				  },
    				  {
    				   "$group": {
    				    "_id": {
    				     "group": "$metrics_type_name_v" 
    				     
    				    },
    				    "values":{ $push:  {"x": "$month_id_n", "y": "$totalQty" } }
    				 
    				   }
    				  }
    				 
    				  ]
    				 }
    				};
    var myString =  JSON.stringify(request); 
    var username='dpc';
    var password='password';
    $.ajax({
        url: 'http://localhost:8080/olap/rest/json/aggregate/getAggregatedData',
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data:myString,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic "
                + btoa(username + ":" + password));
        }        
        // snip repeat
    }).done (function(data){ 
    var obj = jQuery.parseJSON(JSON.stringify(data));
			var ls=obj.responseMessage.ls;
			
			nv.addGraph(function() {
			    var chart = nv.models.multiBarChart()
			      .reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
			      .rotateLabels(0)      //Angle to rotate x-axis labels.
			      .showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
			      .groupSpacing(0.1)    //Distance between each group of bars.
			    ;

			    chart.xAxis
			        .tickFormat(d3.format(',f'));

			    chart.yAxis
			        .tickFormat(d3.format(',.1f'));
			 
			    d3.select('#chart1 svg')
			        .datum(ls)
			        .call(chart);

			    nv.utils.windowResize(chart.update);

			    return chart;
			});
		
  
	});
    });
});


</script>
</body>
</html>

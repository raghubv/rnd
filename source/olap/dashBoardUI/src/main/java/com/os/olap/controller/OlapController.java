package com.os.olap.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.os.olap.model.AggAndComputationDefinition;
import com.os.olap.model.EtlDefinition;
import com.os.olap.model.EventDefinition;
import com.os.olap.model.ScheduleAggDefinition;
import com.os.olap.model.ScheduleEtlDefinition;
import com.os.olap.restService.RestClient;
import com.os.olap.util.Configuration;
import com.os.olap.util.PropertiesBean;


@Controller
@RequestMapping("/olap")
public class OlapController {

	@Autowired
	PropertiesBean propertiesBean;

	//String host = "http://192.168.2.164:8080";

	@RequestMapping(value = "/etl", method = RequestMethod.POST)
	public String addETL(@Valid EtlDefinition etlDefinition,
			BindingResult result, ModelMap model) {
		AggAndComputationDefinition aggAndComputationDefinition = new AggAndComputationDefinition();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map hashMap = objectMapper.readValue(RestClient.post(
					propertiesBean.getUsername(), propertiesBean.getPassword(),
					propertiesBean.getHost()
							+ "/olap/rest/json/etl/addETLDefinition",
					etlDefinition.getEtlDfn()), HashMap.class);

			if (!((String) hashMap.get("responseCode")).equalsIgnoreCase("0"))
				result.rejectValue("etlDfn", etlDefinition.getEtlDfn(),
						(String) hashMap.get("responseDesc"));

			etlDefinition.setEtlDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionQueue")));
			
			
			model.addAttribute("etlDefinitionQueue", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionQueue")));
			
			model.addAttribute("etlDefinitionJSON", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionJSON")));
			
			model.addAttribute("etlDefinitionXML", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionXML")));
			
			model.addAttribute("etlDefinitionCSV", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionCSV")));

			

			aggAndComputationDefinition.setAggAndComputeDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("aggAndComputationDefinition")));
		} catch (Exception e) {

			result.rejectValue("etlDfn", etlDefinition.getEtlDfn(),
					"Error while calling ETL Service");
		}

		if (result.hasErrors()) {
			return "ETLForm";
		} else {

			model.addAttribute("aggAndComputationDefinition",
					aggAndComputationDefinition);
			return "AggForm";
		}

	}

	@RequestMapping(value = "/agg", method = RequestMethod.POST)
	public String addAgg(
			@Valid AggAndComputationDefinition aggAndComputationDefinition,
			BindingResult result, ModelMap model) {

		EventDefinition eventDefinition = new EventDefinition();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map hashMap = objectMapper
					.readValue(
							RestClient.post(
									propertiesBean.getUsername(),
									propertiesBean.getPassword(),
									propertiesBean.getHost()
											+ "/olap/rest/json/aggregate/addAggregateAndComputeFormula",
									aggAndComputationDefinition
											.getAggAndComputeDfn()),
							HashMap.class);
			if (!((String) hashMap.get("responseCode")).equalsIgnoreCase("0"))
				result.rejectValue("aggAndComputeDfn",
						aggAndComputationDefinition.getAggAndComputeDfn(),
						(String) hashMap.get("responseDesc"));
			eventDefinition.setEventMappingDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("eventDefinition")));
		} catch (Exception e) {

			result.rejectValue("aggAndComputeDfn",
					aggAndComputationDefinition.getAggAndComputeDfn(),
					"Error while adding aggregate defintion");
		}

		if (result.hasErrors()) {
			return "AggForm";
		} else {
			model.addAttribute("eventDefinition", eventDefinition);
			return "EventForm";
		}

	}

	@RequestMapping(value = "/event", method = RequestMethod.POST)
	public String addEvent(@Valid EventDefinition eventDefinition,
			BindingResult result, ModelMap model) {

		ScheduleEtlDefinition scheduleEtlDefinition = new ScheduleEtlDefinition();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map hashMap = objectMapper.readValue(RestClient.post(
					propertiesBean.getUsername(), propertiesBean.getPassword(),
					propertiesBean.getHost()
							+ "/olap/rest/json/event/addEventMapping",
					eventDefinition.getEventMappingDfn()), HashMap.class);
			if (!((String) hashMap.get("responseCode")).equalsIgnoreCase("0"))
				result.rejectValue("eventMappingDfn",
						eventDefinition.getEventMappingDfn(),
						(String) hashMap.get("responseDesc"));

			scheduleEtlDefinition.setEtlScheduleDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("scheduleEtlDefinition")));
		} catch (Exception e) {

			result.rejectValue("eventMappingDfn",
					eventDefinition.getEventMappingDfn(),
					"Error while adding event defintion");
		}

		if (result.hasErrors()) {
			return "EventForm";
		} else {
			model.addAttribute("scheduleEtlDefinition", scheduleEtlDefinition);
			return "ScheduleEtlForm";
		}

	}

	@RequestMapping(value = "/etlschedule", method = RequestMethod.POST)
	public String addSchedule(
			@Valid ScheduleEtlDefinition scheduleEtlDefinition,
			BindingResult result, ModelMap model) {
		ScheduleAggDefinition scheduleAggDefinition = new ScheduleAggDefinition();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map hashMap = objectMapper.readValue(RestClient.post(
					propertiesBean.getUsername(), propertiesBean.getPassword(),
					propertiesBean.getHost()
							+ "/olap/rest/json/etl/scheduleETLProcess",
					scheduleEtlDefinition.getEtlScheduleDfn()), HashMap.class);
			;
			if (!((String) hashMap.get("responseCode")).equalsIgnoreCase("0"))
				result.rejectValue("etlScheduleDfn",
						scheduleEtlDefinition.getEtlScheduleDfn(),
						(String) hashMap.get("responseDesc"));

			scheduleAggDefinition.setAggScheduleDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("scheduleAggDefinition")));

		} catch (Exception e) {

			result.rejectValue("etlScheduleDfn",
					scheduleEtlDefinition.getEtlScheduleDfn(),
					"Error while adding ETL schedule");
		}
		if (result.hasErrors()) {
			return "ScheduleEtlForm";
		} else {
			model.addAttribute("scheduleAggDefinition", scheduleAggDefinition);
			return "ScheduleAggForm";
		}

	}

	@RequestMapping(value = "/aggschedule", method = RequestMethod.POST)
	public String addSchedule(
			@Valid ScheduleAggDefinition scheduleAggDefinition,
			BindingResult result, ModelMap model) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map hashMap = objectMapper.readValue(RestClient.post(
					propertiesBean.getUsername(), propertiesBean.getPassword(),
					propertiesBean.getHost()
							+ "/olap/rest/json/aggregate/scheduleAggregate",
					scheduleAggDefinition.getAggScheduleDfn()), HashMap.class);
			if (!((String) hashMap.get("responseCode")).equalsIgnoreCase("0"))
				result.rejectValue("aggScheduleDfn", "",
						(String) hashMap.get("responseDesc"));
		} catch (Exception e) {

			result.rejectValue("aggScheduleDfn", "",
					"Error while adding aggregate schedule ");
		}

		if (result.hasErrors()) {
			return "ScheduleAggForm";
		} else {
			return "Finish";
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayCustomerForm(ModelMap model) {
		EtlDefinition etlDefinition = new EtlDefinition();
		try {
			ObjectMapper objectMapper = new ObjectMapper();

			etlDefinition.setEtlDfn(objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionQueue")));
			
			model.addAttribute("etlDefinitionQueue", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionQueue")));
			
			model.addAttribute("etlDefinitionJSON", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionJSON")));
			
			model.addAttribute("etlDefinitionXML", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionXML")));
			
			model.addAttribute("etlDefinitionCSV", objectMapper
					.writeValueAsString(Configuration.getConfigurationData()
							.get("etlDefinitionCSV")));
		
		} catch (JsonGenerationException e) {

			e.printStackTrace();
		} catch (JsonMappingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		
		model.addAttribute("etlDefinition", etlDefinition);
		return "ETLForm";

	}

}
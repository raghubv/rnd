package com.os.olap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/productsales")
public class ProductSellsController {
	@RequestMapping( method = RequestMethod.GET)
	public ModelAndView getPages() {

		ModelAndView model = new ModelAndView("productSells");
		return model;

	}
}

package com.os.olap.model;

import org.hibernate.validator.constraints.NotEmpty;

public class AggAndComputationDefinition {



	@NotEmpty
	String aggAndComputeDfn;

	public String getAggAndComputeDfn() {
		return aggAndComputeDfn;
	}

	public void setAggAndComputeDfn(String aggAndComputeDfn) {
		this.aggAndComputeDfn = aggAndComputeDfn;
	}
	
	


}
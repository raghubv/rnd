package com.os.olap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/canvasproductsales")
public class ProductSellsOneController {
	@RequestMapping( method = RequestMethod.GET)
	public ModelAndView getPages() {

		ModelAndView model = new ModelAndView("canvasproductsales");
		return model;

	}
}

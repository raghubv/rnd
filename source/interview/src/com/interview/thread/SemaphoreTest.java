package com.interview.thread;

import java.util.concurrent.*;
import java.util.*;
import java.text.*;

/**
 * 
 * Semaphore can be used to create a set of Children Threads even when the size
 * of the Threads to be created is not known fore hand. This is because a
 * Semaphore can wait until a number of releases have been made but that number
 * is not required to initialize the Semaphore. Semaphores can be used in other
 * scenarios such as Synchronizing between different threads such as Publisher,
 * Subscriber scenario.
 * 
 * Practical Example : Traversing through a folder with sub folders within sub
 * folders and if JPEG files are found, move them to a destination directory and
 * then zip them. In this scenario the folder traversing is done recursively
 * until a JPEG file is found. And then a Thread is invoked to move it to
 * destination directory. But zipping needs to wait until all JPEG files are
 * moved to the destination directory. In this scenario no of JPEG files
 * available in the folder structure is not known but the zipping needs to wait
 * till all files are successfully moved. Ideal scenario for a Semaphore based
 * waiting.
 */
public class SemaphoreTest {
	private static final int MAX_THREADS = 5;

	public static void main(String[] args) throws Exception {
		Semaphore semaphore = new Semaphore(0);

		System.out.println("Spawning Threads");
		int threadCount = 0;
		Random random = new Random();
		for (int i = 0; i < MAX_THREADS; i++) {
			// Threads created will not always be MAX_THREADS
			// Because Threads are created only if Random no is Even.
			// Thus the No of Threads unknown at Semaphore Initialization
			if (random.nextInt(9999) % 2 == 0) {
				Thread t = new Thread(new WorkerThread(semaphore, String.format("Thread-%d", i)));
				t.start();
				threadCount++;
			}
		}
		System.out.println("Spawning Finished");
		System.out.println("Waiting All Threads to Finish");
		semaphore.acquire(threadCount);
		System.out.println("Redo..");
		Thread t= new Thread(new WorkerThread1(semaphore, "realese"));
		t.start();
		semaphore.acquire(MAX_THREADS);
		System.out.println("All Threads are Finished");
	}

	private static class WorkerThread implements Runnable {
		private Semaphore semaphore;

		private String name;

		public WorkerThread(Semaphore semaphore, String name) {
			this.name = name;
			this.semaphore = semaphore;
		}

		public void run() {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				System.out.printf("%s : Doing Some Work on %s\n", getFormattedDate(sdf), name);
				Thread.sleep(getRandomWaitTime());
				System.out.printf("%s : Doing Some more work on %s\n", getFormattedDate(sdf), name);
				Thread.sleep(getRandomWaitTime());
				System.out.printf("%s : Finished work on %s\n", getFormattedDate(sdf), name);
				semaphore.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private String getFormattedDate(SimpleDateFormat sdf) {
			return sdf.format(new Date());
		}

		private int getRandomWaitTime() {
			return (int) ((Math.random() + 1) * 1000);
		}

	}
	
	private static class WorkerThread1 implements Runnable {
		private Semaphore semaphore;

		private String name;

		public WorkerThread1(Semaphore semaphore, String name) {
			this.name = name;
			this.semaphore = semaphore;
		}

		public void run() {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				System.out.printf("%s : Doing Some Work on %s\n", getFormattedDate(sdf), name);
				Thread.sleep(getRandomWaitTime());
				System.out.printf("%s : Doing Some more work on %s\n", getFormattedDate(sdf), name);
				Thread.sleep(getRandomWaitTime());
				System.out.printf("%s : Finished work on %s\n", getFormattedDate(sdf), name);
				semaphore.release();
				semaphore.release();
				semaphore.release();
				semaphore.release();
				semaphore.release();
				System.out.println(">>>>>>>>>>>>>>>>");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private String getFormattedDate(SimpleDateFormat sdf) {
			return sdf.format(new Date());
		}

		private int getRandomWaitTime() {
			return (int) ((Math.random() + 1) * 1000);
		}

	}
}
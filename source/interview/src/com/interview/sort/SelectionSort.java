package com.interview.sort;
/**
 * 
 * @author os-Raghu
 *Select high and lowest select insert in to specific position
 */
public class SelectionSort {

	static int[] array= {8,1,7,9,0,10,5}; 
	
	/**
	 * iterate throw find highest element 
	 */
	public void sortBasedOnHighestValue(){
		display();
		for(int i=array.length-1;i>=0;i--){
			int index=i;
			for(int j=0;j<i;j++){
				if(array[j]>=array[index])
				index=j;
			}
			int higher=array[index];
			array[index]=array[i];
			array[i]=higher;
			System.err.println("[greater]"+higher);
			display();
			
			
		}
		
		
		
	}
	/**
	 * iterate throw find lowest element 
	 */
	public void sortBasedOnLowestValue(){
		
		for(int i=0;i<array.length-1;i++){
			int index=i;
			for(int j=i;j<array.length;j++){
				if(array[j]<array[index])
				index=j;
			}
			int smaller=array[index];
			array[index]=array[i];
			array[i]=smaller;
			System.err.println("[smaller]"+smaller);
			display();
			
			
		}
		
	}
	
	public void display(){
		System.out.println(" Sort Array");
		System.out.print( "[" );
		for ( int i : array) 
			System.out.print(" "+ i +" ");
		System.out.println("]" );		
	}
	
public static void main(String [] args){
	
	SelectionSort ss= new SelectionSort();
	//ss.sortBasedOnLowestValue();
	ss.sortBasedOnHighestValue();
	ss.display();
}
	
}

package com.interview.sort;
/**
 * 
 * buble up highest elemet to end of the array 
 * Data structure Array
 * 
 * Worst case O(nlogn)
 * Best  Case O(nlogn)
 * Averge  case O(nlogn)
 * Space complexity O(n)
 */
public class BubleSort {

	static int[] array= {8,1,7,9,0,10,5}; 
	public BubleSort() {
		// TODO Auto-generated constructor stub
	}

	public void sort(){
		System.out.println("Unsorted");
		display();
		System.out.println("while sorting");
		
		for(int i=array.length-1;i>=0;i--){
		 for(int j=0;j<i;j++){
			 if(array[j]>=array[j+1]){
				System.err.println(array[j]+ "<=Swaping=>"+array[j+1]);
				 
				 int t=array[j];
				  array[j]=array[j+1];
				  array[j+1]=t;
			 }				display();
			 
		 }
			
		}
		System.out.println("after sorting");
		display();
		
		
	}
	public void display(){
		System.out.println(" Sort Array");
		System.out.print( "[" );
		for ( int i : array) 
			System.out.print(" "+ i +" ");
		System.out.println("]" );		
	}
	
	public static void main(String[] args){
		BubleSort bs= new BubleSort();
		bs.sort();
		bs.display();
		
		
	}
}

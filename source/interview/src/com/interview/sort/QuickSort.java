package com.interview.sort;

public class QuickSort {

	int[] array;

	int[] tempArray;
	int length;

	public QuickSort(int[] array) {
		this.array = array;
		
		this.quickQuick(0, this.array.length-1);
	}

	public void quickQuick(int low, int high) {
		int i = low;
		int j = high;
		int pivot = array[low + (high - low)];
		while (i <= j) {

			while (array[i] < pivot) {
				i++;
			}
			while (array[j] > pivot) {
				j--;
			}

			if (i <= j) {
				exchangeNumbers(i, j);
				// move index to next position on both sides
				i++;
				j--;

			}

		}
		if (low < j)
			quickQuick(low, j);
		if (i < high)
			quickQuick(i, high);

	}

	private void exchangeNumbers(int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	
	public static void main(String[] args){
		
		int[] input = {24,2,45,20,56,75,2,56,99,53,12};
		QuickSort sort= new QuickSort(input);
		for(int i:sort.array){
            System.out.print(i);
            System.out.print(" ");
        }
	}
}

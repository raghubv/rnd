package com.interview.sort;

public class MergeSort {

	int[] array;
	int[] tempArray;
	int length;
	
	
	public void doMerge(int low,int high ){
		
		if(low<high){
			int middle=low+(high-low)/2;
			doMerge(low,middle);
			doMerge(middle+1,high);
			
			doSort(low,middle,high);
			
		}
		
	}
	
	public  MergeSort(int inputArr[]) {
        this.array = inputArr;
        this.length = inputArr.length;
        this.tempArray = new int[length];
        doMerge(0, length - 1);
    }
	
	public void doSort(int low, int middle, int high){
		
		for(int i=low;i<=high;i++){
			tempArray[i]=array[i];
		}
		int i=low,j=middle+1,k=low;
				
		while(i<=middle&&j<=high){

			if(tempArray[i]<=tempArray[j]){
				array[k++]=tempArray[i++];
				
			}else{
				array[k++]=tempArray[j++];
			}
					
			
		}
		while(i<=middle){
			array[k++]=tempArray[i++];
			
		}
		
		
		
	}

	public static void main(String[] args) {
		int[] inputArr = {45,23,11,89,77,98,4,28,65,43};
		MergeSort mergeSort= new MergeSort(inputArr);
		
		for (int i = 0; i < mergeSort.array.length; i++) {
			System.out.println(">>"+mergeSort.array[i]);
		}

	}

}

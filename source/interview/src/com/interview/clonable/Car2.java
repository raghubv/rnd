package com.interview.clonable;
/**
 * 
 * 
 * 
 * @author os-Raghu
 *Cloning allow for Single tone
 *Static properties take reference
 */
public class Car2 implements Cloneable {
    private static Car2 car = null;
    public  static String color; 
    
    public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Car2(String color){
    	this.color=color;
    }

    public static Car2 GetInstance(String color) {
        if (car == null) {
            car = new Car2(color);
        }
        return car;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public static void main(String arg[]) throws CloneNotSupportedException {
        car = Car2.GetInstance("Read");
        Car2 car1 = (Car2) car.clone();
        car1.setColor("blue");
        
        System.out.println(car.getColor());// getting the hash code
        System.out.println(car1.getColor());
        System.out.println(car.getColor());
    }
}

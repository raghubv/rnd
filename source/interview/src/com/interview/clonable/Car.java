package com.interview.clonable;
/**
 * 
 * 
 * 
 * @author os-Raghu
 *Cloning allow for Single tone
 *Static properties take reference
 */
public class Car implements Cloneable {
    private static Car car = null;
    public String color; 
    
    public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Car(String color){
    	this.color=color;
    }

    public static Car GetInstance(String color) {
        if (car == null) {
            car = new Car(color);
        }
        return car;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public static void main(String arg[]) throws CloneNotSupportedException {
        car = Car.GetInstance("Read");
        Car car1 = (Car) car.clone();
        car1.setColor("blue");
        
        System.out.println(car.getColor());// getting the hash code
        System.out.println(car1.getColor());
        System.out.println(car.getColor());
    }
}

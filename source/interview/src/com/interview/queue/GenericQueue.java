package com.interview.queue;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author os-Raghu
 * FIFO -Data structure 
 * Time complexity O(logn)
 * @param <T>
 */
public class GenericQueue<T> {

	private List<T> queue = new ArrayList<T>();
	private int maxSize = 10;
	private int currentSize = 0;
	private int front = 0;
	private int rear = -1;

	public void eQqueue(T item) {
		if (currentSize == maxSize)
			System.err.println("Queue full");
		else {
			queue.add(++rear, item);
			;
			currentSize++;
		}
	}

	public T dQqueue() {
		T t = null;
		if (currentSize == 0)
			System.err.println("Queue is empty");
		else {
			t = queue.get(front);
			front++;
			currentSize--;
		}

		return t;

	}

	
	public T peek() {
		T t = null;
		if (currentSize == 0)
			System.err.println("Queue is empty");
		else 
			t = queue.get(front);
			
		

		return t;

	}
	
	public int getSize() {
		
		

		return currentSize;

	}
	public void display() {

		for (T t : queue) {
			System.out.println(">>" + t);
		}
	}

	public static void main(String[] args) {

		GenericQueue<Integer> queue = new GenericQueue<Integer>();
		for (int i = 0; i < 15; i++) {
			queue.eQqueue(i);
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		queue.display();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		for (int i = 0; i < 15; i++) {
			System.out.println(queue.dQqueue());
		}

		
		for (int i =20; i < 35; i++) {
			queue.eQqueue(i);
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		queue.display();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		for (int i =20; i < 35; i++) {
			System.out.println(queue.peek());
		}
		System.out.println("Size>>>"+queue.getSize());
	
	}

}

package com.interview.designpattern.factorypattern;

public class ShapeFactory {

	
	public static Shape getShape(String type){
		Shape shape=null;
		
		if(type.contentEquals("circle"))
			shape=new CircleShape();
		if(type.contentEquals("square"))
			shape=new SquareShape();
		
		
		return shape;
		
		
	}

	public static void main(String[] args){
		
		ShapeFactory.getShape("circle").drawShape();;
		ShapeFactory.getShape("square").drawShape();;
	}
	
	
}

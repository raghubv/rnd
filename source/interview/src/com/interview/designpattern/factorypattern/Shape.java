package com.interview.designpattern.factorypattern;

public interface Shape {

	public void drawShape(); 

}

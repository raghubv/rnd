package com.interview.stack;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author os-Raghu
 *
 * @param <T>
 * 
 * FIFO Data Structure 
 * 
 * Time complexity O(n)
 * 
 */

public class GenericStack<T> {

	
	  private int top=-1;
	  private int maxSize=10;
	  private final List<T> stack= new ArrayList<T>();
	 
	  
   public void push(T item){
	   if(isFullSize())
		System.out.println("[stack overflow]");
	   else	   
	   stack.add(++top, item);
   }
 
   
   public T pop(){
	   
	   T item=null; 
	   if(isEmpty())
		   System.out.println("[stack overflow]");
	   else{
	   item=stack.get(top);
	   stack.remove(top--);
	   }
	   return item;
	   
   }
	  
	public boolean isFullSize(){
//		if(stack.size()==maxSize)
//		return false;
//		else
//		return true;	
		return ((stack.size()==maxSize)? true: false);
		
		
	}
	
	public boolean isEmpty(){
//		if(stack.size()==maxSize)
//		return false;
//		else
//		return true;	
		return ((top==-1)? true: false);
		
		
	}
   
	public int getSize(){
		return stack.size();
	}

	
	public static void main(String[]  args){
		
//		GenericStack<String> stack = new GenericStack<String>();
//
//		for(int i=1;i<20;i++){
//			stack.push(String.valueOf(i));
//			
//		}
//		for(int i=1;i<20;i++){
//			System.out.println(stack.pop());
//			
//		}
		
		
		GenericStack<Integer> stack = new GenericStack<Integer>();
		
				for(int i=20;i<35;i++){
					stack.push(i);
					
				}
				for(int i=20;i<35;i++){
					System.out.println(stack.pop());
					
				}
	}
	
	
}

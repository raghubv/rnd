package com.interview.extra;

public class SubstringSum {

	public static boolean checkSubStringSum(int[] array, int sum) {
		boolean isSubStringSumPresent = false;

		for (int i = 0; i < array.length; i++) {
			if(isSubStringSumPresent)
				break;
			int j = i;
			int tempSum = array[j];
			while (tempSum <= sum && j < array.length-1) {
				if (tempSum == sum){
					isSubStringSumPresent=true;
					break;
				}
				tempSum = tempSum + array[++j];

			}

		}

		return isSubStringSumPresent;

	}

	public static void main(String[] args) {
   int[] array={9,7,4,12,8};
   
   System.out.println(">>>>>>>>"+checkSubStringSum(array,20));
		
		
	}

}

package com.interview.extra;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestInterval {

	public int solution(int[] a, int[] b) {
		int count = 0;

		Map<Integer, Integer> countLs = new HashMap<>();

		for (int i = 0; i < a.length; i++) {
		
			int min =  a[i], max =  b[i];
			for (int j = i + 1; j < a.length; j++) {

				// if((a[j]<= min && b[j]>=max) || (a[j]>= min && a[j]<=max)
				// ||(b[j]>= min && b[j]<=max))
				System.out.println("[min:max]>>["+min+":"+max+"]");
				System.out.println("[a[j]:b[j]]>>["+a[j]+":"+b[j]+"]");
				
				if (!(a[j] >= min && b[j] <= max)){
				if ((a[j]<=max &&  b[j]>=max) || (a[j]<=min &&  b[j]>=min) ) {
					if(max<b[j])
					max = b[j]; 
					if(min>a[j])
					min = a[j]; 
				}
				}
				System.out.println("[min:max]>>["+min+":"+max+"]");
//
//				int[] a = { 1, 12, 42, 70, 36, -4, 43, 15 };
//				int[] b = { 5, 15, 44, 72, 36, 2, 69, 24 };
			}
			if (!countLs.containsKey(min)){
			System.out.println(min+">>>>>>>>>>"+max);
				
				countLs.put(min, max);
			}
		}
		Set<Entry<Integer, Integer>>  ls=countLs.entrySet();

		System.out.println("#####################################");
		for (Entry<Integer, Integer> entry : ls) {
			System.out.println(entry.getKey()+":"+entry.getValue());
		}
		
		return countLs.size();
	}

	public static void main(String[] args) {
		// int[] a={-1, 2,4};
		// int[] b={3, 6,9}; //{2,6},{-1,3},{4,9} {-1,9}
		//

		int[] a = { 1, 12, 42, 70, 36, -4, 43, 15 };
		int[] b = { 5, 15, 44, 72, 36, 2, 69, 24 }; // {2,6},{-1,3},{4,9} {-1,9}

		System.out.println((new TestInterval()).solution(a, b));
	}

}

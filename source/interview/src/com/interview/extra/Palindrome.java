package com.interview.extra;

/**
 * 
 * Check palindrome with out suing stringBuilder, strinBuffer ,array & collection 
 *
 */
public class Palindrome {

 
	public static boolean checkPolindrome(String input){
		boolean isPolindrome=true;
		
		int max=input.length(), i=0, j=max-1;
		while(i<max /2&&j>=max /2){
			if(!String.valueOf(input.charAt(i)).equalsIgnoreCase(String.valueOf(input.charAt(j)))){
				isPolindrome=false;
				break;
			}
			
			i++;
			j--;
		}
//		if(j==-1&& i==max)
//			isPolindrome=true;
		
		return isPolindrome;
	}

	
	public static void main(String[] args){
		
		System.out.println("1.check polindrome[abccba]>>"+checkPolindrome("abccba"));
		System.out.println("2.check polindrome[121]>>"+checkPolindrome("121"));
		System.out.println("2.check polindrome[raghu]>>"+checkPolindrome("raghu"));
		
	}

}

package com.interview.extra;

public class BomberAlgo {

	
	public static String runBomberAlgo(String str){
		
		char[] charAr= str.toCharArray();
		if(charAr.length>1)
		for(int i=0;i<charAr.length-1;i++){
			
			
			int j=i+1;
			String tmp=String.valueOf(charAr[i]);
			while(j<=charAr.length-1 &&  charAr[i]==charAr[j] ){
				j++;
				tmp+=String.valueOf(charAr[i]);
			
			}
			if(j>i+1){			
				str=str.replaceFirst(tmp, "");
				str=runBomberAlgo(str);
			}
			
		}
		
		return str;
	}
	
	public static void main(String[] args){
//		System.out.println(runBomberAlgo("abbbCCC"));
//		System.out.println(runBomberAlgo("a"));
//		
		
		System.out.println(runBomberAlgo("xygjaaajhgjhgaggaaaahlkhllkhjfaaaaajgbbbbbbbbbbbbbyxxymn"));
		}

}

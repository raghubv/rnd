package com.interview.extra;

import java.util.ArrayList;
import java.util.List;

public class B extends A {
	public static void test(){
		System.out.println("from b");
	}
	public B() {
		System.out.println("B");
		
	}
	
	public void printB(){
		System.out.println("B");
	}

	public static void main(String[] args){
		B.test();
		
		
		List<A> ls= new ArrayList<A>();
		ls.add(new B());
		B b=(B)ls.get(0);
		b.printA();
		b.printB();
		System.out.println(">>");
		b.test();
		
		
		A a=ls.get(0);
		a.printA();
		System.out.println(">>");
		a.test();
		//a.printB();
		
	}
	
}

package com.interview.extra;

public class ReverseNumber {

	
	public static int reverseNumber(int number){
        
        int reverse = 0;
        while(number != 0){
            reverse = (reverse*10)+(number%10);
            number = number/10;
        } 
        return reverse;
    }
	
	
	public static void main(String[] args){
		
		System.out.println("reverseNumber(7777666)>>"+reverseNumber(7777666));
		
	}
	

}

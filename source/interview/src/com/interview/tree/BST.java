package com.interview.tree;

import java.util.Iterator;

public class BST {
	Node root = null;

	public void insert(int value) {
		if (root == null)
			root = new Node(value);
		else
			insert(root, new Node(value));
	}

	public void insert(Node currentRoot, Node nNode) {

		// left subtree
		if (nNode.value <= currentRoot.value) {
			if (currentRoot.left == null) {
				currentRoot.left = nNode;
				return;
			} else
				insert(currentRoot.left, nNode);

		} else {
			if (currentRoot.right == null) {
				currentRoot.right = nNode;
				return;
			} else
				insert(currentRoot.right, nNode);
		}

	}

	public void preorder() {
		// visit root node
		// Traverse left sub tree
		// Traverse righ sub tree
		preorder(root);
	}

	public void preorder(Node currentNode) {

		if (currentNode != null) {
			System.out.println(">>" + currentNode.value);
			preorder(currentNode.left);
			preorder(currentNode.right);
		}

	}

	public void postorder() {
		// visit root node
		// Traverse left sub tree
		// Traverse righ sub tree
		postorder(root);
	}

	public void postorder(Node currentNode) {

		if (currentNode != null) {

			postorder(currentNode.left);
			postorder(currentNode.right);
			System.out.println(">>" + currentNode.value);
		}

	}

	public void inorder() {
		// visit root node
		// Traverse left sub tree
		// Traverse righ sub tree
		inorder(root);
	}

	public void inorder(Node currentNode) {

		if (currentNode != null) {

			inorder(currentNode.left);
			System.out.println(">>" + currentNode.value);
			inorder(currentNode.right);

		}

	}

	class Node {
		int value;
		Node left, right;

		public Node(int value) {
			this.value = value;
			this.left = null;
			this.right = null;

		}

	}

	public static void main(String[] args) {
		BST bst= new BST();
		int[] array= {9,10,3,2,1,4};
		
		for (int i : array) {
			System.out.println(i);
			bst.insert(i);
		}
	
		System.out.println("preorder");
		bst.preorder();
		System.out.println("inorder");
		bst.inorder();
		System.out.println("postorder");
		bst.postorder();
	}
	
}

package com.interview.linkedList;

public class DoubeLinkedList<T> {

	int size = 0;
	Node head = null;
	Node tail = null;

	static public class Node<T> {
		Node prev, next;
		T data;

		public Node(T data, Node prev, Node next) {
			this.data = data;
			this.prev = prev;
			this.next = next;

		}
	}

	
	
	public void addFirst(T data) {
		Node<T> nNode = new Node(data, null, null);
		if (head != null)
			nNode.next = head;
		head = nNode;
		if (tail == null)
			tail = nNode;
		
		size++;

	}

	public void addLast(T data) {

		Node nNode = new Node(data, null, null);
		if (tail != null){
		nNode.prev = tail;
		tail.next=nNode;
		}
		tail = nNode;
		if (head == null)
			head = nNode;
		
		size++;
	}

	public void display() {
		Node temp = head;
		System.out.print("[");
		while (temp != null) {
			System.out.print(" " + temp.data + " ");
			temp = temp.next;
		}
		System.out.print("]");
	}

	public static void main(String[] args) {

		DoubeLinkedList<Integer> dls = new DoubeLinkedList<Integer>();
		dls.addFirst(2);
		dls.addFirst(1);
		dls.addLast(3);
		dls.addFirst(4);
		dls.addFirst(5);
		dls.addFirst(5);
		dls.addFirst(4);
		dls.addLast(3);
		dls.display();

	}
}

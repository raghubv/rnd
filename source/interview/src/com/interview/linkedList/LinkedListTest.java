package com.interview.linkedList;

public class LinkedListTest {

	public static void main(String args[]) { 
		
		SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
		
		singlyLinkedList.append("Java"); 
		singlyLinkedList.append("JEE"); 
		singlyLinkedList.append("Android ");
		singlyLinkedList.append("tests ");

		System.out.println("Singly linked list contains : " + singlyLinkedList);
		System.out.println("length of linked list : " + singlyLinkedList.length()); 
		System.out.println("is this linked list empty : " + singlyLinkedList.isEmpty());

		
		 SinglyLinkedList iList = new SinglyLinkedList();
        iList.append(202);
        iList.append(404);
        
        System.out.println("linked list : " + iList); 
       System.out.println("length : " + iList.length());
		
	}
}

package com.interview.string;

public class StringDemo {
/**
 * 
 * 1. Why is String required in java
 * - maintain constant
 * - immutable object required for class name , object reference id etc... for JVm 
 */
	
public static void main(String[] args){
String s0="abbc";  //Object
String s1="abbc";  //Object have same reference
String s2=new String("abbc"); //Object get new reference internal used to check
System.out.println("s0 equals s1>>>"+ s0.equals(s1));
System.out.println("s0 == s1>>>"+ (s0==s1));
System.out.println("s1 equals s2>>>"+ s1.equals(s2));
System.out.println("s1 == s2>>>"+ (s1==s2));
	
}


}

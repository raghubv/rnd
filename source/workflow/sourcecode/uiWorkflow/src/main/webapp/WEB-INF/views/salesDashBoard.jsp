<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<link href="${context}/assets/css/nv.d3.css" rel="stylesheet"
	type="text/css">
<script src="${context}/assets/js/d3.min.js" charset="utf-8"></script>
<script src="${context}/assets/js/nv.d3.js"></script>
<script src="${context}/assets/js/stream_layers.js"></script>
<link rel="stylesheet" href="${context}/assets/ammap/ammap.css"
	type="text/css">
<script src="${context}/assets/ammap/ammap.js" type="text/javascript"></script>
<!-- map file should be included after ammap.js -->
<%-- <script src="${context}/assets/ammap/maps/js/worldLow.js" --%>
<!-- 	type="text/javascript"></script> -->
<script src="/olap/js/worldLow.js" type="text/javascript"></script>
<script src="/olap/js/continentsLow.js" type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="${context}/assets/js/salesdashboard/dashboardChart.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/salesdashboard/dashboardMap.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/ajax/ajax.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="${context}/assets/plugins/gritter/css/jquery.gritter.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" media="screen" />
<link
	href="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
	href="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" />
<link rel="stylesheet" type="text/css"
	href="${context}/assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
<!-- END PAGE LEVEL STYLES -->
<style>
text {
	font: 12px sans-serif;
}

svg {
	display: block;
}

html, body, #chart1, svg {
	margin: 0px;
	padding: 0px;
	height: 100%;
	width: 100%;
}
</style>
</head>
<input type="hidden" id="statsBy" name="statsBy" value="Bar" />
<input type="hidden" id="jvectormap-selected-regions"
	name="jvectormap-selected-regions" value="Year" />
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div id="portlet-config" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button"></button>
			<h3>Widget Settings</h3>
		</div>
		<div class="modal-body">Widget settings form goes here</div>
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="row-fluid">
		<div class="span12">
			<div class="clearfix"></div>
		</div>
	</div>

	<!-- BEGIN PAGE CONTAINER-->


	<div class="container-fluid">
		<div id="dashboard">
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>

			<!-- BEGIN REGIONAL STATS PORTLET-->
			<div class="row-fluid form-horizontal">
				<div class="span2">
					<div class="btn-toolbar">
						<div class="btn-group" data-toggle="buttons-radio">
							<a id="statsByBar" class="btn mini active">Bar</a> <a
								id="statsByLine" class="btn mini">Line</a>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group">
						<div class="controls">
							<i class="icon-calendar"></i> <span class="text-inline">&nbsp;&nbsp;</span>
							<span class="text-inline">&nbsp;From&nbsp;</span> <input
								class="m-wrap small" size="16" type="text" value=""
								id="ui_date_picker_range_from" /> <span class="text-inline">&nbsp;To&nbsp;</span>
							<input class="m-wrap small" size="16" type="text" value=""
								id="ui_date_picker_range_to" />
						</div>
					</div>
				</div>
				<div class="span4">
					<label class="control-label">Tenant</label>
					<div class="controls">
						<select class="span4 chosen" data-placeholder="Choose a Category"
							tabindex="1" id="tenant" name="tenant">
							<option value="101">Kyv</option>
							<option value="102">Geo</option>
						</select>
					</div>

				</div>

			</div>
			<div class="clearfix"></div>
			<div class="row-fluid">
				<div class="span4">
					<div id="vmap_world" style="height: 400px;"></div>
				</div>
				<!-- Start -->
				<div class="span8">
					<!-- Sale Analysis Start-->
					<div class="accordion scrollable" id="accordion1"
						id="sales_analysis_statistics_content" style="display: block">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion1" href="#collapse_1_2"
									id="product_category_sales"> Product Category sales </a>
							</div>
							<div id="collapse_1_2" class="accordion-body collapse">
								<div class="accordion-inner ">
									<div class="row-fluid">
										<div class="span6">
											<div id="productCategoryMultiSelectionOptn1" align="center"></div>
										</div>
										<div class="span6">
											<div>
												<div id="chart11" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
											<div>
												<div id="chart12" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion1" href="#collapse_1_3"> Secondary
									sales </a>
							</div>
							<div id="collapse_1_3" class="accordion-body collapse">
								<div class="accordion-inner">
									<!-- 											<div id="secondary_sales"></div> -->
									<div class="row-fluid">
										<div class="span12">
											<div>
												<div id="chart61" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
											<div>
												<div id="chart62" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
											<div>
												<div id="chart63" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion1" href="#collapse_1_4"
									id="product_category_sales"> Stock By Product Category </a>
							</div>
							<div id="collapse_1_4" class="accordion-body collapse">
								<div class="accordion-inner">
									<div class="row-fluid">
										<div class="span6">
											<div id="productCategoryMultiSelectionOptn" align="center"></div>
										</div>
										<div class="span6">

											<div>
												<div id="chart43" style="height: auto;">
													<svg> </svg>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion1" href="#collapse_1_5"
									id="stock-by-org-type"> Stock By Organization Type </a>
							</div>
							<div id="collapse_1_5" class="accordion-body collapse">
								<div class="accordion-inner">
									<div>
										<div id="chart151" style="height: auto;">
											<svg> </svg>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion1" href="#collapse_1_6"
									id="stock-by-org-type"> KS Vs Non-Ks Order</a>
							</div>
							<div id="collapse_1_6" class="accordion-body collapse">
								<div class="accordion-inner">
									<div>
										<div id="chart71" style="height: auto;">
											<svg> </svg>
										</div>
										<div id="chart72" style="height: auto;">
											<svg> </svg>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script
	src="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.resize.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.pulsate.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/date.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/gritter/js/jquery.gritter.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${context}/assets/scripts/app.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/index.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/ui-jqueryui.js"></script>
<script type="text/javascript"
	src="${context}/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script>
	$(document).ready(function() {
		//App.init();
		RevenueLoss.init();
		DashBoardChart.init();

		UIJQueryUI.init();

	});
</script>

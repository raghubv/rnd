<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<link href="${context}/assets/css/nv.d3.css" rel="stylesheet"
	type="text/css">
<script src="${context}/assets/js/d3.min.js" charset="utf-8"></script>
<script src="${context}/assets/js/nv.d3.js"></script>
<script src="${context}/assets/js/stream_layers.js"></script>
<link rel="stylesheet" href="${context}/assets/ammap/ammap.css"
	type="text/css">
<script src="${context}/assets/ammap/ammap.js" type="text/javascript"></script>
<!-- map file should be included after ammap.js -->
<%-- <script src="${context}/assets/ammap/maps/js/worldLow.js" --%>
<!-- 	type="text/javascript"></script> -->
<script src="/olap/js/worldLow.js" type="text/javascript"></script>
<script src="/olap/js/continentsLow.js" type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="${context}/assets/js/dashboard/dashboardChart.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/dashboard/dashboardMap.js"
	type="text/javascript"></script>
<script src="${context}/assets/js/ajax/ajax.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="${context}/assets/plugins/gritter/css/jquery.gritter.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.css"
	rel="stylesheet" type="text/css" />
<link
	href="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" media="screen" />
<link
	href="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css"
	href="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" />
<!-- END PAGE LEVEL STYLES -->
<style>
text {
	font: 12px sans-serif;
}

svg {
	display: block;
}

html, body, #chart1, svg {
	margin: 0px;
	padding: 0px;
	height: 100%;
	width: 100%;
}
</style>
</head>
<input type="hidden" id="statsBy" name="statsBy" value="Bar" />
<input type="hidden" id="jvectormap-selected-regions"
	name="jvectormap-selected-regions" value="Year" />
<div class="">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div id="portlet-config" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button"></button>
			<h3>Widget Settings</h3>
		</div>
		<div class="modal-body">Widget settings form goes here</div>
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="color-panel hidden-phone">
					<div class="color-mode-icons icon-color"></div>
					<div class="color-mode-icons icon-color-close"></div>
					<div class="color-mode">
						<p>THEME COLOR</p>
						<ul class="inline">
							<li class="color-black current color-default"
								data-style="default"></li>
							<li class="color-blue" data-style="blue"></li>
							<li class="color-brown" data-style="brown"></li>
							<li class="color-purple" data-style="purple"></li>
							<li class="color-grey" data-style="grey"></li>
							<li class="color-white color-light" data-style="light"></li>
						</ul>
						<label> <span>Layout</span> <select
							class="layout-option m-wrap small">
								<option value="fluid" selected>Fluid</option>
								<option value="boxed">Boxed</option>
						</select>
						</label> <label> <span>Header</span> <select
							class="header-option m-wrap small">
								<option value="fixed" selected>Fixed</option>
								<option value="default">Default</option>
						</select>
						</label> <label> <span>Sidebar</span> <select
							class="sidebar-option m-wrap small">
								<option value="fixed">Fixed</option>
								<option value="default" selected>Default</option>
						</select>
						</label> <label> <span>Footer</span> <select
							class="footer-option m-wrap small">
								<option value="fixed">Fixed</option>
								<option value="default" selected>Default</option>
						</select>
						</label>
					</div>
				</div>
				<!-- END BEGIN STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Dashboard <small>statistics and more</small>
				</h3>
				<ul class="breadcrumb">
					<li><i class="icon-home"></i> <a href="index.html">Home</a> <i
						class="icon-angle-right"></i></li>
					<li><a href="#">Dashboard</a></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="dashboard">
			<!-- END DASHBOARD STATS -->
			<div class="clearfix"></div>

			<!-- BEGIN REGIONAL STATS PORTLET-->
			<div class="portlet box white">
				<div class="portlet-title">
					<div class="actions">
						<a id="sales_analysis" class="btn green"> Sales Analysis</a> <a
							id="channel_analysis" class="btn yellow">Channel Analysis</a> <a
							id="product_analysis" class="btn blue">Product Analysis</a>
					</div>
				</div>
				<div class="portlet-body">
					<div style="display: block">
						<div class="row-fluid">
							<div class="span6">
								<div class="btn-toolbar">
									<div class="btn-group" data-toggle="buttons-radio">
										<a id="statsByBar" class="btn mini active">Bar</a> <a
											id="statsByLine" class="btn mini">Line</a>
									</div>
								</div>
							</div>
							<div class="span6">
								<div class="control-group">
									<div class="controls">
										<i class="icon-calendar"></i> <span class="text-inline">&nbsp;&nbsp;</span>
										<span class="text-inline">&nbsp;From&nbsp;</span> <input
											class="m-wrap small" size="16" type="text" value=""
											id="ui_date_picker_range_from" /> <span class="text-inline">&nbsp;To&nbsp;</span>
										<input class="m-wrap small" size="16" type="text" value=""
											id="ui_date_picker_range_to" />
									</div>
								</div>
							</div>
						</div>
						<div class="row-fluid">

							<div class="span6">
								<div class="portlet box ">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-reorder"></i>Map
										</div>

									</div>
									<div class="portlet-body">
										<div id="vmap_world" style="height: 400px;"></div>
									</div>
								</div>
							</div>

							<!-- Start -->
							<div class="span6">
								<!-- Sale Analysis Start-->
								<div class="portlet box "
									id="sales_analysis_statistics_content" style="display: block">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-reorder"></i> Sale Analysis
										</div>
									</div>
									<div class="portlet-body">
										<div class="accordion scrollable" id="accordion1">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion1"
														href="#collapse_1_1" id="total_sales"> SIM Sold V/S
														SIM Active </a>
												</div>
												<div id="collapse_1_1" class="accordion-body collapse">
													<div class="accordion-inner">
														<div class="dashboard-stat green">
															<div class="visual">
																<i class="icon-shopping-cart"></i>
															</div>
															<div class="details">
																<div class="number" id="simcount"></div>
																<div class="desc">SIM Sold V/S SIM Active</div>
															</div>
														</div>

														<div id="product_sells_statistics_content">
															<div id="chart10" style="height: 200px;">
																<svg> </svg>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion1"
														href="#collapse_1_2" id="product_category_sales">
														Product Category sales </a>
												</div>
												<div id="collapse_1_2" class="accordion-body collapse">
													<div class="accordion-inner">
														<div>
															<div id="chart11" style="height: 200px;">
																<svg> </svg>
															</div>
														</div>
														<div>
															<div id="chart12" style="height: 200px;">
																<svg> </svg>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion1"
														href="#collapse_1_3" > Secondary
														sales </a>
												</div>
												<div id="collapse_1_3" class="accordion-body collapse">
													<div class="accordion-inner">
														<div  id="secondary_sales"></div>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion1"
														href="#collapse_1_4" id="product_category_sales">
														Stock By Product Category </a>
												</div>
												<div id="collapse_1_4" class="accordion-body collapse">
													<div class="accordion-inner">
														<!-- div>
															<div id="chart41" style="height: 400px;">
																<svg> </svg>
															</div>
														</div-->
														<!-- div>
															<div id="chart42" style="height: 400px;">
																<svg> </svg>
															</div>
														</div-->
														<div>
															<div id="chart43" style="height: 400px;">
																<svg> </svg>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion1"
														href="#collapse_1_5" id="stock-by-org-type">
														Stock By Organization Type </a>
												</div>
												<div id="collapse_1_5" class="accordion-body collapse">
													<div class="accordion-inner">
														<div>
															<div id="chart151" style="height: 400px;">
																<svg> </svg>
															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Sale Analysis End-->
								<!-- Channel Analysis Start-->
								<div class="portlet box "
									id="channel_analysis_statistics_content" style="display: none">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-reorder"></i> Channel Analysis
										</div>

									</div>
									<div class="portlet-body">
										<div class="accordion scrollable" id="accordion2">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion2"
														href="#collapse_2_1" id="channel_analysis"> Channel
														Analysis</a>
												</div>
												<div id="collapse_2_1" class="accordion-body collapse">
													<div class="accordion-inner">
														<div id="chart20" style="height: 200px;">
															<svg> </svg>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Channel Analysis End-->
								<!-- Product Analysis Start -->
								<div class="portlet box "
									id="product_analysis_statistics_content" style="display: none">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-reorder"></i> Product Analysis
										</div>

									</div>
									<div class="portlet-body">
										<div class="accordion scrollable" id="accordion3">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion3"
														href="#collapse_3_1" id="product_analysis"> Product</a>
												</div>
												<div id="collapse_3_1" class="accordion-body collapse">
													<div class="accordion-inner">
														<div>
															<div id="chart30" style="height: 200px;">
																<svg> </svg>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed"
														data-toggle="collapse" data-parent="#accordion3"
														href="#collapse_3_2" id="product_analysis"> Product
														Category</a>
												</div>
												<div id="collapse_3_2" class="accordion-body collapse">
													<div class="accordion-inner">
														<div>
															<div id="chart31" style="height: 200px;">
																<svg> </svg>
															</div>
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<!-- Product Analysis End -->


							</div>
						</div>
						<br />
						<div class="clearfix"></div>
						<div class="row-fluid" id="stats_content" style="display: none">
							<div class="span3 ">
								<div class="well">
									<div class="sparkline-chart"
										style="border-color: black; border-width: 11px;">
										<div class="number" id="sparkline_bar_last_year"></div>
										<a class="title" href="#">Last Year</a>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="well">
									<div class="sparkline-chart"
										style="border-color: black; border-width: 11px;">
										<div class="number" id="sparkline_bar_last_6month"></div>
										<a class="title" href="#">Last 6 Month</a>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="well">
									<div class="sparkline-chart"
										style="border-color: black; border-width: 11px;">
										<div class="number" id="sparkline_bar_last_quarter"></div>
										<a class="title" href="#">Last Quarter</a>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="well">
									<div class="sparkline-chart"
										style="border-color: black; border-width: 11px;">
										<div class="number" id="sparkline_bar_last_month"></div>
										<a class="title" href="#">Last Month</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END REGIONAL STATS PORTLET-->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script
	src="${context}/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/flot/jquery.flot.resize.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.pulsate.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/date.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/gritter/js/jquery.gritter.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"
	type="text/javascript"></script>
<script
	src="${context}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"
	type="text/javascript"></script>
<script src="${context}/assets/plugins/jquery.sparkline.min.js"
	type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${context}/assets/scripts/app.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/index.js" type="text/javascript"></script>
<script src="${context}/assets/scripts/ui-jqueryui.js"></script>
<script>
	$(document).ready(function() {
		//App.init();
		RevenueLoss.init();
		DashBoardChart.init();
		UIJQueryUI.init();

	});
</script>
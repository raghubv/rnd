
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<ul class="page-sidebar-menu">
		<li>
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<div class="sidebar-toggler hidden-phone"></div> <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		</li>
		<li>
			<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
			<form class="sidebar-search">
				<div class="input-box">
					<a href="javascript:;" class="remove"></a> <input type="text"
						placeholder="Search..." /> <input type="button" class="submit"
						value=" " />
				</div>
			</form> <!-- END RESPONSIVE QUICK SEARCH FORM -->
		</li>
		<c:forEach var="menu" items="${menus['menus']}" varStatus="loop">
			<li class="${menu['class']}"><a href="${menu['url']}"> <i
					class="${menu['icon']}"></i> <span class="title">${menu['name']}</span>
					<span class="${menu['selected']}"></span>
			</a>
				<ul class="sub-menu">
					<c:forEach var="subMenu" items="${menu['subMenus']}"
						varStatus="loop">
						<c:if
							test="${subMenu['visible'] eq true or subMenu['visible'] eq 'true' }">
							<li><a href="${subMenu['url']}">${subMenu['name']}</a></li>
						</c:if>
					</c:forEach>
				</ul></li>
		</c:forEach>
	</ul>
	<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
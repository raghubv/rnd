<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.enhancesys.util.Configuration"%>
<%session.setAttribute("menus", Configuration.getMenuData());%>
<%session.setAttribute("context", request.getContextPath());%>
<html>
<body class="page-header-fixed page-sidebar-closed">
	<div id="header">
		<tiles:insertAttribute name="header" />
	</div>
	<div class="page-container">
		<div id="menu">
			<tiles:insertAttribute name="menu" />
		</div>
		<div id="content">
			<tiles:insertAttribute name="content" />
		</div>
	</div>
	<div id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>
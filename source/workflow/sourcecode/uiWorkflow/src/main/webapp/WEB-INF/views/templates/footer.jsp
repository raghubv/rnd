<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- BEGIN FOOTER -->
	<div class="footer">
		<div class="footer-inner">
			2015 &copy; Enhancesys.
		</div>
		<div class="footer-tools">
			<span class="go-top">
			<i class="icon-angle-up"></i>
			</span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="/uiDashBoard/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="/uiDashBoard/assets/plugins/excanvas.min.js"></script>
	<script src="/uiDashBoard/assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="/uiDashBoard/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="/uiDashBoard/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="/uiDashBoard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="/uiDashBoard/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="/uiDashBoard/assets/scripts/app.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->  
	
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   
		});
	</script>
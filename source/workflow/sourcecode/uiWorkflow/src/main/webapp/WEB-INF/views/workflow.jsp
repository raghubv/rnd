<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<script src="${context}/assets/js/ajax/ajax.js" type="text/javascript"></script>
</head>
<div class="page-content">
	<div class="container-fluid">

		<div class="row-fluid form-horizontal">
			<div class="span12">
				<!-- BEGIN REGIONAL STATS PORTLET-->
				<div class="portlet box purple">
					<div class="portlet-title">
						<div class="caption">
							<i class=""></i>Add Workflow Configuration
						</div>
						<div class="tools">
							<a href="" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body row-fluid"
						style="overflow: auto; max-height: 100%; width: 100%">
						<div class="span12">
							Workflow Name &nbsp;&nbsp; <input type="text" name="workflowName"
								id="workflowName" />&nbsp;&nbsp; Event &nbsp;&nbsp; <input
								type="text" name="eventName" id="eventName" />&nbsp;&nbsp;
							Qterms &nbsp;&nbsp; <select multiple="multiple" name="qterms"
								id="qterms" ></select> Created_by &nbsp;&nbsp; <select
								multiple="multiple" name="createdBy" id="createdBy">
							</select>
						</div>
						<div class="clearfix"></div>
						<div class="span12">
							<table cellspacing="10">
								<thead>
									<th>Initial Notification</th>
								</thead>
								<tr>
									<td>Posisition</td>
									<td><select multiple="multiple"
										id="notification_position_0" name="notification_position_0" />
									<td>Linked Organization Type</td>
									<td><select id="notification_link_org_type_0"
										name="notification_link_org_type_0" /></td>
									<td>Global <input type="checkbox"
										id="notification_is_global_0" name="notification_is_global_0"
										value="false" onclick="changeValue(this.id, this.value);">
									</td>
									<td>Future Task <input type="checkbox"
										id="notification_future_task_0"
										name="notification_future_task_0" value="false"
										onclick="changeValue(this.id, this.value);">
									</td>
									<td>Email <input type="checkbox" id="notification_email_0"
										name="notification_email_0" value="false"
										onclick="changeValue(this.id, this.value);">
									</td>
									<td>SMS <input type="checkbox" id="notification_sms_0"
										name="notification_sms_0" value="false"
										onclick="changeValue(this.id, this.value);">
									</td>
									<td>Notification Template</td>
									<td><select id="notification_template_0"
										name="notification_template_0" /></td>

								</tr>
							</table>
						</div>

						<div class="clearfix"></div>

						<div class="span12">
							<span
								style="font: normal 12px agency, arial; color: blue; text-decoration: underline; cursor: pointer;"
								onclick="addMoreRows(this.form);"> Add</span>
							<button onclick="check();" value="Submit">Submit</button>

						</div>
						<div class="clearfix"></div>
						<div class="span12">
							<table id="addedRows" cellspacing="10">
								<thead>
									<th>Configuration</th>
								</thead>
							</table>
						</div>
						<div class="clearfix"></div>
						<div class="span12">
							<table id="approvalRows" cellspacing="10">
								<thead>
									<th>Approvals</th>
								</thead>
							</table>
						</div>
						<div class="clearfix"></div>
						<div class="span12">
							<table id="notificationRows" cellspacing="10">
								<thead>
									<th>Notifications</th>
								</thead>
							</table>
						</div>


					</div>
				</div>
				<!-- END REGIONAL STATS PORTLET-->
			</div>
		</div>
	</div>

</div>
<script>
	var orgTypeOption;
	var positionOption;
	var taskCreateClassOption;
	var rejectClassption;
	var approveClassOption;
	var approveTemplateOption;
	var rejectTemplateOption;

	var rowCount = 0;

	var request = {
		"db" : "SNOC",
		"collection" : "transitionValueCollection",
		"query" : {
			"transitionTypeId" : "4"
		},
		"project" : {
			"_id" : 1,
			"description" : 1
		}
	};

	GlobalAjaxCall.callController('getMasterInfo.do', request, function(data) {
		var res = jQuery.parseJSON(JSON.stringify(data));
		$.each(res.resultLs, function(index, value) {
			$("#qterms").append(
					'<option value="'+value._id+'">' + value.description
							+ '</option>');
			$("#createdBy").append(
					'<option value="'+value._id+'">' + value.description
							+ '</option>');
			$("#notification_position_0").append(
					'<option value="'+value._id+'">' + value.description
							+ '</option>');
			positionOption += '<option value="'+value._id+'">'
					+ value.description + '</option>';
		});

	});

	var orgTypeRequest = {
		"db" : "SNOC",
		"collection" : "transitionValueCollection",
		"query" : {
			"transitionTypeId" : "1"
		},
		"project" : {
			"_id" : 1,
			"description" : 1
		}
	};

	GlobalAjaxCall.callController('getMasterInfo.do', orgTypeRequest, function(
			data) {
		var res = jQuery.parseJSON(JSON.stringify(data));
		$.each(res.resultLs, function(index, value) {
			$("#notification_link_org_type_0").append(
					'<option value="'+value._id+'">' + value.description
							+ '</option>');

			orgTypeOption += '<option value="'+value._id+'">'
					+ value.description + '</option>';
		});

	});

	var taskCreateClassRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "taskCreateClass"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do', taskCreateClassRequest,
			function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					taskCreateClassOption += '<option value="'+value.value+'">'
							+ value.name + '</option>';
				});
			});

	var rejectClassRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "rejectClass"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do', rejectClassRequest,
			function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					rejectClassption += '<option value="'+value.value+'">'
							+ value.name + '</option>';
				});
			});

	var approveClassRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "approveClass"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do', approveClassRequest,
			function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					approveClassOption += '<option value="'+value.value+'">'
							+ value.name + '</option>';
				});
			});

	var approveTemplateRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "approveTemplate"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do', approveTemplateRequest,
			function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					approveTemplateOption += '<option value="'+value.value+'">'
							+ value.name + '</option>';
				});
			});

	var notificationTemplateRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "notificationTemplate"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do',
			notificationTemplateRequest, function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					$("#notification_template_0").append(
							'<option value="'+value.value+'">' + value.name
									+ '</option>');
				});
			});

	var rejectTemplateRequest = {

		"db" : "workflow",
		"collection" : "workFlowMaster",
		"query" : {
			"type" : "approveTemplate"
		},
		"project" : {
			"type" : 1,
			"value" : 1,
			"name" : 1,
			"_id" : 0
		}

	};

	GlobalAjaxCall.callController('getMasterInfo.do', rejectTemplateRequest,
			function(data) {
				var res = jQuery.parseJSON(JSON.stringify(data));
				$.each(res.resultLs, function(index, value) {
					rejectTemplateOption += '<option value="'+value.value+'">'
							+ value.name + '</option>';
				});
			});

	function addMoreRows(frm) {

		rowCount++;

		var header = '<tr id="header_'+rowCount+'">'
				+ '<td>'
				+ rowCount
				+ '.</td><td>Send Notifications</td><td>&nbsp;&nbsp;<input type="checkbox"   name="header_notification_'
				+ rowCount
				+ '" id="header_notification_'
				+ rowCount
				+ '" value="true"  onclick="changeValue(this.id,this.value)"">'
				+ '<td>Global</td><td>&nbsp;&nbsp;<input type="checkbox"   name="header_global_'
				+ rowCount
				+ '" id="header_global_'
				+ rowCount
				+ '" value="true"  onclick="changeValue(this.id,this.value)"">'
				+ '<td>Approval Position &nbsp;&nbsp;</td><td width="17%"><select multiple name="header_approval_position_'+rowCount+'" id="header_approval_position_'+rowCount+'">'
				+ positionOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Organizaation Type&nbsp;&nbsp;</td><td width="17%"><select name="header_organization_type_'+rowCount+'" id="header_organization_type_'+rowCount+'">'
				+ orgTypeOption + '</select>&nbsp;&nbsp;</td>'
				+ ' <td><a href="javascript:void(0);" onclick="removeRow('
				+ rowCount + ');">Delete</a><td></tr>';
		jQuery('#addedRows').append(header);

		var approvalsDetails = '<tr id="approvals_'+rowCount+'">' + '<td>'
				+ rowCount
				+ '.</td><td>Activity&nbsp;&nbsp;</td><td width="17%"><select name="approvals_activity_'+rowCount+'" id="approvals_activity_'+rowCount+'">'
				+ '<option value="userTask">User Task</option>'
				+ '<option value="serviceTask">Service Task</option>'
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td><label>Task Name</label></td><td><input type="text" id="approvals_task_name_'+rowCount+'" name="approvals_task_name_'+rowCount+'" ></td>'
				+ '<td>Task Create&nbsp;&nbsp;</td><td width="17%"><select name="approvals_taskCreateClass_'+rowCount+'" id="approvals_taskCreateClass_'+rowCount+'">'
				+ taskCreateClassOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Reject Class&nbsp;&nbsp;</td><td width="17%"><select name="approvals_rejectClass_'+rowCount+'" id="approvals_rejectClass_'+rowCount+'">'
				+ rejectClassption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Reject Next Flow</td><td>&nbsp;&nbsp;<input type="checkbox"   name="approvals_reject_next_flow_'
				+ rowCount
				+ '" id="approvals_reject_next_flow_'
				+ rowCount
				+ '" value="end"  onclick="changeFlowValue(this.id,this.value)"">'

				+ '<td>Approve Class&nbsp;&nbsp;</td><td width="17%"><select name="approvals_approveClass_'+rowCount+'" id="approvals_approveClass_'+rowCount+'">'
				+ approveClassOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Approve Next Flow</td><td>&nbsp;&nbsp;<input type="checkbox"   name="approvals_approve_next_flow_'
				+ rowCount
				+ '" id="approvals_approve_next_flow_'
				+ rowCount
				+ '" value="end"  onclick="changeFlowValue(this.id,this.value)"">'

				+ '</tr>';

		jQuery('#approvalRows').append(approvalsDetails);

		var notificationDetails = '<tr id="notifications_'+rowCount+'">'
				+ '<td>'
				+ rowCount
				+ '.</td><td>Notification Position&nbsp;&nbsp;</td><td width="17%"><select multiple name="notifications_position_'+rowCount+'" id="notifications_position_'+rowCount+'">'
				+ positionOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Approve Template&nbsp;&nbsp;</td><td width="17%"><select name="notifications_approve_template_'+rowCount+'" id="notifications_approve_template_'+rowCount+'">'
				+ approveTemplateOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td>Reject Template &nbsp;&nbsp;</td><td width="17%"><select name="notifications_reject_template_'+rowCount+'" id="notifications_reject_template_'+rowCount+'">'
				+ rejectTemplateOption
				+ '</select>&nbsp;&nbsp;</td>'
				+ '<td><label>Future Task</label></td><td><input type="checkbox" value="false" onclick="changeValue(this.id,this.value);" id="notifications_future_task_'
				+ rowCount
				+ '" name="notifications_future_task_'
				+ rowCount
				+ ' value="false" onclick="changeValue(this.id,this.value);" /></td>'
				+ '<td><label>Email</label></td><td><input type="checkbox" id="notifications_email_'
				+ rowCount
				+ '" name="notifications_email_'
				+ rowCount
				+ '" value="false" onclick="changeValue(this.id,this.value);"/></td>'
				+ '<td><label>SMS</label></td><td><input type="checkbox" id="notifications_sms_'
				+ rowCount
				+ '" name="notifications_sms_'
				+ rowCount
				+ '" value="false" onclick="changeValue(this.id,this.value);" /></td>'
				+ '</tr>';
		jQuery('#notificationRows').append(notificationDetails);

	}

	function removeRow(removeNum) {
		if (rowCount == removeNum) {
			rowCount--;
			jQuery('#header_' + removeNum).remove();
			jQuery('#approvals_' + removeNum).remove();
			jQuery('#notifications_' + removeNum).remove();
		}

	}
	function changeValue(id, value) {

		if (value == 'false')
			$('#' + id).val('true');
		else
			$('#' + id).val('false');

	}
	function changeFlowValue(id, value) {

		if (value == 'end')
			$('#' + id).val('next');
		else
			$('#' + id).val('end');

	}
	function check() {

		var res = {};

		var workflowName = $('#workflowName').val();

		var eventName = $('#eventName').val();
		var qterms = $('#qterms').val();
		var createdBy = $('#createdBy').val();
		var notification_position_0 = $('#notification_position_0').val();
		var notification_link_org_type_0 = $('#notification_link_org_type_0')
				.val();
		var notification_is_global_0 = $('#notification_is_global_0').val();
		var notification_future_task_0 = $('#notification_future_task_0').val();
		var notification_email_0 = $('#notification_email_0').val();
		var notification_sms_0 = $('#notification_sms_0').val();
		var notification_template_0 = $('#notification_template_0').val();

		if (!(isEmpty('#workflowName')))
			return false;
		if (!(isEmpty('#eventName')))
			return false;
		if (!(isEmpty('#qterms')))
			return false;
		if (!(isEmpty('#createdBy')))
			return false;
		if (!(isEmpty('#notification_position_0')))
			return false;
		if (!(isEmpty('#notification_link_org_type_0')))
			return false;
		if (!(isEmpty('#notification_is_global_0')))
			return false;
		if (!(isEmpty('#notification_future_task_0')))
			return false;
		if (!(isEmpty('#notification_email_0')))
			return false;
		if (!(isEmpty('#notification_sms_0')))
			return false;
		if (!(isEmpty('#notification_template_0')))
			return false;

		res['workflowName'] = workflowName;
		res['eventName'] = eventName;
		res['qterms'] = qterms;
		res['createdBy'] = createdBy;
		res['notification_position_0'] = notification_position_0;
		res['notification_link_org_type_0'] = notification_link_org_type_0;
		res['notification_is_global_0'] = notification_is_global_0;
		res['notification_future_task_0'] = notification_future_task_0;
		res['notification_email_0'] = notification_email_0;
		res['notification_sms_0'] = notification_sms_0;
		res['notification_template_0'] = notification_template_0;

		var ls = [];

		for (var i = 1; i <= rowCount; i++) {
			var header = {}, approve = {}, notification = {}, dtls = {};

			if (!(isEmpty('#header_notification_' + i)))
				return false;
			if (!(isEmpty('#header_global_' + i)))
				return false;
			if (!(isEmpty('#header_notification_' + i)))
				return false;
			if (!(isEmpty('#header_approval_position_' + i)))
				return false;
			if (!(isEmpty('#header_organization_type_' + i)))
				return false;
			if (!(isEmpty('#approvals_activity_' + i)))
				return false;
			if (!(isEmpty('#approvals_task_name_' + i)))
				return false;
			if (!(isEmpty('#approvals_rejectClass_' + i)))
				return false;
			if (!(isEmpty('#approvals_reject_next_flow_' + i)))
				return false;
			if (!(isEmpty('#approvals_rejectClass_' + i)))
				return false;
			if (!(isEmpty('#approvals_reject_next_flow_' + i)))
				return false;
			if (!(isEmpty('#approvals_approveClass_' + i)))
				return false;
			if (!(isEmpty('#approvals_approve_next_flow_' + i)))
				return false;

			header['header_notification_' + i] = $('#header_notification_' + i)
					.val();
			header['header_global_' + i] = $('#header_global_' + i).val();
			header['header_approval_position_' + i] = $(
					'#header_approval_position_' + i).val();
			header['header_organization_type_' + i] = $(
					'#header_organization_type_' + i).val();

			approve['approvals_activity_' + i] = $('#approvals_activity_' + i)
					.val();
			approve['approvals_task_name_' + i] = $('#approvals_task_name_' + i)
					.val();
			
			approve['approvals_taskCreateClass_' + i] = $('#approvals_taskCreateClass_' + i)
			.val();
			
			
			approve['approvals_rejectClass_' + i] = $(
					'#approvals_rejectClass_' + i).val();
			approve['approvals_reject_next_flow_' + i] = $(
					'#approvals_reject_next_flow_' + i).val();
			approve['approvals_approveClass_' + i] = $(
					'#approvals_approveClass_' + i).val();
			approve['approvals_reject_next_flow_' + i] = $(
					'#approvals_reject_next_flow_' + i).val();
			approve['approvals_approve_next_flow_' + i] = $(
					'#approvals_approve_next_flow_' + i).val();

			notification['notifications_position_' + i] = $(
					'#notifications_position_' + i).val();
			notification['notifications_approve_template_' + i] = $(
					'#notifications_approve_template_' + i).val();
			notification['notifications_reject_template_' + i] = $(
					'#notifications_reject_template_' + i).val();
			notification['notifications_future_task_' + i] = $(
					'#notifications_future_task_' + i).val();
			notification['notifications_email_' + i] = $(
					'#notifications_email_' + i).val();
			notification['notifications_sms_' + i] = $(
					'#notifications_sms_' + i).val();

			dtls['header'] = header;
			dtls['approve'] = approve;
			dtls['notification'] = notification;
			ls.push(dtls);

		}

		res['ls'] = ls;
		console.log("res>>" + JSON.stringify(res));

		GlobalAjaxCall.callController('flowConfigure.do', res, function(data) {
			var  res=jQuery.parseJSON(JSON.stringify(data));
			alert("Configuration Id::"+res._id);
			console.log("serviceRes>>" + JSON.stringify(data));
		});
	}
	function isEmpty(id) {
		str = $(id).val();
		if (str == null || str == '') {

			alert("Please enter/select all input");
			$(id).focus();
			return false;
		} else
			return true;

	}
</script>

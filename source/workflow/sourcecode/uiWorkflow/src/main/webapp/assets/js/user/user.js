var TableAdvanced = function () {

    return {

        // main function to initiate the module
        init: function () {
        	
        
	
			
			
            
            if (!jQuery().dataTable) {
                return;
            }

           
            $(function() {
				var map;

				jQuery('#vmap_world').vectorMap(
						{
							map : 'world_en',
							regionsSelectable : true,
							backgroundColor : '#a5bfdd',
							borderColor : '#818181',
							borderOpacity : 0.25,
							borderWidth : 1,
							color : '#f4f3f0',
							enableZoom : true,
							hoverColor : '#c9dfaf',
							hoverOpacity : null,
							normalizeFunction : 'linear',
							scaleColors : [ '#b6d6ff', '#005ace' ],
							selectedColor : '#666666',
							selectedRegion : null,
							showTooltip : true,
							onRegionClick : function(element, code, region) {
								$('#vmap_world').vectorMap('deselect', code);
								$('#vmap_world').vectorMap('select', code);

								if (code.toUpperCase() == 'UA') {
									var request={};
									
							
									GlobalAjaxCall.callController('getNumberOfUserAssociatedToOrder.do', request,
											function(data) {
										$("#user_portlet_body_pie").css({
											'display' : 'block'
										});
										var obj = jQuery.parseJSON(JSON.stringify(data));
										var ls = obj.responseMessage.ls;
										nv.addGraph(function() {
											  var chart = nv.models.pieChart()
											      .x(function(d) { return d.label })
											      .y(function(d) { return d.value })
											      .showLabels(true);
											  chart.valueFormat(d3.format('d'));
											    d3.select("#chart1 svg")
											        .datum(ls)
											        .transition().duration(350)
											        .call(chart);

											  return chart;
											});
									});
								

										
									
										
								} 

							},
							onRegionSelected : function() {
								if (window.localStorage) {
									window.localStorage.setItem(
											'jvectormap-selected-regions',
											JSON.stringify(map
													.getSelectedRegions()));
								}

							}
						});
				map.setSelectedRegions(JSON.parse(window.localStorage
						.getItem('jvectormap-selected-regions')
						|| '[]'));

			});
            
            
        }

    };
  

}();
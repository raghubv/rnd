/**
 * 
 */
var GlobalAjaxCall = function() {
	return {
		callController : function(url, jsonData, callBack) {
			var request = JSON.stringify(jsonData);
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			$.ajax({
				url : url,
				type : "POST",
				dataType : 'json',
				contentType : 'application/json',
				data : request,
				beforeSend : function(xhr) {
					// here it is
					xhr.setRequestHeader(header, token);
					
						$.LoadingOverlay("show");

					
				},
				statusCode : {
					403 : function() {
						 window.location.href ="/uiDashBoard/j_spring_security_logout";
					}
				}
			}).done(function(data) {
				$.LoadingOverlay("hide");
				callBack(data);
			});
		}
	};
}();
var comboCharts=function(){
return{
            drawCanvas: function (data,chartTitle, divId) {
				var array=[];
				alert(">>"+JSON.stringify(data));
				
				var obj = jQuery.parseJSON(JSON.stringify(data));
				alert(">>"+obj);
				var dataList=[];
				var ls=obj.responseMessage.ls;
				$.each(ls, function(i, item) {
					var dataObj={};
					dataObj['type']='column';
					dataObj['dataPoints']=item.values;
					dataList.push(dataObj);
				});
				var chart = new CanvasJS.Chart(divId,
            		{title:{text:chartTitle},   
            	      data:dataList
            	    });
            	   chart.render();
        },
        drawGoogle: function (data,chartTitle, divId) {
			var array=[];
			var obj = jQuery.parseJSON(JSON.stringify(data));
			var dataList=[];
			var ls=obj.responseMessage.ls;
			$.each(ls, function(i, item) {
				var dataObj={};
				dataObj['type']='column';
				dataObj['dataPoints']=item.dataPoints;
				dataList.push(dataObj);
			});
			var chart = new CanvasJS.Chart(divId,
        		{title:{text:chartTitle},   
        	      data:dataList
        	    });
        	   chart.render();
    }
	}
}();
var DashBoardChart = function() {
	var viewAnalysis = 1;
	var date = new Date();
	var loadedInitDashBoard = false;
	var currentMapId = "";
	var startDate, endDate;
	var tenant = "";
	return {

		initDashBoard : function() {
			if (!loadedInitDashBoard) {

				$("#productCategoryMultiSelectionOptn").unbind("click");
				$("#productCategoryMultiSelectionOptn").click(function() {
					DashBoardChart.loadStockProductCategory();

				});
				$("#productCategoryMultiSelectionOptn1").unbind("click");
				$("#productCategoryMultiSelectionOptn1").click(function() {
					DashBoardChart.loadProductSaleCategory();

				});
				loadedInitDashBoard = true;
			}
		},
		init : function() {
			tenant = $("#tenant").val();

			$("#ui_date_picker_range_from").val(
					$.datepicker.formatDate("yy-mm-dd", new Date(date
							.getFullYear(), date.getMonth() - 1, 1)));
			$("#ui_date_picker_range_to").val(
					$.datepicker.formatDate("yy-mm-dd", new Date(date
							.getFullYear(), date.getMonth(), 0)));

			$("#statsByLine").click(function() {
				$('#statsBy').val("Line");
				DashBoardChart.reloadData();
			});
			$("#statsByBar").click(function() {
				$('#statsBy').val("Bar");
				DashBoardChart.reloadData();
			});
			$("#tenant").click(function() {
				tenant = $("#tenant").val();
				loadedInitDashBoard = false;
				DashBoardChart.reloadData();
			});

		},

		formatDate : function(date) {
			var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();
			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;
			return [ year, month, day ].join('-');
		},
		formRequest : function(feedGroup, feedType, continent, country, region,
				startDate, endDate) {
			if (country == "" && region == "") {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else if (region == "") {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"region" : region,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			}
			return null;
		},
		formRequestFromMap : function(continent, country, region) {
			tenant = $("#tenant").val();
			loadedInitDashBoard = false;
			startDate = $("#ui_date_picker_range_from").val()
					+ "T00:00:00.000Z";
			endDate = $("#ui_date_picker_range_to").val() + "T23:59:59.000Z";
			$("#stats_content").css({
				'display' : 'none'
			});

			console.log("[formRequestFromMap:viewAnalysis]" + viewAnalysis);
			switch (viewAnalysis) {

			case 1:
				var requestMap = {
					"requestMessage" : {
						"query" : {
							"map_dfn_id" : region.toUpperCase()
						},
						"collectionName" : "Dfn_Map_Mapping"
					}
				};
				console.log("[requestMap]" + requestMap);
				$("#chart11 svg").empty();
				$("#chart12 svg").empty();
				GlobalAjaxCall.callController('findOne.do', requestMap,
						function(data) {
							currentMapId = data.responseMessage.maaping_id;
							if (currentMapId != null) {
								DashBoardChart.reloadData();

							}
						});

				break;

			}

		},
		reloadData : function() {

			DashBoardChart.loadProductSaleCategory();
			DashBoardChart.loadSecondarySales();
			DashBoardChart.loadStockProductCategory();
			DashBoardChart.loadStockOrganizationType();
			DashBoardChart.loadKsVsNonVs();
		},
		formStockRequest : function(api, region_id, startDate, endDate) {
			console.log("[formStockRequest:api]" + api);
			var prodCat = {
				$nin : [ 0 ]
			};
			if ($("#prod_cat_multi_select").val() != null
					&& $("#prod_cat_multi_select").val() != "") {
				var result = $("#prod_cat_multi_select").val();
				console.log("result" + result);

				for ( var i in result) {
					result[i] = parseInt(result[i], 10);
				}

				prodCat = {
					$in : result
				};
			}

			var request = {
				"api" : api,
				"matchparam" : {
					"tenant_ref.$id" : tenant,
					"region_id" : region_id,
					"prod_category_id" : prodCat,
					"date" : {
						"$gte" : {
							"$date" : startDate
						},
						"$lte" : {
							"$date" : endDate
						}
					}
				}
			};
			console.log("[formStockRequest:request]" + JSON.stringify(request));
			return request;

		},
		formProductCategoryReq : function(api, region_id, startDate, endDate) {
			console.log("[formProductCategoryReq:api]" + api);
			var prodCat = {
				$nin : [ 0 ]
			};
			if ($("#product_category_sales_multi_select").val() != null
					&& $("#product_category_sales_multi_select").val() != "") {
				var result = $("#product_category_sales_multi_select").val();
				console.log("[formProductCategoryReq:formProductCategoryReq]"
						+ result);
				for ( var i in result) {
					result[i] = parseInt(result[i], 10);
				}

				prodCat = {
					$in : result
				};
			}

			var request = {
				"api" : api,
				"matchparam" : {
					"prod_category_id" : prodCat,
					"tenant_ref.$id" : tenant,
					"region_id" : region_id,
					"date" : {
						"$gte" : {
							"$date" : startDate
						},
						"$lte" : {
							"$date" : endDate
						}
					}
				}
			};
			console.log("[formProductCategoryReq:request]"
					+ JSON.stringify(request));
			return request;
		},
		drawChart : function(data, divId, xLabel, xFormat, yLabel, yFormat) {
			$(divId).empty();
			console.log("[response]" + JSON.stringify(data))
			var obj = jQuery.parseJSON(JSON.stringify(data));
			var ls = obj.responseMessage.ls;
			nv.addGraph(function() {
				var chart;
				if ($('#statsBy').val() == 'Bar')
					chart = nv.models.multiBarChart().showControls(true)
							.showLegend(false);
				else
					chart = nv.models.lineChart().showLegend(false);
				chart.xAxis.axisLabel(xLabel);
				chart.yAxis.axisLabel(yLabel).tickFormat(d3.format(yFormat));
				// chart.rotateLabels(-30);
				chart.yAxis.rotateLabels(-45)
				d3.select(divId).datum(ls).transition().duration(700).call(
						chart);
				nv.utils.windowResize(chart.update);
				return chart;
			});
			$(document).ajaxStart(function() {
				$.LoadingOverlay("hide");
			});

		},

		loadProductSaleCategory : function() {
			if (!loadedInitDashBoard) {
				var product_category_sales_drpdwn = DashBoardChart
						.formStockRequest("prod_cat_sales_prod_cat_drpdwn",
								currentMapId, startDate, endDate);
				GlobalAjaxCall
						.callController(
								'getData.do',
								product_category_sales_drpdwn,
								function(data) {
									var obj = jQuery.parseJSON(JSON
											.stringify(data));
									console.log("[loadProductSaleCategory:obj]"
											+ obj);
									console
											.log("[loadProductSaleCategory:data]"
													+ JSON.stringify(data));
									var ls = data.responseMessage.ls;
									var innerAccordianHtml = "<label>Product Category</label>"
									innerAccordianHtml = innerAccordianHtml
											+ "<select multiple=\"multiple\"  "
											+ "id=\"product_category_sales_multi_select\" "
											+ "name=\"product_category_sales_multi_select[]\">";
									$
											.each(
													ls,
													function(index, record) {

														innerAccordianHtml = innerAccordianHtml
																+ "<option value=\""
																+ record._id.value
																+ "\">"
																+ record._id.desc
																+ "</option>";

														console
																.log("val"
																		+ record._id.value);
														console
																.log("desc"
																		+ record._id.desc);

													});
									innerAccordianHtml = innerAccordianHtml
											+ "</select>";
									console.log("innerAccordianHtml"
											+ innerAccordianHtml);
									document
											.getElementById("productCategoryMultiSelectionOptn1").innerHTML = innerAccordianHtml;
									$("#product_category_sales_multi_select")
											.multiSelect();
									DashBoardChart.initDashBoard();

								});
			}
			var getProductCategorySalesValueReq = DashBoardChart
					.formProductCategoryReq("product_category_sales_value",
							currentMapId, startDate, endDate);
			GlobalAjaxCall.callController('getData.do',
					getProductCategorySalesValueReq, function(data) {
						DashBoardChart.drawChart(data, "#chart11 svg", "Month",
								"", "Value", ",.0d");

					});
			var getProductCategorySalesQtyReq = DashBoardChart
					.formProductCategoryReq("product_category_sales_qty",
							currentMapId, startDate, endDate);
			GlobalAjaxCall.callController('getData.do',
					getProductCategorySalesQtyReq, function(data) {
						DashBoardChart.drawChart(data, "#chart12 svg", "Month",
								"", "Quantity", ",.0d");
					});

		},
		formSecondarySaleRequest : function(api) {
			var requestSecondary = {
				"api" : api,
				"matchparam" : {
					"tenant_ref.$id" : tenant,
					"region_id" : currentMapId,
					"date" : {
						"$gte" : {
							"$date" : startDate
						},
						"$lte" : {
							"$date" : endDate
						}
					}
				}
			};

			return requestSecondary;

		},
		loadSecondarySales : function() {
			$("#secondary_sales").empty();

			var secondary_sales_user_count_request = DashBoardChart
					.formSecondarySaleRequest("secondary_sales_user_count");
			GlobalAjaxCall.callController('getData.do',
					secondary_sales_user_count_request, function(data) {
						console.log("[loadSecondarySales:data]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart61 svg", "Month",
								"", "User Count", ",.0d");
					});

			var secondary_sales_order_count_request = DashBoardChart
					.formSecondarySaleRequest("secondary_sales_order_count");
			GlobalAjaxCall.callController('getData.do',
					secondary_sales_order_count_request, function(data) {
						console.log("[loadSecondarySales:data]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart62 svg", "Month",
								"", "Order Count", ",.0d");
					});
			var secondary_sales_value_request = DashBoardChart
					.formSecondarySaleRequest("secondary_sales_value");
			GlobalAjaxCall.callController('getData.do',
					secondary_sales_value_request, function(data) {
						console.log("[loadSecondarySales:data]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart63 svg", "Month",
								"", "Value", ",.0d");
					});

		},

		loadStockProductCategory : function() {
			var stock_value_product_cat_req = DashBoardChart
					.formStockRequest("stock_value_product_cat", currentMapId,
							startDate, endDate);
			GlobalAjaxCall.callController('getData.do',
					stock_value_product_cat_req, function(data) {
						console.log("[loadStockProductCategory:data]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart43 svg", "Month",
								"", "Stock", ",.0d");
					});

			if (!loadedInitDashBoard) {
				var stcok_value_prod_cat_drpdwn = DashBoardChart
						.formStockRequest("stcok_value_prod_cat_drpdwn",
								currentMapId, startDate, endDate);
				GlobalAjaxCall
						.callController(
								'getData.do',
								stcok_value_prod_cat_drpdwn,
								function(data) {
									var obj = jQuery.parseJSON(JSON
											.stringify(data));
									console
											.log("[loadStockProductCategory:obj]"
													+ obj);
									console
											.log("[loadStockProductCategory:data]"
													+ JSON.stringify(data));
									var ls = data.responseMessage.ls;
									var innerAccordianHtml = "<label>Product Category</label>"
									innerAccordianHtml = innerAccordianHtml
											+ "<select multiple=\"multiple\" "
											+ " id=\"prod_cat_multi_select\""
											+ " name=\"prod_cat_multi_select[]\">";
									$
											.each(
													ls,
													function(index, record) {

														innerAccordianHtml = innerAccordianHtml
																+ "<option value=\""
																+ record._id.value
																+ "\">"
																+ record._id.desc
																+ "</option>";
														console
																.log("[loadStockProductCategory:val]"
																		+ record._id.value);
														console
																.log("[loadStockProductCategory:desc]"
																		+ record._id.desc);
													});
									innerAccordianHtml = innerAccordianHtml
											+ "</select>";
									console
											.log("[loadStockProductCategory:innerAccordianHtml]"
													+ innerAccordianHtml);
									document
											.getElementById("productCategoryMultiSelectionOptn").innerHTML = innerAccordianHtml;
									$("#prod_cat_multi_select").multiSelect();
									DashBoardChart.initDashBoard();
								});
			}
		},
		loadStockOrganizationType : function() {
			var stock_value_organization_type_req = DashBoardChart
					.formStockRequest("stock_value_organization_type",
							currentMapId, startDate, endDate);
			GlobalAjaxCall.callController('getData.do',
					stock_value_organization_type_req, function(data) {
						console.log("[stock_value_organization_type_req]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart151 svg",
								"Month", "", "Stock", ",.0d");
					});

		},
		formKsVsNonKsRequest : function(api) {
			var requestSecondary = {
				"api" : api,
				"matchparam" : {
					"tenant_ref.$id" : tenant,
					"region_id" : currentMapId,
					"date" : {
						"$gte" : {
							"$date" : startDate
						},
						"$lte" : {
							"$date" : endDate
						}
					}
				}
			};
		   console.log("[formKsVsNonKsRequest:requestSecondary]"+requestSecondary);
			return requestSecondary;

		},
		loadKsVsNonVs : function() {
			var ks_vs_nonks_order_count_req = DashBoardChart
					.formKsVsNonKsRequest("ks_vs_nonks_order_count");
			GlobalAjaxCall.callController('getData.do',
					ks_vs_nonks_order_count_req, function(data) {
						console.log("[loadKsVsNonVs]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart71 svg", "Month", "",
								"Order Count", ",.0d");
					});

			var ks_vs_nonks_order_qty_req = DashBoardChart.formKsVsNonKsRequest(
					"ks_vs_nonks_order_qty");
			GlobalAjaxCall.callController('getData.do',
					ks_vs_nonks_order_qty_req, function(data) {
						console.log("[loadKsVsNonVs]"
								+ JSON.stringify(data))
						DashBoardChart.drawChart(data, "#chart72 svg", "Month", "",
								"Order Quantity", ",.0d");
					});

		}

	};

}();
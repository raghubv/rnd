var DashBoardChart = function() {
	var viewAnalysis = 1;
	var date = new Date();

	return {
		init : function() {

			$("#ui_date_picker_range_from").val(
					$.datepicker.formatDate("yy-mm-dd", new Date(date
							.getFullYear(), date.getMonth() - 1, 1)));
			$("#ui_date_picker_range_to").val(
					$.datepicker.formatDate("yy-mm-dd", new Date(date
							.getFullYear(), date.getMonth(), 0)));

			$("#statsByLine").click(function() {
				$('#statsBy').val("Line");
			});
			$("#statsByBar").click(function() {
				$('#statsBy').val("Bar");
			});
			$("#sales_analysis").click(function() {
				viewAnalysis = 1;

				$("#sales_analysis_statistics_content").css({
					'display' : 'block'
				});
				$("#channel_analysis_statistics_content").css({
					'display' : 'none'
				});
				$("#product_analysis_statistics_content").css({
					'display' : 'none'
				});
			});
			$("#channel_analysis").click(function() {
				viewAnalysis = 2;
				$("#sales_analysis_statistics_content").css({
					'display' : 'none'
				});

				$("#channel_analysis_statistics_content").css({
					'display' : 'block'
				});
				$("#product_analysis_statistics_content").css({
					'display' : 'none'
				});
			});
			$("#product_analysis").click(function() {
				viewAnalysis = 3;
				$("#sales_analysis_statistics_content").css({
					'display' : 'none'
				});
				$("#channel_analysis_statistics_content").css({
					'display' : 'none'
				});
				$("#product_analysis_statistics_content").css({
					'display' : 'block'
				});
			});

		},
		loadSimCountValues : function(data) {
			var obj = jQuery.parseJSON(JSON.stringify(data));
			var ls = obj.responseMessage.ls;
			var res1 = ls[0];
			var res2 = ls[1];
			$("#simcount").empty();
			$("#simcount").html(res1['values'] + '/' + res2['values']);
		},
		loadChart : function(data, opt) {

			console.log("[viewAnalysis]" + viewAnalysis);
			switch (viewAnalysis) {
			case 1: {
				$("#sales_analysis_statistics_content").css({
					'display' : 'block'
				});

				DashBoardChart.drawChart(data, "#chart10 svg", "Month", "",
						"Units", ",.0d");
			}
				break;

			case 11: {
				DashBoardChart.drawChart(data, "#chart11 svg", "Month", "",
						"Value", ",.0d");
			}
				break;
			case 2: {

				DashBoardChart.drawChart(data, "#chart20 svg", "Month", "",
						"Units", ",.0d");
			}

				break;
			case 3: {
				if (opt == 0) {
					DashBoardChart.drawChart(data, "#chart30 svg", "Month", "",
							"Units", ",.0d");
				} else if (opt == 1) {

					DashBoardChart.drawChart(data, "#chart31 svg", "Month", "",
							"Units", ",.0d");
				}

			}
				break;
			}

		},
		formatDate : function(date) {
			var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
					+ d.getDate(), year = d.getFullYear();
			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;
			return [ year, month, day ].join('-');
		},
		formRequest : function(feedGroup, feedType, continent, country, region,
				startDate, endDate) {
			if (country == "" && region == "") {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else if (region == "") {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else {
				var request = {
					"matchparam" : {
						"feedGroup" : feedGroup,
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"region" : region,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			}
			return null;
		},
		formRequestFromMap : function(continent, country, region) {

			var startDate = $("#ui_date_picker_range_from").val()
					+ "T00:00:00.000Z";
			var endDate = $("#ui_date_picker_range_to").val()
					+ "T23:59:59.000Z";
			$("#stats_content").css({
				'display' : 'none'
			});
			console.log("[viewAnalysis]" + viewAnalysis);
			switch (viewAnalysis) {

			case 1:

				var request = DashBoardChart.formRequestOne("SIM", continent,
						country, region, startDate, endDate);
				console.log("[request]" + request);
				GlobalAjaxCall.callController('getSIMCount.do', request,
						function(data) {
							DashBoardChart.loadSimCountValues(data);
						});
				GlobalAjaxCall.callController('getSIMChart.do', request,
						function(data) {
							DashBoardChart.loadChart(data, 0);
						});

				var year = date.getFullYear();
				var lastYearStartDate = new Date(year - 1, 0, 1);
				var lastYearEndDate = new Date(year - 1, 12, 0);
				console.log("lastYearStartDate" + lastYearStartDate);
				console.log("lastYearEndDate" + lastYearEndDate);
				var lastSixMonthStartDate = new Date(year, date.getMonth() - 7,
						1);
				var lastSixMonthEndDate = new Date(year, date.getMonth() - 1, 0);
				console.log("lastSixMonthStartDate" + lastSixMonthStartDate);
				console.log("lastSixMonthEndDate" + lastSixMonthEndDate);
				var lastQuaterStartDate = new Date(year, (date.getMonth() - 4),
						1);
				var lastQuaterEndDate = new Date(year, (date.getMonth() - 1), 0);
				console.log("lastQuaterStartDate" + lastQuaterStartDate);
				console.log("lastQuaterEndDate" + lastQuaterEndDate);
				var lastMonthStartDate = new Date(year, date.getMonth() - 1, 1);
				var lastMonthEndDate = new Date(year, date.getMonth(), 0);
				console.log("lastMonthStartDate" + lastMonthStartDate);
				console.log("lastMonthEndDate" + lastMonthEndDate);

				var feedType = "SIM";
				var feedGroup = "Active";
				var requestLastYear = DashBoardChart.formRequest(feedGroup,
						feedType, continent, country, region,
						lastYearStartDate, lastYearEndDate);
				var requestLastSixMonth = DashBoardChart.formRequest(feedGroup,
						feedType, continent, country, region,
						lastSixMonthStartDate, lastSixMonthEndDate);
				var requestLastQuatar = DashBoardChart.formRequest(feedGroup,
						feedType, continent, country, region,
						lastQuaterStartDate, lastQuaterEndDate);
				var requestLastMonth = DashBoardChart.formRequest(feedGroup,
						feedType, continent, country, region,
						lastMonthStartDate, lastMonthEndDate);

				$("#stats_content").css({
					'display' : 'block'
				});

				GlobalAjaxCall.callController('getStatics.do', requestLastYear,
						function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							if (ls.length > 0) {
								var res = ls[0];
								$("#sparkline_bar_last_year").sparkline(
										res["values"], {
											type : 'bar',
											width : '100',
											barWidth : 5,
											height : '55',
											barColor : '#005C00',
											negBarColor : '#e02222'
										});
							}
						});
				GlobalAjaxCall.callController('getStatics.do',
						requestLastSixMonth, function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							if (ls.length > 0) {
								var res = ls[0];
								$("#sparkline_bar_last_6month").sparkline(
										res["values"], {
											type : 'bar',
											width : '100',
											barWidth : 5,
											height : '55',
											barColor : '#005C00',
											negBarColor : '#e02222'
										});
							}
						});
				GlobalAjaxCall.callController('getStatics.do',
						requestLastQuatar, function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							if (ls.length > 0) {
								var res = ls[0];
								$("#sparkline_bar_last_quarter").sparkline(
										res["values"], {
											type : 'bar',
											width : '100',
											barWidth : 5,
											height : '55',
											barColor : '#005C00',
											negBarColor : '#e02222'
										});
							}
						});
				GlobalAjaxCall.callController('getStaticsDay.do',
						requestLastMonth, function(data) {
							var obj = jQuery.parseJSON(JSON.stringify(data));
							var ls = obj.responseMessage.ls;
							if (ls.length > 0) {
								var res = ls[0];
								$("#sparkline_bar_last_month").sparkline(
										res["values"], {
											type : 'bar',
											width : '100',
											barWidth : 5,
											height : '55',
											barColor : '#005C00',
											negBarColor : '#e02222'
										});
							}
						});

				var requestMap = {
					"requestMessage" : {
						"query" : {
							"map_dfn_id" : region.toUpperCase()
						},
						"collectionName" : "Dfn_Map_Mapping"
					}
				};
				console.log("[requestMap]" + requestMap);
				$("#chart11 svg").empty();
				$("#chart12 svg").empty();
				GlobalAjaxCall
						.callController(
								'findOne.do',
								requestMap,
								function(data) {

									if (data.responseMessage.maaping_id != null) {

										$("#secondary_sales").empty();
										var requestSecondary = {
											"matchparam" : {

												"region_id" : data.responseMessage.maaping_id,
												"date" : {
													"$gte" : {
														"$date" : startDate
													},
													"$lte" : {
														"$date" : endDate
													}
												}
											}
										};

										GlobalAjaxCall
												.callController(
														'getSecondarySaleDtls.do',
														requestSecondary,
														function(data) {
															var obj = jQuery
																	.parseJSON(JSON
																			.stringify(data));
															var ls = obj.responseMessage.ls;
															var html = "<table class=\"table table-hover\"><thead><tr><th>#</th><th>Year Month</th><th>User Count</th><th>Order Count</th><th>Sale Value</th></tr></thead><tbody>";
															$
																	.each(
																			ls,
																			function(
																					index,
																					record) {
																				html = html
																						+ "<tr><td>"
																						+ index
																						+ "</td><td>"
																						+ record._id.group
																						+ "</td><td>"
																						+ record.order_count
																						+ "</td><td>"
																						+ record.value
																						+ "</td><td>"
																						+ record.user_count
																						+ "</td></tr>";

																			});
															html = html
																	+ "</tbody></table>";
															$(
																	"#secondary_sales")
																	.append(
																			html);
															console
																	.log("[htnl]"
																			+ html);
														});

										/*
										 * var
										 * stock_value_product_cat_stock_in_req =
										 * DashBoardChart.formStockRequest(
										 * "stock_value_product_cat_stock_in",
										 * data.responseMessage.maaping_id,
										 * startDate, endDate); GlobalAjaxCall
										 * .callController( 'getData.do',
										 * stock_value_product_cat_stock_in_req,
										 * function(data) { console
										 * .log("[stock_value_product_cat_stock_in]" +
										 * JSON .stringify(data))
										 * DashBoardChart.drawChart( data,
										 * "#chart41 svg", "Month", "", "Stock
										 * In", ",.0d"); }); var
										 * stock_value_product_cat_stock_out_req =
										 * DashBoardChart.formStockRequest(
										 * "stock_value_product_cat_stock_out",
										 * data.responseMessage.maaping_id,
										 * startDate, endDate); GlobalAjaxCall
										 * .callController( 'getData.do',
										 * stock_value_product_cat_stock_out_req,
										 * function(data) { console
										 * .log("[stock_value_product_cat_stock_out]" +
										 * JSON .stringify(data))
										 * DashBoardChart.drawChart( data,
										 * "#chart42 svg", "Month", "", "Stock
										 * Out", ",.0d"); });
										 */
										var stock_value_product_cat_req = DashBoardChart
												.formStockRequest(
														"stock_value_product_cat",
														data.responseMessage.maaping_id,
														startDate, endDate);
										GlobalAjaxCall
												.callController(
														'getData.do',
														stock_value_product_cat_req,
														function(data) {
															console
																	.log("[stock_value_product_cat_req]"
																			+ JSON
																					.stringify(data))
															DashBoardChart
																	.drawChart(
																			data,
																			"#chart43 svg",
																			"Month",
																			"",
																			"Stock",
																			",.0d");
														});
										var stock_value_organization_type_req = DashBoardChart
												.formStockRequest(
														"stock_value_organization_type",
														data.responseMessage.maaping_id,
														startDate, endDate);
										GlobalAjaxCall
												.callController(
														'getData.do',
														stock_value_organization_type_req,
														function(data) {
															console
																	.log("[stock_value_organization_type_req]"
																			+ JSON
																					.stringify(data))
															DashBoardChart
																	.drawChart(
																			data,
																			"#chart151 svg",
																			"Month",
																			"",
																			"Stock",
																			",.0d");
														});

										var request = {
											"matchparam" : {
												"region_id" : data.responseMessage.maaping_id,
												"date" : {
													"$gte" : {
														"$date" : startDate
													},
													"$lte" : {
														"$date" : endDate
													}
												}
											}
										};
										console.log("[request]"
												+ JSON.stringify(request));

										GlobalAjaxCall
												.callController(
														'getProductCategorySalesValue.do',
														request,
														function(data) {
															DashBoardChart
																	.drawChart(
																			data,
																			"#chart11 svg",
																			"Month",
																			"",
																			"Value",
																			",.0d");

														});

										GlobalAjaxCall
												.callController(
														'getProductCategorySalesQty.do',
														request,
														function(data) {
															DashBoardChart
																	.drawChart(
																			data,
																			"#chart12 svg",
																			"Month",
																			"",
																			"Quantity",
																			",.0d");

														});

									}
								});

				break;
			case 2:
				var request = DashBoardChart.formRequestOne("CHANNEL",
						continent, country, region, startDate, endDate);
				GlobalAjaxCall.callController('getSIMChart.do', request,
						function(data) {

							DashBoardChart.loadChart(data, 0);
						});

				break;
			case 3:
				var request = DashBoardChart.formRequestOne("PRODUCT",
						continent, country, region, startDate, endDate);
				GlobalAjaxCall.callController('getSIMChart.do', request,
						function(data) {

							DashBoardChart.loadChart(data, 0);
						});
				var request1 = DashBoardChart.formRequestOne("PRODUCTCAT",
						continent, country, region, startDate, endDate);
				GlobalAjaxCall.callController('getSIMChart.do', request1,
						function(data) {

							DashBoardChart.loadChart(data, 1);
						});
				break;

			}

		},
		formRequestOne : function(feedType, continent, country, region,
				startDate, endDate) {
			if (country == "" && region == "") {
				var request = {
					"matchparam" : {
						"feedType" : feedType,
						"continent" : continent,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else if (region == "") {
				var request = {
					"matchparam" : {
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}
					}
				};
				return request;
			} else {
				var request = {
					"matchparam" : {
						"feedType" : feedType,
						"continent" : continent,
						"country" : country,
						"region" : region,
						"feedDate" : {
							"$gte" : {
								"$date" : startDate
							},
							"$lte" : {
								"$date" : endDate
							}
						}

					}
				};
				return request;
			}
			return null;
		},

		formStockRequest : function(api, region_id, startDate, endDate) {
			var request = {
				"api" : api,
				"matchparam" : {
					"region_id" : region_id,
					"date" : {
						"$gte" : {
							"$date" : startDate
						},
						"$lte" : {
							"$date" : endDate
						}
					}
				}
			};
			console.log("[request]" + JSON.stringify(request));
			return request;

		},
		drawChart : function(data, divId, xLabel, xFormat, yLabel, yFormat) {
			$(divId).empty();
			console.log("[response]" + JSON.stringify(data))
			var obj = jQuery.parseJSON(JSON.stringify(data));
			var ls = obj.responseMessage.ls;
			nv.addGraph(function() {
				var chart;
				if ($('#statsBy').val() == 'Bar')
					chart = nv.models.multiBarChart();
				else
					chart = nv.models.lineChart();
				chart.xAxis.axisLabel(xLabel);
				chart.yAxis.axisLabel(yLabel).tickFormat(d3.format(yFormat));
				// chart.rotateLabels(-30);
				chart.yAxis.rotateLabels(-45)
				d3.select(divId).datum(ls).transition().duration(700).call(
						chart);
				nv.utils.windowResize(chart.update);
				return chart;
			});
			$(document).ajaxStart(function() {
				$.LoadingOverlay("hide");
			});

		}

	};

}();
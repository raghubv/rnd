package com.dashboard.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dashboard.util.Configuration;
import com.dashboard.util.Constants;
import com.dashboard.util.QueryUtils;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/billpayment")
public class BillPaymentController extends BaseController {

	static final Logger log = Logger.getLogger(BillPaymentController.class);

	public BillPaymentController() {
		super();
		setSuccessView("billpayment.view");
		setRedirectView("redirect:/billpayment/view.do");
	}

	@RequestMapping(value = "/view.do", method = RequestMethod.GET)
	public String view(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = Constants._salesdashboardControllergetData, method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getData(@RequestBody String request) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get(requestJson.get(Constants._API)), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = Constants._salesdashboardControllerfindOneQuery, method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String findOneQuery(@RequestBody String request) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return findOne(objectMapper.writeValueAsString(requestJson));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/postMethod.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String postMethod(@RequestBody String request) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return post("http://localhost:8080/snocRestServices/rs/billpayment/", objectMapper.writeValueAsString(requestJson));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	
}

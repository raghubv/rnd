package com.dashboard.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 13, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Configuration {
	private static final Logger log = Logger.getLogger(Configuration.class);

	private static Map menuData = null;
	private static Map reuqestData = null;
	
	static {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			
			
			InputStream menuDataConfig = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream("/menu.json");
			menuData=objectMapper.readValue(menuDataConfig,
					LinkedHashMap.class);
			menuDataConfig.close();
			
			InputStream requestDataConfig = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream("/request.json");
			reuqestData=objectMapper.readValue(requestDataConfig,
					LinkedHashMap.class);
			requestDataConfig.close();
			
					
		} catch (JsonParseException e) {
			
		} catch (JsonMappingException e) {
			
		} catch (IOException e) {
			
		}
	}
/**
 * 
 * @return
 */
	public static Map getMenuData() {
		return menuData;
	}
	public static Map getReuqestData() {
		return reuqestData;
	}
	
	
	
	
	
	

}

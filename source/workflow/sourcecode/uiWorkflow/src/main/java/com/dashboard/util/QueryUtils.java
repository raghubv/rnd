package com.dashboard.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * QueryUtils: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Dec 9, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class QueryUtils {
	static final Logger log = Logger.getLogger(QueryUtils.class);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. prepareQuery
	 * 
	 * </pre>
	 *
	 * @return Map
	 */
	static public Map prepareQuery(Map query, Map inputParams) {
		Map outMap = new HashMap();
		try {
			Set set = query.entrySet();
			for (Object object : set) {
				Map.Entry entry = (Map.Entry) object;
				String key = (String) entry.getKey();

				if (entry.getValue() instanceof Map)
					outMap.put(key,
							prepareQuery((Map) entry.getValue(), inputParams));
				else if (entry.getValue() instanceof List) {
					List ls = (List) entry.getValue();
					List outLs = new ArrayList();
					for (Object object2 : ls) {
						if (object2 instanceof Map)
							outLs.add(prepareQuery((Map) object2, inputParams));
						else {
							if (inputParams.containsKey(object2))
								outLs.add(inputParams.get(object2));
							else
								outLs.add(object2);
						}
					}
					outMap.put(key, outLs);

				} else {
					if (inputParams.containsKey(entry.getValue()))
						outMap.put(key, inputParams.get(entry.getValue()));
					else
						outMap.put(key, entry.getValue());
				}

			}
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

		log.debug("[outMap]" + outMap);
		return outMap;

	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. extractOutGoingData
		* 
		* </pre> 
		*
		* @return Map
	 */
	public static Map extractOutGoingData(List outField, Map input) {
		Map outputMap = new HashMap();
		Set set = input.entrySet();
		for (Object object : set) {
			if (outField.contains(((Map.Entry) object).getKey()))
				outputMap.put(((Map.Entry) object).getKey(),
						((Map.Entry) object).getValue());
		}

		return outputMap;

	}

}

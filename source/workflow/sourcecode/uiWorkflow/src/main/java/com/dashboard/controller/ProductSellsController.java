package com.dashboard.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dashboard.restService.RestClient;
import com.dashboard.util.Configuration;
import com.dashboard.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/product")
public class ProductSellsController extends BaseController {

	static final Logger log = Logger.getLogger(ProductSellsController.class);

	static private enum viewVolumeType {
		Year, Month, Week,Day
	};

	static private enum viewByType {
		Value, Quantity, Volume
	};

	public ProductSellsController() {
		super();
		setSuccessView("productsells.view");
		setRedirectView("redirect:/product/productsells.do");
	}

	@RequestMapping(value = "/productsells.do", method = RequestMethod.GET)
	public String add(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = "/getProductSells.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getProductSells(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			switch (viewByType.valueOf((String) requestJson.get("searchByYValue"))) {

			case Quantity:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_week_statsByQty")));
				case Month:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_month_statsByQty")));
				case Year:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_year_statsByQty")));
				case Day:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_day_statsByQty")));	
				}
			case Value:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_week_statsByVal")));
				case Month:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_month_statsByVal")));
				case Year:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_year_statsByVal")));
				case Day:
					return getAggregatedData(objectMapper.writeValueAsString(
							Configuration.getReuqestData().get("org_daily_metrics_day_statsByVal")));	

				}

			}
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getProductSellsSub.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getProductSellsSub(@RequestBody String request) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			String str[] = ((String) objectMapper.readValue(request, Map.class).get("key")).split(":");
			switch (viewByType.valueOf((String) requestJson.get("searchByYValue"))) {
			case Quantity:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_week_statsByQty_sub"))
							.replace("\"param\"", str[1]));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_month_statsByQty_sub"))
							.replace("\"param\"", str[1]));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_year_statsByQty_sub"))
							.replace("\"param\"", str[1]));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_Day_statsByQty_sub"))
							.replace("\"param\"", str[1]));

				}
			case Value:
				switch (viewVolumeType.valueOf((String) requestJson.get("searchByXValue"))) {
				case Week:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_week_statsByVal_sub"))
							.replace("\"param\"", str[1]));
				case Month:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_month_statsByVal_sub"))
							.replace("\"param\"", str[1]));
				case Year:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_year_statsByVal_sub"))
							.replace("\"param\"", str[1]));
				case Day:
					return getAggregatedData(objectMapper
							.writeValueAsString(
									Configuration.getReuqestData().get("org_daily_metrics_day_statsByVal_sub"))
							.replace("\"param\"", str[1]));	
				}
			}
		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
}

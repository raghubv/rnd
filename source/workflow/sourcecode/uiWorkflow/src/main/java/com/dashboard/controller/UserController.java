package com.dashboard.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dashboard.util.Configuration;
import com.dashboard.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

	static final Logger log = Logger.getLogger(UserController.class);

	
	public UserController() {
		super();
		setSuccessView("user.view");
		setRedirectView("redirect:/user/user.do");
	}

	@RequestMapping(value = "/user.do", method = RequestMethod.GET)
	public String add(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = "/getNumberOfUserAssociatedToOrder.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getNumberOfUserAssociatedToOrder(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			return getAggregatedData(
					objectMapper.writeValueAsString(Configuration.getReuqestData().get("users_by_sales_count")));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	
}

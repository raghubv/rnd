package com.dashboard.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.ws.rs.PathParam;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dashboard.util.Configuration;
import com.dashboard.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/map")
public class MapController extends BaseController {

	static final Logger log = Logger.getLogger(MapController.class);

	
//	public MapController() {
//		super();
//		setSuccessView("test.view");
//		setRedirectView("redirect:/test/test.do");
//	}
//	
//	@RequestMapping(value = "/test.do", method = RequestMethod.GET)
//	public String add(Model model, HttpSession session) {
//		if (session.getAttribute("message") != null) {
//			model.addAttribute("message", "Insert employee successful.");
//			session.removeAttribute("message");
//		}
//		return getSuccessView();
//	}

	@RequestMapping(value = "/getMap/{jsName}", method = RequestMethod.GET)
	public @ResponseBody String getMap(@PathParam("jsName") String jsName) {

		try {
			return getMap(jsName);

		}  catch (Exception e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	
}

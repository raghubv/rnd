package com.dashboard.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dashboard.util.Configuration;
import com.dashboard.util.Constants;
import com.dashboard.util.QueryUtils;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2015<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Aug 11, 2015   Raghu BV -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Controller
@RequestMapping("/dashboard")
public class DashBoardController extends BaseController {

	static final Logger log = Logger.getLogger(DashBoardController.class);

	public DashBoardController() {
		super();
		setSuccessView("dashboard.view");
		setRedirectView("redirect:/dashboard/view.do");
	}

	@RequestMapping(value = "/view.do", method = RequestMethod.GET)
	public String add(Model model, HttpSession session) {
		if (session.getAttribute("message") != null) {
			model.addAttribute("message", "Insert employee successful.");
			session.removeAttribute("message");
		}
		return getSuccessView();
	}

	@RequestMapping(value = "/getSIMCount.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getSIMCount(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get("sim_feeds_sold_acctivated_count"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getSIMChart.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getSIMChart(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get("sim_feeds_sold_acctivated_perMonth"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/getProductCategorySalesValue.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getProductCategorySalesValue(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get("product_category_sales_value"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	@RequestMapping(value = "/getProductCategorySalesQty.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getProductCategorySalesQty(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get("product_category_sales_qty"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getSecondarySaleDtls.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getSecondarySaleDtls(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils.prepareQuery(
					(Map) Configuration.getReuqestData().get("secondary_sales_user_count_region"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/getStatics.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getStatics(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils
					.prepareQuery((Map) Configuration.getReuqestData().get("sim_feeds_statics"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/getData.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getData(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils
					.prepareQuery((Map) Configuration.getReuqestData().get(requestJson.get("api")), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}
	

	@RequestMapping(value = "/getStaticsDay.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String getStaticsDay(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return getAggregatedData(objectMapper.writeValueAsString(QueryUtils
					.prepareQuery((Map) Configuration.getReuqestData().get("sim_feeds_statics_day"), requestJson)));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/findOne.do", method = RequestMethod.POST, headers = Constants._HEADER_JSON, produces = Constants._RES_JSON_UTF8)
	public @ResponseBody String findOneQuery(@RequestBody String request) {

		try {

			ObjectMapper objectMapper = new ObjectMapper();
			Map requestJson = objectMapper.readValue(request, Map.class);
			log.debug("[requestJson]" + requestJson);
			System.out.println("[requestJson]" + requestJson);
			return findOne(objectMapper.writeValueAsString(requestJson));

		} catch (JsonGenerationException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("[Error]" + e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

}

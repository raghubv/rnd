/*******************************************************************************
 * Signavio Core Components

 * Copyright (C) 2012  Signavio GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

if (!ORYX.Plugins) 
    ORYX.Plugins = new Object();

ORYX.Plugins.Deploy = Clazz.extend({
	
    facade: undefined,
	
	construct: function(facade){
		this.facade = facade;
		
		this.facade.offer({
			'name': ORYX.I18N.Deploy.deploy,
			'functionality': this.deploy.bind(this),
			'group': ORYX.I18N.Deploy.group,
			'icon': ORYX.PATH + "images/deploy.png",
			'description': ORYX.I18N.Deploy.deployDesc,
			'index': 1,
			'minShape': 0,
			'maxShape': 0
		});
		
	},
	
    processDeploy: function(modelInfo){
		
		if (!modelInfo) {
			return;
		}
		var value = window.document.title || document.getElementsByTagName("title")[0].childNodes[0].nodeValue;
		
		if (value.startsWith("*")){
		   Ext.Msg.alert("TITLE", "Save your model before deploying.").setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
		   delete this.deploying;
		   return;
		}
		
        var params = {
       		name: modelInfo.name,
			parent: modelInfo.parent
        };
		  	
		var successFn = function(transport) {
			
  			Ext.Msg.alert("", "Deploy succeeded.").setIcon(Ext.Msg.INFO).getDialog().setWidth(260).center().syncSize();
			delete this.deploying;
						
		}.bind(this);
				
		var failure = function(transport) {
						
			if(transport.status && transport.status === 401) {
				    Ext.Msg.alert(ORYX.I18N.Oryx.title, ORYX.I18N.Save.notAuthorized).setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
			} else if(transport.status && transport.status === 403) {
				    Ext.Msg.alert(ORYX.I18N.Oryx.title, ORYX.I18N.Save.noRights).setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
			} else if(transport.statusText === "transaction aborted") {
				    Ext.Msg.alert(ORYX.I18N.Oryx.title, ORYX.I18N.Save.transAborted).setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
			} else if(transport.statusText === "communication failure") {
				    Ext.Msg.alert(ORYX.I18N.Oryx.title, ORYX.I18N.Save.comFailed).setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
			} else {
					var msg = transport.responseText;
					if (msg != null && msg != "") {
					  msg = Ext.decode(msg);
					  msg = msg == null ? ORYX.I18N.Save.failed : msg.message;
					}
					// TODO Ext.Msg does not support new line, becuse it renders <span> HTML element, figure out how to display multiline messages
					Ext.Msg.alert(ORYX.I18N.Oryx.title, msg).setIcon(Ext.Msg.WARNING).getDialog().setWidth(260).center().syncSize();
			}
						
			delete this.deploying;
						
		}.bind(this);
			
		var str = window.document.title; 
		str=str.replace("|Signavio","");
		str=str.replace("| Signavio","");
		str=str.replace(" ","");
		str=str.replace("- Signavio","")
		var res = str.replace(" - Signavio","");
		var myParam = window.location.href.split('id=')[1];
		var url='/workflowmodeler/deployServlet?id='+myParam;
		this.sendDeployRequest('GET',url, params, successFn, failure);
		
    },
	
	
	
	sendDeployRequest: function(method, url, params, success, failure){
		
		
		// Send the request to the server.
		Ext.Ajax.request({
			url				: url,
			method			: method,
			timeout			: 1800000,
			disableCaching	: true,
			params			: params,
			success			: success,
			failure			: failure
		});
	},
    
   
    deploy: function(){
        
		// Check if currently is deploying
		if (this.deploying){
			return;
		}
		
		this.deploying = true;
		
        window.setTimeout((function(){
    		var meta = this.facade.getModelMetaData();
	     	this.processDeploy(meta);
	    }).bind(this), 10);
    
        return true;
    }	
});
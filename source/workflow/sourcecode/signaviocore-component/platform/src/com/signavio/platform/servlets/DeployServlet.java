package com.signavio.platform.servlets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

/**
 * 
 * <b>Purpose:</b><br>
 * ExportImage <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 3, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class DeployServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7400783693917472193L;
	private static final Logger log = Logger.getLogger(DeployServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String filepath = (String) req.getParameter("id");
		String body = readFile(
				System.getenv("BMPN_REPO") + filepath.replaceAll("root-directory", "").replace(";", "/"));

		HttpClient client = new HttpClient();
		client.getState().setCredentials(new AuthScope("localhost", 8080, "realm"),
				new UsernamePasswordCredentials("dpc", "password"));
		String[] str = filepath.replaceAll("root-directory", "").replace(";", "/").replaceAll(".signavio.xml", "")
				.split("/");

		System.out.println("body>>" + body);
		PostMethod method = new PostMethod(
				System.getenv("REST_INTEGRATION_ADD_RESOURCE_BPMN_URI") + str[str.length - 1]);
		method.setRequestBody(body);
		;
		client.executeMethod(method);
		System.out.println(">>>" + method.getResponseBodyAsString());

	}

	private static String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		// String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				// stringBuilder.append(ls);
			}

			return stringBuilder.toString();
		} finally {
			reader.close();
		}
	}

}

package com.os.workflow.bpmnCore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.ExclusiveGateWayHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;

/**
 * 
 * <b>Purpose:</b><br>
 * ExclusiveGateWayHandlerImpl <br>
 * <br>
 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ExclusiveGateWayHandlerImpl implements ExclusiveGateWayHandler {
	static private enum SequenceFlow {
		None, Expression, Default
	};

	private static enum invokeTaskProcessorMethod {
		updatePathStatus, createTaskInstances
	};

	private static final Logger log = Logger.getLogger(ExclusiveGateWayHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	UpdateStatusHandler updateStatusHandler;

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. isAllTaskProcessed
	 * To check is all task processed
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @return
	 */
	private boolean isAllTaskProcessed(String processInstanceId) {

		DBObject taskInstanceQry = new BasicDBObject();
		taskInstanceQry.put(Constants.status, Constants._IN_Execution);
		taskInstanceQry.put(Constants.processInstanceRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));
		if (mongoTemplate.getCollection(Constants.workFlowTaskInstances).find(taskInstanceQry)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)))
				.count() > 0)
			return false;
		else
			return true;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. isAllIncomingXORPathProcessed
	 * To check is all incoming xor path processed
	 * </pre>
	 * 
	 * @param processDfnId
	 * @param resourceId
	 * @param processInstanceId
	 * @return
	 */
	private Boolean isAllIncomingXORPathProcessed(String processDfnId, String resourceId, String processInstanceId) {
		DBObject taskDfnQry = new BasicDBObject();
		taskDfnQry.put(Constants.processRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(processDfnId)));
		taskDfnQry.put(Constants._outgoingResourceIdPath, resourceId);
		log.debug("[taskDfnQry]" + taskDfnQry);
		DBCursor tasks = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(taskDfnQry)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskDefinitions)));
		for (DBObject task : tasks) {
			log.debug("[task]" + task);
			DBObject taskInstanceQry = new BasicDBObject();
			taskInstanceQry.put(Constants.processInstanceRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));
			taskInstanceQry.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
					(ObjectId) task.get(Constants._id)));
			log.debug("[taskInstanceQry]" + taskInstanceQry);
			DBObject taskInstance = mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.findOne(taskInstanceQry);
			log.debug("[taskInstance]" + taskInstance);
			if (taskInstance.get(Constants.status).toString().equalsIgnoreCase(Constants._Waiting_For_Execute))
				return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.coreLogic.ExclusiveGateWay#
	 * processXORGateOutGoingPath (java.lang.String, java.util.List,
	 * java.lang.String, java.lang.String, boolean, java.lang.Object)
	 */
	public void processXORGateOutGoingPath(String processDfnId, List<Map> resourceIdLs, String processInstanceId,
			String status, boolean initialRecursion, Object outGoingData)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		DBObject tasksDfnQry = new BasicDBObject();
		tasksDfnQry.put(Constants.processRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(processDfnId)));
		tasksDfnQry.put(Constants._$or, resourceIdLs);
		DBCursor tasksDfnCursor = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(tasksDfnQry)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskDefinitions)));
		log.debug("[tasksDfnQry]" + tasksDfnQry);
		Map resourcesMap = new LinkedHashMap();
		Map resourcesDefaultMap = new LinkedHashMap();
		for (DBObject dbObject : tasksDfnCursor) {
			String conditiontype = (String) PropertyUtils.getProperty(dbObject, Constants._conditiontypePath);

			if (SequenceFlow.valueOf(conditiontype).equals(SequenceFlow.Default)) {
				resourcesDefaultMap.put((String) dbObject.get(Constants.resourceId), dbObject);
			} else {

				resourcesMap.put((String) dbObject.get(Constants.resourceId), dbObject);
			}

		}
		resourcesMap.putAll(resourcesDefaultMap);
		log.debug("[resourcesMap]" + resourcesMap);
		boolean isAlreadyTakenPath = false;
		Set resourceSet = resourcesMap.entrySet();
		for (Object object : resourceSet) {
			Map.Entry entry = (Map.Entry) object;

			String resourceId = (String) entry.getKey();
			log.debug("[resourceId]" + entry.getKey());
			DBObject taskDfn = (DBObject) entry.getValue();
			log.debug("[taskDfn]" + taskDfn);
			boolean isExecuted = false;
			String conditiontype = (String) PropertyUtils.getProperty(taskDfn, Constants._conditiontypePath);
			log.debug("[conditiontype]" + conditiontype);
			switch (SequenceFlow.valueOf(conditiontype)) {
			case None:// Converging
				isExecuted = true;
				break;
			case Expression:// Diverging
				if (!isAlreadyTakenPath) {
					String conditionExpression = (String) PropertyUtils.getProperty(taskDfn,
							Constants._conditionExpressionPath);
					DBObject evaluateConditionExpressionQry = new BasicDBObject();
					evaluateConditionExpressionQry.putAll((DBObject) JSON.parse(conditionExpression));
					evaluateConditionExpressionQry.put(Constants._id, new ObjectId(processInstanceId));
					log.debug("[evaluateConditionExpressionQry]" + evaluateConditionExpressionQry);
					DBObject qryRes = mongoTemplate.getCollection(Constants.workFlowProcessInstances)
							.findOne(evaluateConditionExpressionQry);
					log.debug("[qryRes]" + qryRes);
					if (qryRes != null) {
						isExecuted = true;
					}
				}

				break;
			case Default:// Diverging
				isExecuted = true;

				break;

			}
			if (!isAlreadyTakenPath && isExecuted) {
				// Update status
				updateStatusHandler.invokeUpdatePathStatus(processDfnId, resourceId, processInstanceId, status,
						outGoingData, initialRecursion);
				isAlreadyTakenPath = true;
			} else {
				// To remove from execution
				updateXORPath(processDfnId, resourceId, processInstanceId, initialRecursion);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.coreLogic.ExclusiveGateWay#updateXORPath(java
	 * .lang.String, java.lang.String, java.lang.String, boolean)
	 */
	public void updateXORPath(String processDfnId, String resourceId, String processInstanceId,
			boolean initialRecursion) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		DBObject taskDfnQry = new BasicDBObject();
		taskDfnQry.put(Constants.processRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(processDfnId)));
		taskDfnQry.put(Constants.resourceId, resourceId);
		DBObject taskDfn = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskDefinitions)
				.findOne(taskDfnQry);

		DBObject taskInstanceQry = new BasicDBObject();
		taskInstanceQry.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
				(ObjectId) taskDfn.get(Constants._id)));
		taskInstanceQry.put(Constants.processInstanceRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));

		DBObject taskInstance = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskInstances)
				.findOne(taskInstanceQry);
		String stencilId = PropertyUtils.getProperty(taskDfn, Constants._taskStencilPath).toString();
		if (!stencilId.equalsIgnoreCase(Constants._Exclusive_Databased_Gateway)
				|| (stencilId.equalsIgnoreCase(Constants._Exclusive_Databased_Gateway)
						&& isAllIncomingXORPathProcessed(processDfnId, resourceId, processInstanceId))) {
			taskInstance.put(Constants.status, Constants._Not_IN_Execution);
			mongoTemplate.getCollection(Constants.workFlowTaskInstances).findAndModify(taskInstanceQry, taskInstance);
			List<Map> outgoingLs = (List<Map>) taskDfn.get(Constants._outgoing);
			for (Map map : outgoingLs) {
				updateXORPath(processDfnId, map.get(Constants.resourceId).toString(), processInstanceId, false);
			}

		}

	}

}

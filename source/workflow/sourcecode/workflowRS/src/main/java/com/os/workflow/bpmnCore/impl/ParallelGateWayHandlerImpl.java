package com.os.workflow.bpmnCore.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.os.workflow.bpmnCore.ParallelGateWayHandler;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;

/**
 * 
 * <b>Purpose:</b><br>
 * ParallelGateWayHandlerImpl <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ParallelGateWayHandlerImpl implements ParallelGateWayHandler {

	private static final Logger log = Logger.getLogger(ParallelGateWayHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. isAllIncomingParallelPathProcessed
	 * To check incoming path to parallel gate way is processed/not
	 * </pre>
	 * 
	 * @param workFlowId
	 * @param resourceId
	 * @param workFlowInstanceId
	 * @return
	 */
	public Boolean isAllIncomingParallelPathProcessed(String workFlowId, String resourceId, String workFlowInstanceId) {
		DBObject taskDfnQry = new BasicDBObject();
		taskDfnQry.put(Constants.processRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(workFlowId)));
		taskDfnQry.put(Constants._outgoingResourceIdPath, resourceId);
		log.debug("[ taskDfnQry]" + taskDfnQry);
		DBCursor tasks = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(taskDfnQry)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskDefinitions)));
		for (DBObject task : tasks) {
			log.debug("[task]" + task);
			DBObject taskInstanceQry = new BasicDBObject();
			taskInstanceQry.put(Constants.processInstanceRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowProcessInstances, new ObjectId(workFlowInstanceId)));
			taskInstanceQry.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
					(ObjectId) task.get(Constants._id)));
			log.debug("[taskInstanceQry]" + taskInstanceQry);
			DBObject taskInstance = mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.findOne(taskInstanceQry);
			log.debug("[taskInstance]" + taskInstance);
			if (taskInstance.get(Constants.status).toString().equalsIgnoreCase(Constants._Waiting_For_Execute))
				return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.bpmnCore.ParallelGateWayHandler#
	 * isParallelGateWayExecuted(java.util.Map)
	 */
	public Boolean isParallelGateWayExecuted(Map taskInstanceQry) {

		log.debug("[taskInstanceQry]" + taskInstanceQry);
		DBObject taskInstance = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskInstances)
				.findOne(new BasicDBObject(taskInstanceQry));
		log.debug("[taskInstance]" + taskInstance);
		if (((String) taskInstance.get(Constants.status)).equalsIgnoreCase(Constants._Completed)) {
			return true;
		} else
			return false;

	}

	@Override
	public synchronized Boolean executeParallelGateWay(String workFlowId, String resourceId, String workFlowInstanceId,
			Map taskInstanceQry, Map taskInstance) {
		log.debug("[taskInstanceQry]" + taskInstanceQry);
		log.debug("[taskInstance]" + taskInstance);
		if (isAllIncomingParallelPathProcessed(workFlowId, resourceId, workFlowInstanceId)
				&& !isParallelGateWayExecuted(taskInstanceQry)) {
			taskInstance.put(Constants.status, Constants._Completed);
			mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.findAndModify(new BasicDBObject(taskInstanceQry), new BasicDBObject(taskInstance));

			return true;
		} else
			return false;
	}
}

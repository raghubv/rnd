package com.os.workflow.bpmnCore.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.os.workflow.bpmnCore.ExclusiveGateWayHandler;
import com.os.workflow.bpmnCore.IntermediateTimerEventHandler;
import com.os.workflow.bpmnCore.ParallelGateWayHandler;
import com.os.workflow.bpmnCore.ScheduleJobHandler;
import com.os.workflow.bpmnCore.ServiceTaskHandler;
import com.os.workflow.bpmnCore.SubProcessHandler;
import com.os.workflow.bpmnCore.TaskExecutorHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.WorkFlow;
import com.os.workflow.util.Constants;

/**
 * 
 * <b>Purpose:</b><br>
 * TaskExecutorHandlerImpl <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Feb 14, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class TaskExecutorHandlerImpl implements TaskExecutorHandler {
	

	public static enum invokeTaskProcessorMethod {
		updatePathStatus, createTaskInstances
	}

	private static final Logger log = Logger.getLogger(TaskExecutorHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ServiceTaskHandler serviceTaskHandler;

	@Autowired
	ScheduleJobHandler scheduleJobHandler;

	@Autowired
	SubProcessHandler subProcessHandler;

	@Autowired
	ExclusiveGateWayHandler exclusiveGateWayHandler;

	@Autowired
	ParallelGateWayHandler parallelGateWayHandler;

	@Autowired
	IntermediateTimerEventHandler intermediateTimerEventHandler;

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	WorkFlow workFlow;

	@Autowired
	UpdateStatusHandler updateStatusHandler;

	public void taskExecute(final DBObject dbObject) {
		taskExecutor.execute(new Thread() {
			public void run() {
				try{
				if (dbObject != null) {
					log.debug("[dbObject]" + dbObject);
					switch (invokeTaskProcessorMethod.valueOf((String) dbObject.get(Constants.INVOKE_METHOD))) {
					case updatePathStatus: {
						updateStatusHandler.updatePathStatus(dbObject.get(Constants.processDfnId).toString(),
								dbObject.get(Constants.resourceId).toString(),
								dbObject.get(Constants.processInstanceId).toString(),
								dbObject.get(Constants.status).toString(), dbObject.get(Constants.outGoingData),
								Boolean.valueOf(dbObject.get(Constants.isUpdatingFirstStep).toString()));
						mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances)
						.remove(new BasicDBObject(Constants._id, dbObject.get(Constants._id)));
						
					}
						break;
					case createTaskInstances: {
						workFlow.createTaskInstances((String) dbObject.get(Constants.processDfnId),
								(String) dbObject.get(Constants.processInstanceId),
								(String) dbObject.get(Constants.processDfnName),
								(String) dbObject.get(Constants.tenant),
								(String) dbObject.get(Constants.nameProcessInstance),
								(DBObject) dbObject.get(Constants._req));
						mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances)
						.remove(new BasicDBObject(Constants._id, dbObject.get(Constants._id)));
					}
						break;
					}
				}
				
				} catch (IllegalAccessException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				} catch (InvocationTargetException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				} catch (NoSuchMethodException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				} catch (JsonParseException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				} catch (JsonMappingException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				} catch (IOException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				}catch (ParseException e) {
					log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
				}

			}
		}

		);
	}
}

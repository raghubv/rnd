package com.os.workflow.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.Resource;
import com.os.workflow.service.WorkFlow;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;

public class ResourceImpl implements Resource {
	private static final Logger log = Logger.getLogger(DiagramImpl.class);

	private static final ResourceBundle message = ResourceBundle
			.getBundle("Message");

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	WorkFlow workFlow;

	public String addProcessDfn(String fileName,String body){
		return workFlow.addProcessDfn( fileName, body);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Resource#getResource(java.lang.String)
	 */
	public String getResource(String fileName) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			log.debug("[fileName]" + fileName);
			if (Constants._bpmn_file.equalsIgnoreCase(fileName)) {
				DBObject bpmn2 = new BasicDBObject(Configuration.getBpmn2());
				// Logic
				List<DBObject> propertyPackages = (List<DBObject>) bpmn2
						.get(Constants._propertyPackages);
				List<DBObject> resPropertyPackages = new ArrayList<DBObject>();
				for (DBObject dbObject : propertyPackages) {
					if (dbObject.get(Constants._name).toString()
							.equalsIgnoreCase(Constants._task)) {
						DBObject query = new BasicDBObject();
						query.put(Constants._propertyType, Constants._task);
						DBCursor resourceMapRes = mongoTemplate.getCollection(
								Constants.bpmnWebModelerCustomField)
								.find(query);
						for (DBObject dbResMap : resourceMapRes) {
							log.debug("[content]"
									+ dbResMap.get(Constants._content));
							((List<DBObject>) dbObject
									.get(Constants._properties))
									.add((DBObject) dbResMap
											.get(Constants._content));
						}

					} else if (dbObject.get(Constants._name).toString()
							.equalsIgnoreCase(Constants._diagram)) {
						DBObject query = new BasicDBObject();
						query.put(Constants._propertyType, Constants._diagram);
						DBCursor resourceMapRes = mongoTemplate.getCollection(
								Constants.bpmnWebModelerCustomField)
								.find(query);
						for (DBObject dbResMap : resourceMapRes) {
							log.debug("[content]"
									+ dbResMap.get(Constants._content));

							((List<DBObject>) dbObject
									.get(Constants._properties))
									.add((DBObject) dbResMap
											.get(Constants._content));
						}

					}
					resPropertyPackages.add(dbObject);
				}

				log.debug("[resPropertyPackages]" + resPropertyPackages);
				//
				DBObject resDBObj = new BasicDBObject();
				resDBObj.put(Constants._title, bpmn2.get(Constants._title));
				resDBObj.put(Constants._namespace,
						bpmn2.get(Constants._namespace));
				resDBObj.put(Constants._description,
						bpmn2.get(Constants._description));
				resDBObj.put(Constants._description_de,
						bpmn2.get(Constants._description_de));
				resDBObj.put(Constants._propertyPackages, resPropertyPackages);
				resDBObj.put(Constants._stencils,
						bpmn2.get(Constants._stencils));
				resDBObj.put(Constants._rules, bpmn2.get(Constants._rules));

				return objectMapper.writeValueAsString(resDBObj);
			}

		} catch (JsonGenerationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(Constants.responseCode.Failed);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Resource#addChoiceTypeOption(java.lang
	 * .String)
	 */
	public String addChoiceTypeOption(String input) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			DBObject dbObjInput = (DBObject) JSON.parse(input);
			DBObject query = new BasicDBObject();

			query.put(Constants._name, dbObjInput.get(Constants._name));
			query.put(Constants._propertyType,
					dbObjInput.get(Constants._propertyType));
			DBObject resourceObject = mongoTemplate.getCollection(
					Constants.bpmnWebModelerCustomField).findOne(query);
			((List<DBObject>) ((DBObject) resourceObject
					.get(Constants._content)).get(Constants._items))
					.add((DBObject) dbObjInput.get(Constants._item));
			mongoTemplate.getCollection(Constants.bpmnWebModelerCustomField)
					.findAndModify(query, resourceObject);
			return MessageUtility.response(
					Constants.responseCode.addChoiceTypeOptionSuccess,
					resourceObject);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(Constants.responseCode.addChoiceTypeOptionFailed);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Resource#addCustomfield(java.lang.String)
	 */
	public String addCustomfield(String input) {
		try {
			DBObject dbObjInput = (DBObject) JSON.parse(input);
			mongoTemplate.getCollection(Constants.bpmnWebModelerCustomField)
					.insert(dbObjInput);
			return MessageUtility
					.response(Constants.responseCode.addCustomFieldSuccess);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(Constants.responseCode.addCustomFieldFailed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Resource#updateCustomfield(java.lang.
	 * String)
	 */
	public String updateCustomfield(String input) {
		try {
			DBObject dbObjInput = (DBObject) JSON.parse(input);
			DBObject query = new BasicDBObject();

			query.put(Constants._name, dbObjInput.get(Constants._name));
			query.put(Constants._propertyType,
					dbObjInput.get(Constants._propertyType));

			mongoTemplate.getCollection(Constants.bpmnWebModelerCustomField)
					.update(query, dbObjInput);
			return MessageUtility
					.response(Constants.responseCode.updateCustomfieldSuccess);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(Constants.responseCode.updateCustomfieldFailed);
	}

}

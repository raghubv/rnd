package com.os.workflow.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.Diagram;

/**
 * 
 * <b>Purpose:</b><br>
 * ExportImage <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 3, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ExportImage extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7400783693917472193L;
	private static final Logger log = Logger.getLogger(ExportImage.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			// TODO Auto-generated method stub
			WebApplicationContext context = WebApplicationContextUtils
					.getWebApplicationContext(getServletContext());
			Diagram mongoTemplate = (Diagram) context.getBean("diagram");
			// JPEGTranscoder t = new JPEGTranscoder();
			PNGTranscoder t = new PNGTranscoder();
			String processInstanceId = req.getPathInfo().replaceAll("/", "");
			log.debug("[processInstanceId]" + processInstanceId);
			String scgContent = mongoTemplate
					.getBMPNDiagramByProcessInstanceId(processInstanceId);
			TranscoderInput input = new TranscoderInput(
					new ByteArrayInputStream(scgContent.getBytes()));
			OutputStream ostream = resp.getOutputStream();
			TranscoderOutput output = new TranscoderOutput(ostream);
			t.transcode(input, output);
			ostream.flush();
			ostream.close();
			resp.setContentType("image/png");
			super.doGet(req, resp);
		} catch (TranscoderException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
	}

}

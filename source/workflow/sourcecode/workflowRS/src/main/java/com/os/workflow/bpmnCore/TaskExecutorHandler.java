package com.os.workflow.bpmnCore;

import com.mongodb.DBObject;

/**
 * 
 * <b>Purpose:</b><br>
 * TaskClaimHandler <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface TaskExecutorHandler{
	
	/**
	 * 
	 * @param dbObject
	 */
	public void taskExecute(final DBObject dbObject);
	
}

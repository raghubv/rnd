package com.os.workflow.rest;

import java.util.ResourceBundle;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.Diagram;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;

/**
 * 
*<b>Purpose:</b><br> 
* DiagramRS <br><br> 
*

* <b>RevisionHistory:</b> 
* <pre><b> 
* Sl No Modified Date Author</b> 
* ==============================================
* 1 Apr 1, 2014  @author Raghu -- Base Release 
* 
* </pre><br>
 */
@Component
@Path("/diagramRS")
public class DiagramRS {
	static final Logger log = Logger.getLogger(DiagramRS.class);
	static final ResourceBundle bundle = ResourceBundle
			.getBundle("RestService");
	@Autowired
	Diagram diagram;

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeDiagramService
	 * 
	 * </pre>
	 * 
	 * @param method
	 * @param filename
	 * @return
	 */
	@Path("/diagram/{method}/{filename}")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeDiagramService(@PathParam("method") String method,
			@PathParam("filename") String filename) {
		log.info("[>>]" + method + "[>>]Begin..");
		log.info("[>>]" + filename + "[>>]Begin..");
		String outPut = "{}";
		try {
			outPut = (String) diagram.getClass()
					.getDeclaredMethod(method, String.class)
					.invoke(diagram, filename);
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
			outPut = MessageUtility.response(Constants.responseCode.Failed);
		}
		log.debug("[outPut]"+outPut);
		log.info("[" + method + "]end..");
		return outPut;
	}

}
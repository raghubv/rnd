package com.os.workflow.service;


public interface Diagram {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getBMPNDiagramByWorkFlowId
	 * To get process definiton diagram by process definintion id
	 * </pre>
	 * 
	 * @param request
	 * @return
	 */
	public String getBMPNDiagramByProcessDfnId(String request);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getBMPNDiagramByProcessInstanceId
	 * To get process instance diagram by process instance id
	 * </pre>
	 * 
	 * @param request
	 * @return
	 */
	public String getBMPNDiagramByProcessInstanceId(String request);

}

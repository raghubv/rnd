package com.os.workflow.helper;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import com.os.workflow.util.Constants;


public class Producer {

	public static Logger logger = Logger.getLogger(Producer.class);
	
	public static void sendQMessage(String target,String reqMessage)
	{
		try 
		{
			ConnectionFactory connectionFactory =new ActiveMQConnectionFactory(PropertiesLoader.getValueFor(Constants.ACTIVEMQ_URL));
	        Connection connection = connectionFactory.createConnection();
	        connection.start();
	        Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
	        Destination destination = session.createQueue(target);
	        MessageProducer producer = session.createProducer(destination);
	        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
//	        producer.setTimeToLive(Long.valueOf(PropertiesLoader.getValueFor("time_to_live")));
	        TextMessage message = session.createTextMessage(reqMessage);
	        producer.send(message);
	        logger.info("Sent message '" + message.getText() + "'"+" to "+target+" destination:"+destination);
	        session.close();
	        connection.close();
		}
		catch (JMSException e) {
			e.printStackTrace();
			logger.error("Exception occured in :["+logger.getClass().getName()+"] function:sendQMessage "+e.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in :["+logger.getClass().getName()+"] function:sendQMessage "+e.getMessage());
		}
	}
}

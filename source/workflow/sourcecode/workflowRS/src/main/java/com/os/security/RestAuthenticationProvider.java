package com.os.security;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * 
 * <b>Purpose:</b><br>
 * RestAuthenticationProvider:To enable baisc authentication <br>
 * <br>
 * 
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1        Dec 11, 2013		    @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class RestAuthenticationProvider implements AuthenticationProvider {
	static final ResourceBundle bundle = ResourceBundle.getBundle("RestService_en_US");

	static final Logger log = Logger.getLogger(RestAuthenticationProvider.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		RestToken restToken = (RestToken) authentication;

		String key = restToken.getKey();
		String credentials = restToken.getCredentials();
		log.info("[key]" + key);
		if ((!key.equals(bundle.getString("userName"))) || (!credentials.equals(bundle.getString("password")))) {
			throw new BadCredentialsException(bundle.getString("autherror"));
		}
		return getAuthenticatedUser(key, credentials);
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 	1. getAuthenticatedUser
	 * 
	 * </pre>
	 * 
	 * @param key
	 * @param credentials
	 * @return
	 */
	private Authentication getAuthenticatedUser(String key, String credentials) {
		List authorities = new ArrayList();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

		return new RestToken(key, credentials, authorities);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * supports (java.lang.Class)
	 */
	public boolean supports(Class<?> authentication) {
		return RestToken.class.equals(authentication);
	}
}

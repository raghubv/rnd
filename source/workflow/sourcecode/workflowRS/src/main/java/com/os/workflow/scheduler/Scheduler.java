package com.os.workflow.scheduler;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Scheduler: <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Aug 27, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface Scheduler {

	/**
	 * To run scheduler
	 */
	public void runScheduler();
}

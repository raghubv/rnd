package com.os.workflow.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class TLogger {
	private static TLogger getInstance() {
		try {
			_logWrapper = new TLogger(loggerName);
			return _logWrapper;

		} catch (Exception e) {
		}

		return _logWrapper;
	}

	private static String formatMessage(String message) {
		String fullClassName = java.lang.Thread.currentThread().getStackTrace()[3]
				.getClassName();
		String className = fullClassName.substring(fullClassName
				.lastIndexOf(".") + 1);
		int lineNumber = java.lang.Thread.currentThread().getStackTrace()[3]
				.getLineNumber();
		if (lineNumber == -1) {
			lineNumber = java.lang.Thread.currentThread().getStackTrace()[0]
					.getLineNumber();
		}
		String printmessage = "[ClassName]: " + fullClassName
				+ " [LineNumber]:  " + lineNumber + "  " + message;
		return printmessage;
	}

	private static String convertToString(Throwable t) {
		String logMessage = null;
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		logMessage = sw.toString();
		return logMessage;
	}

	public static void debug(String message) {
		getInstance();
		if ((!(isEnable)) || (_logWrapper == null))
			return;
		_log.debug(formatMessage(message));
	}

	public static void warn(String message) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.warn(formatMessage(message));
	}

	public static void info(String message) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.info(formatMessage(message));
	}

	public static void error(String message) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.error(formatMessage(message));
	}

	public static void error(String message, Throwable t) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.error(message + " : " + convertToString(t));
	}

	public static void error(String message, Exception t) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.error(message + " : " + convertToString(t));
	}

	/*public static void error(String message, ApplicationException t) {
		getInstance();
		if (_logWrapper == null)
			return;
		_log.error(message + " : " + convertToString(t));
	}*/

	public static void setLogLevel(String level) {
		if ("debug".equalsIgnoreCase(level)) {
			Logger.getRootLogger().setLevel(Level.DEBUG);
		} else if ("info".equalsIgnoreCase(level)) {
			Logger.getRootLogger().setLevel(Level.INFO);
		} else if ("error".equalsIgnoreCase(level)) {
			Logger.getRootLogger().setLevel(Level.ERROR);
		} else if ("fatal".equalsIgnoreCase(level)) {
			Logger.getRootLogger().setLevel(Level.FATAL);
		} else {
			if (!("warn".equalsIgnoreCase(level)))
				return;
			Logger.getRootLogger().setLevel(Level.WARN);
		}
	}

	private TLogger(String loggerName) throws Exception {
		try {
			_log = Logger.getLogger(loggerName);
			isEnable = _log.isDebugEnabled();
		} catch (Exception e) {
		}
	}

	public static void setLoggerName(String logName) {
		if (logName == null) {
			loggerName = "treetlogs";
			getInstance();
		} else {
			loggerName = logName;
			getInstance();
		}
	}

	public static String getLoggerName() {
		return loggerName;
	}

	public static void removeAll(){
		_logWrapper = null;
	}

	public static boolean debugIsEnabled() {
		getInstance();
		return isEnable;
	}

	private static Logger _log = null;
	private static TLogger _logWrapper = null;
	private static boolean isEnable = false;
	private static String loggerName = "treetlogs";
}

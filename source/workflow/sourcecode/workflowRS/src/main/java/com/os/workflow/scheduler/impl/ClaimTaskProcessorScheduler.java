package com.os.workflow.scheduler.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.os.workflow.bpmnCore.impl.TaskClaimHandlerImpl;
import com.os.workflow.bpmnCore.impl.UpdateStatusHandlerImpl;
import com.os.workflow.scheduler.Scheduler;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;
import com.os.workflow.util.Utility;

public class ClaimTaskProcessorScheduler implements Scheduler {
	private static final Logger log = Logger.getLogger(ClaimTaskProcessorScheduler.class);

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	UpdateStatusHandlerImpl updateStatusHandler;
	@Autowired
	TaskClaimHandlerImpl taskClaimHandler;

	@Override
	public void runScheduler() {

		DBObject query = new BasicDBObject();
		query.put(Constants.status, Constants._Activated);
		query.put(Constants._claimExpiryTime, new BasicDBObject(Constants._$lte, Utility.getDate()));
		log.debug("[query]" + query);
		DBObject sort=new BasicDBObject(Constants._id, -1);
		DBCursor schedulerCursor = mongoTemplate.getCollection(Constants.claimTaskInstances).find(query).sort(sort)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.claimTaskInstances))).limit(200);
		for (final DBObject schedulerInstance : schedulerCursor) {
			log.debug("[schedulerInstance]" + schedulerInstance);
			taskExecutor.execute(new Thread() {
				public void run() {
					taskClaimHandler.realeaseClaim(schedulerInstance, Constants._claimReleasedByTimer);
				}

			});

		}

	}

}

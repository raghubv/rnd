package com.os.workflow.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.mongodb.DBObject;
import com.os.workflow.helper.LoggerTrace;

/**
 * 
 * <b>Purpose:</b><br>
 * MessageUtility <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 21, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class MessageUtility {

	private static final ResourceBundle message = ResourceBundle
			.getBundle("Message_en_US");

	private static final Logger log = Logger.getLogger(MessageUtility.class);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. response
	 * 
	 * </pre>
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return
	 */
	public synchronized static String response(
			Constants.responseCode responseCode, String responseMessage) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map responseMap = new HashMap();
			responseMap.put(message.getString("responseCode"),
					responseCode.getResponseCode());
			responseMap.put(message.getString("responseDesc"),
					message.getString(responseCode.getResponseCode()));
			responseMap.put(message.getString("responseMessage"),
					responseMessage);
			return objectMapper.writeValueAsString(responseMap);
		} catch (JsonGenerationException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. response
	 * 
	 * </pre>
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return
	 */
	public synchronized static String response(
			Constants.responseCode responseCode, DBObject responseMessage) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map responseMap = new HashMap();
			responseMap.put(message.getString("responseCode"),
					responseCode.getResponseCode());
			responseMap.put(message.getString("responseDesc"),
					message.getString(responseCode.getResponseCode()));
			responseMap.put(message.getString("responseMessage"),
					responseMessage);
			return objectMapper.writeValueAsString(responseMap);
		} catch (JsonGenerationException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. response
	 * 
	 * </pre>
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return
	 */
	public synchronized static String response(
			Constants.responseCode responseCode, Map responseMessage) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setDateFormat(Constants.YYYY_MM_DD_HH_MM_SS_DATE_FORMAT);
			Map responseMap = new HashMap();
			responseMap.put(message.getString("responseCode"),
					responseCode.getResponseCode());
			responseMap.put(message.getString("responseDesc"),
					message.getString(responseCode.getResponseCode()));
			responseMap.put(message.getString("responseMessage"),
					responseMessage);
			return objectMapper.writeValueAsString(responseMap);
		} catch (JsonGenerationException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. response
	 * 
	 * </pre>
	 * 
	 * @param responseCode
	 * @return
	 */
	public synchronized static String response(
			Constants.responseCode responseCode) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Map responseMap = new HashMap();
			responseMap.put(message.getString("responseCode"),
					responseCode.getResponseCode());
			responseMap.put(message.getString("responseDesc"),
					message.getString(responseCode.getResponseCode()));

			return objectMapper.writeValueAsString(responseMap);
		} catch (JsonGenerationException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}
}

package com.os.workflow.bpmnCore;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.bson.types.ObjectId;

import com.mongodb.DBObject;

/**
 * 
 * <b>Purpose:</b><br>
 * ScheduleJobHandler <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface ScheduleJobHandler {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. schedule
	 * To schedule the job
	 * 
	 * </pre>
	 * 
	 * @param type
	 * @param taskDfn
	 * @param taskInstance
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws ParseException
	 */

	public void schedule(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException;

	/**
	 * 
	 * @param objectId
	 * @param type
	 * @param timeOut
	 * @param timerType
	 * @param isBoundryEvent
	 */
	public void scheduleEvent(ObjectId objectId, String type, String timeOut, String timerType);

	/**
	 * 
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. scheduleEvent
	 * 
	 * </pre>
	 *
	 * @return void
	 */
	public void scheduleEvent(ObjectId taskObjectId, ObjectId parentTaskObjectId, String type, String timeOut,
			String timerType);

	/**
	 * 
	 * @param taskObjectId
	 * @param parentTaskObjectId
	 * @param type
	 * @param timeOut
	 * @param timerType
	 * @param isBoundryEvent
	 */
	public void cancelEvent(ObjectId taskObjectId, ObjectId parentTaskObjectId, String type, String timeOut,
			String timerType);
}

package com.os.workflow.service;


public interface Resource {
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getResource
		* 
		* </pre> 
		*
		* @return String
	 */
	public String getResource(String fileName);
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. addChoiceTypeOption
	* To add choice type field
	* </pre> 
	* @param input
	* @return
	 */
	public String addChoiceTypeOption(String input);
	
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. addChoiceTypeOption
	* To add option to existing choice type
	* </pre> 
	* @param input
	* @return
	 */
	public String addCustomfield(String input);
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. updateCustomfield
	* To add custom field
	* </pre> 
	* @param input
	* @return
	 */
	public String updateCustomfield(String input);
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public String addProcessDfn(String fileName,String body);

}

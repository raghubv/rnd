package com.os.workflow.bpmnCore;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.mongodb.DBObject;

/**
 * 
 * <b>Purpose:</b><br>
 * SubProcessHandler <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface SubProcessHandler {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. collapsedSubProcess
	 * To handled collapse subprocess
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @param taskDfn
	 * @param taskInstance
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IOException
	 */
	public DBObject collapsedSubProcess(String processInstanceId,
			DBObject taskDfn, DBObject taskInstance) throws JsonParseException,
			JsonMappingException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException, IOException;

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createSubProcessInstance
	 * To create sub process instance
	 * </pre>
	 * 
	 * @param collapsedSubProcessName
	 * @param workflowType
	 * @param taskInstanceId
	 * @param payload
	 * @param nameProcessInstance
	 * @return
	 */
	public String createSubProcessInstance(String collapsedSubProcessName,
			String workflowType, String taskInstanceId, String payload,
			String nameProcessInstance);
}

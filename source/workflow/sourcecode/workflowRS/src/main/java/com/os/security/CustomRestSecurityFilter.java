package com.os.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.filter.GenericFilterBean;

/**
 * 
 * <b>Purpose:</b><br>
 * CustomRestSecurityFilter:To enable baisc authentication <br>
 * <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1        Dec 11, 2013		    @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class CustomRestSecurityFilter extends GenericFilterBean {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private AuthenticationEntryPoint authenticationEntryPoint;

	static final Logger log = Logger.getLogger(CustomRestSecurityFilter.class);

	/**
	 * 
	 * @param authenticationManager
	 */
	public CustomRestSecurityFilter(AuthenticationManager authenticationManager) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		log.info("Begin....");

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		String authorization = request.getHeader("Authorization");

		if (authorization == null) {
			chain.doFilter(request, response);
			return;
		}

		String[] credentials = decodeHeader(authorization);
		assert (credentials.length == 2);
		Authentication authentication = new RestToken(credentials[0], credentials[1]);
		try {
			Authentication successfulAuthentication = this.authenticationManager.authenticate(authentication);

			SecurityContextHolder.getContext().setAuthentication(successfulAuthentication);

			chain.doFilter(request, response);
		} catch (AuthenticationException authenticationException) {
			SecurityContextHolder.clearContext();
			this.authenticationEntryPoint.commence(request, response, authenticationException);
		}
		log.info("End....");
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 	1. decodeHeader: to decode basic authentication
	 * 
	 * </pre>
	 * 
	 * @param authorization
	 * @return
	 */
	public String[] decodeHeader(String authorization) {
		try {
			byte[] decoded = Base64.decode(authorization.substring(6).getBytes("UTF-8"));

			String credentials = new String(decoded);
			return credentials.split(":");
		} catch (UnsupportedEncodingException e) {
			throw new UnsupportedOperationException(e);
		}
	}
}

package com.os.workflow.util;

import java.util.Date;

/**
 * 
 * <b>Purpose:</b><br>
 * Utility <br>
 * <br>
 *
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 *
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 *
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Utility {

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. checkNullString
	 * 
	 * </pre>
	 * 
	 * @param inputString
	 * @return
	 */
	public static String checkNullString(String inputString) {
		if (inputString != null)
			return inputString;
		else
			return "";
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. checkNullInteger
	 * 
	 * </pre>
	 * 
	 * @param inputString
	 * @return
	 */
	public static Integer checkNullInteger(String inputString) {
		if (inputString != null && !inputString.isEmpty())
			return Integer.valueOf(inputString);
		else
			return 0;
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getScheduledDateAndTime
		* 
		* </pre> 
		*
		* @return Date
	 */
	public static synchronized Date getScheduledDateAndTime(String timeOut) {
		return ( new Date(getDate().getTime()+(Integer.valueOf(timeOut.substring(0, 2)) * 24 * 60
					* Constants.ONE_MINUTE_IN_MILLIS
					+ Integer.valueOf(timeOut.substring(3, 5)) * 60
					* Constants.ONE_MINUTE_IN_MILLIS + Integer.valueOf(timeOut
					.substring(6, 8)) * Constants.ONE_MINUTE_IN_MILLIS)));

	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. getDate
		* 
		* </pre> 
		*
		* @return Date
	 */
	public static synchronized Date getDate() {
		return new Date();
	}
	public static void main(String[] args) {
		System.out.println("executiondate" + getScheduledDateAndTime("00D00H10M"));
	
	}
	
	

	
	
	
}


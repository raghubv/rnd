package com.os.workflow.util;

import java.text.SimpleDateFormat;

/**
 * 
 * <b>Purpose:</b><br>
 * Constants <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 1, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface Constants {

	public static enum responseCode {
		Success(0), Failed(100), addProcessDfnSuccess(1), addProcessDfnFailed(
				101), getProcessDfnIdSuccess(2), getProcessDfnIdFailed(102), getProcessListSuccess(
				3), getProcessListFailed(103), createProcessInstanceSuccess(4), createProcessInstanceFailed(
				104), getTaskCountSuccess(5), getTaskCountFailed(105), getTasksSuccess(
				6), getTasksFailed(106), getTaskInfoSuccess(7), getTaskInfoFailed(
				107), getTasksInfoByProcessIdSuccess(8), getTasksInfoByProcessIdFailed(
				108), getSubProcessByProcessInstanceIdSuccess(9), getSubProcessByProcessInstanceIdFailed(
				109), updateTaskStatusSuccess(10), updateTaskStatusFailed(110), acquireClaimSuccess(
				11), acquireClaimFailed(111), realeaseClaimSuccess(12), realeaseClaimFailed(
				112), getClaimHistoryByTaskInstanceIdSuccess(13), getClaimHistoryByTaskInstanceIdFailed(
				113), getResourceSuccess(14), getResourceFailed(114), addCustomFieldSuccess(
				15), addCustomFieldFailed(115), updateCustomfieldSuccess(16), updateCustomfieldFailed(
				116), addChoiceTypeOptionSuccess(17), addChoiceTypeOptionFailed(
				117), getBMPNDiagramByProcessDfnIdSuccess(18), getBMPNDiagramByProcessDfnIdFailed(
				118), getBMPNDiagramByProcessInstanceIdSuccess(19), getBMPNDiagramByProcessInstanceIdFailed(
				119), mongoDBConnectionError(201),reAssginTaskSucess(20),reAssginTaskFailed(120);
		//
		// insertionSuccess(1), updatedSuccess(2), insertionFailed(
		// 102), updatedFailed(103), claimSuccess(3), claimFailure(104),
		// claimReleaseSuccess(
		// 4),
		// claimReleaseFailure(105),claimHistorySuccess(5),claimHistoryFailure(106)
		// ;

		private int value;

		private responseCode(int value) {
			this.value = value;
		}

		public String getResponseCode() {
			return String.valueOf(this.value);
		}
	};

	public static final SimpleDateFormat DD_MM_YYYY_DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yyyy");
	public static final SimpleDateFormat YYYY_MM_DD_HH_MM_SS_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static final SimpleDateFormat MONGODB_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public static final long ONE_MINUTE_IN_MILLIS = 60000;

	/** Mongo DB-Workflow Collection Names -Start **/
	public static final String workFlowProcessDefinitions = "workFlowProcessDefinitions";
	public static final String workFlowProcessInstances = "workFlowProcessInstances";
	public static final String workFlowTaskDefinitions = "workFlowTaskDefinitions";
	public static final String workFlowTaskInstances = "workFlowTaskInstances";
	public static final String quartzSchedulerInstances = "quartzSchedulerInstances";
	public static final String claimTaskInstances = "claimTaskInstances";
	public static final String workFlowTaskProcessingIntances = "workFlowTaskProcessingIntances";
	
	/** Mongo DB-Workflow Collection Names -End **/

	/** Web Modeler start **/
	public static final String bpmnWebModelerResources = "bpmnWebModelerResources";
	public static final String bpmnWebModelerCustomField = "bpmnWebModelerCustomField";
	/** Web Modeler end **/
	/** Mongo DB Operator-Start **/
	public static final String _$exists = "$exists";
	public static final String _$set = "$set";
	public static final String _$gte = "$gte";
	public static final String _$gt = "$gt";
	public static final String _$lte = "$lte";
	public static final String _$lt = "$lt";
	public static final String _$in = "$in";
	public static final String _$nin = "$nin";
	public static final String _$or = "$or";
	public static final String _$and = "$and";
	/** Mongo DB Operator-End **/
	/** Mongod DB Aggregation command **/
	public static final String _$match = "$match";
	public static final String _$group = "$group";
	public static final String _$sum = "$sum";
	public static final String _$ = "$";

	/** Work Flow Configuration SVG --Start **/
	/** SVG Id search **/
	public static final String _idSearchPattern = "//*[@id = '%1$s' or @Id = '%1$s' or @ID = '%1$s' or @iD = '%1$s' ]";
	public static final String _svgEventOnclick = "onclick";
	public static final String _svgEventWindowLocation = "javascript:window.location='./";
	public static final String _singleQuote = "'";

	/** SVG --Processing constants **/
	public static final String signavioXMLFileExtn = ".signavio.xml";
	public static final String svgRepresentationExtn = ".svg";
	public static final String _svg = "svg";
	public static final String _svg_ = "svg-";
	public static final String _svgStop = "stop";
	public static final String _svgStopColor = "stop-color";
	/** SVG & JSON Tags **/
	public static final String jsonRepresentationTag = "json-representation";
	public static final String svgRepresentationTag = "svg-representation";
	public static final String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	/** Work Flow Configuration SVG --End **/

	public static final String bpmnFileName = "bpmnFileName";
	public static final String status = "status";
	public static final String payload = "payload";
	public static final String processDfnId = "processDfnId";
	public static final String processDfnName = "processDfnName";

	public static final String processInstanceId = "processInstanceId";
	public static final String processInstancePayload = "processInstancePayload";
	public static final String processInstanceCompletedDateTime = "processInstanceCompletedDateTime";
	public static final String taskInstanceId = "taskInstanceId";
	public static final String subProcessInstanceId = "subProcessInstanceId";
	public static final String subProcessInstanceName = "subProcessInstanceName";
	public static final String callBackTaskInstanceId = "callBackTaskInstanceId";
	public static final String taskDfnproperties = "taskDfnproperties";
	public static final String taskInstancePayload = "taskInstancePayload";
	public static final String taskInstancePayloadLs = "taskInstancePayloadLs";
	public static final String childShapes = "childShapes";
	public static final String subprocessLs = "subprocessLs";
	public static final String taskLs = "taskLs";
	public static final String count = "count";
	public static final String totalCount = "totalCount";
	public static final String processInstanceLs = "processInstanceLs";
	public static final String dateWithTimeStamp = "dateWithTimeStamp";
	public static final String ruleType = "ruleType";
	public static final String taskType = "taskType";
	public static final String tenant = "tenant";
	public static final String workflowType = "workflowType";
	public static final String nameProcessInstance = "nameProcessInstance";
	public static final String actionCommand = "actioncommand";
	public static final String outGoingData = "outGoingData";
	public static final String inComingData = "inComingData";
	public static final String properties = "properties";
	public static final String stencilId = "stencilId";
	public static final String childrenResources = "childrenResources";
	public static final String taskName = "taskName";
	public static final String taskInitiatedDateTime = "taskInitiatedDateTime";
	public static final String taskCompletedDateTime = "taskCompletedDateTime";
	public static final String resourceId = "resourceId";
	public static final String fallOut = "Fall Out";
	public static final String jeopardy = "Jeopardy";
	public static final String escalation = "Escalation";
	public static final String delayTimeOut = "Delay TimeOut";

	public static final String _Queue = "Queue";
	public static final String _subprocess = "subprocess";
	public static final String processLs = "processLs";
	public static final String subProcessDtls = "subProcessDtls";
	public static final String claimHistoryLs = "claimHistoryLs";

	public static final String _Jeopardy = "Jeopardy";
	public static final String _Fall_Out = "Fall Out";
	public static final String _outgoing = "outgoing";
	public static final String _id = "_id";
	public static final String _User = "User";
	public static final String _Service = "Service";
	public static final String digramFields = "digramFields";
	public static final String filterFields = "filterFields";
	public static final String fieldName = "name";
	public static final String xmlJsonPath = "xmlJsonPath";
	public static final String dbPath = "dbPath";
	public static final String processDfnConfig = "processDfnConfig";
	public static final String taskDfnConfig = "taskDfnConfig";

	/** BMPN Modeler Resources **/
	public static final String _bpmn_file = "bpmn2.0.json";
	public static final String _fileName = "fileName";
	public static final String _content = "content";

	public static final String _task = "task";
	public static final String _diagram = "diagram";
	public static final String _name = "name";
	public static final String _properties = "properties";
	public static final String _items = "items";
	public static final String _item = "item";

	public static final String _title = "title";
	public static final String _namespace = "namespace";
	public static final String _description = "description";
	public static final String _description_de = "description_de";
	public static final String _propertyPackages = "propertyPackages";
	public static final String _propertyType = "propertyType";
	public static final String _stencils = "stencils";
	public static final String _rules = "rules";

	/** Load configuration from class loader path- start **/
	public static final String _Configuration_Json = "/Configuration.json";
	public static final String _MapReduceConfiguration_Json = "/MapReduceConfiguration.json";
	public static final String _Bpmn2_Json = "/bpmn2.json";
	public static final String _BpmnWebModelerCustomField_Json = "/bpmnWebModelerCustomField.json";
	public static final String _IndexKeys_Json = "/indexKeys.json";
	/** Load configuration from class loader path- end **/

	/** BPMN Definition configuration Path--start **/
	public static final String _ServiceImplementationPath = "properties.implementation";
	public static final String _conditionExpressionPath = "properties.conditionexpression";
	public static final String _conditiontypePath = "properties.conditiontype";
	public static final String _bpmnDiagramPath = "bpmnDiagram";
	public static final String _treeStructurePath = "treeStructure";
	public static final String _taskTaskTypePath = "properties.tasktype";
	public static final String _taskRuleTypePath = "properties.ruletype";
	public static final String _taskStencilPath = "stencil.id";
	public static final String _taskNamePath = "properties.name";

	public static final String _taskTimerType = "properties.timertype";
	public static final String _subProcessRefPath = "properties.processref";
	public static final String _queueTargetPath = "properties.queuetarget";
	public static final String _queuePriorityPath = "properties.queuepriority";
	public static final String _queueApplicationIdPath = "properties.queueapplicationid";
	public static final String _restAPIURLPath = "properties.restapiurl";
	public static final String _restAPIUserNamePath = "properties.restapiusername";
	public static final String _restAPIPasswordPath = "properties.restapipassword";
	public static final String _restAPIJSONStringPath = "properties.restapijsonstring";
	// Time bound
	public static final String _taskAsyncPath = "properties.async";
	public static final String _taskTimeOutPath = "properties.timeout";
	public static final String _fallOuttimeOutPath = "properties.fallouttimeout";
	public static final String _jeopardytimeOutPath = "properties.jeopardytimeout";
	public static final String _outGoingDatatPath = "properties.outgoingdata";
	// StartTimerEvent
	public static final String _StartTimerEvent_Properties_Timedate = "properties.timedate";
	public static final String _StartTimerEvent_Properties_Timecycle = "properties.timecycle";
	public static final String _StartTimerEvent_Properties_Timeduration = "properties.timeduration";
	public static final String _IntermediateTimerEvent_Properties_Timedate = "properties.timedate";
	public static final String _IntermediateTimerEvent_Properties_Timecycle = "properties.timecycle";
	public static final String _IntermediateTimerEvent_Properties_Timeduration = "properties.timeduration";
	public static final String _IntermediateTimerEvent_Properties_Boundarycancelactivity2 = "properties.boundarycancelactivity2";
	public static final String _IntermediateTimerEvent_Properties_TimerType = "properties.timertype";

	public static final String _claimtimeoutPath = "properties.claimtimeout";

	/** BPMN Definition configuration Path--end **/

	// public static final String _treeStructureInitialResourceIdPath =
	// "treeStructure.resourceId";
	public static final String _processInstanceRefId = "processInstanceRef.$id.$oid";
	public static final String _taskInsatancesRefId = "taskInsatancesRef.$id.$oid";
	public static final String _parentTaskInsatancesRefId = "parentTaskInsatancesRef.$id.$oid";

	public static final String _idPath = "_id.$oid";
	public static final String _outgoingResourceIdPath = "outgoing.resourceId";
	public static final String _$id = "$id";
	public static final String _payloadSubProcessPath = "payload.subprocess";
	// DBRef for multiple collection
	public static final String processRef = "processRef";
	public static final String taskRef = "taskRef";
	public static final String processInstanceRef = "processInstanceRef";

	/** Support core bpmn core element-start **/
	public static final String _Exclusive_Databased_Gateway = "Exclusive_Databased_Gateway";
	public static final String _ParallelGateway = "ParallelGateway";
	public static final String _Task = "Task";
	public static final String _StartNoneEvent = "StartNoneEvent";
	public static final String _StartMessageEvent = "StartMessageEvent";
	public static final String _StartEventLs = "StartEventLs";
	public static final String _StartTimerEvent = "StartTimerEvent";
	public static final String _IntermediateTimerEvent = "IntermediateTimerEvent";
	public static final String _EndNoneEvent = "EndNoneEvent";
	public static final String _SequenceFlow = "SequenceFlow";
	public static final String _Lane = "Lane";
	public static final String _Pool = "Pool";
	/** Support core bpmn core element-end **/

	// Status Code
	public static final String _Waiting_For_Execute = "W";
	public static final String _Activated = "A";
	public static final String _Deactivated = "D";
	public static final String _IN_Execution = "I";
	public static final String _Not_IN_Execution = "X";
	public static final String _Completed = "C";
	public static final String _In_Compensation = "A";
	public static final String _Compensation = "A";
	public static final String _In_Error = "E";
	public static final String _In_FallOut = "F";
	public static final String _Delay_TimeOut = "T";
	public static final String _Escalation = "S";

	public static final String _In_Cancellation = "C";
	public static final String _Cancelled = "A";

	// Status color
	public static final String _Waiting_For_Execute_color = "W";
	public static final String _Activated_Color = "A";
	public static final String _Deactivated_Color = "D";
	public static final String _IN_Execution_Color = "#FFCC00";
	public static final String _Not_IN_Execution_Color = "#C0C0C0";
	public static final String _Completed_Color = "#339966";
	public static final String _In_Compensation_Color = "A";
	public static final String _Compensation_Color = "A";
	public static final String _In_Error_Color = "#FF0000";
	public static final String _In_FallOut_Color = "#00FF00";
	public static final String _Delay_TimeOut_Color = "#993366";
	public static final String _Escalation_color = "#993366";
	public static final String _In_Cancellation_Color = "C";
	public static final String _Cancelled_Color = "A";

	/** Quartz scheduler start **/
	public static final String quartzSchedulerId = "quartzSchedulerId";
	public static final String quartzSchedulerType = "quartzSchedulerType";
	public static final String taskInsatancesRef = "taskInsatancesRef";
	public static final String parentTaskInsatancesRef = "parentTaskInsatancesRef";
	public static final String quartzScheduledDate = "quartzScheduledDate";
	public static final String quartzScheduledExecutionDate = "quartzScheduledExecutionDate";
	public static final String quartzScheduledTimeOut = "quartzScheduledTimeOut";
	public static final String quartzScheduledRepeatInterval = "quartzScheduledRepeatInterval";
	public static final String quartzStatus = "quartzStatus";
	public static final String quartzScheduledTimerType = "quartzScheduledTimerType";
	public static final String quartzStatusActive = "A";
	public static final String quartzStatusInprogress = "I";
	public static final String quartzStatusExecuted = "E";

	static public enum ScheduleType {
		Task, StartTimerEvent, IntermediateTimerEvent
	};

	/** Quartz scheduler end **/
	/** Claim -start **/
	// Claim Status
	public static final String _claimAcquire = "A";
	public static final String _claimRelease = "R";
	public static final String _claimReleasedByUser = "U";
	public static final String _claimReleasedByTimer = "T";
	public static final String _claimReleasedBy = "claimReleasedBy";
	// Claim Details
	public static final String _userCode = "userCode";
	public static final String _userName = "userName";
	public static final String _claimDtls = "claimDtls";
	public static final String _claimStatus = "claimStatus";
	public static final String _claimExpiryTime = "claimExpiryTime";
	public static final String _claimAcquireTime = "claimAcquireTime";

	/** Claim -end **/

	public static final String _getTasks = "getTasks";
	/** Fine tuned Query out put field-form Configuration.json -start **/
	public static final String _getTasksQueryFields = "getTasksQueryFields";
	public static final String _getProcessListQueryFields = "getProcessListQueryFields";
	public static final String _getCreateProcessInstanceQueryFields = "getCreateProcessInstanceQueryFields";
	public static final String _getClaimHistoryByTaskInstanceIdQueryFields = "getClaimHistoryByTaskInstanceIdQueryFields";
	/** Fine tuned Query out put field-form Configuration.json -end **/
	/** Pagination-start **/
	public static final String _pageIndex = "pageIndex";
	public static final int _numOfRecords = 100;
	/** Pagination-end **/

	/** Spring Bean **/
	public static final String _mongoTemplateBean = "mongoTemplate";
	public static final String _taskClaimBean = "taskClaimHandler";
	public static final String _updateStatusBean = "updateStatusHandler";
	
	public static final String ACTIVEMQ_URL = "activemq.broker1.amq01.uri";
	public static final String isUpdatingFirstStep="isUpdatingFirstStep";
	
	
	
	public static final String INVOKE_METHOD = "invoke_method";
	public static final String _req = "req";
		
}

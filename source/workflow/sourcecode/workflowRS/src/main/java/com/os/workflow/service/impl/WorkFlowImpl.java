package com.os.workflow.service.impl;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.MongoServerSelectionException;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.TaskClaimHandler;
import com.os.workflow.bpmnCore.TaskExecutorHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.WorkFlow;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;

public class WorkFlowImpl implements WorkFlow {
	private static final Logger log = Logger.getLogger(WorkFlowImpl.class);
	private static final ResourceBundle bundle = ResourceBundle.getBundle("workflow");
	private static DocumentBuilder builder = null;

	public static enum invokeTaskProcessorMethod {
		updatePathStatus, createTaskInstances
	}

	static {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

		} catch (ParserConfigurationException e) {
			log.error("[>>]Error[>>]" + e.toString());
			e.printStackTrace();
		}
	}

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	UpdateStatusHandler updateStatusHandler;

	@Autowired
	TaskClaimHandler taskClaimHandler;

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	TaskExecutorHandler taskExecutorHandler;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#addProcessDfn(java.lang.String)
	 */
	public String addProcessDfn(String fileName, String body) {
		ObjectMapper objectMapper = new ObjectMapper();
		Document doc;
		try {

			// String source = "This is the source of my input stream";
			// In
			//
			// log.debug("[WORKFLOW_MODELER_REPOSITORY]"
			// + System.getenv("BMPN_HOST"));
			// log.debug("[fileName]" + fileName);
			// File file = new File(System.getenv("BMPN_REPO") + fileName);
			doc = builder.parse(new StringBufferInputStream(body));
			NodeList jsonNodes = doc.getElementsByTagName(Constants.jsonRepresentationTag);
			NodeList svgNodes = doc.getElementsByTagName(Constants.svgRepresentationTag);
			Element jsonElement = (Element) jsonNodes.item(0);
			Element svgElement = (Element) svgNodes.item(0);

			DBObject newProcessDfn = (DBObject) JSON.parse(jsonElement.getTextContent());

			DBObject mapReducedProcessDfn = new BasicDBObject(
					Configuration.mapReduce(objectMapper.readValue(newProcessDfn.toString(), HashMap.class)));
			log.debug("[mapReducedProcessDfn]" + mapReducedProcessDfn);
			mapReducedProcessDfn.put(Constants.processDfnName, fileName.replaceAll(Constants.signavioXMLFileExtn, ""));
			mapReducedProcessDfn.put(Constants.bpmnFileName, fileName);
			mapReducedProcessDfn.put(Constants.status, Constants._Activated);
			mapReducedProcessDfn.put(Constants._bpmnDiagramPath, Constants.xmlHeader + svgElement.getTextContent());
			DBObject query = new BasicDBObject();
			query.put(Constants.bpmnFileName, fileName);
			query.put(Constants.status, Constants._Activated);
			DBObject processDfn = mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).findOne(query);
			String startNoneEventRsId = "";
			if (processDfn != null) {
				processDfn.put(Constants.status, Constants._Deactivated);
				log.debug("Updating existing work flow status...");
				log.debug("[query]" + query);
				log.debug("[processDfn]" + processDfn);
				mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).findAndModify(query, processDfn);
			}

			//
			List<Map> fields = (List<Map>) Configuration.getConfiguration().get(Constants.digramFields);
			Map<String, String> diagraminputMap = new HashMap<String, String>();
			Map<String, String> taskinputMap = new HashMap<String, String>();
			for (Map field : fields) {
				log.debug("[field]" + field);
				if (Boolean.valueOf(field.get(Constants.processDfnConfig).toString()))
					diagraminputMap.put(field.get(Constants.fieldName).toString(), (String) PropertyUtils
							.getProperty(mapReducedProcessDfn, (String) field.get(Constants.xmlJsonPath)));
				if (Boolean.valueOf(field.get(Constants.taskDfnConfig).toString()))
					taskinputMap.put(field.get(Constants.fieldName).toString(), (String) PropertyUtils
							.getProperty((Object) mapReducedProcessDfn, (String) field.get(Constants.xmlJsonPath)));
			}
			// Add for process definition
			for (Map.Entry<String, String> entry : diagraminputMap.entrySet()) {
				mapReducedProcessDfn.put(entry.getKey(), entry.getValue());
			}

			//
			mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).insert(mapReducedProcessDfn);
			DBObject dbObjectRes = mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).findOne(query);
			log.debug("[dbObjectRes]" + dbObjectRes);

			List<String> startEventLs = new ArrayList<String>();

			Set set = addTaskDefinintion(dbObjectRes.get(Constants._id), dbObjectRes, taskinputMap).entrySet();
			for (Object object : set) {
				Map.Entry entry = (Map.Entry) object;
				String key = (String) entry.getKey();
				Object value = (Object) entry.getValue();
				DBObject taskDfn = new BasicDBObject((Map) value);
				mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).insert(taskDfn);
				String stencilId = (String) PropertyUtils.getProperty((Map) value, Constants._taskStencilPath);
				if (stencilId.equalsIgnoreCase(Constants._StartNoneEvent)
						|| stencilId.equalsIgnoreCase(Constants._StartMessageEvent)
						|| stencilId.equalsIgnoreCase(Constants._StartTimerEvent))
					startEventLs.add(stencilId);

			}
			dbObjectRes.put(Constants._StartEventLs, startEventLs);
			mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).findAndModify(
					new BasicDBObject(Constants._id, (ObjectId) dbObjectRes.get(Constants._id)), dbObjectRes);
			return MessageUtility.response(Constants.responseCode.addProcessDfnSuccess);
		} catch (SAXException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility.response(Constants.responseCode.addProcessDfnFailed);
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. addTaskDefinintion
	 * To add Task definition & thier property
	 * </pre>
	 * 
	 * @param id
	 * @param dfn
	 * @param taskinputMap
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private Map addTaskDefinintion(Object id, DBObject dfn, Map<String, String> taskinputMap)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		BasicDBList taskDefinitons = (BasicDBList) PropertyUtils.getProperty(dfn, Constants.childShapes);
		Map taskMap = new HashMap();
		for (Object dbObj : taskDefinitons) {
			log.debug("[dbObj]" + dbObj);
			log.debug("[_id]" + id);
			((DBObject) dbObj).put(Constants.processRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, (ObjectId) id));
			for (Map.Entry<String, String> entry : taskinputMap.entrySet()) {
				((DBObject) dbObj).put(entry.getKey(), entry.getValue());
			}
			String stencilId = (String) PropertyUtils.getProperty(((DBObject) dbObj), Constants._taskStencilPath);
			if (!stencilId.equalsIgnoreCase(Constants._Lane) && !stencilId.equalsIgnoreCase(Constants._Pool))
				taskMap.put(((DBObject) dbObj).toMap().get(Constants.resourceId), ((DBObject) dbObj).toMap());

			BasicDBList childtaskDefinitons = (BasicDBList) PropertyUtils.getProperty(dfn, Constants.childShapes);
			for (Object object : childtaskDefinitons) {
				taskMap.putAll(addTaskDefinintion(id, (DBObject) object, taskinputMap));
			}

		}
		log.debug("[taskMap]" + taskMap);
		return taskMap;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getProcessList
	 * To get Process list
	 * </pre>
	 * 
	 * @param query
	 * @param startEvent
	 * @return
	 */
	private String getProcessList(String query, String startEvent) {

		DBObject queryDBObj = (DBObject) JSON.parse(query);
		queryDBObj.put(Constants.status, Constants._Activated);
		queryDBObj.put(Constants._StartEventLs, startEvent);

		Map result = new LinkedHashMap();
		log.debug("[>>]queryDBObj[>>]" + queryDBObj);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			DBCursor processDfnDbCursor = mongoTemplate.getCollection(Constants.workFlowProcessDefinitions)
					.find(queryDBObj,
							new BasicDBObject(
									((Map) Configuration.getConfiguration().get(Constants._getProcessListQueryFields))))
					.hint(new BasicDBObject(
							(Map) Configuration.getIndexKeys().get(Constants.workFlowProcessDefinitions)));
			log.debug("[" + processDfnDbCursor.toString() + "]");
			List rsLs = new ArrayList();
			for (DBObject processDfn : processDfnDbCursor) {
				Map res = new HashMap();
				log.debug("[processDfn]" + processDfn);
				res.put(Constants.processDfnId, PropertyUtils.getProperty(
						objectMapper.readValue(processDfn.toString(), LinkedHashMap.class), Constants._idPath));
				res.put(Constants.processDfnName, processDfn.get(Constants.processDfnName));
				rsLs.add(res);
			}
			result.put(Constants.processLs, rsLs);
			return MessageUtility.response(Constants.responseCode.getProcessListSuccess, result);
		} catch (MongoServerSelectionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(Constants.responseCode.mongoDBConnectionError);
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonGenerationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility.response(Constants.responseCode.getProcessListFailed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getProcessList(java.lang.String)
	 */
	public String getProcessList(String query) {

		return getProcessList(query, Constants._StartNoneEvent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#createTaskInstances(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, com.mongodb.DBObject)
	 */
	public void createTaskInstances(String processDfnId, String processInstanceId, String processDfnName, String tenant,
			String nameProcessInstance, DBObject req) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
	
			DBObject tasksDfnQry = new BasicDBObject(Constants.processRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(processDfnId)));
			log.debug("[tasksDfnQry]" + tasksDfnQry);
			DBCursor taskDfns = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(tasksDfnQry)
					.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskDefinitions)));
			List<String> initialStartNoneEventList = new ArrayList<String>();
			List<String> initialStartTimerEventList = new ArrayList<String>();
			for (DBObject taskDfn : taskDfns) {
				log.debug("[taskDfn]" + taskDfn);
				DBObject taskInstance = new BasicDBObject();
				taskInstance.put(Constants.processInstanceRef, new DBRef(mongoTemplate.getDb(),
						Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));
				taskInstance.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
						(ObjectId) taskDfn.get(Constants._id)));
				taskInstance.put(Constants.status, Constants._Waiting_For_Execute);
				taskInstance.put(Constants.taskName, PropertyUtils.getProperty(taskDfn, Constants._taskNamePath));
				taskInstance.put(Constants.processDfnName, processDfnName);
				taskInstance.put(Constants.tenant, tenant);
				taskInstance.put(Constants.nameProcessInstance, nameProcessInstance);
				List<Map> fields = (List<Map>) Configuration.getConfiguration().get(Constants.filterFields);
				for (Map map : fields) {
					if (PropertyUtils.getProperty(taskDfn, map.get(Constants.dbPath).toString()) != null)
						taskInstance.put(map.get(Constants.fieldName).toString(),
								PropertyUtils.getProperty(taskDfn, map.get(Constants.dbPath).toString()));
				}

				String stencilId = (String) PropertyUtils.getProperty(taskDfn, Constants._taskStencilPath);
				if (stencilId.equalsIgnoreCase(Constants._StartNoneEvent)
						|| stencilId.equalsIgnoreCase(Constants._StartMessageEvent))
					initialStartNoneEventList.add((String) taskDfn.get(Constants.resourceId));
				else if (stencilId.equalsIgnoreCase(Constants._StartTimerEvent))
					initialStartTimerEventList.add((String) taskDfn.get(Constants.resourceId));
				mongoTemplate.getCollection(Constants.workFlowTaskInstances).insert((DBObject) (taskInstance));
			}

			for (String resourceId : initialStartNoneEventList) {

				// updateStatusHandler.updatePathStatus((String)
				// req.get(Constants.processDfnId), resourceId,
				// res.get(Constants.processInstanceId).toString(),
				// Constants._Completed, null, true);
				DBObject dbObject = new BasicDBObject();
				dbObject.put(Constants.INVOKE_METHOD, invokeTaskProcessorMethod.updatePathStatus.toString());
				dbObject.put(Constants.processDfnId, processDfnId);
				dbObject.put(Constants.resourceId, resourceId);
				dbObject.put(Constants.processInstanceId, processInstanceId);
				dbObject.put(Constants.status, Constants._Completed);
				dbObject.put(Constants.outGoingData, null);
				dbObject.put(Constants.isUpdatingFirstStep, true);
				mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).insert(dbObject);
				taskExecutorHandler.taskExecute(dbObject);

				// updateStatusHandler.updatePathStatus((String)
				// req.get(Constants.processDfnId), resourceId,
				// processInstanceId, Constants._Completed, null, true);
			}

			for (String resourceId : initialStartTimerEventList) {

				// updateStatusHandler.updatePathStatus((String)
				// req.get(Constants.processDfnId), resourceId,
				// res.get(Constants.processInstanceId).toString(),
				// Constants._IN_Execution, null, true);
				DBObject dbObject = new BasicDBObject();
				dbObject.put(Constants.INVOKE_METHOD, invokeTaskProcessorMethod.updatePathStatus.toString());
				dbObject.put(Constants.processDfnId, processDfnId);
				dbObject.put(Constants.resourceId, resourceId);
				dbObject.put(Constants.processInstanceId, processInstanceId);
				dbObject.put(Constants.status, Constants._IN_Execution);
				dbObject.put(Constants.outGoingData, null);
				dbObject.put(Constants.isUpdatingFirstStep, true);

				mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).insert(dbObject);
				taskExecutorHandler.taskExecute(dbObject);

				// updateStatusHandler.updatePathStatus((String)
				// req.get(Constants.processDfnId), resourceId,
				// processInstanceId, Constants._IN_Execution, null, true);
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#createProcessInstance(java.lang
	 * .String)
	 */
	public String createProcessInstance(String request) {
		ObjectMapper objectMapper = new ObjectMapper();
		final Map res = new HashMap();
		try {

			DBObject req = (DBObject) JSON.parse(request);
			DBObject processDfnQry = new BasicDBObject();
			processDfnQry.put(Constants._id, new ObjectId((String) req.get(Constants.processDfnId)));

			String nameProcessInstance = (String) req.get(Constants.nameProcessInstance);
			DBObject processDfn = (DBObject) mongoTemplate.getCollection(Constants.workFlowProcessDefinitions)
					.findOne(processDfnQry, new BasicDBObject(((Map) Configuration.getConfiguration()
							.get(Constants._getCreateProcessInstanceQueryFields))));
			String tenant = ((DBObject) processDfn.get(Constants.properties)).get(Constants.tenant).toString();
			log.debug("[processDfn]" + processDfn);
			String processDfnName = (String) PropertyUtils.getProperty(processDfn, Constants.processDfnName);
			DBObject processInstance = new BasicDBObject();
			processInstance.put(Constants.processRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowProcessDefinitions, (ObjectId) processDfn.get(Constants._id)));
			processInstance.put(Constants.status, Constants._IN_Execution);
			processInstance.put(Constants.payload, req.get(Constants.payload));
			processInstance.put(Constants.tenant, tenant);
			processInstance.put(Constants.nameProcessInstance, nameProcessInstance);
			processInstance.put(Constants.processDfnName, processDfnName);
			mongoTemplate.getCollection(Constants.workFlowProcessInstances).insert((DBObject) (processInstance));
			log.debug("[processInstance]" + processInstance);

			String processInstanceId = (String) PropertyUtils.getProperty(
					objectMapper.readValue(processInstance.toString(), LinkedHashMap.class), Constants._idPath);
			res.put(Constants.processInstanceId, processInstanceId);
			invokeCreateTaskInstances((String) req.get(Constants.processDfnId), processInstanceId, processDfnName,
					tenant, nameProcessInstance, req);

			return MessageUtility.response(Constants.responseCode.createProcessInstanceSuccess, res);
		} catch (MongoServerSelectionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(Constants.responseCode.mongoDBConnectionError);
		} catch (JsonParseException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(Constants.responseCode.createProcessInstanceFailed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.service.WorkFlow#getTasks(java.lang.String)
	 */
	public String getTasks(String query) {
		DBObject taskInstanceQuery = (DBObject) JSON.parse(query);
		Map result = new LinkedHashMap();
		log.debug("[taskInstanceQuery]" + taskInstanceQuery);
		ObjectMapper objectMapper = new ObjectMapper();
		int pageIndex = Integer.valueOf((String) taskInstanceQuery.get(Constants._pageIndex));
		taskInstanceQuery.removeField(Constants._pageIndex);
		try {

			DBCursor taskInstancesdbCursor = mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.find(taskInstanceQuery,
							new BasicDBObject(
									((Map) Configuration.getConfiguration().get(Constants._getTasksQueryFields))))
					.sort(new BasicDBObject(Constants._id, -1)).skip((pageIndex - 1) * Constants._numOfRecords)
					.limit(Constants._numOfRecords)
					.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)));
			log.debug("[taskInstancesdbCursor]" + taskInstancesdbCursor.toString());
			List responseLs = new ArrayList();
			for (DBObject taskInstance : taskInstancesdbCursor) {
				Map res = new HashMap();
				log.debug("[taskInstance]" + taskInstance);
				String taskId = PropertyUtils
						.getProperty(objectMapper.readValue(taskInstance.toString(), LinkedHashMap.class),
								Constants._idPath)
						.toString();
				String processInstanceId = PropertyUtils
						.getProperty(objectMapper.readValue(taskInstance.toString(), LinkedHashMap.class),
								Constants._processInstanceRefId)
						.toString();
				res.put(Constants.taskInstanceId, taskId);
				res.put(Constants.processInstanceId, processInstanceId);
				Set set = ((Map) Configuration.getConfiguration().get(Constants._getTasks)).entrySet();
				for (Object object : set) {
					Map.Entry entry = (Map.Entry) object;
					if (taskInstance.get(entry.getValue().toString()) != null)
						res.put(entry.getKey(), taskInstance.get(entry.getValue().toString()));
				}
				res.put(Constants.nameProcessInstance, taskInstance.get(Constants.nameProcessInstance));
				responseLs.add(res);
			}
			result.put(Constants.taskLs, responseLs);
			return MessageUtility.response(Constants.responseCode.getTasksSuccess, result);
		} catch (MongoServerSelectionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(Constants.responseCode.mongoDBConnectionError);
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonGenerationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(Constants.responseCode.getTasksFailed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getTasksCount(java.lang.String)
	 */
	public String getTasksCount(String query) {
		DBObject queryDBObj = (DBObject) JSON.parse(query);
		Map result = new LinkedHashMap();
		log.debug("[queryDBObj]" + queryDBObj);
		ObjectMapper objectMapper = new ObjectMapper();
		result.put(Constants.count, mongoTemplate.getCollection(Constants.workFlowTaskInstances).count(queryDBObj));
		return MessageUtility.response(Constants.responseCode.getTaskCountSuccess, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getTaskInfo(java.lang.String)
	 */
	public String getTaskInfo(String query) {
		log.info("[>>]begin...");
		DBObject queryDBObj = (DBObject) JSON.parse(query);
		Map result = new LinkedHashMap();
		DBObject taskInstQry = new BasicDBObject();
		taskInstQry.put(Constants._id, new ObjectId((String) queryDBObj.get(Constants.taskInstanceId)));
		log.debug("[taskInstQry]" + taskInstQry);
		ObjectMapper objectMapper = new ObjectMapper();
		DBObject taskInstance = mongoTemplate.getCollection(Constants.workFlowTaskInstances).findOne(taskInstQry);
		log.debug("[taskInstance]" + taskInstance);
		DBObject taskInfoQry = new BasicDBObject();
		log.debug("[taskInstance]" + ((DBRef) taskInstance.get(Constants.taskRef)).getId());
		taskInfoQry.put(Constants._id, (ObjectId) ((DBRef) taskInstance.get(Constants.taskRef)).getId());
		DBObject taskInfo = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).findOne(taskInfoQry);
		log.debug("[taskInstance]" + taskInfo.get(Constants.properties));
		result.put(Constants.taskDfnproperties, taskInfo.get(Constants.properties));
		result.put(Constants.taskInstancePayload, taskInstance.get(Constants.payload));
		return MessageUtility.response(Constants.responseCode.getTaskInfoSuccess, result);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.service.WorkFlow#updateTaskStatus(java.lang.
	 * String )
	 */
	public String updateTaskStatus(String query) {
		return updateStatusHandler.updateTaskStatus(query);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getTasksInfoByProcessId(java
	 * .lang.String)
	 */

	public String getTasksInfoByProcessId(String query) {

		DBObject processInstanceQry = (DBObject) JSON.parse(query);
		Map result = new LinkedHashMap();
		log.debug("[processInstanceQry]" + processInstanceQry);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			DBObject processInstance = mongoTemplate.getCollection(Constants.workFlowProcessInstances)
					.findOne(new BasicDBObject(Constants._id,
							new ObjectId(processInstanceQry.get(Constants.processInstanceId).toString())));
			result.put(Constants.processInstancePayload, (DBObject) processInstance.get(Constants.payload));
			result.put(Constants.processInstanceCompletedDateTime,
					processInstance.get(Constants.processInstanceCompletedDateTime));
			DBObject taskInstancesQry = new BasicDBObject();
			taskInstancesQry.put(Constants.payload, new BasicDBObject(Constants._$exists, true));
			taskInstancesQry.put(Constants.processInstanceRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances,
							new ObjectId(processInstanceQry.get(Constants.processInstanceId).toString())));
			taskInstancesQry.put(Constants.status, Constants._Completed);
			DBCursor dbCursor = mongoTemplate.getCollection(Constants.workFlowTaskInstances).find(taskInstancesQry)
					.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)))
					.sort(new BasicDBObject(Constants._id, -1));
			log.debug("[dbCursor]" + dbCursor.toString());
			List rsLs = new ArrayList();
			for (DBObject dbObject : dbCursor) {
				Map res = new HashMap();
				log.debug("[>>]" + dbObject);

				String taskId = PropertyUtils.getProperty(
						objectMapper.readValue(dbObject.toString(), LinkedHashMap.class), Constants._idPath).toString();
				String workFlowId = PropertyUtils
						.getProperty(objectMapper.readValue(dbObject.toString(), LinkedHashMap.class),
								Constants._processInstanceRefId)
						.toString();
				res.put(Constants.taskInstanceId, taskId);
				res.put(Constants.processInstanceId, workFlowId);

				res.put(Constants.taskCompletedDateTime, dbObject.get(Constants.taskCompletedDateTime));
				res.put(Constants.taskInitiatedDateTime, dbObject.get(Constants.taskInitiatedDateTime));
				res.put(Constants.processDfnName, dbObject.get(Constants.processDfnName));
				res.put(Constants.taskName, dbObject.get(Constants.taskName));
				res.put(Constants._userCode, dbObject.get(Constants._userCode));
				res.put(Constants._userName, dbObject.get(Constants._userName));
				rsLs.add(res);
			}
			result.put(Constants.taskLs, rsLs);
			return MessageUtility.response(Constants.responseCode.getTasksInfoByProcessIdSuccess, result);
		} catch (MongoServerSelectionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(Constants.responseCode.mongoDBConnectionError);
		} catch (IllegalAccessException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (JsonGenerationException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility.response(Constants.responseCode.getTasksInfoByProcessIdFailed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getSubProcessByProcessInstanceId
	 * (java.lang.String)
	 */
	public String getSubProcessByProcessInstanceId(String query) {
		DBObject queryInput = (DBObject) JSON.parse(query);
		DBObject querySubProcess = new BasicDBObject();
		querySubProcess.put(Constants.processInstanceRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances,
						new ObjectId(queryInput.get(Constants.processInstanceId).toString())));
		querySubProcess.put(Constants.subProcessDtls, new BasicDBObject(Constants._$exists, true));
		DBCursor tasks = mongoTemplate.getCollection(Constants.workFlowTaskInstances).find(querySubProcess)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)));
		Map response = new HashMap();
		List subProcessLs = new ArrayList();
		for (DBObject task : tasks) {
			DBObject res = new BasicDBObject();
			DBObject subProcessDtls = (DBObject) task.get(Constants.subProcessDtls);
			res.put(Constants.subProcessInstanceId, subProcessDtls.get(Constants.subProcessInstanceId).toString());
			res.put(Constants.subProcessInstanceName, subProcessDtls.get(Constants.subProcessInstanceName).toString());
			subProcessLs.add(res);
		}
		response.put(Constants.subprocessLs, subProcessLs);
		return MessageUtility.response(Constants.responseCode.getSubProcessByProcessInstanceIdSuccess, response);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#acquireClaim(java.lang.String)
	 */
	public String acquireClaim(String input) {
		return taskClaimHandler.acquireClaim(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getClaimHistoryByTaskInstanceId
	 * (java.lang.String)
	 */
	public String getClaimHistoryByTaskInstanceId(String input) {
		return taskClaimHandler.getClaimHistoryByTaskInstanceId(input);
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getProcessDfnId
	 * To get Process defition id
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	private String getProcessDfnId(String input, String startEvent) {

		DBObject queryDBObj = (DBObject) JSON.parse(input);
		queryDBObj.put(Constants.status, Constants._Activated);
		queryDBObj.put(Constants._StartEventLs, startEvent);

		log.debug("[>>]queryDBObj[>>]" + queryDBObj);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			DBObject processDfn = mongoTemplate.getCollection(Constants.workFlowProcessDefinitions).findOne(queryDBObj,
					new BasicDBObject(
							((Map) Configuration.getConfiguration().get(Constants._getProcessListQueryFields))));

			Map res = new HashMap();
			log.debug("[processDfn]" + processDfn);
			res.put(Constants.processDfnId, PropertyUtils.getProperty(
					objectMapper.readValue(processDfn.toString(), LinkedHashMap.class), Constants._idPath));
			res.put(Constants.processDfnName, processDfn.get(Constants.processDfnName));

			return MessageUtility.response(Constants.responseCode.getProcessDfnIdSuccess, res);
		} catch (MongoServerSelectionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return MessageUtility.response(Constants.responseCode.mongoDBConnectionError);
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonGenerationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility.response(Constants.responseCode.getProcessDfnIdFailed);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getProcessInstanceId(java.lang
	 * .String)
	 */
	public String getProcessDfnId(String input) {
		return getProcessDfnId(input, Constants._StartNoneEvent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#createCollectionIndex(java.lang
	 * .String)
	 */
	public String createCollectionIndex(String input) {
		ObjectMapper objectMapper = new ObjectMapper();
		log.debug("[indexKyes]" + Configuration.getIndexKeys());
		DBObject dbObject = new BasicDBObject(Configuration.getIndexKeys());
		Map collectionMap = dbObject.toMap();
		log.debug("[collectionMap]" + collectionMap);
		Set set = collectionMap.entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String key = (String) entry.getKey();
			Map value = (Map) entry.getValue();
			log.debug("[key]" + key);
			log.debug("[value]" + value);
			mongoTemplate.getCollection(key).createIndex(new BasicDBObject(value));
			mongoTemplate.getCollection(key);
		}
		return MessageUtility.response(Constants.responseCode.Success);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#realeaseClaim(java.lang.String)
	 */
	public String realeaseClaim(String input) {
		return taskClaimHandler.realeaseClaim(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#getProcessInstancesCountByStatus
	 * (java.lang.String)
	 */
	public String getProcessInstancesCountByStatus(String query) {

		DBObject dbMatch = (DBObject) JSON.parse(query);
		List<DBObject> pipeline = new ArrayList<DBObject>();
		pipeline.add(new BasicDBObject(Constants._$match, dbMatch));
		DBObject groupByDBObj = new BasicDBObject();
		groupByDBObj.put(Constants._id, Constants._$ + Constants.processDfnName);
		groupByDBObj.put(Constants.count, new BasicDBObject(Constants._$sum, 1));

		pipeline.add(new BasicDBObject(Constants._$group, groupByDBObj));
		log.debug("[pipeline]" + pipeline);
		Iterable<DBObject> itr = mongoTemplate.getCollection(Constants.workFlowProcessInstances).aggregate(pipeline)
				.results();
		int totalCount = 0;
		for (DBObject dbObject : itr) {
			totalCount = totalCount + (Integer) dbObject.get(Constants.count);
		}
		Map output = new HashMap();
		output.put(Constants.processInstanceLs, itr);
		output.put(Constants.totalCount, totalCount);
		return MessageUtility.response(Constants.responseCode.Success, output);

	}

	public String reassignTask(String input) {
		return taskClaimHandler.reassignTask(input);
	}

	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.WorkFlow#invokeCreateTaskInstances(java.
	 * lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, com.mongodb.DBObject)
	 */
	public synchronized void invokeCreateTaskInstances(String processDfnId, String processInstanceId,
			String processDfnName, String tenant, String nameProcessInstance, DBObject request) {
		DBObject createTaskInstances = new BasicDBObject();
		createTaskInstances.put(Constants.INVOKE_METHOD, invokeTaskProcessorMethod.createTaskInstances.toString());
		createTaskInstances.put(Constants.processDfnId, processDfnId);
		createTaskInstances.put(Constants.processInstanceId, processInstanceId);
		createTaskInstances.put(Constants.processDfnName, processDfnName);
		createTaskInstances.put(Constants.tenant, tenant);
		createTaskInstances.put(Constants.nameProcessInstance, nameProcessInstance);
		createTaskInstances.put(Constants._req, request);
		// QueueHandler.enqueue(createTaskInstances);
		mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).insert(createTaskInstances);
		taskExecutorHandler.taskExecute(createTaskInstances);

	}

}

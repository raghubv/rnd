package com.os.workflow.bpmnCore;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.mongodb.DBObject;

/**
 * 
 * <b>Purpose:</b><br>
 * ServiceTaskHandler <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface ServiceTaskHandler {

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. executeServiceTask
	 * To handle service task
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @param serviceTaskImpl
	 * @param taskDfn
	 * @param taskInstance
	 * @param outGoingData
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws ApplicationException
	 * @throws IOException
	 */
	public void executeServiceTask(String processInstanceId, String serviceTaskImpl, DBObject taskDfn,
			DBObject taskInstance, Object outGoingData) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, JsonGenerationException, JsonMappingException, IOException;

}

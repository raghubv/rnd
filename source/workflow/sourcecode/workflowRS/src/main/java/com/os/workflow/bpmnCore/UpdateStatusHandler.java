package com.os.workflow.bpmnCore;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

/**
 * 
 * <b>Purpose:</b><br>
 * UpdateStatusHandler <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface UpdateStatusHandler {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. updateTaskStatus
	 * To Update task status
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String updateTaskStatus(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. updatePathStatus
	 * To update path element based on bmpn core element condition, gateway etc..
	 * 
	 * </pre>
	 * 
	 * @param proccessDfnId
	 * @param resourceId
	 * @param processInstanceId
	 * @param status
	 * @param outGoingData
	 * @param isUpdatingFirstStep
	 */
	public void updatePathStatus(String proccessDfnId, String resourceId, String processInstanceId, String status,
			Object outGoingData, boolean isUpdatingFirstStep) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, ParseException, JsonParseException, JsonMappingException, IOException;

	/**
	 * 
	 * @param proccessDfnId
	 * @param resourceId
	 * @param processInstanceId
	 * @param status
	 * @param outGoingData
	 * @param isUpdatingFirstStep
	 */
	public void invokeUpdatePathStatus(String proccessDfnId, String resourceId, String processInstanceId, String status,
			Object outGoingData, boolean isUpdatingFirstStep);

}

package com.os.workflow.service;

import java.lang.reflect.InvocationTargetException;

import com.mongodb.DBObject;


public interface WorkFlow {
	/**
	 * 
	 * @param fileName
	 * @param body
	 * @return
	 */
	public String addProcessDfn(String fileName,String body);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getProcessList
	 * To get process definition list
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String getProcessList(String query);

	/**
	 * 
	  * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createTaskInstances
	 * To create task instances
	 * </pre>
	 * @param processDfnId
	 * @param processInstanceId
	 * @param processDfnName
	 * @param tenant
	 * @param nameProcessInstance
	 * @param request
	 */
	public void createTaskInstances(String processDfnId, String processInstanceId, String processDfnName,
			String tenant, String nameProcessInstance, DBObject request)throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;
	
	/**
	 * 
	  * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createTaskInstances
	 * To create task instances
	 * </pre>
	 * @param processDfnId
	 * @param processInstanceId
	 * @param processDfnName
	 * @param tenant
	 * @param nameProcessInstance
	 * @param request
	 */
	public void invokeCreateTaskInstances(String processDfnId, String processInstanceId, String processDfnName,
			String tenant, String nameProcessInstance, DBObject request);
	
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createProcessInstance
	 * To create process instance
	 * </pre>
	 * 
	 * @param request
	 * @return
	 */
	public String createProcessInstance(String request);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getTasks
	 * To get tasks by query input
	 * 
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String getTasks(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getTasksCount
	 * To get tasks cout by query input
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String getTasksCount(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getTasksInfoByProcessId
	 * To get tasks info by passing process instance  id
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */

	public String getTasksInfoByProcessId(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getTaskInfo
	 * To get task info by task instance id
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String getTaskInfo(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. updateTaskStatus
	 * Update task status along with payload against task instance id
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String updateTaskStatus(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getSubProcessByProcessInstanceId
	 * To get list of sub process by process instance id
	 * </pre>
	 * 
	 * @return
	 */
	public String getSubProcessByProcessInstanceId(String query);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. acquireClaim
	 * To acquire task claim
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	public String acquireClaim(String input);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getClaimHistoryByTaskInstanceId
	 * To get claim history  by task instance id
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	public String getClaimHistoryByTaskInstanceId(String input);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getProcessDfnId
	 * To get process definition id
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	public String getProcessDfnId(String input);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. createCollectionIndex
	 * To create collection index
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	public String createCollectionIndex(String input);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. realeaseClaim
	 * To realease task claim
	 * </pre>
	 * 
	 * @param input
	 * @return
	 */
	public String realeaseClaim(String input);

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getProcessInstancesCountByStatus
	 * 
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String getProcessInstancesCountByStatus(String query);
	
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. reassignTask
	 * 
	 * </pre>
	 * 
	 * @param query
	 * @return
	 */
	public String reassignTask(String query);
}

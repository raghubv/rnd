package com.os.workflow.helper;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader
{
  public static String getValueFor(String key)
  {
    try
    {
      if (properties == null)
      {
        loadProperties();
      }
      return ((String)properties.get(key));
    }
    catch (Exception e)
    {
      TLogger.error("Key not found in the properties file. Key :" + key, e);
    }
    return "";
  }

  public static String getErrorCodeFor(String key)
  {
    try
    {
      if (properties == null)
      {
        loadProperties();
      }
      return ((String)properties1.get(key));
    }
    catch (Exception e)
    {
      TLogger.error("Key not found in the properties file. Key :" + key, e);
    }
    return "";
  }

  public static String getErrorDescriptionFor(String key)
  {
    try
    {
      if (properties == null)
      {
        loadProperties();
      }
      return ((String)properties2.get(key));
    }
    catch (Exception e)
    {
      TLogger.error("Key not found in the properties file. Key :" + key, e);
    }
    return "";
  }

  public static void loadProperties()
  {
    try
    {
      InputStream inputStream = null;
      InputStream inputStream1 = null;
      InputStream inputStream2 = null;


      inputStream = new FileInputStream(readFile());

      properties = new Properties();
      properties1 = new Properties();
      properties2 = new Properties();
      properties.load(inputStream);
      inputStream1 = new FileInputStream(properties.get("error_code_mapping").toString());
      properties1.load(inputStream1);
      inputStream2 = new FileInputStream(properties.get("error_code_description").toString());
      properties2.load(inputStream2);
    }
    catch (Exception e)
    {
      TLogger.error("Unexpected error while loading the properties file", e);
    }
  }

  private static String readFile()
  {
    try
    {
      if (TLogger.debugIsEnabled())
      {
        TLogger.debug("Reading Properties file...");
      }
      path = System.getenv("APPSERVER_CONF_PATH");
    }
    catch (Exception e)
    {
      TLogger.error("Unexpected error while reading the configution path ", e);
    }
    return path;
  }

  static String path = "";
  static Properties properties = null;
  static Properties properties1 = null;
  static Properties properties2 = null;
}

package com.os.workflow.bpmnCore.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.ExclusiveGateWayHandler;
import com.os.workflow.bpmnCore.IntermediateTimerEventHandler;
import com.os.workflow.bpmnCore.ParallelGateWayHandler;
import com.os.workflow.bpmnCore.ScheduleJobHandler;
import com.os.workflow.bpmnCore.ServiceTaskHandler;
import com.os.workflow.bpmnCore.SubProcessHandler;
import com.os.workflow.bpmnCore.TaskExecutorHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.WorkFlow;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;
import com.os.workflow.util.Utility;

/**
 * 
 * <b>Purpose:</b><br>
 * UpdateStatusHandlerImpl <br>
 * <br>
 * 
  
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Feb 14, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class UpdateStatusHandlerImpl implements UpdateStatusHandler {
	static private enum Stencil {
		StartTimerEvent, StartMessageEvent, StartNoneEvent, EndNoneEvent, Task, Exclusive_Databased_Gateway, ParallelGateway, SequenceFlow, CollapsedSubprocess, Association_Unidirectional, Message, MessageFlow, IntermediateTimerEvent
	};

	public static enum invokeTaskProcessorMethod {
		updatePathStatus, createTaskInstances
	}

	private static final Logger log = Logger.getLogger(UpdateStatusHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ServiceTaskHandler serviceTaskHandler;

	@Autowired
	ScheduleJobHandler scheduleJobHandler;

	@Autowired
	SubProcessHandler subProcessHandler;

	@Autowired
	ExclusiveGateWayHandler exclusiveGateWayHandler;

	@Autowired
	ParallelGateWayHandler parallelGateWayHandler;

	@Autowired
	IntermediateTimerEventHandler intermediateTimerEventHandler;

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	WorkFlow workFlow;

	@Autowired
	TaskExecutorHandler taskExecutorHandler;

	private ObjectMapper objectMapper = new ObjectMapper();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.UpdateStatus#updateTaskStatus(java.lang
	 * .String)
	 */
	public String updateTaskStatus(String query) {
		try {

			DBObject request = (DBObject) JSON.parse(query);
			log.debug("[request]" + request);
			DBObject inQuery = new BasicDBObject();
			List statusLs = new ArrayList();
			statusLs.add(Constants._In_Error);
			statusLs.add(Constants._IN_Execution);
			inQuery.put(Constants._$in, statusLs);
			DBObject taskInstanceQry = new BasicDBObject(Constants._id,
					new ObjectId((String) request.get(Constants.taskInstanceId)));
			taskInstanceQry.put(Constants.status, inQuery);
			DBObject taskInstance = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.findOne(taskInstanceQry);
			if (taskInstance != null) {
				taskInstance.putAll(request);
				taskInstance.removeField(Constants.taskInstanceId);
				log.debug("[>>]workFlowDBObject[>>]" + taskInstance);
				mongoTemplate.getCollection(Constants.workFlowTaskInstances).update(
						new BasicDBObject(Constants._id, new ObjectId((String) request.get(Constants.taskInstanceId))),
						taskInstance);
				// Add to task payload to proce instance payload
				if (taskInstance.get(Constants.payload) != null) {
					DBObject procesInstanceQry = new BasicDBObject();
					procesInstanceQry.put(Constants._id,
							(ObjectId) ((DBRef) taskInstance.get(Constants.processInstanceRef)).getId());
					DBObject processInstance = mongoTemplate.getCollection(Constants.workFlowProcessInstances)
							.findOne(procesInstanceQry);
					List<DBObject> taskInstancePayloadLs = null;
					if (processInstance.get(Constants.taskInstancePayloadLs) != null)
						taskInstancePayloadLs = (List<DBObject>) processInstance.get(Constants.taskInstancePayloadLs);
					else
						taskInstancePayloadLs = new ArrayList<DBObject>();
					taskInstancePayloadLs.add((DBObject) taskInstance.get(Constants.payload));
					processInstance.put(Constants.taskInstancePayloadLs, taskInstancePayloadLs);
					mongoTemplate.getCollection(Constants.workFlowProcessInstances).findAndModify(procesInstanceQry,
							processInstance);
				}
				// End
				DBObject taskDfnQry = new BasicDBObject();
				taskDfnQry.put(Constants._id, (ObjectId) ((DBRef) taskInstance.get(Constants.taskRef)).getId());
				DBObject taskDfn = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).findOne(taskDfnQry);
				log.debug("[taskDfn]" + taskDfn);
				log.debug("[" + Constants.processRef + "]"
						+ (((DBRef) taskInstance.get(Constants.processInstanceRef))).toString());
				log.debug("[" + Constants.processInstanceRef + "]"
						+ (((DBRef) taskInstance.get(Constants.processInstanceRef))).toString());
				ObjectMapper objectMapper = new ObjectMapper();
				String processId = PropertyUtils.getProperty(
						objectMapper.readValue(((DBRef) taskDfn.get(Constants.processRef)).toString(), HashMap.class),
						Constants._$id).toString();
				String processInstanceId = PropertyUtils.getProperty(
						objectMapper.readValue((((DBRef) taskInstance.get(Constants.processInstanceRef))).toString(),
								HashMap.class),
						Constants._$id).toString();
				log.debug("[processId]" + processId);
				log.debug("[processInstanceId]" + processInstanceId);

				String outGoingData = (String) PropertyUtils.getProperty(taskDfn, Constants._outGoingDatatPath);
				Map outGoiningPayloadData = new HashMap();
				if (outGoingData != null && !outGoingData.isEmpty()) {
					log.debug("[outGoingData]" + outGoingData);
					String[] outGoingDataField = outGoingData.split(",");
					DBObject taskinstancePayload = (DBObject) taskInstance.get(Constants.payload);
					for (String field : outGoingDataField) {
						log.debug("[field]" + field);
						outGoiningPayloadData.put(field, taskinstancePayload.get(field));

					}
					log.debug("[outGoiningPayloadData]" + outGoiningPayloadData);
				}

				invokeUpdatePathStatus(processId, taskDfn.get(Constants.resourceId).toString(), processInstanceId,
						request.get(Constants.status).toString(), outGoiningPayloadData, true);

			} else {
				return MessageUtility.response(Constants.responseCode.updateTaskStatusFailed);

			}
			return MessageUtility.response(Constants.responseCode.updateTaskStatusSuccess);
		} catch (IllegalAccessException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (JsonParseException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility.response(Constants.responseCode.updateTaskStatusFailed);

	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. isAllTaskProcessed
	 * 
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @return
	 */
	private boolean isAllTaskProcessed(String processInstanceId) {

		DBObject taskInstanceQry = new BasicDBObject();
		taskInstanceQry.put(Constants.status, Constants._IN_Execution);
		taskInstanceQry.put(Constants.processInstanceRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));
		if (mongoTemplate.getCollection(Constants.workFlowTaskInstances).find(taskInstanceQry)
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)))
				.count() > 0)
			return false;
		else
			return true;
	}

	/*
	 */
	public void processTasks() {

		DBCursor dbCursor = mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).find();
		for (DBObject dbObject : dbCursor) {
			taskExecutorHandler.taskExecute(dbObject);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.UpdateStatus#updatePathStatus(java.lang
	 * .String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.Object, boolean)
	 */
	public void updatePathStatus(String proccessDfnId, String resourceId, String processInstanceId, String status,
			Object outGoingData, boolean isUpdatingFirstStep) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, ParseException, JsonParseException, JsonMappingException, IOException {

		DBObject taskDfnQry = new BasicDBObject();
		taskDfnQry.put(Constants.processRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessDefinitions, new ObjectId(proccessDfnId)));
		taskDfnQry.put(Constants.resourceId, resourceId);
		log.debug("[taskDfnQry]" + taskDfnQry);
		DBObject taskDfn = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskDefinitions)
				.findOne(taskDfnQry);
		log.debug("[taskDfn]" + taskDfn);
		DBObject taskInstanceQry = new BasicDBObject();
		taskInstanceQry.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
				(ObjectId) taskDfn.get(Constants._id)));
		taskInstanceQry.put(Constants.processInstanceRef,
				new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances, new ObjectId(processInstanceId)));
		log.debug("[taskInstanceQry]" + taskInstanceQry);
		DBObject taskInstance = (DBObject) mongoTemplate.getCollection(Constants.workFlowTaskInstances)
				.findOne(taskInstanceQry);
		log.debug("[taskInstance]" + taskInstance);
		String stencilId = PropertyUtils.getProperty(taskDfn, Constants._taskStencilPath).toString();
		boolean continuePathProcessing = false;
		boolean isTaskInstanceModified = false;
		boolean isCompletedProcessInstance = false;
		// boolean updateXORPath = false;
		boolean isXORGateWay = false;
		boolean isCollapsedSubProcess = false;
		boolean isServiceTask = false;
		boolean schedule = false;

		boolean checkAndScheduleBoundryEvent = false;
		boolean checkAndCancelBoundryEvent = false;

		String serviceTaskImpl = null;
		log.debug("[stencilId]" + Stencil.valueOf(stencilId));
		switch (Stencil.valueOf(stencilId)) {
		case StartMessageEvent:
			taskInstance.put(Constants.status, status);
			isTaskInstanceModified = true;
			continuePathProcessing = true;

			break;
		case StartTimerEvent:
			taskInstance.put(Constants.status, status);
			isTaskInstanceModified = true;
			schedule = true;
			if (status.equalsIgnoreCase(Constants._Completed)) {
				continuePathProcessing = true;
				schedule = false;
			}

			break;
		case StartNoneEvent:
			taskInstance.put(Constants.status, status);
			isTaskInstanceModified = true;
			continuePathProcessing = true;
			break;
		case EndNoneEvent:
			taskInstance.put(Constants.status, Constants._Completed);
			isTaskInstanceModified = true;
			isCompletedProcessInstance = isAllTaskProcessed(processInstanceId);
			continuePathProcessing = false;
			break;
		case Exclusive_Databased_Gateway:
			taskInstance.put(Constants.status, Constants._Completed);
			isTaskInstanceModified = true;
			isXORGateWay = true;
			break;
		case ParallelGateway:
			continuePathProcessing = parallelGateWayHandler.executeParallelGateWay(proccessDfnId, resourceId,
					processInstanceId, taskInstanceQry.toMap(), taskInstance.toMap());
			// if (continuePathProcessing) {
			// taskInstance.put(Constants.status, Constants._Completed);
			// isTaskInstanceModified = true;
			// }
			break;
		case SequenceFlow:
			taskInstance.put(Constants.status, Constants._Completed);
			continuePathProcessing = true;
			isTaskInstanceModified = true;
			break;
		case MessageFlow:
			taskInstance.put(Constants.status, status);
			continuePathProcessing = true;
			isTaskInstanceModified = true;
			break;
		case Task:
			if (isUpdatingFirstStep) {

				taskInstance.put(Constants.status, status);

				if (status.equalsIgnoreCase(Constants._Completed) || status.equalsIgnoreCase(Constants._In_FallOut)) {
					continuePathProcessing = true;
					checkAndCancelBoundryEvent = true;
				}
				taskInstance.put(Constants.taskCompletedDateTime, Utility.getDate());
			} else {
				checkAndScheduleBoundryEvent = true;
				taskInstance.put(Constants.status, Constants._IN_Execution);
				continuePathProcessing = false;
				if (taskInstance.get(Constants.taskType).toString().equalsIgnoreCase(Constants._Service)) {
					isServiceTask = true;
					serviceTaskImpl = (String) PropertyUtils.getProperty(taskDfn, Constants._ServiceImplementationPath);
				}
				taskInstance.put(Constants.taskInitiatedDateTime, Utility.getDate());
			}

			isTaskInstanceModified = true;
			break;
		case CollapsedSubprocess:
			if (isUpdatingFirstStep) {
				taskInstance.put(Constants.status, status);
				if (status.equalsIgnoreCase(Constants._Completed) || status.equalsIgnoreCase(Constants._In_FallOut))
					continuePathProcessing = true;
			} else {
				taskInstance.put(Constants.status, Constants._IN_Execution);
				isCollapsedSubProcess = true;

			}
			isTaskInstanceModified = true;
			break;
		case Association_Unidirectional:
			taskInstance.put(Constants.status, Constants._Completed);
			isTaskInstanceModified = true;
			continuePathProcessing = true;
			break;

		case Message:
			taskInstance.put(Constants.status, Constants._Completed);
			isTaskInstanceModified = true;
			continuePathProcessing = false;
			break;

		case IntermediateTimerEvent:

			if (isUpdatingFirstStep) {
				taskInstance.put(Constants.status, Constants._Completed);
				continuePathProcessing = true;
				isTaskInstanceModified = true;
			} else {
				// Already in execution
				if (!((String) taskInstance.get(Constants.status)).equalsIgnoreCase(Constants._IN_Execution)) {
					taskInstance.put(Constants.status, Constants._IN_Execution);
					isTaskInstanceModified = true;
					schedule = true;
				} else {

					if (!((String) PropertyUtils.getProperty(taskDfn,
							Constants._IntermediateTimerEvent_Properties_TimerType))
									.equalsIgnoreCase(Constants.delayTimeOut)) {
						isTaskInstanceModified = true;
						continuePathProcessing = true;
						taskInstance.put(Constants.status, Constants._Completed);
					}
				}

			}

			break;
		}
		log.debug("[isTaskInstanceModified]" + isTaskInstanceModified);
		log.debug("[continuePathProcessing]" + continuePathProcessing);
		// Create CollapsedSubProcess
		if (isCollapsedSubProcess)
			taskInstance = subProcessHandler.collapsedSubProcess(processInstanceId, taskDfn, taskInstance);

		// To Update Task Instance
		if (isTaskInstanceModified)
			mongoTemplate.getCollection(Constants.workFlowTaskInstances).findAndModify(taskInstanceQry, taskInstance);
		// XOR GateWay
		if (isXORGateWay) {
			List<Map> outgoingLs = (List<Map>) taskDfn.get(Constants._outgoing);
			exclusiveGateWayHandler.processXORGateOutGoingPath(proccessDfnId, outgoingLs, processInstanceId, status,
					false, outGoingData);
		}
		// Continue path processing
		if (continuePathProcessing) {
			List<Map> outgoingLs = (List<Map>) taskDfn.get(Constants._outgoing);
			for (Map map : outgoingLs) {
				invokeUpdatePathStatus(proccessDfnId, map.get(Constants.resourceId).toString(), processInstanceId,
						status, outGoingData, false);
			}
		}
		// Schedule Task
		if (schedule)
			scheduleJobHandler.schedule(stencilId, taskDfn, taskInstance);
		// Task-Service Type
		if (isServiceTask)
			serviceTaskHandler.executeServiceTask(processInstanceId, serviceTaskImpl, taskDfn, taskInstance,
					outGoingData);
		// Process instance is completed
		if (isCompletedProcessInstance)
			updateCompletedProcessInstance(processInstanceId);
		if (checkAndScheduleBoundryEvent)
			intermediateTimerEventHandler.checkAndScheduleBoundryEvent(stencilId, taskDfn, taskInstance);
		if (checkAndCancelBoundryEvent)
			intermediateTimerEventHandler.checkAndCancelBoundryEvent(stencilId, taskDfn, taskInstance);

	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. updateCompletedProcessInstance
	 * Update Process Instance status on completion
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IOException
	 */
	private void updateCompletedProcessInstance(String processInstanceId)
			throws JsonParseException, JsonMappingException, IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, IOException {
		DBObject processInstance = mongoTemplate.getCollection(Constants.workFlowProcessInstances)
				.findOne(new BasicDBObject(Constants._id, new ObjectId(processInstanceId)));
		processInstance.put(Constants.status, Constants._Completed);
		processInstance.put(Constants.processInstanceCompletedDateTime, Utility.getDate());
		mongoTemplate.getCollection(Constants.workFlowProcessInstances)
				.findAndModify(new BasicDBObject(Constants._id, new ObjectId(processInstanceId)), processInstance);
		// On completing Collapsed Subprocess call called process
		if (processInstance.get(Constants.taskInsatancesRef) != null) {
			DBObject taskInstanceUpdateDtls = new BasicDBObject();
			taskInstanceUpdateDtls.put(Constants.taskInstanceId,
					PropertyUtils.getProperty(objectMapper.readValue(processInstance.toString(), LinkedHashMap.class),
							Constants._taskInsatancesRefId));
			taskInstanceUpdateDtls.put(Constants.status, Constants._Completed);
			log.debug("[taskInstanceUpdateDtls]" + taskInstanceUpdateDtls);
			updateTaskStatus(taskInstanceUpdateDtls.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.bpmnCore.UpdateStatusHandler#
	 * invokeUpdatePathStatus(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.Object, boolean)
	 */
	public synchronized void invokeUpdatePathStatus(String proccessDfnId, String resourceId, String processInstanceId,
			String status, Object outGoingData, boolean isUpdatingFirstStep) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants.INVOKE_METHOD, invokeTaskProcessorMethod.updatePathStatus.toString());
		dbObject.put(Constants.processDfnId, proccessDfnId);
		dbObject.put(Constants.resourceId, resourceId);
		dbObject.put(Constants.processInstanceId, processInstanceId);
		dbObject.put(Constants.status, status);
		dbObject.put(Constants.outGoingData, outGoingData);
		dbObject.put(Constants.isUpdatingFirstStep, isUpdatingFirstStep);
		mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).insert(dbObject);
		taskExecutorHandler.taskExecute(dbObject);

	}

}

package com.os.workflow.bpmnCore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.TaskClaimHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;
import com.os.workflow.util.Utility;
/**
 * 
*<b>Purpose:</b><br> 
* TaskClaimHandlerImpl <br><br> 
*

* <b>RevisionHistory:</b> 
* <pre><b> 
* Sl No Modified Date Author</b> 
* ==============================================
* 1 May 5, 2014  @author Raghu -- Base Release 
* 
* </pre><br>
 */
public class TaskClaimHandlerImpl implements TaskClaimHandler {
	private static final Logger log = Logger.getLogger(TaskClaimHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.TaskClaim#acquireClaim(java.lang.String)
	 */
	public String acquireClaim(String input) {
		try {
			DBObject dbObject = (DBObject) JSON.parse(input);
			DBObject taskInstanceQry = new BasicDBObject();
			taskInstanceQry.put(
					Constants._id,
					new ObjectId((String) dbObject
							.get(Constants.taskInstanceId)));
			taskInstanceQry.put(Constants.status, Constants._IN_Execution);
			log.debug("[taskInstanceQry]" + taskInstanceQry);
			DBObject taskInstance = mongoTemplate.getCollection(
					Constants.workFlowTaskInstances).findOne(taskInstanceQry);
			log.debug("[taskInstance]" + taskInstance);
			if (taskInstance == null)
				return MessageUtility
						.response(Constants.responseCode.acquireClaimFailed);
			DBObject taskDfnQry = new BasicDBObject();
			taskDfnQry.put(Constants._id, (ObjectId) ((DBRef) taskInstance
					.get(Constants.taskRef)).getId());
			log.debug("[taskDfnQry]" + taskDfnQry);
			DBObject taskDfn = mongoTemplate.getCollection(
					Constants.workFlowTaskDefinitions).findOne(taskDfnQry);
			log.debug("[taskDfn]" + taskDfn);
			String claimTimeout = (String) PropertyUtils
					.getProperty(taskDfn, Constants._claimtimeoutPath);

			log.debug("[claimTimeout]" + claimTimeout);
			DBObject dbInput = new BasicDBObject();

			dbInput.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowTaskInstances, new ObjectId(
							(String) dbObject.get(Constants.taskInstanceId))));
			dbInput.put(Constants.status, Constants._claimAcquire);
			if (mongoTemplate.getCollection(Constants.claimTaskInstances)
					.findOne(dbInput) == null) {

				dbInput.put(Constants._userCode,
						(String) dbObject.get(Constants._userCode));
				dbInput.put(Constants._userName,
						(String) dbObject.get(Constants._userName));
				
				dbInput.put(Constants._claimAcquireTime, Utility.getDate());
				dbInput.put(Constants._claimExpiryTime, Utility.getScheduledDateAndTime(claimTimeout));
				mongoTemplate.getCollection(Constants.claimTaskInstances)
						.insert(dbInput);

				DBObject claimDtls = new BasicDBObject();
				claimDtls.put(Constants._userCode,
						(String) dbObject.get(Constants._userCode));
				claimDtls.put(Constants._userName,
						(String) dbObject.get(Constants._userName));
				taskInstance.put(Constants._claimDtls, claimDtls);
				log.debug("[taskInstancce]" + taskInstance);
				mongoTemplate.getCollection(Constants.workFlowTaskInstances)
						.findAndModify(taskInstanceQry, taskInstance);

			} else {

				return MessageUtility
						.response(Constants.responseCode.acquireClaimFailed);
			}

		} catch (NumberFormatException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}

		return MessageUtility
				.response(Constants.responseCode.acquireClaimSuccess);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.TaskClaim#realeaseClaim(java.lang.Object
	 * , java.lang.String)
	 */
	public String realeaseClaim(Object input, String releasedBy) {
		DBObject dbObject = (DBObject) input;
		dbObject.put(Constants.status, Constants._claimRelease);
		dbObject.put(Constants._claimReleasedBy, releasedBy);
		DBObject claimTaskInstanceQry = new BasicDBObject(Constants._id,
				dbObject.get(Constants._id));
		log.debug("[claimTaskInstanceQry]" + claimTaskInstanceQry);
		log.debug("[dbObject]" + dbObject);
		mongoTemplate.getCollection(Constants.claimTaskInstances)
				.findAndModify(claimTaskInstanceQry, dbObject);
		DBObject taskInstanceQry = new BasicDBObject();
		taskInstanceQry.put(Constants._id,
				(ObjectId) ((DBRef) dbObject.get(Constants.taskRef)).getId());
		DBObject taskInstancce = mongoTemplate.getCollection(
				Constants.workFlowTaskInstances).findOne(taskInstanceQry);
		log.debug("[taskInstancce]" + taskInstancce);
		if (taskInstancce.get(Constants._claimDtls) != null) {
			taskInstancce.removeField(Constants._claimDtls);
			mongoTemplate.getCollection(Constants.workFlowTaskInstances)
					.findAndModify(taskInstanceQry, taskInstancce);

		}
		log.debug("[taskInstancce]" + taskInstancce);

		return MessageUtility
				.response(Constants.responseCode.realeaseClaimSuccess);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.TaskClaim#getClaimHistoryByTaskInstanceId
	 * (java.lang.String)
	 */
	public String getClaimHistoryByTaskInstanceId(String input) {
		DBObject dbObject = (DBObject) JSON.parse(input);
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put(
				Constants.taskRef,
				new DBRef(mongoTemplate.getDb(),
						Constants.workFlowTaskInstances,
						new ObjectId((String) dbObject
								.get(Constants.taskInstanceId))));
		log.debug("[dbQuery]" + dbQuery);
		DBCursor dbCursor = mongoTemplate
				.getCollection(Constants.claimTaskInstances)
				.find(dbQuery,
						new BasicDBObject(
								((Map) Configuration
										.getConfiguration()
										.get(Constants._getClaimHistoryByTaskInstanceIdQueryFields))))
				.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(
						Constants.claimTaskInstances)));

		Map outPutMap = new HashMap();
		List claimHistoryLs = new ArrayList();
		for (DBObject claimTaskInstance : dbCursor) {
			claimHistoryLs.add(claimTaskInstance);
			log.debug("[claimTaskInstance]" + claimTaskInstance);
		}
		outPutMap.put(Constants.claimHistoryLs, claimHistoryLs);
		return MessageUtility.response(Constants.responseCode.getClaimHistoryByTaskInstanceIdSuccess,
				outPutMap);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.TaskClaim#realeaseClaim(java.lang.String
	 * )
	 */
	public String realeaseClaim(String input) {

		DBObject dbObjInput = (DBObject) JSON.parse(input);

		DBObject claimInstanceQry = new BasicDBObject();
		claimInstanceQry.put(
				Constants.taskRef,
				new DBRef(mongoTemplate.getDb(),
						Constants.workFlowTaskInstances, new ObjectId(
								(String) dbObjInput
										.get(Constants.taskInstanceId))));
		claimInstanceQry.put(Constants.status, Constants._claimAcquire);
		claimInstanceQry.put(Constants._userCode,
				dbObjInput.get(Constants._userCode));
		claimInstanceQry.put(Constants._userName,
				dbObjInput.get(Constants._userName));
		DBObject claimTaskInstance = mongoTemplate.getCollection(
				Constants.claimTaskInstances).findOne(claimInstanceQry);
		if (claimTaskInstance != null)
			return realeaseClaim(claimTaskInstance,
					Constants._claimReleasedByUser);
		else
			return MessageUtility
					.response(Constants.responseCode.realeaseClaimFailed);
	}
	/*
	 * (non-Javadoc)
	 * @see com.enhancesys.workflow.bpmnCore.TaskClaimHandler#reassignTask(java.lang.String)
	 */
	public String reassignTask(String input) {
		try {

			DBObject request = (DBObject) JSON.parse(input);
			log.debug("[request]" + request);
			DBObject taskInstanceQry = new BasicDBObject(
					Constants._id,
					new ObjectId((String) request.get(Constants.taskInstanceId)));
			taskInstanceQry.put(Constants.status, Constants._IN_Execution);
			DBObject taskInstance = (DBObject) mongoTemplate.getCollection(
					Constants.workFlowTaskInstances).findOne(taskInstanceQry);
			
			if (taskInstance != null) {
				DBObject reassginDtls = new BasicDBObject();
				reassginDtls.put(Constants._userCode,
						(String) request.get(Constants._userCode));
				reassginDtls.put(Constants._userName,
						(String) request.get(Constants._userName));
				mongoTemplate
				.getCollection(Constants.workFlowTaskInstances)
				.update(new BasicDBObject(Constants._id, new ObjectId(
						(String) request.get(Constants.taskInstanceId))),
						new BasicDBObject(Constants._$set,reassginDtls));
				return MessageUtility
						.response(Constants.responseCode.reAssginTaskSucess);
			}
			else {
				return MessageUtility
						.response(Constants.responseCode.reAssginTaskFailed);
			}
		  }	
			catch (Exception e) {
				log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		}
		return MessageUtility
				.response(Constants.responseCode.reAssginTaskFailed);
	}

}

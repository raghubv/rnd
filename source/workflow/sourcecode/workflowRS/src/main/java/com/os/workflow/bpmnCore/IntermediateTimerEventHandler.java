package com.os.workflow.bpmnCore;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * IntermediateTimerEventHandler: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Aug 25, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */

public interface IntermediateTimerEventHandler {

	/**
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. checkAndScheduleBoundryEvent To check and schedule Boundry Event for
	 * task
	 * 
	 * @param type
	 * @param taskDfn
	 * @param taskInstance
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws ParseException
	 */
	public void checkAndScheduleBoundryEvent(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException;

	/**
	 * 
	 * @param type
	 * @param taskDfn
	 * @param taskInstance
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws ParseException
	 */
	public void checkAndCancelBoundryEvent(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException;

	/**
	 * 
	 * @param schedulerInstance
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws ParseException
	 */
	public void runTimerJob(DBObject schedulerInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException;

}

package com.os.workflow.bpmnCore;

/**
 * 
 * <b>Purpose:</b><br>
 * TaskClaimHandler <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface TaskClaimHandler{
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. acquireClaim
	* To claim a task by user
	* </pre> 
	* @param input
	* @return
	 */
	public String acquireClaim(String input);
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. realeaseClaim
	* To release task claim  
	* </pre> 
	* @param input
	* @param releasedBy
	* @return
	 */
	public String realeaseClaim(Object input, String releasedBy);
	
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. realeaseClaim
	* 
	* </pre> 
	* @param input
	* @return
	 */
	public String realeaseClaim(String input);
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. getClaimHistoryByTaskInstanceId
	* To get the claim history by task instance id
	* </pre> 
	* @param input
	* @return
	 */
	public String getClaimHistoryByTaskInstanceId(String input);
	
	/**
	 * <b>Algorithm:</b>
	 * <pre>
	 * Re-assign the task to User
	 * </pre>
	 * @param input
	 * @return
	 */
	public String reassignTask(String input);
	
}

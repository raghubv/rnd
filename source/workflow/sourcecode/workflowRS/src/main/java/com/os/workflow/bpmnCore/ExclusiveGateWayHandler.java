package com.os.workflow.bpmnCore;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * 
 * <b>Purpose:</b><br>
 * ExclusiveGateWayHandler <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface ExclusiveGateWayHandler {

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. processXORGateOutGoingPath: To process outgoing path from XOR gate way
	 * 
	 * </pre>
	 * 
	 * @param processDfnId
	 * @param resourceIdLs
	 * @param processInstanceId
	 * @param status
	 * @param initialRecursion
	 * @param outGoingData
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public void processXORGateOutGoingPath(String processDfnId, List<Map> resourceIdLs, String processInstanceId,
			String status, boolean initialRecursion, Object outGoingData)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. updateXORPath: To update XOR outgoing path
	 * 
	 * </pre>
	 * 
	 * @param processDfnId
	 * @param resourceId
	 * @param processInstanceId
	 * @param initialRecursion
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public void updateXORPath(String processDfnId, String resourceId, String processInstanceId,
			boolean initialRecursion) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;

}

package com.os.workflow.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.os.workflow.helper.LoggerTrace;

/**
 * 
 * <b>Purpose:</b><br>
 * Configuration <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * dpc-cyp-0.0.2-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Mar 28, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Configuration {
	private static final Logger log = Logger.getLogger(Configuration.class);
	private static Map configuration = null;
	private static Map mapReduce = null;
	private static Map bpmn2 = null;
	private static Map indexKeys = null;
	static {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			InputStream config = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(Constants._Configuration_Json);
			configuration = objectMapper.readValue(config, LinkedHashMap.class);
			config.close();
			//Map Reduce
			InputStream mapReduceConfig = Thread
					.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(Constants._MapReduceConfiguration_Json);
			mapReduce = objectMapper.readValue(mapReduceConfig,
					LinkedHashMap.class);
			mapReduceConfig.close();
			//BPMN 2.0
			InputStream bpmn2Config = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(Constants._Bpmn2_Json);
			bpmn2 = ((DBObject) JSON
					.parse(getStringFromInputStream(bpmn2Config))).toMap();
			bpmn2Config.close();
			//Index Keys
			InputStream indexKeysConfig = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(Constants._IndexKeys_Json);
			indexKeys=objectMapper.readValue(indexKeysConfig,
					LinkedHashMap.class);
			indexKeysConfig.close();	
					
		} catch (JsonParseException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		}
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getMapReduce
	 * 
	 * </pre>
	 * 
	 * @return
	 */
	public static synchronized Map getMapReduce() {
		return mapReduce;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getConfiguration
	 * 
	 * </pre>
	 * 
	 * @return
	 */
	public static synchronized Map getConfiguration() {
		return configuration;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getBpmn2
	 * 
	 * </pre>
	 * 
	 * @return
	 */

	public static synchronized Map getBpmn2() {
		return bpmn2;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. mapReduce
	 * 
	 * </pre>
	 * 
	 * @param bmpmnDfnMap
	 * @return
	 */
	
	public static synchronized Map mapReduce(Map bmpmnDfnMap) {
		Map outMap = new HashMap();
		Set set = getMapReduce().entrySet();
		for (Object object : set) {
			Map.Entry entry = (Map.Entry) object;
			String key = (String) entry.getKey();
			Object value = (Object) bmpmnDfnMap.get(key);
			if (bmpmnDfnMap.containsKey(key)) {
				if (value instanceof Map) {
					outMap.put(key, mapReduce((Map) value));
				} else if (value instanceof List) {

					List<Object> values = (List<Object>) value;

					List<Object> newValues = new ArrayList<Object>();
					if (!values.isEmpty())
						for (Object map : values) {
							newValues.add(mapReduce((Map) map));
						}
					outMap.put(key, newValues);

				} else {
					outMap.put(key, value);
				}
			}

		}

		return outMap;
	}
	/**
	 * 
	* <b>Algorithm:</b> 
	* <pre> 
	* 1. getIndexKeys
	* 
	* </pre> 
	* @return
	 */
	public static synchronized Map getIndexKeys() {
		return indexKeys;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. loadConfiguration
	 * 
	 * </pre>
	 * 
	 * @param mongoTemplate
	 */
	public static void loadConfiguration(MongoTemplate mongoTemplate) {
		Map master = null;
		Map customField = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			InputStream customFieldConfig = Thread
					.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(
							Constants._BpmnWebModelerCustomField_Json);
			customField = objectMapper.readValue(customFieldConfig,
					LinkedHashMap.class);
			List<Map> customFieldLs = (List<Map>) customField
					.get(Constants.bpmnWebModelerCustomField);
			log.debug("[customFieldLs]" + customFieldLs);
			for (Map map : customFieldLs) {
				log.debug("[map]" + map);
				DBObject query = new BasicDBObject();
				query.put(Constants._propertyType,
						map.get(Constants._propertyType));
				query.put(Constants._name, map.get(Constants._name));
				if (mongoTemplate.getCollection(
						Constants.bpmnWebModelerCustomField).findOne(query) == null)
					mongoTemplate.getCollection(
							Constants.bpmnWebModelerCustomField).insert(
							new BasicDBObject(map));

			}

		} catch (JsonParseException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));
		}
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getStringFromInputStream
	 * To convert input stream to string
	 * </pre>
	 * 
	 * @param is
	 * @return
	 */
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("[Error]" + LoggerTrace.getStackTrace(e));
				}
			}
		}

		return sb.toString();

	}

}

package com.os.workflow.bpmnCore.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.ServiceTaskHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.Producer;
import com.os.workflow.helper.RestClient;
import com.os.workflow.util.Constants;
import com.os.workflow.util.Utility;

/**
 * 
 * <b>Purpose:</b><br>
 * ServiceTaskHandlerImpl <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ServiceTaskHandlerImpl implements ServiceTaskHandler {

	static private enum ServiceImplentation {
		HumanTaskWebService, BusinessRuleWebService, WebService, Other, Unspecified, Queue, JSONRestAPI
	};

	private static final Logger log = Logger.getLogger(ServiceTaskHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	UpdateStatusHandler updateStatusHandler  ;
	private ObjectMapper objectMapper = new ObjectMapper();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.ServiceTask#executeServiceTask(java.
	 * lang.String, java.lang.String, com.mongodb.DBObject,
	 * com.mongodb.DBObject, java.lang.Object)
	 */
	public void executeServiceTask(String processInstanceId,
			String serviceTaskImpl, DBObject taskDfn, DBObject taskInstance,
			Object outGoingData) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			JsonGenerationException, JsonMappingException,
			IOException {
		
		DBObject processInstance = null;
		if (serviceTaskImpl != null && !serviceTaskImpl.isEmpty()) {
			log.debug("[serviceTaskImpl]" + serviceTaskImpl);
			switch (ServiceImplentation.valueOf(serviceTaskImpl)) {
			case Queue: {
				DBObject processInstanceQry = new BasicDBObject();
				processInstanceQry.put(Constants._id, new ObjectId(
						processInstanceId));
				processInstance = mongoTemplate.getCollection(
						Constants.workFlowProcessInstances).findOne(
						processInstanceQry);

				String target = Utility.checkNullString((String) PropertyUtils
						.getProperty(taskDfn.toMap(),
								Constants._queueTargetPath));
				Integer priority = Utility
						.checkNullInteger((String) PropertyUtils.getProperty(
								taskDfn.toMap(), Constants._queuePriorityPath));
				String applicationID = Utility
						.checkNullString((String) PropertyUtils.getProperty(
								taskDfn.toMap(),
								Constants._queueApplicationIdPath));

				log.debug("[target]" + target + "[priority]" + priority
						+ "[applicationID]" + applicationID);
				Map payload = new HashMap();
				payload.put(Constants.payload,
						processInstance.get(Constants.payload));
				payload.put(Constants.actionCommand, ((Map) taskDfn
						.get(Constants.properties))
						.get(Constants.actionCommand));
				if (outGoingData != null && !((Map) outGoingData).isEmpty())
					payload.put(Constants.inComingData, (Map) outGoingData);

				payload.put(Constants.taskInstanceId, (String) PropertyUtils
						.getProperty(objectMapper.readValue(
								taskInstance.toString(), LinkedHashMap.class),
								Constants._idPath));

				log.debug("[payload]" + payload);
				
				Producer.sendQMessage(target, taskDfn.toString());
			}

				break;
			case JSONRestAPI: {
				DBObject processInstanceQry = new BasicDBObject();
				processInstanceQry.put(Constants._id, new ObjectId(
						processInstanceId));
				processInstance = mongoTemplate.getCollection(
						Constants.workFlowProcessInstances).findOne(
						processInstanceQry);
				String restAPIURL = Utility
						.checkNullString((String) PropertyUtils.getProperty(
								taskDfn.toMap(), Constants._restAPIURLPath));
				log.debug("[restAPIURL]" + restAPIURL);
				String restAPIUserName = Utility
						.checkNullString((String) PropertyUtils.getProperty(
								taskDfn.toMap(), Constants._restAPIUserNamePath));
				log.debug("[restAPIUserName]" + restAPIUserName);
				String restAPIUserPassword = Utility
						.checkNullString((String) PropertyUtils.getProperty(
								taskDfn.toMap(), Constants._restAPIPasswordPath));
				log.debug("[restAPIUserPassword]" + restAPIUserPassword);
				String restJSONString = Utility
						.checkNullString((String) PropertyUtils.getProperty(
								taskDfn.toMap(),
								Constants._restAPIJSONStringPath));
				log.debug("[restJSONString]" + restJSONString);
				Map payload = new HashMap();
				payload.putAll(((DBObject) JSON.parse(restJSONString)).toMap());
				payload.put(Constants.payload,
						processInstance.get(Constants.payload));
				log.debug("[payload]" + payload);
				updateStatusHandler.updateTaskStatus(RestClient.post(restAPIUserName,
						restAPIUserPassword, restAPIURL,
						objectMapper.writeValueAsString(payload)));
			}

				break;

			}
			if (taskInstance != null)
			{
				DBObject taskInstanceUpdateDtls = new BasicDBObject();
				taskInstanceUpdateDtls.put(Constants.taskInstanceId, (String) PropertyUtils
						.getProperty(objectMapper.readValue(
								taskInstance.toString(), LinkedHashMap.class),
								Constants._idPath));
				taskInstanceUpdateDtls.put(Constants.status, Constants._Completed);
				log.debug("[taskInstanceUpdateDtls]" + taskInstanceUpdateDtls);
				updateStatusHandler.updateTaskStatus(taskInstanceUpdateDtls.toString());
			}
		}
	}

}

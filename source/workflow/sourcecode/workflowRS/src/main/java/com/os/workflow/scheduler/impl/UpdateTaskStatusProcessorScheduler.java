package com.os.workflow.scheduler.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.os.workflow.bpmnCore.TaskExecutorHandler;
import com.os.workflow.scheduler.Scheduler;
import com.os.workflow.util.Constants;

public class UpdateTaskStatusProcessorScheduler implements Scheduler {

	private static boolean initialLoad = true;
	private static final Logger log = Logger.getLogger(UpdateTaskStatusProcessorScheduler.class);

	@Autowired
	MongoTemplate mongoTemplate;
	
	
	@Autowired
	TaskExecutorHandler taskExecutorHandler;


	@Override
	public void runScheduler() {
		if (initialLoad) {
			DBObject sort = new BasicDBObject(Constants._id, -1);
			DBCursor dbCursor = mongoTemplate.getCollection(Constants.workFlowTaskProcessingIntances).find().sort(sort);
			for (DBObject dbObject : dbCursor) {
				log.debug("[dbObject]" + dbObject);
				//QueueHandler.enqueue(dbObject);
				taskExecutorHandler.taskExecute(dbObject);
			}
			initialLoad = false;
		}
	}

}

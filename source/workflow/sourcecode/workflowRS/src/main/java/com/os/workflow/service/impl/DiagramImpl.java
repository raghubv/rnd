package com.os.workflow.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.Diagram;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;


public class DiagramImpl implements Diagram {
	private static final Logger log = Logger.getLogger(DiagramImpl.class);

	private static final ResourceBundle bundle = ResourceBundle.getBundle("workflow");

	private static final ResourceBundle message = ResourceBundle.getBundle("Message_en_US");

	private static DocumentBuilder builder = null;
	private static TransformerFactory tf = TransformerFactory.newInstance();

	private static XPath xPath = XPathFactory.newInstance().newXPath();

	private static Map<String, String> statusColorMap = new HashMap<String, String>();

	static {
		statusColorMap.put(Constants._Completed, Constants._Completed_Color);
		statusColorMap.put(Constants._IN_Execution, Constants._IN_Execution_Color);
		statusColorMap.put(Constants._Not_IN_Execution, Constants._Not_IN_Execution_Color);
		statusColorMap.put(Constants._In_Error, Constants._In_Error_Color);
		statusColorMap.put(Constants._In_FallOut, Constants._In_FallOut_Color);

		try {

			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

		} catch (ParserConfigurationException e) {
			log.error("[>>]Error[>>]" + LoggerTrace.getStackTrace(e));

		}
	}

	@Autowired
	MongoTemplate mongoTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Diagram#getBMPNDiagramByProcessDfnId(
	 * java.lang.String)
	 */
	public String getBMPNDiagramByProcessDfnId(String query) {

		DBObject workFlowDBObject = (DBObject) mongoTemplate.getCollection(Constants.workFlowProcessDefinitions)
				.findOne(new BasicDBObject(Constants._id,
						new ObjectId(query.replaceAll(Constants.svgRepresentationExtn, ""))));
		return workFlowDBObject.get(Constants._bpmnDiagramPath).toString();

	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. buildProcessInstanceDiagram
	 * To build process instance diagram
	 * </pre>
	 * 
	 * @param processInstanceId
	 * @return
	 */
	private String buildProcessInstanceDiagram(String processInstanceId) {
		try {

			synchronized (builder) {

				log.debug("[processInstanceId]" + processInstanceId);
				DBObject processInstanceQry = new BasicDBObject(Constants._id, new ObjectId(processInstanceId));
				log.debug("[processInstanceQry]" + processInstanceQry);
				DBObject processInstance = (DBObject) mongoTemplate.getCollection(Constants.workFlowProcessInstances)
						.findOne(processInstanceQry);

				DBObject processDefinitionQry = new BasicDBObject(Constants._id,
						(ObjectId) ((DBRef) processInstance.get(Constants.processRef)).getId());
				DBObject processDefinition = (DBObject) mongoTemplate
						.getCollection(Constants.workFlowProcessDefinitions).findOne(processDefinitionQry);
				String bmpmDigramDfn = processDefinition.get(Constants._bpmnDiagramPath).toString();

				DBObject taskInstanceQry = new BasicDBObject(Constants.processInstanceRef,
						new DBRef(mongoTemplate.getDb(), Constants.workFlowProcessInstances,
								(ObjectId) processInstance.get(Constants._id)));
				DBCursor tasks = (DBCursor) mongoTemplate.getCollection(Constants.workFlowTaskInstances)
						.find(taskInstanceQry).hint(new BasicDBObject(
								(Map) Configuration.getIndexKeys().get(Constants.workFlowTaskInstances)));
				InputStream in = new ByteArrayInputStream(bmpmDigramDfn.getBytes());

				Document doc = builder.parse(in);
				for (DBObject task : tasks) {

					DBObject taskDfnQry = new BasicDBObject(Constants._id,
							(ObjectId) ((DBRef) task.get(Constants.taskRef)).getId());
					String resourceId = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions)
							.findOne(taskDfnQry).get(Constants.resourceId).toString();

					Element svg = (Element) doc.getElementsByTagName(Constants._svg).item(0);
					Element node = this.getElementById(doc.getDocumentElement(), Constants._svg_ + resourceId);

					NodeList nodeLS = (NodeList) this
							.getElementById(doc.getDocumentElement(), Constants._svg_ + resourceId)
							.getElementsByTagName(Constants._svgStop);
					if (nodeLS.getLength() == 2) {
						String status = task.get(Constants.status).toString();
						if (!status.equalsIgnoreCase(Constants._Waiting_For_Execute)) {
							((Element) nodeLS.item(1)).setAttribute(Constants._svgStopColor,
									statusColorMap.get(status));
							StringBuilder displayText = new StringBuilder();
							Node titleNode = doc.createElement("title");
							if (task.get(Constants.subProcessDtls) != null) {
								((Element) ((Element) nodeLS.item(1)).getParentNode().getParentNode().getParentNode())
										.setAttribute(Constants._svgEventOnclick,
												Constants._svgEventWindowLocation
														+ ((DBObject) task.get(Constants.subProcessDtls))
																.get(Constants.processInstanceId)
														+ Constants.svgRepresentationExtn + Constants._singleQuote);
								displayText.append(message.getString("SVG_SubProcessTitle"));

							}
							displayText.append(message.getString("SVG_Status") + message.getString("SVG_" + status));
							if (task.get(Constants.payload) != null)
								displayText.append(message.getString("SVG_Payload")
										+ ((DBObject) task.get(Constants.payload)).toString());

							titleNode.setTextContent(displayText.toString());
							((Element) ((Element) ((Element) nodeLS.item(1)).getParentNode().getParentNode()
									.getParentNode()).getParentNode()).appendChild(titleNode);
							((Element) ((Element) ((Element) nodeLS.item(1)).getParentNode().getParentNode()
									.getParentNode()).getParentNode()).setAttribute("title", displayText.toString());
						}
					}
				}
				in.close();

				return convertTOString(doc);
			}
		} catch (SAXException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}

		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.service.Diagram#getBMPNDiagramByProcessInstanceId
	 * (java.lang.String)
	 */

	public String getBMPNDiagramByProcessInstanceId(String query) {
		String processInstanceId = query.replaceAll(Constants.svgRepresentationExtn, "");
		return buildProcessInstanceDiagram((processInstanceId));

	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getElementById
	 * To node element by element id
	 * </pre>
	 * 
	 * @param rootElement
	 * @param id
	 * @return
	 */
	private static synchronized Element getElementById(Element rootElement, String id) {
		try {
			String path = String.format(Constants._idSearchPattern, id);

			NodeList nodes = (NodeList) xPath.evaluate(path, rootElement, XPathConstants.NODESET);
			log.debug("[" + (Element) nodes.item(0) + "]");

			return (Element) nodes.item(0);
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			return null;
		}
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. convertTOString
	 * To Convert xml to string
	 * </pre>
	 * 
	 * @param doc
	 * @return
	 */

	private static synchronized String convertTOString(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(domSource, result);
			log.debug("[writer]" + writer);
			String response = writer.toString();
			writer.close();
			transformer.reset();
			domSource = null;
			return response;
		} catch (TransformerConfigurationException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (TransformerException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}

}

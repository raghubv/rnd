package com.os.workflow.bpmnCore.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;
import com.os.workflow.bpmnCore.SubProcessHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.util.Configuration;
import com.os.workflow.util.Constants;

/**
 * 
 * <b>Purpose:</b><br>
 * SubProcessHandlerImpl <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class SubProcessHandlerImpl implements SubProcessHandler {

	private static final Logger log = Logger.getLogger(SubProcessHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	UpdateStatusHandler updateStatusHandler;

	private ObjectMapper objectMapper = new ObjectMapper();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.SubProcess#collapsedSubProcess(java.
	 * lang.String, com.mongodb.DBObject, com.mongodb.DBObject)
	 */
	public DBObject collapsedSubProcess(String processInstanceId, DBObject taskDfn, DBObject taskInstance)
			throws JsonParseException, JsonMappingException, IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, IOException {
		DBObject processInstanceQry = new BasicDBObject();
		processInstanceQry.put(Constants._id, new ObjectId(processInstanceId));
		DBObject processInstance = mongoTemplate.getCollection(Constants.workFlowProcessInstances)
				.findOne(processInstanceQry);
		Map subprocessMap = (Map) ((DBObject) ((DBObject) processInstance.get(Constants.payload))
				.get(Constants._subprocess));
		String collapsedSubProcessName = ((DBObject) processInstance.get(Constants.payload))
				.get(Constants._subprocess) != null
						? subprocessMap
								.get(PropertyUtils
										.getProperty(objectMapper.readValue(taskDfn.toString(), LinkedHashMap.class),
												Constants._subProcessRefPath)
										.toString())
								.toString()
						: PropertyUtils.getProperty(objectMapper.readValue(taskDfn.toString(), LinkedHashMap.class),
								Constants._subProcessRefPath).toString();
		String taskInstanceId = PropertyUtils
				.getProperty(objectMapper.readValue(taskInstance.toString(), LinkedHashMap.class), Constants._idPath)
				.toString();
		taskInstance.put(Constants.subProcessDtls,
				JSON.parse(createSubProcessInstance(collapsedSubProcessName,
						(String) taskInstance.get(Constants.workflowType), taskInstanceId,
						objectMapper.writeValueAsString(processInstance.get(Constants.payload)),
						(String) taskInstance.get(Constants.nameProcessInstance))));

		return taskInstance;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.SubProcess#createSubProcessInstance(
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	public String createSubProcessInstance(String collapsedSubProcessName, String workflowType, String taskInstanceId,
			String payload, String nameProcessInstance) {

		log.debug("[collapsedSubProcessName]" + collapsedSubProcessName + "[workflowType]" + workflowType
				+ "[taskInstanceId]" + taskInstanceId + "[payload]" + payload);

		ObjectMapper objectMapper = new ObjectMapper();
		Map response = new HashMap();
		try {
			DBObject processDfnQuery = new BasicDBObject();
			processDfnQuery.put(Constants.processDfnName, collapsedSubProcessName);
			processDfnQuery.put(Constants.workflowType, workflowType);
			processDfnQuery.put(Constants.status, Constants._Activated);
			DBObject processDfn = (DBObject) mongoTemplate.getCollection(Constants.workFlowProcessDefinitions)
					.findOne(processDfnQuery, new BasicDBObject(((Map) Configuration.getConfiguration()
							.get(Constants._getCreateProcessInstanceQueryFields))));
			String tenant = ((DBObject) processDfn.get(Constants.properties)).get(Constants.tenant).toString();
			String processDfnName = (String) PropertyUtils.getProperty(processDfn, Constants.processDfnName);
			log.debug("[processDfn]" + processDfn);
			DBObject processInstance = new BasicDBObject();
			processInstance.put(Constants.processRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowProcessDefinitions, (ObjectId) processDfn.get(Constants._id)));
			processInstance.put(Constants.status, Constants._IN_Execution);
			processInstance.put(Constants.payload, (DBObject) JSON.parse(payload));
			processInstance.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, new ObjectId(taskInstanceId)));
			processInstance.put(Constants.tenant, tenant);
			processInstance.put(Constants.processDfnName, processDfnName);
			processInstance.put(Constants.nameProcessInstance, nameProcessInstance);
			mongoTemplate.getCollection(Constants.workFlowProcessInstances).insert((DBObject) (processInstance));
			log.debug("[processInstance]" + processInstance);
			DBObject taskDfnQry = new BasicDBObject(Constants.processRef, new DBRef(mongoTemplate.getDb(),
					Constants.workFlowProcessDefinitions, (ObjectId) processDfn.get(Constants._id)));
			log.debug("[taskDfnQry]" + taskDfnQry);
			DBCursor taskDfns = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(taskDfnQry)
					.hint(new BasicDBObject((Map) Configuration.getIndexKeys().get(Constants.workFlowTaskDefinitions)));
			List<String> initialStartEventList = new ArrayList<String>();
			for (DBObject taskDfn : taskDfns) {
				log.debug("[taskDfn]" + taskDfn);
				DBObject taskInstance = new BasicDBObject();
				taskInstance.put(Constants.processInstanceRef, new DBRef(mongoTemplate.getDb(),
						Constants.workFlowProcessInstances, (ObjectId) processInstance.get(Constants._id)));
				taskInstance.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskDefinitions,
						(ObjectId) taskDfn.get(Constants._id)));
				taskInstance.put(Constants.status, Constants._Waiting_For_Execute);
				taskInstance.put(Constants.taskName, PropertyUtils.getProperty(taskDfn, Constants._taskNamePath));
				taskInstance.put(Constants.processDfnName, processDfnName);
				taskInstance.put(Constants.nameProcessInstance, nameProcessInstance);
				taskInstance.put(Constants.tenant, tenant);
				List<Map> fields = (List<Map>) Configuration.getConfiguration().get(Constants.filterFields);
				for (Map map : fields) {
					if (PropertyUtils.getProperty(taskDfn, map.get(Constants.dbPath).toString()) != null)
						taskInstance.put(map.get(Constants.fieldName).toString(),
								PropertyUtils.getProperty(taskDfn, map.get(Constants.dbPath).toString()));
				}
				String stencilId = (String) PropertyUtils.getProperty(taskDfn, Constants._taskStencilPath);
				if (stencilId.equalsIgnoreCase(Constants._StartNoneEvent))
					initialStartEventList.add((String) taskDfn.get(Constants.resourceId));
				mongoTemplate.getCollection(Constants.workFlowTaskInstances).insert((DBObject) (taskInstance));
			}
			response.put(Constants.processInstanceId, PropertyUtils.getProperty(
					objectMapper.readValue(processInstance.toString(), LinkedHashMap.class), Constants._idPath));

			response.put(Constants.subProcessInstanceId, PropertyUtils.getProperty(
					objectMapper.readValue(processInstance.toString(), LinkedHashMap.class), Constants._idPath));
			response.put(Constants.subProcessInstanceName, collapsedSubProcessName);

			String processId = PropertyUtils
					.getProperty(objectMapper.readValue(processDfn.toString(), LinkedHashMap.class), Constants._idPath)
					.toString();
			log.debug("[processId]" + processId);
			String processInstanceId = PropertyUtils
					.getProperty(objectMapper.readValue(processInstance.toString(), LinkedHashMap.class),
							Constants._idPath)
					.toString();
			log.debug("[processInstanceId]" + processInstanceId);
			for (String resourceId : initialStartEventList) {
				updateStatusHandler.invokeUpdatePathStatus(processId, resourceId, processInstanceId,
						Constants._Completed, null, true);

			}

			return objectMapper.writeValueAsString(response);
		} catch (JsonParseException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		return null;
	}
}

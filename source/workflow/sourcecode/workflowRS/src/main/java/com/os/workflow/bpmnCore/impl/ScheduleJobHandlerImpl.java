package com.os.workflow.bpmnCore.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.os.workflow.bpmnCore.ScheduleJobHandler;
import com.os.workflow.util.Constants;
import com.os.workflow.util.Utility;

/**
 * 
 * <b>Purpose:</b><br>
 * ScheduleJobHandlerImpl <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class ScheduleJobHandlerImpl implements ScheduleJobHandler {
	static private enum ScheduleType {
		Task, StartTimerEvent, IntermediateTimerEvent
	};

	private static final Logger log = Logger.getLogger(ScheduleJobHandlerImpl.class);
	@Autowired
	MongoTemplate mongoTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.ScheduleJob#schedule(java.lang.String,
	 * com.mongodb.DBObject, com.mongodb.DBObject)
	 */
	public void schedule(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException {
		synchronized (taskInstance) {

			switch (ScheduleType.valueOf(type)) {
			case Task:
				if ((PropertyUtils.getProperty(taskDfn.toMap(), Constants._taskAsyncPath) instanceof Boolean
						&& (Boolean) PropertyUtils.getProperty(taskDfn.toMap(), Constants._taskAsyncPath))) {
					String timerType = Utility.checkNullString(
							(String) PropertyUtils.getProperty(taskDfn.toMap(), Constants._taskTimerType));
					String timeout = timerType.equalsIgnoreCase(Constants._Fall_Out)
							? Utility.checkNullString(
									(String) PropertyUtils.getProperty(taskDfn.toMap(), Constants._fallOuttimeOutPath))
							: Utility.checkNullString((String) PropertyUtils.getProperty(taskDfn.toMap(),
									Constants._jeopardytimeOutPath));

					scheduleTask((ObjectId) taskInstance.get(Constants._id), type, timeout, timerType);
				}
				break;
			case StartTimerEvent: {
				log.debug("[StartTimerEvent]");
				log.debug(Constants._StartTimerEvent_Properties_Timecycle
						+ PropertyUtils.getProperty(taskDfn, Constants._StartTimerEvent_Properties_Timecycle));
				log.debug(Constants._StartTimerEvent_Properties_Timedate
						+ PropertyUtils.getProperty(taskDfn, Constants._StartTimerEvent_Properties_Timedate));

				log.debug(Constants._StartTimerEvent_Properties_Timeduration
						+ PropertyUtils.getProperty(taskDfn, Constants._StartTimerEvent_Properties_Timeduration));
				scheduleStartTimerEvent((ObjectId) taskInstance.get(Constants._id), type,
						Constants.MONGODB_DATE_FORMAT.parse((String) PropertyUtils.getProperty(taskDfn,
								Constants._StartTimerEvent_Properties_Timedate)),
						"StartTimerEvent");
			}
				break;

			case IntermediateTimerEvent: {
				log.debug("[IntermediateTimerEvent]");
				log.debug(Constants._IntermediateTimerEvent_Properties_Timedate
						+ PropertyUtils.getProperty(taskDfn, Constants._IntermediateTimerEvent_Properties_Timedate));
				log.debug(Constants._IntermediateTimerEvent_Properties_Timecycle
						+ PropertyUtils.getProperty(taskDfn, Constants._IntermediateTimerEvent_Properties_Timecycle));

				log.debug(Constants._IntermediateTimerEvent_Properties_Timeduration + PropertyUtils.getProperty(taskDfn,
						Constants._IntermediateTimerEvent_Properties_Timeduration));
				scheduleEvent((ObjectId) taskInstance.get(Constants._id),
						(String) PropertyUtils.getProperty(taskDfn,
								Constants._IntermediateTimerEvent_Properties_TimerType),
						(String) PropertyUtils.getProperty(taskDfn,
								Constants._IntermediateTimerEvent_Properties_Timeduration),

						type);
			}
				break;
			}
		}
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. scheduleTask
	 * To schedule Task
	 * </pre>
	 * 
	 * @param objectId
	 * @param type
	 * @param timeOut
	 * @param timerType
	 */
	private void scheduleTask(ObjectId objectId, String type, String timeOut, String timerType) {

		synchronized (objectId) {

			log.debug("[objectId]" + objectId + "[type]" + type + "[timeOut]" + timeOut);

			// To Schedule time bound task -Fallout/Jeopardy
			DBObject dbObject = new BasicDBObject();
			dbObject.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, objectId));
			dbObject.put(Constants.quartzSchedulerType, type);
			dbObject.put(Constants.quartzScheduledDate, Utility.getDate());
			dbObject.put(Constants.quartzScheduledTimerType, timerType);
			dbObject.put(Constants.quartzScheduledExecutionDate, Utility.getScheduledDateAndTime(timeOut));
			dbObject.put(Constants.quartzStatus, Constants.quartzStatusActive);
			log.debug("[dbObject]" + dbObject);
			mongoTemplate.getCollection(Constants.quartzSchedulerInstances).insert(dbObject);
		}

	}

	/**
	 * 
	 * @param objectId
	 * @param type
	 * @param timeOut
	 * @param timerType
	 */
	public void scheduleEvent(ObjectId objectId, String type, String timeOut, String timerType) {
		synchronized (objectId) {

			log.debug("[objectId]" + objectId + "[type]" + type + "[timeOut]" + timeOut);
			// To Schedule time bound task -Fallout/Jeopardy
			DBObject dbObject = new BasicDBObject();
			dbObject.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, objectId));
			dbObject.put(Constants.quartzSchedulerType, type);
			dbObject.put(Constants.quartzScheduledDate, Utility.getDate());
			dbObject.put(Constants.quartzScheduledTimerType, timerType);
			dbObject.put(Constants.quartzScheduledExecutionDate, Utility.getScheduledDateAndTime(timeOut));
			dbObject.put(Constants.quartzStatus, Constants.quartzStatusActive);

			log.debug("[dbObject]" + dbObject);
			mongoTemplate.getCollection(Constants.quartzSchedulerInstances).insert(dbObject);
		}

	}

	/**
	 * 
	 * @param taskObjectId
	 * @param parentTaskObjectId
	 * @param type
	 * @param timeOut
	 * @param timerType
	 * @param isBoundryEvent
	 */
	public void scheduleEvent(ObjectId taskObjectId, ObjectId parentTaskObjectId, String type, String timeOut,
			String timerType) {

		synchronized (taskObjectId) {

			log.debug("[objectId]" + taskObjectId + "[type]" + type + "[timeOut]" + timeOut);
			// To Schedule time bound task -Fallout/Jeopardy
			DBObject dbObject = new BasicDBObject();
			dbObject.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, taskObjectId));
			if (parentTaskObjectId != null)
				dbObject.put(Constants.parentTaskInsatancesRef,
						new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, parentTaskObjectId));

			dbObject.put(Constants.quartzSchedulerType, type);
			dbObject.put(Constants.quartzScheduledDate, Utility.getDate());
			dbObject.put(Constants.quartzScheduledTimerType, timerType);
			dbObject.put(Constants.quartzScheduledExecutionDate, Utility.getScheduledDateAndTime(timeOut));
			dbObject.put(Constants.quartzStatus, Constants.quartzStatusActive);

			log.debug("[dbObject]" + dbObject);
			mongoTemplate.getCollection(Constants.quartzSchedulerInstances).insert(dbObject);
		}

	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. scheduleStartTimerEvent
	 * To schedule start timer event
	 * </pre>
	 * 
	 * @param objectId
	 * @param type
	 * @param executiondate
	 * @param timerType
	 */
	private void scheduleStartTimerEvent(ObjectId objectId, String type, Date executiondate, String timerType) {

		synchronized (objectId) {

			log.debug("[objectId]" + objectId + "[type]" + type + "[executiondate]" + executiondate);

			DBObject dbObject = new BasicDBObject();
			dbObject.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, objectId));
			dbObject.put(Constants.quartzSchedulerType, type);
			dbObject.put(Constants.quartzScheduledDate, Utility.getDate());
			dbObject.put(Constants.quartzScheduledTimerType, timerType);
			dbObject.put(Constants.quartzScheduledExecutionDate, executiondate);
			dbObject.put(Constants.quartzStatus, Constants.quartzStatusActive);
			log.debug("[dbObject]" + dbObject);
			mongoTemplate.getCollection(Constants.quartzSchedulerInstances).insert(dbObject);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.workflow.bpmnCore.ScheduleJobHandler#cancelEvent(org.bson
	 * .types.ObjectId, org.bson.types.ObjectId, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	public void cancelEvent(ObjectId taskObjectId, ObjectId parentTaskObjectId, String type, String timeOut,
			String timerType) {
		synchronized (taskObjectId) {

			log.debug("[objectId]" + taskObjectId + "[type]" + type + "[timeOut]" + timeOut);
			DBObject scheduledInstanceQry = new BasicDBObject();
			scheduledInstanceQry.put(Constants.taskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, taskObjectId));
			scheduledInstanceQry.put(Constants.parentTaskInsatancesRef,
					new DBRef(mongoTemplate.getDb(), Constants.workFlowTaskInstances, parentTaskObjectId));
			scheduledInstanceQry.put(Constants.quartzStatus, Constants.quartzStatusActive);
			log.debug("[scheduledInstanceQry]" + scheduledInstanceQry);
			DBObject scheduledInstance = mongoTemplate.getCollection(Constants.quartzSchedulerInstances)
					.findOne(scheduledInstanceQry);
			if (scheduledInstance != null) {
				scheduledInstance.put(Constants.quartzStatus, Constants.quartzStatusExecuted);
				log.debug("[scheduledInstance]" + scheduledInstance);
				mongoTemplate.getCollection(Constants.quartzSchedulerInstances).findAndModify(scheduledInstanceQry,
						scheduledInstance);
			}
		}
	}

}

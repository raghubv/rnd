package com.os.workflow.bpmnCore;

import java.util.Map;

/**
 * 
 * <b>Purpose:</b><br>
 * ParallelGateWayHandler <br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Apr 23, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface ParallelGateWayHandler {

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. isAllIncomingParallelPathProcessed 
	 * to check all incoming path of parallel gateway is processed or not
	 * 
	 * </pre>
	 * 
	 * @param workFlowId
	 * @param resourceId
	 * @param workFlowInstanceId
	 * @return
	 */
	public Boolean isAllIncomingParallelPathProcessed(String workFlowId, String resourceId, String workFlowInstanceId);

	/**
	 * 
	 * @param taskInstanceQry
	 * @return
	 */
	public Boolean isParallelGateWayExecuted(Map taskInstanceQry);

	/**
	 * 
	 * @param workFlowId
	 * @param resourceId
	 * @param workFlowInstanceId
	 * @param taskInstanceQry
	 * @param taskInstance
	 * @return
	 */
	public Boolean executeParallelGateWay(String workFlowId, String resourceId, String workFlowInstanceId,
			Map taskInstanceQry, Map taskInstance);

}

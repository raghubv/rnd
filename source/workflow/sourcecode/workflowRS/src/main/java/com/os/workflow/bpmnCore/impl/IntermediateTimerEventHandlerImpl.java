package com.os.workflow.bpmnCore.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.os.workflow.bpmnCore.IntermediateTimerEventHandler;
import com.os.workflow.bpmnCore.ScheduleJobHandler;
import com.os.workflow.bpmnCore.UpdateStatusHandler;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.util.Constants;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * IntermediateTimerEventHandlerImpl: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1       Aug 25, 2014		   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class IntermediateTimerEventHandlerImpl implements IntermediateTimerEventHandler {
	@Autowired
	MongoTemplate mongoTemplate;
	private static final Logger log = Logger.getLogger(ScheduleJobHandlerImpl.class);
	@Autowired
	ScheduleJobHandler scheduleJobHandler;
	@Autowired
	UpdateStatusHandler updateStatusHandler;

	public void runTimerJob(DBObject schedulerInstance) {

		ObjectMapper objectMapper = new ObjectMapper();
		try {

			synchronized (schedulerInstance) {

				log.debug("[schedulerInstance]" + schedulerInstance);
				Map req = new HashMap();
				log.debug("[quartzScheduledTimerType]" + schedulerInstance.get(Constants.quartzScheduledTimerType));
				if (((String) schedulerInstance.get(Constants.quartzScheduledTimerType))
						.equalsIgnoreCase(Constants.fallOut)
						|| ((String) schedulerInstance.get(Constants.quartzScheduledTimerType))
								.equalsIgnoreCase(Constants.jeopardy)) {

					if (((String) schedulerInstance.get(Constants.quartzScheduledTimerType))
							.equalsIgnoreCase(Constants.fallOut))
						req.put(Constants.status, Constants._In_FallOut);
					else
						req.put(Constants.status, Constants._In_Error);
					req.put(Constants.taskInstanceId,
							PropertyUtils.getProperty(
									objectMapper.readValue(schedulerInstance.toString(), LinkedHashMap.class),
									Constants._taskInsatancesRefId));

					log.debug("[updateStatus]" + updateStatusHandler);
					schedulerInstance.put(Constants.quartzStatus, Constants.quartzStatusExecuted);
				} else if (((String) schedulerInstance.get(Constants.quartzScheduledTimerType))
						.equalsIgnoreCase(Constants._StartTimerEvent)) {

					req.put(Constants.status, Constants._Completed);
					req.put(Constants.taskInstanceId,
							PropertyUtils.getProperty(
									objectMapper.readValue(schedulerInstance.toString(), LinkedHashMap.class),
									Constants._taskInsatancesRefId));
					log.debug("Calling  update task status");
					schedulerInstance.put(Constants.quartzStatus, Constants.quartzStatusExecuted);
				} else if (((String) schedulerInstance.get(Constants.quartzScheduledTimerType))
						.equalsIgnoreCase(Constants._IntermediateTimerEvent)) {

					req.put(Constants.taskInstanceId,
							PropertyUtils.getProperty(
									objectMapper.readValue(schedulerInstance.toString(), LinkedHashMap.class),
									Constants._taskInsatancesRefId));
					log.debug("[req]" + req);
					if (((String) schedulerInstance.get(Constants.quartzSchedulerType))
							.equalsIgnoreCase(Constants.fallOut))
						req.put(Constants.status, Constants._In_FallOut);
					else if (((String) schedulerInstance.get(Constants.quartzSchedulerType))
							.equalsIgnoreCase(Constants.jeopardy))
						req.put(Constants.status, Constants._In_Error);
					else if (((String) schedulerInstance.get(Constants.quartzSchedulerType))
							.equalsIgnoreCase(Constants.escalation))
						req.put(Constants.status, Constants._Escalation);
					else if (((String) schedulerInstance.get(Constants.quartzSchedulerType))
							.equalsIgnoreCase(Constants.delayTimeOut))
						req.put(Constants.status, Constants._Delay_TimeOut);
					log.debug("[req]" + req);
					if (schedulerInstance.get(Constants.parentTaskInsatancesRef) != null) {
						req.put(Constants.taskInstanceId,
								PropertyUtils.getProperty(
										objectMapper.readValue(schedulerInstance.toString(), LinkedHashMap.class),
										Constants._parentTaskInsatancesRefId));
						log.debug("[updateStatus]" + updateStatusHandler);
						updateStatusHandler.updateTaskStatus(objectMapper.writeValueAsString(req));
					}
					req.put(Constants.status, Constants._Completed);
					req.put(Constants.taskInstanceId,
							PropertyUtils.getProperty(
									objectMapper.readValue(schedulerInstance.toString(), LinkedHashMap.class),
									Constants._taskInsatancesRefId));
					log.debug("[updateStatusHandler]" + updateStatusHandler);
					schedulerInstance.put(Constants.quartzStatus, Constants.quartzStatusExecuted);

				}
				updateStatusHandler.updateTaskStatus(objectMapper.writeValueAsString(req));
				log.debug("[req]" + req);
				log.debug("[schedulerInstance]" + schedulerInstance);
				mongoTemplate.getCollection(Constants.quartzSchedulerInstances).findAndModify(
						new BasicDBObject(Constants._id, schedulerInstance.get(Constants._id)), schedulerInstance);
			}
		} catch (JsonGenerationException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		} catch (JsonMappingException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IOException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		} catch (IllegalAccessException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		} catch (InvocationTargetException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		} catch (NoSuchMethodException e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		}

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.bpmnCore.ScheduleJobHandler#
	 * checkAndScheduleBoundryEvent(java.lang.String, com.mongodb.DBObject,
	 * com.mongodb.DBObject)
	 */
	public void checkAndScheduleBoundryEvent(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException {
		if (type.equalsIgnoreCase(Constants._task)) {
			List<DBObject> outGoingPath = (List<DBObject>) taskDfn.get(Constants._outgoing);
			List<String> resourceIdLs = new ArrayList<String>();
			for (DBObject dbObject : outGoingPath) {
				resourceIdLs.add((String) dbObject.get(Constants.resourceId));
			}
			DBObject boundryEventQuery = new BasicDBObject();
			boundryEventQuery.put(Constants.processRef, taskDfn.get(Constants.processRef));
			boundryEventQuery.put(Constants._taskStencilPath, Constants._IntermediateTimerEvent);
			boundryEventQuery.put(Constants.resourceId, new BasicDBObject(Constants._$in, resourceIdLs));

			log.debug("[boundryEventQuery]" + boundryEventQuery);
			DBCursor dbCursor = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(boundryEventQuery);
			for (DBObject intermediateTaskDfn : dbCursor) {
				log.debug("[dbObject]" + intermediateTaskDfn);
				log.debug("[IntermediateTimerEvent]");
				log.debug(Constants._IntermediateTimerEvent_Properties_Timedate + PropertyUtils
						.getProperty(intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timedate));
				log.debug(Constants._IntermediateTimerEvent_Properties_Timecycle + PropertyUtils
						.getProperty(intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timecycle));

				log.debug(Constants._IntermediateTimerEvent_Properties_Timeduration + PropertyUtils
						.getProperty(intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timeduration));

				DBObject taskInstanceQuery = new BasicDBObject();
				taskInstanceQuery.put(Constants.status, Constants._Waiting_For_Execute);
				taskInstanceQuery.put(Constants.processInstanceRef, taskInstance.get(Constants.processInstanceRef));
				taskInstanceQuery.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(),
						Constants.workFlowTaskDefinitions, intermediateTaskDfn.get(Constants._id)));
				DBObject interMediateTimerEventInstance = mongoTemplate.getCollection(Constants.workFlowTaskInstances)
						.findOne(taskInstanceQuery);
				log.debug("[interMediateTimerEventInstance]" + interMediateTimerEventInstance);
				if (interMediateTimerEventInstance != null) {
					interMediateTimerEventInstance.put(Constants.status, Constants._IN_Execution);
					mongoTemplate.getCollection(Constants.workFlowTaskInstances).findAndModify(taskInstanceQuery,
							interMediateTimerEventInstance);
					scheduleJobHandler.scheduleEvent((ObjectId) interMediateTimerEventInstance.get(Constants._id),
							(ObjectId) taskInstance.get(Constants._id),
							(String) PropertyUtils.getProperty(intermediateTaskDfn,
									Constants._IntermediateTimerEvent_Properties_TimerType),
							(String) PropertyUtils.getProperty(intermediateTaskDfn,
									Constants._IntermediateTimerEvent_Properties_Timeduration),
							Constants._IntermediateTimerEvent);

				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.workflow.bpmnCore.IntermediateTimerEventHandler#
	 * checkAndCancelBoundryEvent(java.lang.String, com.mongodb.DBObject,
	 * com.mongodb.DBObject)
	 */
	public void checkAndCancelBoundryEvent(String type, DBObject taskDfn, DBObject taskInstance)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException {
		if (type.equalsIgnoreCase(Constants._task)) {
			List<DBObject> outGoingData = (List<DBObject>) taskDfn.get(Constants._outgoing);
			List<String> resourceIdLs = new ArrayList<String>();
			for (DBObject dbObject : outGoingData) {
				resourceIdLs.add((String) dbObject.get(Constants.resourceId));
			}
			DBObject boundryEventQuery = new BasicDBObject();
			boundryEventQuery.put(Constants.processRef, taskDfn.get(Constants.processRef));
			boundryEventQuery.put(Constants._taskStencilPath, Constants._IntermediateTimerEvent);
			boundryEventQuery.put(Constants.resourceId, new BasicDBObject(Constants._$in, resourceIdLs));
			log.debug("[boundryEventQuery]" + boundryEventQuery);
			DBCursor dbCursor = mongoTemplate.getCollection(Constants.workFlowTaskDefinitions).find(boundryEventQuery);
			for (DBObject intermediateTaskDfn : dbCursor) {
				if (!((String)PropertyUtils.getProperty( intermediateTaskDfn,Constants._IntermediateTimerEvent_Properties_TimerType))
						.equalsIgnoreCase(Constants.delayTimeOut)) {
					log.debug("[intermediateTaskDfn]" + intermediateTaskDfn);
					log.debug("[IntermediateTimerEvent]");
					log.debug(Constants._IntermediateTimerEvent_Properties_Timedate + PropertyUtils
							.getProperty(intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timedate));
					log.debug(Constants._IntermediateTimerEvent_Properties_Timecycle + PropertyUtils
							.getProperty(intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timecycle));

					log.debug(Constants._IntermediateTimerEvent_Properties_Timeduration + PropertyUtils.getProperty(
							intermediateTaskDfn, Constants._IntermediateTimerEvent_Properties_Timeduration));

					DBObject taskInstanceQuery = new BasicDBObject();
					taskInstanceQuery.put(Constants.processInstanceRef, taskInstance.get(Constants.processInstanceRef));
					taskInstanceQuery.put(Constants.taskRef, new DBRef(mongoTemplate.getDb(),
							Constants.workFlowTaskDefinitions, intermediateTaskDfn.get(Constants._id)));
					DBObject interMediateTimerEventInstance = mongoTemplate
							.getCollection(Constants.workFlowTaskInstances).findOne(taskInstanceQuery);
					log.debug("[interMediateTimerEvent]" + interMediateTimerEventInstance);
					interMediateTimerEventInstance.put(Constants.status, Constants._Completed);
					mongoTemplate.getCollection(Constants.workFlowTaskInstances).findAndModify(taskInstanceQuery,
							interMediateTimerEventInstance);
					scheduleJobHandler.cancelEvent((ObjectId) interMediateTimerEventInstance.get(Constants._id),
							(ObjectId) taskInstance.get(Constants._id),
							(String) PropertyUtils.getProperty(intermediateTaskDfn,
									Constants._IntermediateTimerEvent_Properties_TimerType),
							(String) PropertyUtils.getProperty(intermediateTaskDfn,
									Constants._IntermediateTimerEvent_Properties_Timeduration),
							Constants._IntermediateTimerEvent);
				}
			}
		}
	}
}

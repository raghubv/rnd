package com.os.workflow.scheduler.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.os.workflow.bpmnCore.impl.IntermediateTimerEventHandlerImpl;
import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.scheduler.Scheduler;
import com.os.workflow.util.Constants;
import com.os.workflow.util.Utility;

public class IntermediateTimerProcessorScheduler implements Scheduler {
	private static final Logger log = Logger.getLogger(IntermediateTimerProcessorScheduler.class);

	private static boolean initialLoad = true;

	@Autowired
	ThreadPoolTaskExecutor timerTaskExecutor;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	IntermediateTimerEventHandlerImpl intermediateTimerEventHandler;

	@Override
	public void runScheduler() {
		try {
			DBObject timerQuery = new BasicDBObject();
			timerQuery.put(Constants.quartzScheduledExecutionDate,
					new BasicDBObject(Constants._$lte, Utility.getDate()));
			timerQuery.put(Constants.quartzStatus, Constants.quartzStatusActive);
			if (initialLoad)
				timerQuery.put(Constants.quartzStatus, Constants.quartzStatusInprogress);

			log.debug("[timerQuery]" + timerQuery);
			DBObject sort = new BasicDBObject(Constants._id, -1);
			DBCursor schedulerCursor = mongoTemplate.getCollection(Constants.quartzSchedulerInstances).find(timerQuery)
					.sort(sort);
			for (final DBObject schedulerInstance : schedulerCursor) {
				log.debug("[schedulerInstance]" + schedulerInstance);
				timerTaskExecutor.execute(new Thread() {
					public void run() {
						intermediateTimerEventHandler.runTimerJob(schedulerInstance);
					}
				});
			}
			if (!initialLoad) {
				DBObject update = new BasicDBObject();
				update.put(Constants._$set,
						new BasicDBObject(Constants.quartzStatus, Constants.quartzStatusInprogress));
				mongoTemplate.getCollection(Constants.quartzSchedulerInstances).update(timerQuery, update, false, true);
			}

			initialLoad = false;

		} catch (Exception e) {
			log.debug("[error]" + LoggerTrace.getStackTrace(e));
		}
	}

}

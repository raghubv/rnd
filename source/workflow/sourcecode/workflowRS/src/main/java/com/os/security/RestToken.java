package com.os.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * 
 * <b>Purpose:</b><br>
 * RestToken:To enable basic authentication <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b>
 * Sl No   Modified Date        Author</b>
 * ==============================================
 * 1        Dec 11, 2013		    @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class RestToken extends UsernamePasswordAuthenticationToken {
	/**
	 * 
	 * @param key
	 * @param credentials
	 */
	public RestToken(String key, String credentials) {
		super(key, credentials);
	}

	/**
	 * 
	 * @param key
	 * @param credentials
	 * @param authorities
	 */
	public RestToken(String key, String credentials, Collection<? extends GrantedAuthority> authorities) {
		super(key, credentials, authorities);
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 *  
	* 1. getKey
	 * 
	 * </pre>
	 * 
	 * @return
	 */
	public String getKey() {
		return (String) super.getPrincipal();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.
	 * UsernamePasswordAuthenticationToken#getCredentials()
	 */
	public String getCredentials() {
		return (String) super.getCredentials();
	}
}

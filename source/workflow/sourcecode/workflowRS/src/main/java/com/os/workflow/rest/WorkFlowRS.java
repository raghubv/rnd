package com.os.workflow.rest;

import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.os.workflow.helper.LoggerTrace;
import com.os.workflow.service.Diagram;
import com.os.workflow.service.WorkFlow;
import com.os.workflow.util.Constants;
import com.os.workflow.util.MessageUtility;

/**
 * 
 * <b>Purpose:</b><br>
 * WorkFlowRS <br>
 * <br>
 * <b>RevisionHistory:</b>
 * 
 * <pre>
 * <b> 
 * Sl No Modified Date Author</b> 
 * ==============================================
 * 1 Jan 21, 2014  @author Raghu -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
@Component
@Path("/workflowRS")
public class WorkFlowRS {
	static final Logger log = Logger.getLogger(WorkFlowRS.class);
	static final ResourceBundle bundle = ResourceBundle
			.getBundle("RestService");
	@Autowired
	WorkFlow workFlow;
	@Autowired
	Diagram diagram;

	@PostConstruct
	public void createCollectionIndex() {
		workFlow.createCollectionIndex("");
	}
	
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeJsonService
	 * 
	 * </pre>
	 * 
	 * @param method
	 * @param filename
	 * @return
	 */
	@Path("/json/{method}/{filename}")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeJsonService(@PathParam("method") String method,
			@PathParam("filename") String filename) {
		log.info("[method]" + method + " Begin..");
		log.info("[filename]" + filename );
		String outPut = "{}";
		try {
			outPut = (String) workFlow.getClass()
					.getDeclaredMethod(method, String.class)
					.invoke(workFlow, filename);
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
			outPut = MessageUtility.response(Constants.responseCode.Failed);
		}
		log.debug("[outPut]"+outPut);
		log.info("[method]" + method + " end..");
		return outPut;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeDiagramService
	 * 
	 * </pre>
	 * 
	 * @param method
	 * @param filename
	 * @return
	 */
	@Deprecated
	@Path("/diagram/{method}/{filename}")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeDiagramService(@PathParam("method") String method,
			@PathParam("filename") String filename) {
		log.info("[method]" + method + " begin..");
		log.info("[filename]" + filename + " begin..");
		String outPut = "{}";
		try {
			outPut = (String) diagram.getClass()
					.getDeclaredMethod(method, String.class)
					.invoke(diagram, filename);
		} catch (Exception e) {
			log.error("[Error>]" + LoggerTrace.getStackTrace(e));
			outPut = MessageUtility.response(Constants.responseCode.Failed);
		}
		log.debug("[outPut]"+outPut);
		log.info("[method]" + method + " end..");
		return outPut;
	}

	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. invokeJsonServices
	 * 
	 * </pre>
	 * 
	 * @param method
	 * @param input
	 * @return
	 */
	@Path("/json/{method}")
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String invokeJsonServices(@PathParam("method") String method,
			String input) {
		log.info("[method]" + method + " begin..");
		log.info("[input]" + input);
		String outPut = "{}";
		try {
			outPut = (String) workFlow.getClass()
					.getDeclaredMethod(method, String.class)
					.invoke(workFlow, input);
		} catch (Exception e) {
			log.error("[Error]" + LoggerTrace.getStackTrace(e));
			outPut = MessageUtility.response(Constants.responseCode.Failed);
		}
		log.debug("[outPut]"+outPut);
		log.info("[method]" + method + " end..");
		return outPut;
	}

}
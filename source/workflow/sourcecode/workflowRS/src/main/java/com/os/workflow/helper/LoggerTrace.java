package com.os.workflow.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LoggerTrace {

	public static String getStackTrace(Exception e)
	  {
	    StringWriter stack = new StringWriter();
	    e.printStackTrace(new PrintWriter(stack));
	    return stack.toString();
	  }
}

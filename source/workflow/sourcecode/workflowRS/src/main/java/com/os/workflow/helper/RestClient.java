package com.os.workflow.helper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class RestClient {

	public static final String REST_URI_GET_OFFER = "http://localhost:8080/dpcRS/rest/dpc/getPolicyOffer";
	public static final String REST_URI_ADD_OFFER = "http://localhost:8080/dpcRS/rest/dpc/addOffer";
	public static final String REST_URI_ADD_PRDUCT_AFFINITY = "http://localhost:8080/dpcRS/rest/productRS/product/saveProduct";
	public static final String REST_URI_GET_PRODUCT_AFFINITY = "http://localhost:8080/dpcRS/rest/productRS/product/getProducts";
	static Client client = Client.create();

	public static synchronized String query(String uri, String jsonString) {
		ClientResponse response = null;
		try {
			WebResource webResource = client.resource(uri);
			response = (ClientResponse) webResource.type("application/json")
					.post(ClientResponse.class, jsonString);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());

			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
		}

		return ((String) response.getEntity(String.class));
	}

	public static synchronized String add(String uri, String jsonString) {
		ClientResponse response = null;
		try {
			WebResource webResource = client.resource(uri);
			response = (ClientResponse) webResource.type("application/json")
					.post(ClientResponse.class, jsonString);
			System.out.println("[response]" + response);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

		} catch (RuntimeException runtimeException) {
			System.out.println("runtimeException");
			throw runtimeException;
		} catch (Exception e) {
			System.out.println("exception");
			e.printStackTrace();

		} finally {
		}

		return ((String) response.getEntity(String.class));
	}

	public static synchronized String post(String userName, String password,
			String uri, String jsonString) {
		ClientResponse response = null;
		try {
			WebResource webResource = client.resource(uri);
			webResource.addFilter(new HTTPBasicAuthFilter(userName, password));
			response = (ClientResponse) webResource.type("application/json")
					.post(ClientResponse.class, jsonString);
			System.out.println("[response]" + response);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

		} catch (RuntimeException runtimeException) {
			System.out.println("runtimeException");
			throw runtimeException;

		} catch (Exception e) {
			System.out.println("e");
			e.printStackTrace();

		} finally {
		}

		return ((String) response.getEntity(String.class));
	}

	public static synchronized String post(String uri, String jsonString) {
		ClientResponse response = null;
		try {
			WebResource webResource = client.resource(uri);
			webResource.addFilter(new HTTPBasicAuthFilter("dpc", "password"));
			response = (ClientResponse) webResource.type("application/json")
					.post(ClientResponse.class, jsonString);
			System.out.println("[response.getStatus()]" + response.getStatus());
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

		} catch (RuntimeException runtimeException) {
			System.out.println("runtimeException");
			throw runtimeException;

		} catch (Exception e) {
			System.out.println("[Exception]");
			e.printStackTrace();

		} finally {
		}

		return ((String) response.getEntity(String.class));
	}

	public static synchronized String postJSON(String uri, String jsonString) {
		ClientResponse response = null;
		try {
			WebResource webResource = client.resource(uri);
			response = (ClientResponse) webResource.type("application/json")
					.post(ClientResponse.class, jsonString);
			System.out.println("[response.getStatus()]" + response.getStatus());

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

		} catch (RuntimeException runtimeException) {
			System.out.println("runtimeException");
			throw runtimeException;

		} catch (Exception e) {
			System.out.println("[Exception]");
			e.printStackTrace();

		} finally {
		}

		return ((String) response.getEntity(String.class));
	}
}

/*
@author Raghu B V
@date 13/9/2016
@description  To save transaction information to respective collection
*/
function(id) {
   
       var performOperation=function(doc) {
        var transactions=doc['txn'];
       for (var txn in transactions) {
           performCUD(transactions[txn]);
       }   
            db.getCollection('trans').remove({'txnId':id});
           return {"code":"0","desc":"succcess"};
   };
   /*
   @author Raghu B V
   @date 13/9/2016
   @description  to perform create update delete operation
   */
    var performCUD=function(txn) {
           var operation=txn['operation'];
           var collection=txn['collection'];
           var val=txn['value'];
           var query=txn['query'];
           if(operation=='create')
               db.getCollection(collection).insertOne(val);
           else if(operation=='update')
              db.getCollection(collection).update(query,val);
           else if(operation=='delete')    
              db.getCollection(collection).remove(val);
   };

      var dbObject = db.getCollection('trans').findOne({'txnId':id});
      if(dbObject!=null)
      return performOperation(dbObject);
      else
       return {"code":"1","desc":"failed"};;
}
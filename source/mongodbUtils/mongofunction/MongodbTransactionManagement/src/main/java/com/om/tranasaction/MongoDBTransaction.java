package com.enhancesys.tranasaction;

import java.util.List;

import com.enhancesys.model.Transaction;
/**
 * 
* 
* <b>Purpose:</b><br>
* MongoDBTransaction: <br>
* <br>
* 
* <b>DesignReference:</b><br>
* master-SNAPSHOT<br>
* <br>
* 
* <b>CopyRights:</b><br>
* Enhancesys 2013<br>
* <br>
* 
* <b>RevisionHistory:</b>
* 
* <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
* 
* </pre>
* 
* <br>
 */
public interface MongoDBTransaction {
	/*
	 * To load the JS function mongodb
	 */
	public void loadScript();
	
	/*
	 * To begin the transaction
	 */
	public MongoDBTransaction beginTransaction();
	/*
	 * To save the transaction to mongodb
	 */
	public void saveTransaction(List<Transaction> transactionList);
	/*
	 * To commit the transaction
	 */
	public void commitTransaction();
	/*
	 * To rollback the transaction
	 */
	public void rollBackTransaction();

}

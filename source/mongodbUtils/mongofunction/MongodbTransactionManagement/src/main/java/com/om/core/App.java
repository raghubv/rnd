package com.enhancesys.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.config.SpringMongoConfig;
import com.enhancesys.model.Transaction;
import com.enhancesys.tranasaction.MongoDBTransaction;
import com.enhancesys.tranasaction.impl.MongoDBTransactionImpl;
import com.mongodb.BasicDBObject;

/**
 * 
 * @author Enhancesys-Raghu
 *
 */
public class App {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
    
		
		MongoDBTransaction mongoTransaction= new MongoDBTransactionImpl(mongoOperation);
		List<Transaction> ls= new ArrayList<Transaction>();
			
		/*
		mongoTransaction.beginTransaction();
		for(int i=0;i<1000;i++){
			//Transaction trans =Transaction.delete("testcs", new BasicDBObject("key", i));
			
			Transaction trans =Transaction.create("testcs", new BasicDBObject("key", i));
			
			//Transaction trans =Transaction.update("testcs", new BasicDBObject("key", i),new BasicDBObject("key", i+2000) );
		    ls.add(trans);
		}
		
		
		mongoTransaction.saveTransaction(ls);
		
		
		mongoTransaction.commitTransaction();
		
		*/
		
		
		
	
		
		/*
		mongoTransaction.beginTransaction();
		ls= new ArrayList<Transaction>();
		for(int i=0;i<1000;i++){
			//Transaction trans =Transaction.delete("testcs", new BasicDBObject("key", i));
			
			//Transaction trans =Transaction.create("testcs", new BasicDBObject("key", i));
			
			Transaction trans =Transaction.update("testcs", new BasicDBObject("key", i),new BasicDBObject("key", i+2000) );
		    ls.add(trans);
		}
		
		
		mongoTransaction.saveTransaction(ls);
		
		
		mongoTransaction.commitTransaction();
		*/
		
		
		//mongoTransaction.loadScript();
		
		mongoTransaction.beginTransaction();
		ls= new ArrayList<Transaction>();
		for(int i=0;i<10000;i++){
			//Transaction trans =Transaction.delete("testcs", new BasicDBObject("key", i));
			
			Transaction trans =Transaction.create("testcs", new BasicDBObject("key", i));
			
			//Transaction trans =Transaction.update("testcs", new BasicDBObject("key", i),new BasicDBObject("key", i+2000) );
		    ls.add(trans);
		}
		
		
		mongoTransaction.saveTransaction(ls);
		
		
		mongoTransaction.commitTransaction();
		
		
	}
}
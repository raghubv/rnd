package com.enhancesys.model;

/**
 * 
* 
* <b>Purpose:</b><br>
* Transaction: <br>
* <br>
* 
* <b>DesignReference:</b><br>
* master-SNAPSHOT<br>
* <br>
* 
* <b>CopyRights:</b><br>
* Enhancesys 2013<br>
* <br>
* 
* <b>RevisionHistory:</b>
* 
* <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
* 
* </pre>
* 
* <br>
 */
public class Transaction {

	private String collection = null;
	private String operation = null;
	private Object queryObject = null;
	private Object objectValue = null;

	
	public String getCollection() {
		return collection;
	}

	public String getOperation() {
		return operation;
	}

	public Object getQueryObject() {
		return queryObject;
	}

	public Object getObjectValue() {
		return objectValue;
	}

	public Transaction(String collection, String operation, Object objectValue) {
		super();
		this.collection = collection;
		this.operation = operation;
		this.objectValue = objectValue;
	}

	public Transaction(String collection, String operation, Object queryObject, Object objectValue) {
		super();
		this.collection = collection;
		this.operation = operation;
		this.queryObject = queryObject;
		this.objectValue = objectValue;
	}

	public static synchronized Transaction create(String collectionName, Object objectValue) {
		return new Transaction(collectionName, "create", objectValue);
	}

	public static synchronized Transaction delete(String collectionName, Object queryObject) {
		return new Transaction(collectionName, "delete", queryObject);
	}

	public static synchronized Transaction update(String collectionName, Object queryObject, Object objectValue) {
		return new Transaction(collectionName, "update", queryObject, objectValue);
	}

	
	
}

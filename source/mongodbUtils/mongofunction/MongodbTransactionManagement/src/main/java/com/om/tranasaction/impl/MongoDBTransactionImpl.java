package com.enhancesys.tranasaction.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.types.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.model.Transaction;
import com.enhancesys.tranasaction.MongoDBTransaction;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
/**
 * 
* 
* <b>Purpose:</b><br>
* MongoDBTransactionImpl: <br>
* <br>
* 
* <b>DesignReference:</b><br>
* master-SNAPSHOT<br>
* <br>
* 
* <b>CopyRights:</b><br>
* Enhancesys 2013<br>
* <br>
* 
* <b>RevisionHistory:</b>
* 
* <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
* 
* </pre>
* 
* <br>
 */
public class MongoDBTransactionImpl implements MongoDBTransaction {

	private MongoOperations mongoOperation = null;
	private DBObject dbObject = null;
	final static Logger logger = LoggerFactory.getLogger(MongoDBTransactionImpl.class);

	public MongoDBTransactionImpl(MongoOperations mongoOperation) {
		super();
		this.mongoOperation = mongoOperation;
	}

	/*
	 * (non-Javadoc)
	 * @see com.enhancesys.tranasaction.MongoDBTransaction#loadScript()
	 */
	public void loadScript() {

		logger.info("begin..");

		try {
			InputStream in = getClass().getResourceAsStream("/executeTransaction.js");
			StringWriter writer = new StringWriter();
			IOUtils.copy(in, writer, "UTF-8");
			Code functionCode = new Code(writer.toString());
			logger.debug("[functionCode]"+functionCode);
			DBObject doc = new BasicDBObject("_id", "executeTransaction").append("value", functionCode);
			mongoOperation.getCollection("system.js").update(new BasicDBObject("_id", "executeTransaction"), doc, true,
					false);
		} catch (IOException e) {
			logger.error("[error]"+e);
			e.printStackTrace();
		}
		logger.info("end..");


	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.MongoDBTransaction#beginTransaction()
	 */
	public MongoDBTransaction beginTransaction() {
		logger.info("begin...");
		dbObject = new BasicDBObject();
		dbObject.put("txnId", (new Date()).getTime());
		mongoOperation.getCollection("trans").insert(dbObject);
		logger.debug("[dbObject]"+dbObject);
		logger.info("end...");
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.MongoDBTransaction#commitTransaction()
	 */
	public void commitTransaction() {
		logger.info("begin..");
		mongoOperation.executeCommand(new BasicDBObject("$eval", "db.loadServerScripts()"));
		CommandResult res = mongoOperation
				.executeCommand(new BasicDBObject("$eval", "executeTransaction(" + dbObject.get("txnId") + ")"));
		logger.debug("[res]" + res);
		logger.info("end..");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.tranasaction.MongoDBTransaction#saveTransaction(java.util.
	 * List)
	 */
	public void saveTransaction(List<Transaction> transactionList) {
		logger.info("begin..");
		List<DBObject> transactionLs = new ArrayList<DBObject>();
		for (Transaction transaction : transactionList) {

			DBObject dbObject = new BasicDBObject();
			dbObject.put("collection", transaction.getCollection());
			dbObject.put("operation", transaction.getOperation());
			dbObject.put("value", transaction.getObjectValue());
			dbObject.put("query", transaction.getQueryObject());
			transactionLs.add(dbObject);

		}
		dbObject.put("txn", transactionLs);
		logger.debug("[dbObject]"+dbObject);
		mongoOperation.getCollection("trans").update(new BasicDBObject("txnId", dbObject.get("txnId")), dbObject);
		logger.info("end..");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.MongoDBTransaction#rollBackTransaction()
	 */
	public void rollBackTransaction() {
		logger.info("begin..");
		mongoOperation.getCollection("trans").remove(new BasicDBObject("txnId", dbObject.get("txnId")));
		logger.info("end..");
		
	}

}

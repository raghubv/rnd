/*
@author Raghu B V
@date 13/9/2016
@description  To save transaction information to respective collection
*/
function(id) {
	
	var operationCount=0;
	var keys={'dleq':'$eq','dllte':'$lte','dllt':'$lt','dlgt':'$gt','dlset':'$set'};
	var placeDBKeys=function(doc) {
            for (var prop in doc) {
            var propValue=  doc[prop];
            var key=(prop in keys)? keys[prop]:prop; 
            var keyVal=(propValue in keys)? keys[propValue]:propValue; 
            if(propValue instanceof Array || propValue instanceof Object){
                if(prop in keys){
                doc[key]=placeDBKeys(propValue);
                }else{
                doc[prop]=placeDBKeys(propValue);
                }   
            }else
                doc[key]=keyVal;
           }
         return doc;
    };
    var removeKeys=function(doc){
        for(var prop in doc){
            var propValue=  doc[prop];
           
            if(prop in keys){
                delete doc[prop];
                delete doc.prop;
           }else if(propValue instanceof Array || propValue instanceof Object)
                doc[prop] =removeKeys(propValue);
        }     
        return doc;
        
    }; 
  var getDBKeys=function(doc){
	  var res=removeKeys(placeDBKeys(doc));
	  return res;
  };
	
	
	var splitTransaction=function() {
		var _transCount=10;
		var count=db.getCollection('trans').find({'tId':id,'txn':{$exists: true}}).count();
		var  max=((count-(count%_transCount))/_transCount) + ((count%_transCount>0)?1:0) ; 
		for(i=0;i<max;i++){
		 var transArray = db.getCollection('trans').find({'tId':id,'txn':{$exists: true}}).sort({'_id':-1}).limit(_transCount).toArray();
	      for(var txn in transArray){
	    	  var doc=transArray[txn];
	    	  var transactions=doc['txn'];
	    	  insertTransactionData(transactions);
	    	  db.getCollection('trans').remove(doc['_id']);
	    	 
	    	 
	      }
		} 
		       		
					
	};
	
    var performCommitOperation=function() {
			var _transCount=1000;
			var count=db.getCollection('trans').find({'tId':id,"s":"I",'txn':{$exists: false}}).count();
			var  max=((count-(count%_transCount))/_transCount) + ((count%_transCount>0)?1:0) ; 
			for(i=0;i<max;i++){
	   		var dbCursor = db.getCollection('trans').find({'tId':id,"s":"I",'txn':{$exists: false}}).sort({'i':1}).limit(_transCount).toArray();
			for(var index in dbCursor){
				var dbObject=dbCursor[index];
				//db.getCollection('trans').update({'_id':dbObject['_id']},{"$set":{"s":"E"}});
				var oldVersion=performCUD(dbObject);
				dbObject['s']="C";
				dbObject['v']=oldVersion;
				db.getCollection('transc').insert(dbObject);
				db.getCollection('trans').remove({'_id':dbObject['_id']});
				//db.getCollection('trans').update({'_id':dbObject['_id']},{"$set":{"v":oldVersion,"s":"C"}});
			}
			}
			
			return true;
   };
    var insertTransactionData=function(transactions) {
    	var trans = db.trans.initializeOrderedBulkOp();
    	for (var txn in transactions){
    		var txnDoc=transactions[txn];
    		txnDoc['tId']=id;	
    		txnDoc['s']="I";
    		txnDoc['i']=operationCount;
    	    trans.insert(txnDoc);
    	    operationCount++;
    	}
    	trans.execute();
   };
   var performCUD=function(txn) {
		   var oldVersion=[];	
           var operation=txn['op'];
           var collection=txn['c'];
	       var val=getDBKeys(txn['o']);
	       var query=getDBKeys(txn['q']);
           if(operation=='c'){
				var inserted=db.getCollection(collection).insertOne(val).insertedId;
               oldVersion.push({"_id":inserted}); 
		  }else if(operation=='u'){
              	oldVersion=db.getCollection(collection).find(query).toArray();
				db.getCollection(collection).update(query,val);
		  }else if(operation=='ua'){
			var upsert=txn['u'];
			var multi=txn['m'];
			oldVersion=db.getCollection(collection).find(query).toArray();
			db.getCollection(collection).update(query,val,upsert,multi);
	   	  }else if(operation=='d'||operation=='da'){
				oldVersion=db.getCollection(collection).find(query).toArray();	   
                db.getCollection(collection).remove(query);
		  }	 
		operationCount--;
	
          
           return oldVersion;
	};
	
	var revert=function(e){
		var count=db.getCollection('transc').find({'tId':id,"s":"c",'txn':{$exists: false},'i':{'$lte':operationCount}}).count();
		var  max=((count-(count%100))/100) + ((count%100<100)?1:0) ; 
		for(i=0;i<max;i++){
		 var cursor = db.getCollection('transc').find({'tId':id,'s':'C','txn':{$exists: false},'i':{'$lte':operationCount}}).sort({i:-1}).limit(100).toArray()
		 for(var dbObj in cursor){
		 var obj=cursor[dbObj];
		 	revertCUD(obj);
		 }
		}
		 var message ="Transaction failed due to "+e;
		 return {"code":1,"desc":message,"stackTrace":e.stack};
	}
	
	 var revertCUD=function(txn) {
			var operation=txn['op'];
           	var collection=txn['c'];
           	var val=txn['o'];
           	var query=txn['q'];
        	var oldVersion=txn['v'];	
          	if(operation=='c'){
				for(var index in oldVersion)
					db.getCollection(collection).remove(oldVersion[index]['_id']);
		  	}else if(operation=='u'||operation=='ua'){
            	for(var index in oldVersion)
					db.getCollection(collection).update(oldVersion[index]['_id'],oldVersion[index]);
		  	}else if(operation=='d'||operation=='da'){
				for(var index in oldVersion)
					db.getCollection(collection).insert(oldVersion[index]);	   
          }	
          txn['s']='R';
		  db.getCollection('transc').update({'_id':txn['_id']},txn);	
		  operationCount--;
    };

      
      
      try{
    	  print("Transaction begin");
    	  splitTransaction();
    	  //performCommitOperation();
    	  if(operationCount==0){
		 		//db.getCollection('trans').remove({'tId':id})
		 		return {"code":0,"desc":"Transaction success"};
	      }else
      		throw new transactionException("Transaction Opearation failed");	
    	  print("transaction end")
      }catch (e) {
    	print("[ERROR]"+e.stack);  
  		print("[tId]"+id);
  		print("[reverting...]");
  		return revert(e);
  	}
        
    
      function transactionException(message) {
    	   this.message = message;
    	   this.stack = "Transaction Exception";
    	}
      
}
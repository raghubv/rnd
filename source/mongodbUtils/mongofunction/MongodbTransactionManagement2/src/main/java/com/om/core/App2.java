package com.enhancesys.core;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.config.SpringMongoConfig;
import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.MongoUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * App: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class App2 {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
		
		   DBCollection collection = mongoOperation.getCollection("testData");
		   System.out.println(">>"+(new Date()));
		  //Set up the transaction
	        BasicDBObject transaction = new BasicDBObject();
	        transaction.append("beginTransaction", 1);
	        transaction.append("isolation", "mvcc");
	        mongoOperation.executeCommand(transaction);

	       for(int i=0;i<1000000;i++){
	        collection.insert(  (new BasicDBObject()).append("batch", i).append("key", i));
	       }
	        //Commit the transactions
	        BasicDBObject commitTransaction = new BasicDBObject();
	        commitTransaction.append("commitTransaction", 1);
	        mongoOperation.executeCommand(commitTransaction);
	        System.out.println(">>"+(new Date()));
	}
}
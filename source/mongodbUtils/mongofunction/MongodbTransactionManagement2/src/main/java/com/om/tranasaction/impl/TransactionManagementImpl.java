package com.enhancesys.tranasaction.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.types.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.Constants;
import com.enhancesys.utils.MongoUtility;
import com.github.fakemongo.Fongo;
import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class TransactionManagementImpl implements TransactionManagement {
	private MongoOperations mongoOperation = null;
	private Long txnId = (new Date()).getTime();
	private List transactionList = new ArrayList();
	private Fongo fongo = new Fongo(Constants._cache);
	private DB db = fongo.getDB(Constants._transactionDB);
	final static Logger logger = LoggerFactory.getLogger(TransactionManagementImpl.class);
	private boolean transactionSnapShot = false;

	public TransactionManagementImpl(MongoOperations mongoOperation, boolean transactionSnapShot) {
		super();
		this.mongoOperation = mongoOperation;
		this.transactionSnapShot = transactionSnapShot;
		mongoOperation.getCollection(Constants._trans).createIndex((new BasicDBObject()).append(Constants._txnId, 1)
				.append(Constants._status, 1));
		mongoOperation.getCollection("temp").createIndex((new BasicDBObject()).append(Constants._txnId, 1));
	}

	@Override
	public DBObject findOne(String collectionName, DBObject qryObject) throws Exception {
		DBObject dbObject = null;
		if (transactionSnapShot) {
			dbObject = db.getCollection(collectionName).findOne(qryObject);
			if (dbObject == null)
				dbObject = mongoOperation.getCollection(collectionName).findOne(qryObject);
		} else {
			dbObject = mongoOperation.getCollection(collectionName).findOne(qryObject);
		}
		return dbObject;
	}

	@Override
	public DBObject insertOne(String collectionName, DBObject object) throws Exception {
		DBObject dbObject = null;

		dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._create);
		dbObject.put(Constants._value, object);
		if (transactionSnapShot) {
			if (db.getCollection(collectionName).insert(object).getN() != 1)
				throw new Exception(Constants._insert_error);
		}
		transactionList.add(dbObject);
		return object;
	}

	@Override
	public DBObject updateOne(String collectionName, DBObject objectQry, DBObject object) throws Exception {
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._update);
		dbObject.put(Constants._value, object);
		dbObject.put(Constants._query, objectQry);
		if (transactionSnapShot) {
			DBCollection collection = db.getCollection(collectionName);
			if (collection.update(objectQry, object).getN() == 0) {
				DBObject dbobject = mongoOperation.getCollection(collectionName).findOne(objectQry);
				if (dbobject != null)
					if (collection.insert(dbobject).getN() == 1)
						if (collection.update(objectQry, object).getN() == 0)
							throw new Exception(Constants._update_error);
			}
		}
		transactionList.add(MongoUtility.convertToDBObject(dbObject));
		return null;
	}

	@Override
	public DBObject deleteOne(String collectionName, DBObject object) throws Exception {
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._delete);
		dbObject.put(Constants._query, object);
		if (transactionSnapShot) {
			DBCollection collection = db.getCollection(collectionName);
			if (collection.findOne(object) != null)
				if (collection.remove(object).getN() != 1)
					throw new Exception(Constants._delete_error);
		}
		transactionList.add(dbObject);
		return object;
	}

	@Override
	public DBObject delete(String collectionName, DBObject object) throws Exception {
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._deleteAll);
		dbObject.put(Constants._query, object);
		if (transactionSnapShot) {
			DBCollection collection = db.getCollection(collectionName);
			if (collection.find(object) != null)
				if (collection.remove(object).getN() == 0)
					throw new Exception(Constants._delete_error);
		}
		transactionList.add(dbObject);
		return object;
	}

	@Override
	public DBObject commitTransaction() throws Exception {
		// mongoOperation
		// .executeCommand(new BasicDBObject(Constants._$eval,
		// Constants._fsyncUnlock));
		loadScript();
		mongoOperation.executeCommand(new BasicDBObject(Constants._$eval, Constants._loadServerScripts));
		CommandResult res = mongoOperation
				.executeCommand(new BasicDBObject(Constants._$eval, Constants._executeTransaction + "(" + txnId + ")"));
		db.dropDatabase();
		logger.info("[res]" + res);

		return res;
	}

	@Override
	public DBObject rollBackTransaction() {
		db.dropDatabase();
		transactionList.clear();
		return null;
	}

	private void loadScript() {
		logger.info("begin..");
		try {
			InputStream in = getClass().getResourceAsStream(Constants._jsPath);
			StringWriter writer = new StringWriter();
			IOUtils.copy(in, writer, Constants._UTF8);
			Code functionCode = new Code(writer.toString());
			logger.debug("[functionCode]" + functionCode);
			DBObject doc = new BasicDBObject(Constants._id, Constants._executeTransaction)
					.append(Constants._functionValue, functionCode);
			mongoOperation.getCollection(Constants._systemJS)
					.update(new BasicDBObject("_id", Constants._executeTransaction), doc, true, false);
		} catch (IOException e) {
			logger.error("[error]" + e);
			e.printStackTrace();
		}
		logger.info("end..");

	}

	@Override
	public DBObject pushTransaction() throws Exception {
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._txnId, txnId);
		dbObject.put(Constants._txn, transactionList);
		if (mongoOperation.getCollection(Constants._trans).insert(dbObject).getN() != 0)
			throw new Exception(Constants._push_error);
		transactionList.clear();
		return (new BasicDBObject(Constants._code, 0));
	}

	@Override
	public DBObject find(String collectionName, DBObject qryObject) throws Exception {
		logger.info("begin..");
		List<DBObject> dbLs = new ArrayList<DBObject>();
		List<DBObject> ls = new ArrayList<DBObject>();
		if (transactionSnapShot)
			dbLs = db.getCollection(collectionName).find(qryObject).toArray();
		if (dbLs.size() == 0) {
			ls = mongoOperation.getCollection(collectionName).find(qryObject).toArray();
			dbLs.addAll(dbLs);
		}
		if (transactionSnapShot) {
			BulkWriteOperation bwo = db.getCollection(collectionName).initializeOrderedBulkOperation();
			for (DBObject dbObject : ls)
				bwo.insert(dbObject);
			bwo.execute();
		}
		logger.info("end..");
		return new BasicDBObject(Constants._response, dbLs);
	}

	@Override
	public DBObject update(String collectionName, DBObject query, DBObject object, boolean upsert, boolean multi)
			throws Exception {
		logger.info("begin..");
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._updateAll);
		dbObject.put(Constants._value, object);
		dbObject.put(Constants._query, query);
		dbObject.put(Constants._multi, multi);
		dbObject.put(Constants._upsert, upsert);
		if (transactionSnapShot) {
			if (db.getCollection(collectionName).update(query, object, upsert, multi).getN() == 0) {
				List<DBObject> ls = mongoOperation.getCollection(collectionName).find(query).toArray();
				BulkWriteOperation bwo = db.getCollection(collectionName).initializeOrderedBulkOperation();
				for (DBObject dbObj : ls) {
					bwo.insert(dbObj);
					logger.debug("[dbObj]" + dbObj);
				}
				bwo.execute();
				db.getCollection(collectionName).update(query, object, upsert, multi);
			}

		}
		logger.debug("[dbObject]" + dbObject);
		transactionList.add(MongoUtility.convertToDBObject(dbObject));

		logger.info("end..");
		return new BasicDBObject(Constants._code, 0);
	}

}

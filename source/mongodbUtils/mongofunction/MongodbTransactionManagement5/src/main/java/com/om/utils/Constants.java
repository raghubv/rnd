package com.enhancesys.utils;
/**
 * 
 * 
 * <b>Purpose:</b><br>
 * MongoUtility: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Constants {
	public static final String _collection="c";
	public static final String _operation="op";
	public static final String _value="o";
	public static final String _functionValue="value";
	public static final String _query="q";
	public static final String _version="v";
	public static final String _cache="cache";
	public static final String _multi="m";
	public static final String _upsert="us";
	public static final String _transactionDB="transactionDB";
	
	public static final String _create="c";
	public static final String _update="u";
	public static final String _updateAll="ua";
	public static final String _delete="d";
	public static final String _deleteAll="da";
	
	public static final String _trans="trans";
	public static final String _transId="transId";
	
	public static final String _insert_error= "record is not inserted";
	public static final String _update_error= "record is not updated";
	public static final String _delete_error= "record is not deleted";
	public static final String _push_error= "transaction is pushed to server";
	public static final String _find_error = "no record is available";
	public static final String _revert_success = "revert success";
	public static final String _revert_failed = "revert failed";
	public static final String _message = "message";
	public static final String _nOp = "nOp";
	
	
	public static final String _message_Push_Success="trans";
	
	public static final String _$eval="$eval";
	public static final String _loadServerScripts="db.loadServerScripts()";
	public static final String _executeTransaction= "executeTransaction";
	public static final String _fsyncLock= "db.fsyncLock()";
	public static final String _fsyncUnlock= "db.fsyncUnlock()";
	
	
	public static final String _txnId="tId";
	public static final String _index="i";
	public static final String _status="s";
	public static final String _code="code";
	public static final String _response="response";
	public static final String _txn="txn";
	public static final String _UTF8="UTF-8";
	public static final String _id="_id";
	public static final String _jsPath= "/executeTransaction.js";
	public static final String _systemJS= "system.js";
	
	

}

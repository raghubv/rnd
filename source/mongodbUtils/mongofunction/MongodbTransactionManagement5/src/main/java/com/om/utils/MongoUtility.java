package com.enhancesys.utils;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.tranasaction.impl.TransactionManagementImpl;
import com.mongodb.MongoClient;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * MongoUtility: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class MongoUtility {
	/**
	 * 
	 * @param mongoClient
	 * @param dbName
	 * @return
	 */
	public static TransactionManagement beginTransaction(MongoClient mongoClient, String dbName) {
		return new TransactionManagementImpl(mongoClient,dbName);
	}
	
	
	

	
}

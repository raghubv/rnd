package com.enhancesys.tranasaction;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * MongoDBTransaction: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public interface TransactionManagement {

	/**
	 * 
	 * @param collectionName
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public DBObject insert(String collectionName, DBObject object) throws Exception;

	/**
	 * 
	 * @param collectionName
	 * @param query
	 * @param object
	 * @param upsert
	 * @param multi
	 * @return
	 * @throws Exception
	 */
	public DBObject update(String collectionName, DBObject query, DBObject object, boolean upsert, boolean multi)
			throws Exception;

	/**
	 * 
	 * @param collectionName
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public DBObject delete(String collectionName, DBObject query) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBObject commitTransaction() throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBObject rollBackTransaction();

	public Long getTxnId();

	public void setTxnId(Long txnId);

	public DBCollection getCollection(String collectionName) throws Exception;

}

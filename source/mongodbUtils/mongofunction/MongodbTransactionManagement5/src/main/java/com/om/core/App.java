package com.enhancesys.core;

import java.net.UnknownHostException;
import java.util.Date;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.MongoUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * App: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class App {

	public static void main(String[] args) {

		// Pass mongodb connection object

		MongoClient md = null;
		try {
			md = new MongoClient("localhost");
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		TransactionManagement mongoDBTransactionManagement = MongoUtility.beginTransaction(md, "snoc");
		try {

			int m = 100000, k= 100;
			mongoDBTransactionManagement.insert("Collection02",
					(new BasicDBObject()).append("batch", k).append("key", m));
//			for (int k = 0; k < 10; k++) {
//				System.out
//						.println("###################################################################################");
//				for (int i = 0; i < 10; i++) {
//					mongoDBTransactionManagement.insert("Collection02",
//							(new BasicDBObject()).append("batch", k).append("key", m));
//					m++;
//				}
//			}
			System.out.println(new Date());
			mongoDBTransactionManagement.commitTransaction();
			// mongoDBTransactionManagement.rollBackTransaction();
			System.out.println(new Date());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mongoDBTransactionManagement.rollBackTransaction();
		}

	}
}
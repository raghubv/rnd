package com.enhancesys.tranasaction.impl;

import com.mongodb.DBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * Transaction: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class Transaction {

	private String collection = null;
	private String operation = null;
	private DBObject queryObject = null;
	private DBObject objectValue = null;
	private boolean upsert = false;
	private boolean multi = false;

	public String getCollection() {
		return collection;
	}

	public String getOperation() {
		return operation;
	}

	public DBObject getQueryObject() {
		return queryObject;
	}

	public DBObject getObjectValue() {
		return objectValue;
	}

	public boolean isUpsert() {
		return upsert;
	}

	public void setUpsert(boolean upsert) {
		this.upsert = upsert;
	}

	public boolean isMulti() {
		return multi;
	}

	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	public Transaction(String collection, String operation, DBObject objectValue) {
		super();
		this.collection = collection;
		this.operation = operation;
		this.objectValue = objectValue;
	}

	public Transaction(String collection, String operation, DBObject queryObject, DBObject objectValue, boolean upsert,
			boolean multi) {
		super();
		this.collection = collection;
		this.operation = operation;
		this.queryObject = queryObject;
		this.objectValue = objectValue;
		this.upsert = upsert;
		this.multi = multi;
	}

}

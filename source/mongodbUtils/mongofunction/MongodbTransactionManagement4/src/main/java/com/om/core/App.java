package com.enhancesys.core;

import java.net.UnknownHostException;
import java.util.Date;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.MongoUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * App: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class App {

	public static void main(String[] args) {

		
		// Pass mongodb connection object

		MongoClient md = null;
		try {
			md = new MongoClient("localhost");
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		TransactionManagement mongoDBTransactionManagement = MongoUtility.beginTransaction(md,"tetttt");

		int initial = 0, max = 100;

		try {

//			System.out.println("###################################################################################");
//			System.out.println(" Count test ");
//			System.out.println("###################################################################################");
//			System.out.println("Count >> " + mongoDBTransactionManagement.getCollection("test").find().count());
//
//			System.out.println("###################################################################################");
//			System.out.println(" distinct test ");
//			System.out.println("###################################################################################");
//			System.out.println("distinct >> " + mongoDBTransactionManagement.getCollection("test").distinct("batch"));
//
//			System.out.println("###################################################################################");
//			System.out.println(" aggregate test ");
//			System.out.println("###################################################################################");
//			System.out.println("aggregate >> " + mongoDBTransactionManagement.getCollection("test")
//					.aggregate(new BasicDBObject("$group", new BasicDBObject("_id", "$batch"))));

			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			// }
			// System.out.println("###################################################################################");
			// System.out.println(" Insert one ");
			// System.out.println("###################################################################################");
			// for(int j=0;j<3;j++)
			// for (int i = initial; i < 1000; i++) {
			// mongoDBTransactionManagement.insertOne("test", (new
			// BasicDBObject()).append("key", i));
			// //mongoDBTransactionManagement.insertOne("test1", (new
			// BasicDBObject()).append("key", i));
			// //mongoDBTransactionManagement.insertOne("test1", (new
			// BasicDBObject()).append("key", i));
			//
			// }
			// mongoDBTransactionManagement.pushTransaction();
			//// System.out.println("###################################################################################");
			// System.out.println(" find one ");
			// System.out.println("###################################################################################");
			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			//
			// }
			// System.out.println("###################################################################################");
			// System.out.println(" Update one ");
			// System.out.println("###################################################################################");
			//
			// for (int i = initial; i < max; i++) {
			// mongoDBTransactionManagement.updateOne("test", (new
			// BasicDBObject()).append("key", i),
			// (new BasicDBObject()).append("key", i).append("val", i ));
			// }
			//
			// System.out.println("###################################################################################");
			// System.out.println(" find one ");
			// System.out.println("###################################################################################");
			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			//
			// }

			// mongoDBTransactionManagement.pushTransaction();

			// System.out.println("###################################################################################");
			// System.out.println(" Update ");
			// System.out.println("###################################################################################");

			// mongoDBTransactionManagement.insert("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1));
			// mongoDBTransactionManagement.update("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1),
			// (new BasicDBObject()).append("$set", new BasicDBObject("date",
			// new Date())), false, true);
			// mongoDBTransactionManagement.delete("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1));

			int m = 1010000;
			for (int k = 0; k < 10; k++) {
				System.out
						.println("###################################################################################");
				for (int i = 0; i < 10; i++) {
					mongoDBTransactionManagement.update("test", new BasicDBObject("key",m), new BasicDBObject("$set", new BasicDBObject("date" , new Date())), true, true);
					// mongoDBTransactionManagement.update("test",
					// (new BasicDBObject()).append("batch", k).append("key",
					// m),
					// (new BasicDBObject()).append("$set", new
					// BasicDBObject("date", new Date())), false, true);

					// mongoDBTransactionManagement.insert("test",
					// (new BasicDBObject()).append("batch", k).append("key",
					// m));
					// mongoDBTransactionManagement.update("test",
					// (new BasicDBObject()).append("batch", k).append("key",
					// m),
					// (new BasicDBObject("$set", new BasicDBObject("date", new
					// Date()))), false, false);
					m++;
					// // mongoDBTransactionManagement.insertOne("test1", (new
					// // BasicDBObject()).append("batch", k).append("key", i));
					// // mongoDBTransactionManagement.update("test",
					// // (new BasicDBObject()).append("batch", k).append("key",
					// // i),
					// // (new BasicDBObject("$set", new BasicDBObject("value",
					// new
					// // Date()))), false, false);
					// // mongoDBTransactionManagement.update("test1",
					// // (new BasicDBObject()).append("batch", k).append("key",
					// // i),
					// // (new BasicDBObject("$set", new BasicDBObject("value",
					// new
					// // Date()))), false, false);
					// //
					//
				}

				// mongoDBTransactionManagement.pushTransaction();
				//
				//
				//// for (int i = 0; i < 5000; i++) {
				//// mongoDBTransactionManagement.update("test", (new
				// BasicDBObject()).append("batch", k).append("key", m),(new
				// BasicDBObject("$set",new BasicDBObject("date",new
				// Date()))),false, false);
				//// m++;
				////// mongoDBTransactionManagement.insertOne("test1", (new
				// BasicDBObject()).append("batch", k).append("key", i));
				////// mongoDBTransactionManagement.update("test",
				////// (new BasicDBObject()).append("batch", k).append("key",
				// i),
				////// (new BasicDBObject("$set", new BasicDBObject("value", new
				// Date()))), false, false);
				////// mongoDBTransactionManagement.update("test1",
				////// (new BasicDBObject()).append("batch", k).append("key",
				// i),
				////// (new BasicDBObject("$set", new BasicDBObject("value", new
				// Date()))), false, false);
				//////
				////
				//// }
				////
				//// mongoDBTransactionManagement.pushTransaction();
			}
			System.out.println(new Date());
			mongoDBTransactionManagement.commitTransaction();
			// mongoDBTransactionManagement.rollBackTransaction();
			System.out.println(new Date());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mongoDBTransactionManagement.rollBackTransaction();
		}

	}
}
package com.enhancesys.tranasaction.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.Constants;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

public class TransactionManagementImpl implements TransactionManagement {
	
	
	private DB db = null;
	private Long txnId = (new Date()).getTime();

	final static Logger logger = LoggerFactory.getLogger(TransactionManagementImpl.class);

	public TransactionManagementImpl(MongoClient mongoClient, String dbName) {
		super();
		this.db = mongoClient.getDB(dbName);
		db.getCollection(Constants._trans).createIndex((new BasicDBObject()).append(Constants._txnId, 1));

		try {
			while (db.getCollection(Constants._transId).find().count() > 0) {
				synchronized (this) {
					this.wait(3000);
				}
			}
			db.getCollection(Constants._transId).insert(new BasicDBObject(Constants._txnId, txnId));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public DBCollection getCollection(String collectionName) throws Exception {
		return db.getCollection(collectionName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#insert(java.lang.
	 * String, com.mongodb.DBObject)
	 */
	public DBObject insert(String collectionName, DBObject object) throws Exception {
		logger.debug("begin..");
		db.getCollection(collectionName).insert(object);
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._create);
		dbObject.put(Constants._txnId, this.txnId);
		List ls = new ArrayList();
		ls.add(object);
		dbObject.put(Constants._version, ls);
		db.getCollection(Constants._trans).insert(dbObject);
		logger.debug("end..");
		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#update(java.lang.
	 * String, com.mongodb.DBObject, com.mongodb.DBObject, boolean, boolean)
	 */
	public DBObject update(String collectionName, DBObject query, DBObject object, boolean upsert, boolean multi)
			throws Exception {
		System.out.println("begin update..");
		logger.debug("begin..");

		DBObject dbObject = new BasicDBObject();
		// upsert = false;
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._updateAll);
		dbObject.put(Constants._txnId, this.txnId);
		List<DBObject> ls = db.getCollection(collectionName).find(query).toArray();
		dbObject.put(Constants._version, ls);
		if (ls.size() > 0)
			db.getCollection(Constants._trans).insert(dbObject);
		WriteResult wr = db.getCollection(collectionName).update(query, object, upsert, multi);
		if (wr.getUpsertedId() != null) {
			ls.add(new BasicDBObject(Constants._id, wr.getUpsertedId()));
			dbObject.put(Constants._version, ls);
			dbObject.put(Constants._operation, Constants._upsert);
			db.getCollection(Constants._trans).insert(dbObject);
		}
		logger.debug("end..");
		return new BasicDBObject(Constants._code, wr.getN());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#delete(java.lang.
	 * String, com.mongodb.DBObject)
	 */
	public DBObject delete(String collectionName, DBObject object) throws Exception {
		logger.debug("begin..");
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._deleteAll);
		dbObject.put(Constants._txnId, this.txnId);
		dbObject.put(Constants._version, db.getCollection(collectionName).find(object).toArray());
		db.getCollection(Constants._trans).insert(dbObject);
		WriteResult wr = db.getCollection(collectionName).remove(object);
		logger.debug("end..");
		return new BasicDBObject(Constants._code, wr.getN());
	}

	public DBObject commitTransaction() throws Exception {
		WriteResult wr = db.getCollection(Constants._trans).remove((new BasicDBObject()));
		logger.info("[res]" + wr);
		db.getCollection(Constants._transId).remove(new BasicDBObject(Constants._txnId, txnId));
		return new BasicDBObject(Constants._code, 0);

	}

	public DBObject rollBackTransaction() {
		int nOp = 0;
		try {
			int count = db.getCollection(Constants._trans).find(new BasicDBObject(Constants._txnId, txnId))
					.count();
			while (count > 0) {
				List<DBObject> ls = db.getCollection(Constants._trans)
						.find(new BasicDBObject(Constants._txnId, txnId)).sort(new BasicDBObject(Constants._id, -1))
						.limit(500).toArray();
				for (DBObject dbObject : ls) {
					revertCUD(dbObject);
					db.getCollection(Constants._trans)
							.remove((new BasicDBObject(Constants._id, (ObjectId) dbObject.get(Constants._id))));
					count--;
					nOp++;
				}

			}
		} catch (Exception e) {
			return (new BasicDBObject()).append(Constants._code, 0).append(Constants._message, e.getMessage())
					.append(Constants._nOp, nOp);
		}
		db.getCollection(Constants._transId).remove(new BasicDBObject(Constants._txnId, txnId));
		return (new BasicDBObject()).append(Constants._code, 0).append(Constants._message, Constants._revert_success)
				.append(Constants._nOp, nOp);

	}

	static private enum Operation {
		c, u, us, ua, d, da
	};

	/**
	 * To perform cud revert operation
	 * 
	 * @param dbObject
	 * @throws Exception
	 */
	private void revertCUD(DBObject dbObject) throws Exception {
		String opertionType = (String) dbObject.get(Constants._operation);
		String collection = (String) dbObject.get(Constants._collection);
		List<DBObject> vls = (List<DBObject>) dbObject.get(Constants._version);

		switch (Operation.valueOf(opertionType)) {
		case c: {
			for (DBObject dbObj : vls)
				db.getCollection(collection)
						.remove((new BasicDBObject(Constants._id, (dbObj.get(Constants._id) instanceof Long) ? (Long) dbObj.get(Constants._id) : (ObjectId) dbObj.get(Constants._id))));
		}
		case us: {
			for (DBObject dbObj : vls)
				db.getCollection(collection)
				.remove((new BasicDBObject(Constants._id, (dbObj.get(Constants._id) instanceof Long) ? (Long) dbObj.get(Constants._id) : (ObjectId) dbObj.get(Constants._id))));
		}
			break;

		case u: {
			for (DBObject dbObj : vls)
				db.getCollection(collection)
						.update((new BasicDBObject(Constants._id, (dbObj.get(Constants._id) instanceof Long) ? (Long) dbObj.get(Constants._id) : (ObjectId) dbObj.get(Constants._id))), dbObj);

		}
			break;
		case ua: {
			for (DBObject dbObj : vls)
				db.getCollection(collection)
						.update((new BasicDBObject(Constants._id, (dbObj.get(Constants._id) instanceof Long) ? (Long) dbObj.get(Constants._id) : (ObjectId) dbObj.get(Constants._id))), dbObj);

		}
			break;
		case d: {
			for (DBObject dbObj : vls)
				db.getCollection(collection).insert(dbObj);

		}
			break;
		case da: {
			for (DBObject dbObj : vls)
				db.getCollection(collection).insert(dbObj);
		}
			break;
		}

	}

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}

}

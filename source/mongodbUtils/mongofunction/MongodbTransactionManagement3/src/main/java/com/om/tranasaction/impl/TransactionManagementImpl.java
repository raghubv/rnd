package com.enhancesys.tranasaction.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.types.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.Constants;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

/**
* 
* <b>Purpose:</b><br>
* TransactionManagementImpl: <br>
* <br>
* 
* <b>DesignReference:</b><br>
* master-SNAPSHOT<br>
* <br>
* 
* <b>CopyRights:</b><br>
* Enhancesys 2013<br>
* <br>
* 
* <b>RevisionHistory:</b>
* 
* <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        29-Sep-2016	   @author Anshuman   -- Base Release
* 
* </pre>
* 
* <br>
*/
/**
* 
* <b>Purpose:</b><br>
* TransactionManagementImpl: <br>
* <br>
* 
* <b>DesignReference:</b><br>
* master-SNAPSHOT<br>
* <br>
* 
* <b>CopyRights:</b><br>
* Enhancesys 2013<br>
* <br>
* 
* <b>RevisionHistory:</b>
* 
* <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        29-Sep-2016	   @author Anshuman   -- Base Release
* 
* </pre>
* 
* <br>
*/
public class TransactionManagementImpl implements TransactionManagement {
	private MongoOperations mongoOperation = null;
	private Long txnId = (new Date()).getTime();

	final static Logger logger = LoggerFactory.getLogger(TransactionManagementImpl.class);

	public TransactionManagementImpl(MongoOperations mongoOperation) {
		super();
		this.mongoOperation = mongoOperation;
		mongoOperation.getCollection(Constants._trans)
				.createIndex((new BasicDBObject()).append(Constants._txnId, 1).append(Constants._status, 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.enhancesys.tranasaction.TransactionManagement#find(java.lang.String,
	 * com.mongodb.DBObject)
	 */
	public DBObject find(String collectionName, DBObject qryObject) throws Exception {
		logger.debug("begin..");
		DBObject res = new BasicDBObject(Constants._response,
				mongoOperation.getCollection(collectionName).find(qryObject).toArray());
		logger.debug("end..");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#insert(java.lang.
	 * String, com.mongodb.DBObject)
	 */
	public DBObject insert(String collectionName, DBObject object) throws Exception {
		logger.debug("begin..");
		mongoOperation.getCollection(collectionName).insert(object);
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._create);
		dbObject.put(Constants._txnId, this.txnId);
		List ls = new ArrayList();
		ls.add(object);
		dbObject.put(Constants._version, ls);
		mongoOperation.getCollection(Constants._trans).insert(dbObject);
		logger.debug("end..");
		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#update(java.lang.
	 * String, com.mongodb.DBObject, com.mongodb.DBObject, boolean, boolean)
	 */
	public DBObject update(String collectionName, DBObject query, DBObject object, boolean upsert, boolean multi)
			throws Exception {
		logger.debug("begin..");
		upsert = false;
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._updateAll);
		dbObject.put(Constants._txnId, this.txnId);
		dbObject.put(Constants._version, mongoOperation.getCollection(collectionName).find(query).toArray());
		mongoOperation.getCollection(Constants._trans).insert(dbObject);
		WriteResult wr = mongoOperation.getCollection(collectionName).update(query, object, upsert, multi);
		logger.debug("end..");
		return new BasicDBObject(Constants._code, wr.getN());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.enhancesys.tranasaction.TransactionManagement#delete(java.lang.
	 * String, com.mongodb.DBObject)
	 */
	public DBObject delete(String collectionName, DBObject object) throws Exception {
		logger.debug("begin..");
		DBObject dbObject = new BasicDBObject();
		dbObject.put(Constants._collection, collectionName);
		dbObject.put(Constants._operation, Constants._deleteAll);
		dbObject.put(Constants._txnId, this.txnId);
		dbObject.put(Constants._version, mongoOperation.getCollection(collectionName).find(object).toArray());
		mongoOperation.getCollection(Constants._trans).insert(dbObject);
		WriteResult wr = mongoOperation.getCollection(collectionName).remove(object);
		logger.debug("end..");
		return new BasicDBObject(Constants._code, wr.getN());
	}

	private void loadScript() {
		logger.debug("begin..");
		try {
			InputStream in = getClass().getResourceAsStream(Constants._jsPath);
			StringWriter writer = new StringWriter();
			IOUtils.copy(in, writer, Constants._UTF8);
			Code functionCode = new Code(writer.toString());
			logger.debug("[functionCode]" + functionCode);
			DBObject doc = new BasicDBObject(Constants._id, Constants._executeTransaction)
					.append(Constants._functionValue, functionCode);
			mongoOperation.getCollection(Constants._systemJS)
					.update(new BasicDBObject("_id", Constants._executeTransaction), doc, true, false);
		} catch (IOException e) {
			logger.error("[error]" + e);
			e.printStackTrace();
		}
		logger.debug("end..");

	}

	public DBObject commitTransaction() throws Exception {
		WriteResult wr = mongoOperation.getCollection(Constants._trans).remove((new BasicDBObject()));
		logger.info("[res]" + wr);
		return new BasicDBObject(Constants._code, 0);

	}
	
	public DBObject rollBackTransaction() {
		loadScript();
		mongoOperation.executeCommand(new BasicDBObject(Constants._$eval, Constants._loadServerScripts));
		CommandResult res = mongoOperation.executeCommand(
				new BasicDBObject(Constants._$eval, Constants._executeTransaction + "(" + getTxnId() + ")"));
		logger.info("[res]" + res);
		return res;

	}

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}

}

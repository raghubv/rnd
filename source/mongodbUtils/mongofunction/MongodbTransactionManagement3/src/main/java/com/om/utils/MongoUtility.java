package com.enhancesys.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.tranasaction.impl.TransactionManagementImpl;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * MongoUtility: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class MongoUtility {
	/**
	 * 
	 * @param mongoOperations
	 * @return
	 */
	public static TransactionManagement beginTransaction(MongoOperations mongoOperations) {
		return new TransactionManagementImpl(mongoOperations);
	}

	public static Object convertToDBObject(Object object) {

		if (object instanceof List) {
			List<Object> ls = new ArrayList<Object>();
			for (Object obj : (List<Object>) object) {
				ls.add(convertToDBObject(object));
			}
			return ls;
		} else if (object instanceof Map) {
			Map nObject = new HashMap();
			for (Map.Entry<String, Object> entry : ((Map<String, Object>) object).entrySet()) {
				if (entry.getValue() instanceof Map || entry.getValue() instanceof List)
					nObject.put(getDBValue(entry.getKey()), convertToDBObject(entry.getValue()));
				else
					nObject.put(getDBValue(entry.getKey()), entry.getValue());
			}
			return nObject;

		}
		return object;

	}

	private static Map keys = new HashMap();
	static {
		keys.put("$lte", "dllte");
		keys.put("$gte", "dlgte");
		keys.put("$lt", "dllt");
		keys.put("$gt", "dlgt");
		keys.put("$eq", "dleq");
		keys.put("$set", "dlset");

	}

	private static Object getDBValue(Object key) {
		return keys.containsKey(key) ? keys.get(key) : key;
	}
}

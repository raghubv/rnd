package com.enhancesys.core;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

import com.enhancesys.config.SpringMongoConfig;
import com.enhancesys.tranasaction.TransactionManagement;
import com.enhancesys.utils.MongoUtility;
import com.mongodb.BasicDBObject;

/**
 * 
 * 
 * <b>Purpose:</b><br>
 * App: <br>
 * <br>
 * 
 * <b>DesignReference:</b><br>
 * master-SNAPSHOT<br>
 * <br>
 * 
 * <b>CopyRights:</b><br>
 * Enhancesys 2013<br>
 * <br>
 * 
 * <b>RevisionHistory:</b>
 * 
 * <pre>
* <b>
* Sl No   Modified Date        Author</b>
* ==============================================
* 1        14-Sep-2016	   @author Raghu   -- Base Release
 * 
 * </pre>
 * 
 * <br>
 */
public class App {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
		TransactionManagement mongoDBTransactionManagement = MongoUtility.beginTransaction(mongoOperation);
//		mongoDBTransactionManagement.setTxnId(1475152681937L);
//		mongoDBTransactionManagement.rollBackTransaction();
//		System.exit(0);

		Long initial, max, record;
		String opType = null, isCommit = null;

		initial = Long.valueOf(args[0]);
		record = Long.valueOf(args[1]);
		max = Long.valueOf(args[2].toString());
		opType = args[3];
		isCommit = args[4];

		System.out.println("initial :" + initial + ", record :" + record + ", max :" + max + ", opType :" + opType
				+ ", isCommit :" + isCommit);

		try {
			// System.out.println("###################################################################################");
			// System.out.println(" find one ");
			// System.out.println("###################################################################################");
			//
			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			// }
			// System.out.println("###################################################################################");
			// System.out.println(" Insert one ");
			// System.out.println("###################################################################################");
			// for(int j=0;j<3;j++)
			// for (int i = initial; i < 1000; i++) {
			// mongoDBTransactionManagement.insertOne("test", (new
			// BasicDBObject()).append("key", i));
			// //mongoDBTransactionManagement.insertOne("test1", (new
			// BasicDBObject()).append("key", i));
			// //mongoDBTransactionManagement.insertOne("test1", (new
			// BasicDBObject()).append("key", i));
			//
			// }
			// mongoDBTransactionManagement.pushTransaction();
			//// System.out.println("###################################################################################");
			// System.out.println(" find one ");
			// System.out.println("###################################################################################");
			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			//
			// }
			// System.out.println("###################################################################################");
			// System.out.println(" Update one ");
			// System.out.println("###################################################################################");
			//
			// for (int i = initial; i < max; i++) {
			// mongoDBTransactionManagement.updateOne("test", (new
			// BasicDBObject()).append("key", i),
			// (new BasicDBObject()).append("key", i).append("val", i ));
			// }
			//
			// System.out.println("###################################################################################");
			// System.out.println(" find one ");
			// System.out.println("###################################################################################");
			// for (int i = initial; i < max; i++) {
			// DBObject dbObject = (DBObject)
			// mongoDBTransactionManagement.findOne("test",
			// (new BasicDBObject()).append("key", i));
			// System.out.println("dbObject" + dbObject);
			//
			// }

			// mongoDBTransactionManagement.pushTransaction();

			// System.out.println("###################################################################################");
			// System.out.println(" Update ");
			// System.out.println("###################################################################################");

			// mongoDBTransactionManagement.insert("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1));
			// mongoDBTransactionManagement.update("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1),
			// (new BasicDBObject()).append("$set", new BasicDBObject("date",
			// new Date())), false, true);
			// mongoDBTransactionManagement.delete("test",
			// (new BasicDBObject()).append("batch", 1).append("key", 1));

			System.out.println("before transaction " + new Date());
			Long m = initial;
			if (opType.equalsIgnoreCase("insert")) {
				for (int k = 0; k < record; k++) {
					System.out.println(
							"###################################################################################");
					for (int i = 0; i < max; i++) {
						mongoDBTransactionManagement.insert("test",
								new BasicDBObject().append("batch", k).append("key", m));
						m++;
					}
				}
			} else if (opType.equalsIgnoreCase("update")) {
				for (int k = 0; k < record; k++) {
					System.out.println(
							"###################################################################################");
					for (int i = 0; i < max; i++) {
						mongoDBTransactionManagement.update("test", new BasicDBObject().append("key", m),
								new BasicDBObject("$unset", new BasicDBObject("date", "")), false, false);
						m++;
					}
				}
			} else if (opType.equalsIgnoreCase("delete")) {
				for (int k = 0; k < record; k++) {
					System.out.println(
							"###################################################################################");
					for (int i = 0; i < max; i++) {
						mongoDBTransactionManagement.delete("test", new BasicDBObject().append("key", m));
						m++;
					}
				}
			}
			if (isCommit.equalsIgnoreCase("true")) {
				mongoDBTransactionManagement.commitTransaction();
			} else
				mongoDBTransactionManagement.rollBackTransaction();
			System.out.println(new Date());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mongoDBTransactionManagement.rollBackTransaction();
		}

	}
}
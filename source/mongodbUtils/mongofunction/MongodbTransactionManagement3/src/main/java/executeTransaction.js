/*
@author Raghu B V
@date 13/9/2016
@description  To save transaction information to respective collection
*/
function(id) {
	var operationCount=0;
	var revert=function(){
		var count=db.getCollection('trans').find({'tId':id}).count();
		while(count>0){
			var cursor = db.getCollection('trans').find({'tId':id}).sort({_id:-1}).limit(100).toArray()
			for(var dbObj in cursor){
				revertCUD(cursor[dbObj]);
				count--;
			}
		}
	};
	var revertCUD=function(txn) {
		var operation=txn['op'];
        var collection=txn['c'];
        // var val=txn['o'];
        // var query=txn['q'];
        var oldVersion=txn['v'];	
        if(operation=='c'){
        	for(var index in oldVersion)
        		db.getCollection(collection).remove(oldVersion[index]['_id']);
       }else if(operation=='u'||operation=='ua'){
        	for(var index in oldVersion)
					db.getCollection(collection).update(oldVersion[index]['_id'],oldVersion[index]);
		}else if(operation=='d'||operation=='da'){
			for(var index in oldVersion)
					db.getCollection(collection).insert(oldVersion[index]);	   
        }	
        db.getCollection('trans').remove({'_id':txn['_id']});
        operationCount++;
	};
      
   try{
	   print("Transaction begin");
	   revert();
	   print("transaction end");
       return {"code":0,"desc":"Transaction revert success","totalOperation":operationCount};
   }catch (e) {
    	print("[error]"+e.stack);  
  		print("[tId]"+id);
  		print("[reverting begin...]");
  		revert();
  		print("[reverting  end...]");
  		var message ="Transaction failed due to "+e;
		return {"code":1,"desc":message,"stackTrace":e.stack};
  	}
      
}
package com.os.snoc.mapper;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.os.snoc.oplog.OplogLine;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;


public class NoneMapper implements Runnable {

	private MongoTemplate outMongoTemplate;
	private OplogLine oplogLine;
	private Map map;
	private static final Logger log = Logger.getLogger(NoneMapper.class);

	public NoneMapper(MongoTemplate outMongoTemplate, OplogLine oplogLine,
			Map map) {
		super();
		this.outMongoTemplate = outMongoTemplate;
		this.oplogLine = oplogLine;
		this.map = map;
	}

	//@Override
	public void run() {

		try {
			Map dbObject = oplogLine.getData().toMap();

			log.debug("[dbObject]" + dbObject);
			log.debug("[operation]" + oplogLine.getOperation());
			switch (oplogLine.getOperation()) {

			case Insert: {

				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).insert(
						new BasicDBObject(dbObject));
			}
				break;

			case Update: {
				
				
				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).update(
						oplogLine.getObject(), new BasicDBObject(dbObject));
			}
				break;
			case Delete:
				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).remove(
						oplogLine.getData());
				break;

			}
		} catch (Exception e) {
			log.error("[error]"+LoggerTrace.getStackTrace(e));
		}
		
		log.info("Exiting..");
	}

}

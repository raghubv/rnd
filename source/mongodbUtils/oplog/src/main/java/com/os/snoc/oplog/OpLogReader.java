package com.os.snoc.oplog;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.os.snoc.util.Configuration;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;


public class OpLogReader implements Runnable {

	private static final Logger log = Logger.getLogger(OpLogReader.class);

	private MongoTemplate mongoTemplate;
	private BlockingQueue<OplogLine> queue;
	private boolean isOldOpLog;
	private String nameSpace;
	private Date oplogStartDateAndTime;
	/**
	 * 
	 * @param mongoTemplate
	 * @param queue
	 * @param isOldOpLog
	 * @param nameSpace
	 * @param oplogStartDateAndTime
	 */
	public OpLogReader(MongoTemplate mongoTemplate,
			BlockingQueue<OplogLine> queue, boolean isOldOpLog,
			String nameSpace, Date oplogStartDateAndTime) {
		super();
		this.mongoTemplate = mongoTemplate;
		this.queue = queue;
		this.isOldOpLog = isOldOpLog;
		this.nameSpace = nameSpace;
		this.oplogStartDateAndTime = oplogStartDateAndTime;
	}

	//@Override
	public void run() {
		while (true) {
			try {
				Date ts = null;
				if (isOldOpLog) {
					ts = oplogStartDateAndTime;

				} else {
					DBObject last = null;
					{
						DBCursor lastCursor = mongoTemplate
								.getCollection(Constants._oplog_rs)
								.find()
								.sort(new BasicDBObject(Constants._$natural, -1))
								.limit(1);

						if (!lastCursor.hasNext()) {
							log.fatal("[no oplog configured for this connection.  Please restart mongo with the --master option.]");
							return;
						}
						last = lastCursor.next();
					}
					ts = (Date) last.get(Constants._ts);
				}
				log.info("[starting point]" + ts);

				while (true) {

					log.debug("[ts] " + ts);
					DBObject qry = new BasicDBObject();
					// qry.put("fromMigrate", new BasicDBObject("$exists",
					// false));
					qry.put(Constants._ts,
							new BasicDBObject(Constants._$gt, ts));// Time
																	// Stamp
					
					qry.put(Constants._opsns,
							new BasicDBObject(Constants._$in, Configuration.getNameSpaces()));
					// Optimized query

					log.debug("[qry]" + qry);
					System.out.print("[qry]" + qry);
					DBCursor cursor = mongoTemplate
							.getCollection(Constants._oplog_rs).find(qry).hint(new BasicDBObject(Constants._$natural, -1))
							.addOption(Bytes.QUERYOPTION_TAILABLE)
							.addOption(Bytes.QUERYOPTION_AWAITDATA);

					ts = insertOplogInQueue(cursor, ts);

				}
			} catch (Exception e) {
				log.error("[Error][" + nameSpace + "]"
						+ LoggerTrace.getStackTrace(e));
			}

		}

	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. insertOplogInQueue
		* 
		* </pre> 
		*
		* @return Date
	 */
	private Date insertOplogInQueue(DBCursor cursor, Date ts)
			throws Exception {

		while (cursor.hasNext()) {
			DBObject x = cursor.next();
			ts = (Date) x.get(Constants._ts);

			List<DBObject> ls = (List<DBObject>) x.get(Constants._ops);
			for (DBObject dbObject : ls) {
				if (Configuration.getNameSpaces().contains(
						(String) dbObject.get(Constants._ns))) {
					OplogLine line = parseLogLine(ts, dbObject);
					if (log.isDebugEnabled()) {
						log.debug(line);
					}

					// if (!queue.offer(line, 10, TimeUnit.SECONDS)) {
					if (!queue.offer(line)) {
						log.error( "[Failed to insert oplog into queue.  Queue size"
								+ queue.size() + " while trying to insert]: "
								+ line);
					}

				}
			}
		}
		this.wait(10000);
		return ts;
	}
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. parseLogLine
		* 
		* </pre> 
		*
		* @return OplogLine
	 */
	private OplogLine parseLogLine(Date ts, DBObject x) {
		DateTime timestamp = new DateTime(ts.getTime() * 1000l);
		MongoOplogOperation operation = MongoOplogOperation.find((String) x
				.get(Constants._op));
		String nameSpace = (String) x.get(Constants._ns);

		BasicDBObject data = (BasicDBObject) x.get(Constants._o);
		BasicDBObject object = (BasicDBObject) x.get(Constants._o2);
		OplogLine line = new OplogLine(operation, timestamp, nameSpace, object,
				data);
		return line;
	}
}

package com.os.snoc.mapper;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBRef;
import com.os.snoc.oplog.OpLogReader;
import com.os.snoc.oplog.OplogLine;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;

public class SimpleMapper implements Runnable {

	private MongoTemplate outMongoTemplate;
	private OplogLine oplogLine;
	private Map map;
	private static final Logger log = Logger.getLogger(OpLogReader.class);

	public SimpleMapper(MongoTemplate outMongoTemplate, OplogLine oplogLine,
			Map map) {
		super();
		this.outMongoTemplate = outMongoTemplate;
		this.oplogLine = oplogLine;
		this.map = map;
	}

//	@Override
	public void run() {

		try {
			Map dbObject = oplogLine.getData().toMap();

			log.debug("[dbObject]" + dbObject);
			log.debug("[operation]" + oplogLine.getOperation());
			switch (oplogLine.getOperation()) {

			case Insert: {

				List<Map> ls = (List<Map>) map.get(Constants._fields);
				for (Map map : ls) {
					dbObject.put(
							map.get(Constants._field),
							new DBRef(outMongoTemplate.getDb(), (String) map
									.get(Constants._collectionName), dbObject
									.get(map.get(Constants._field))));
				}
				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).insert(
						new BasicDBObject(dbObject));
			}
				break;

			case Update: {
				List<Map> ls = (List<Map>) map.get(Constants._fields);
				for (Map map : ls) {
					if(dbObject
							.containsKey(map.get(Constants._field)))
					dbObject.put(
							map.get(Constants._field),
							new DBRef(outMongoTemplate.getDb(), (String) map
									.get(Constants._collectionName), dbObject
									.get(map.get(Constants._field))));
				}
				
				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).update(
						oplogLine.getObject(), new BasicDBObject(dbObject));
			}
				break;
			case Delete:
				outMongoTemplate.getCollection(
						(String) map.get(Constants._collectionName)).remove(
						oplogLine.getData());
				break;

			}
		} catch (Exception e) {
			log.error("[error]"+LoggerTrace.getStackTrace(e));
		}
		log.info("Exiting..");
	}

}

package com.os.snoc.standalone;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.MongoException;
import com.os.snoc.oplog.OpLogReader;
import com.os.snoc.oplog.OplogLine;
import com.os.snoc.oplog.QueueDelegate;
import com.os.snoc.util.Configuration;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;


public class AppInit {

	private static final Logger log = Logger.getLogger(AppInit.class);

	public static void main(String[] args) {

		ApplicationContext context = null;
		boolean isOldOplog = false;
		ExecutorService executor = null;
		ExecutorService queueExecutor = null;
		try {
			// Executor for reading oplog data
			executor = Executors.newSingleThreadExecutor();
			//
			queueExecutor = Executors.newFixedThreadPool(Configuration
					.getNameSpaces().size());
			Date date = new Date();
			if (args != null && args.length != 0) {
				isOldOplog = true;
				date = (new SimpleDateFormat(
						Constants._config_Old_Op_log_DateFormat))
						.parse(args[0]);
				System.out.println("[Oplog start time]" + date);
			}

			context = new ClassPathXmlApplicationContext(
					Constants._Application_Context);
			BlockingQueue<OplogLine> queue = new LinkedBlockingQueue<OplogLine>();

			OpLogReader reader = new OpLogReader(
					(MongoTemplate) context
							.getBean(Constants._OpLogTemplateBean),
					queue, isOldOplog, "", date);
			executor.execute(reader);

			for (String namespace : Configuration.getNameSpaces()) {

				queueExecutor.execute(new QueueDelegate(context, queue,
						executor));
			}

		} catch (MongoException e) {
			queueExecutor.shutdown();
			executor.shutdown();
			((ClassPathXmlApplicationContext)context).close();
			log.error("[error]" + LoggerTrace.getStackTrace(e));

		} catch (ParseException e) {
			queueExecutor.shutdown();
			executor.shutdown();
			((ClassPathXmlApplicationContext)context).close();
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}

	}

}

package com.os.snoc.oplog;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.os.snoc.oplogHandler.OplogCRUD;
import com.os.snoc.oplogHandlerImpl.OplogCRUDImpl;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;

public class QueueDelegate implements Runnable {
	private static final Logger log = Logger.getLogger(QueueDelegate.class);

	private ApplicationContext context = null;
	private BlockingQueue<OplogLine> queue = null;
	private ExecutorService executor = null;

	public QueueDelegate(ApplicationContext context,
			BlockingQueue<OplogLine> queue, ExecutorService executor) {
		super();
		this.context = context;
		this.queue = queue;
		this.executor = executor;
	}

	//@Override
	public void run() {
		// TODO Auto-generated method stub

		while (!executor.isTerminated()) {
			OplogLine line = null;
			try {

				OplogCRUD oplogCrud = context.getBean(Constants._OplogCrudBean,
						OplogCRUDImpl.class);
				line = queue.poll(1, TimeUnit.NANOSECONDS);

				log.debug("[line]" + line);
				if (line != null)
					switch (line.getOperation()) {
					case Delete:

						oplogCrud.delete(line);
						break;

					case Insert:
						oplogCrud.create(line);
						break;

					case Update:
						oplogCrud.update(line);
						break;

					default:
						break;
					}

			} catch (RejectedExecutionException e) {
				log.error("[error]" + LoggerTrace.getStackTrace(e));
				queue.offer(line);
			} catch (Exception e) {
				log.error("[error]" + LoggerTrace.getStackTrace(e));
				executor.shutdownNow();
				((ClassPathXmlApplicationContext) context).close();
			}

		}

	}

}

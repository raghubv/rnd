package com.os.snoc.util;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class QueueHandler {
	// This Queue class is a thread safe (written in house) class
	public static Queue<Object> readQ = new ArrayBlockingQueue(1000);

	public static void enqueue(Object object) {
		// do some stuff
		readQ.add(object);
	}

	public static Object dequeue() {
		// do some stuff
		Object objecct = readQ.poll();
		try {

			while (objecct == null) {
				readQ.wait(5000);
				objecct = readQ.poll();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objecct;
	}
}

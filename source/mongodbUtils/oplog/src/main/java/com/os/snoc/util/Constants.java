package com.os.snoc.util;

public final class Constants {

	public static final String _collectionName = "collectionName";
	public static final String _fields = "fields";
	public static final String _field = "field";
	public static final String _db = "db";
	public static final String _mappingType = "mappingType";

	public static final String _config_Old_Op_log_DateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String _Application_Context = "spring-application-context.xml";

	public static final String _OpLogTemplateBean = "opLogTemplate";
	public static final String _OplogCrudBean = "oplogCrud";
	public static final String _oplog_rs = "oplog.rs";
	public static final String _$natural = "$natural";
	public static final String _$gt = "$gt";
	public static final String _$in = "$in";
	public static final String _ts = "ts";
	public static final String _opsns = "ops.ns";
	public static final String _ops = "ops";
	public static final String _op = "op";
	public static final String _ns = "ns";
	public static final String _o = "o";
	public static final String _o2 = "o2";
}

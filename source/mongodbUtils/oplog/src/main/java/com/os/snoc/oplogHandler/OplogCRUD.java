package com.os.snoc.oplogHandler;

import java.util.concurrent.RejectedExecutionException;

import com.os.snoc.oplog.OplogLine;
public interface OplogCRUD {
   /**
    * 
   	 * 
   	* <b>Algorithm:</b> 
   	* <pre> 
   	* 1. create
   	* 
   	* </pre> 
   	*
   	* @return void
    */
	public void create(OplogLine oplogLine) throws RejectedExecutionException;
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. read
		* 
		* </pre> 
		*
		* @return void
	 */
	public void read(OplogLine oplogLine)throws RejectedExecutionException;
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. update
		* 
		* </pre> 
		*
		* @return void
	 */
	public void update(OplogLine oplogLine)throws RejectedExecutionException;
	
	/**
	 * 
		 * 
		* <b>Algorithm:</b> 
		* <pre> 
		* 1. delete
		* 
		* </pre> 
		*
		* @return void
	 */
	public void delete(OplogLine oplogLine) throws RejectedExecutionException;
	
}

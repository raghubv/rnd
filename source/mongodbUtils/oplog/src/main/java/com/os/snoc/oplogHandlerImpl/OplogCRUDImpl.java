package com.os.snoc.oplogHandlerImpl;

import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.os.snoc.mapper.NoneMapper;
import com.os.snoc.mapper.SimpleMapper;
import com.os.snoc.oplog.OplogLine;
import com.os.snoc.util.Configuration;
import com.os.snoc.util.Constants;
import com.os.snoc.util.LoggerTrace;


public class OplogCRUDImpl implements com.os.snoc.oplogHandler.OplogCRUD {

	private static final Logger log = Logger.getLogger(OplogCRUDImpl.class);

	public enum mappingType {
		none, simple, complex
	};

	@Autowired
	private MongoTemplate outTemplate;
	@Autowired
	private ThreadPoolTaskExecutor crudOplogTaskExecutor;

//	@Override
	public void create(OplogLine oplogLine)throws RejectedExecutionException {
		log.info("Entering...");
		try {
			if (Configuration.getConfigurationData().containsKey(
					oplogLine.getNameSpace())) {
				Map config = (Map) Configuration.getConfigurationData().get(
						oplogLine.getNameSpace());

				switch (mappingType.valueOf((String) config
						.get(Constants._mappingType))) {
				case none:
					crudOplogTaskExecutor.execute(new NoneMapper(outTemplate,
							oplogLine, config));
					break;
				case simple:
					crudOplogTaskExecutor.execute(new SimpleMapper(outTemplate,
							oplogLine, config));
					break;
				case complex:

					break;
				}
			}
		} catch (RejectedExecutionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			throw e;
		} catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		log.info("Exiting..");
	}

	//@Override
	public void read(OplogLine oplogLine) {
		// TODO Auto-generated method stub

	}

	//@Override
	public void update(OplogLine oplogLine)throws RejectedExecutionException {
		log.info("Entering...");
		try {
			if (Configuration.getConfigurationData().containsKey(
					oplogLine.getNameSpace())) {
			Map config = (Map) Configuration.getConfigurationData().get(
					oplogLine.getNameSpace());

			switch (mappingType.valueOf((String) config
					.get(Constants._mappingType))) {
			case simple:
				crudOplogTaskExecutor.execute(new SimpleMapper(outTemplate,
						oplogLine, config));
				break;
			case complex:

				break;
			}
			}
		}  catch (RejectedExecutionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			throw e;
		}catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		log.info("Exiting..");
	}

	//@Override
	public void delete(OplogLine oplogLine)throws RejectedExecutionException {
		log.info("Entering...");
		try {
			if (Configuration.getConfigurationData().containsKey(
					oplogLine.getNameSpace())) {
			Map config = (Map) Configuration.getConfigurationData().get(
					oplogLine.getNameSpace());

			switch (mappingType.valueOf((String) config
					.get(Constants._mappingType))) {
			case simple:
				crudOplogTaskExecutor.execute(new SimpleMapper(outTemplate,
						oplogLine, config));
				break;
			case complex:

				break;
			}
			}
		} catch (RejectedExecutionException e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
			throw e;
		}catch (Exception e) {
			log.error("[error]" + LoggerTrace.getStackTrace(e));
		}
		log.info("Exiting..");
	}

}

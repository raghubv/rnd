package com.os.snoc.util;

import java.io.PrintWriter;
import java.io.StringWriter;


public class LoggerTrace {
	/**
	 * 
	 * <b>Algorithm:</b>
	 * 
	 * <pre>
	 * 1. getStackTrace
	 * 
	 * </pre>
	 * 
	 * @param e
	 * @return
	 */
	public static String getStackTrace(Exception e) {
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		return stack.toString();
	}

}
